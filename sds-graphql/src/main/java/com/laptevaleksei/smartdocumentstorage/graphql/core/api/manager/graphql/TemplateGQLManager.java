/*
 * Copyright (c) 2019 - 2024, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.graphql.core.api.manager.graphql;

import com.laptevaleksei.smartdocumentstorage.core.domain.entity.CustomField;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.Document;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.template.FieldBlock;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.template.Template;
import com.laptevaleksei.smartdocumentstorage.manager.CustomFieldManager;
import com.laptevaleksei.smartdocumentstorage.manager.DocumentManager;
import com.laptevaleksei.smartdocumentstorage.manager.TemplateManager;
import com.laptevaleksei.smartdocumentstorage.graphql.core.util.GQLMapperHelper;
import com.laptevaleksei.smartdocumentstorage.graphql.core.web.graphql.entity.CustomFieldGQLO;
import com.laptevaleksei.smartdocumentstorage.graphql.core.web.graphql.entity.DocumentGQLO;
import com.laptevaleksei.smartdocumentstorage.graphql.core.web.graphql.entity.FieldBlockGQLO;
import com.laptevaleksei.smartdocumentstorage.graphql.core.web.graphql.entity.TemplateGQLO;
import jakarta.transaction.Transactional;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * GraphQL manager for {@link TemplateGQLO}
 * @author <a href="mailto:laptevalexander28@gmail.com">Aleksandr Laptev</a> on 7-Nov-2023
 */
@Service
public class TemplateGQLManager {
    private final TemplateManager templateManager;
    private final DocumentManager documentManager;
    private final DocumentGQLManager documentGQLManager;
    private final CustomFieldGQLManager customFieldGQLManager;
    private final CustomFieldManager customFieldManager;

    public TemplateGQLManager( TemplateManager templateManager, DocumentManager documentManager, @Lazy DocumentGQLManager documentGQLManager, CustomFieldGQLManager customFieldGQLManager, CustomFieldManager customFieldManager ) {
        this.templateManager = templateManager;
        this.documentManager = documentManager;
        this.documentGQLManager = documentGQLManager;
        this.customFieldGQLManager = customFieldGQLManager;
        this.customFieldManager = customFieldManager;
    }

    /**
     * Finds templates assigned to document by document id.
     * @param id document's id
     * @return if document exists - list of templates assigned to document, else - {@link Optional}.empty()
     */
    public Optional<List<TemplateGQLO>> getTemplatesAssignedToDocument( Long id ) {
        return documentManager
                .getDocument( id )
                .map( this::getTemplatesAssignedToDocument );
    }

    /**
     * Finds templates assigned to document
     * @param document document which attached templates need to find
     * @return Optionally (if document exists) list of templates assigned to document
     */
    private List<TemplateGQLO> getTemplatesAssignedToDocument( Document document ) {
        return templateManager
                .getTemplatesAssignedToDocument( document )
                .stream().map( template ->
                        GQLMapperHelper.getGQLObjectFromEntity(
                                template,
                                getFieldBlocksForTemplate( template.getId() ).get(),
                                getDefaultCustomFieldsForTemplate( template.getId() ).get()
                        )
                )
                .toList();
    }

    /**
     * Finds all {@link DocumentGQLO} attached to template by template's id
     * @param pageable page request
     * @param templateId {@link TemplateGQLO} id to find documents attached to
     * @return if {@link TemplateGQLO} with such id exists - list of documents attached to template, else - {@link Optional}.empty()
     */
    public Optional<List<DocumentGQLO>> documentsAttachedWithTemplate( Pageable pageable, Long templateId ) {
        return templateManager.getTemplate( templateId ).map( template ->
                documentsAttachedWithTemplate( pageable, template )
        );
    }

    /**
     * Finds all {@link DocumentGQLO} attached to template
     * @param pageable page request
     * @param template {@link Template} to find documents attached to
     * @return if such {@link Template} exists - list of documents attached to template, else - {@link Optional}.empty()
     */
    private List<DocumentGQLO> documentsAttachedWithTemplate( Pageable pageable, Template template ) {
        return templateManager
                .getDocumentsWhichUsesTemplate( pageable, template ).map(
                        document -> documentGQLManager.getDocument( document.getId() ).get()
                ).stream().toList();
    }

    /**
     * Finds {@link TemplateGQLO} by id
     * @param id of {@link TemplateGQLO} to find
     * @return {@link TemplateGQLO} if one with such id exists, else - {@link Optional}.empty()
     */
    public Optional<TemplateGQLO> getTemplate( Long id ) {
        Optional<Template> realTemplate = templateManager.getTemplate( id );
        return realTemplate.map( template ->
                GQLMapperHelper.getGQLObjectFromEntity(
                        template,
                        getFieldBlocksForTemplate( template ),
                        getDefaultCustomFieldsForTemplate( template )
                )
        );
    }

    /**
     * Finds {@link FieldBlockGQLO} by id
     * @param id id of {@link FieldBlockGQLO} to find
     * @return {@link FieldBlockGQLO} if one with such id exists, else - {@link Optional}.empty()
     */
    public Optional<FieldBlockGQLO> getFieldBlock( Long id ) {
        return templateManager.getFieldBlock( id ).map( fieldBlock ->
                GQLMapperHelper.getGQLObjectFromEntity(
                        fieldBlock,
                        getRelatedCustomFieldsWithFieldBlock( fieldBlock )
                )
        );
    }

    /**
     * Requests page of {@link FieldBlockGQLO}s
     * @param pageable {@link FieldBlockGQLO}s page request
     * @return list of {@link FieldBlockGQLO}s found according to page request
     */
    public List<FieldBlockGQLO> fieldBlocks( Pageable pageable ) {
        return templateManager.getAllManagedFieldBlocks( pageable ).stream()
                .map( fieldBlock ->
                        GQLMapperHelper.getGQLObjectFromEntity(
                                fieldBlock,
                                getRelatedCustomFieldsWithFieldBlock( fieldBlock )
                        )
                ).toList();
    }

    /**
     * Requests page of {@link TemplateGQLO}s
     * @param pageable {@link TemplateGQLO}s page request
     * @return list of {@link TemplateGQLO}s found according to page request
     */
    public List<TemplateGQLO> templates( Pageable pageable ) {
        return templateManager.getAllManagedTemplates( pageable ).stream()
                .map( fieldBlock -> getTemplate( fieldBlock.getId() ).get() ).toList();
    }

    private Optional<List<CustomFieldGQLO>> getDefaultCustomFieldsForTemplate( Long templateId ) {
        Optional<Template> template = templateManager.getTemplate( templateId );
        return template.map( this::getDefaultCustomFieldsForTemplate );
    }

    private List<CustomFieldGQLO> getDefaultCustomFieldsForTemplate( Template template ) {
        return templateManager.getDefaultCustomFields( template ).stream().map(
                customField -> customFieldGQLManager.getCustomField( customField.getId() ).get()
        ).toList();
    }

    private Optional<List<FieldBlockGQLO>> getFieldBlocksForTemplate( Long templateId ) {
        Optional<Template> template = templateManager.getTemplate( templateId );
        return template.map( this::getFieldBlocksForTemplate );
    }

    private List<FieldBlockGQLO> getFieldBlocksForTemplate( Template template ) {
        return templateManager.getFieldBlocksOfTheTemplate( template ).stream().map( fieldBlock ->
                GQLMapperHelper.getGQLObjectFromEntity(
                        fieldBlock,
                        getRelatedCustomFieldsWithFieldBlock( fieldBlock )
                )
        ).toList();
    }

    private Optional<List<CustomFieldGQLO>> getRelatedCustomFieldsWithFieldBlock( Long fieldBlockId ) {
        return templateManager
                .getFieldBlock( fieldBlockId )
                .map( this::getRelatedCustomFieldsWithFieldBlock );
    }

    private List<CustomFieldGQLO> getRelatedCustomFieldsWithFieldBlock( FieldBlock fieldBlock ) {
        return templateManager.getRelatedCustomFieldsWithFieldBlock( fieldBlock )
                .stream().map(
                        customField -> customFieldGQLManager.getCustomField( customField.getId() ).get()
                ).toList();
    }

    /**
     * Assigns {@link Template} to {@link Document}
     * @param documentId {@link Document}'s id to assign {@link Template} to
     * @param templateId {@link Template}'s id to assign to {@link Document}
     * @return document
     */
    public Optional<DocumentGQLO> assignTemplateToDocument( Long documentId, Long templateId ) {
        Optional<Document> document = documentManager.getDocument( documentId );
        Optional<Template> template = templateManager.getTemplate( templateId );

        if( document.isEmpty() || template.isEmpty() )
            return Optional.empty();

        templateManager.assignTemplateToDocument( document.get(), template.get() );
        return documentGQLManager.getDocument( documentId );
    }

    /**
     * Unassigns {@link Template} from {@link Document}
     * @param documentId {@link Document}'s id to unassign {@link Template} from
     * @param templateId {@link Template}'s id to unassign from {@link Document}
     */
    public void unassignTemplateFromDocument( Long documentId, Long templateId ) {
        Optional<Document> document = documentManager.getDocument( documentId );
        Optional<Template> template = templateManager.getTemplate( templateId );

        if( document.isPresent() && template.isPresent() )
            templateManager.unassignTemplateFromDocument( document.get(), template.get() );
    }

    /**
     * Adds {@link FieldBlock} to {@link Template}
     * @param templateId {@link Template}'s it to add {@link FieldBlock} to
     * @param fieldBlockId {@link FieldBlock}'s id to add to {@link Template}
     */
    public void addFieldBlockToTheTemplate( Long templateId, Long fieldBlockId ) {
        FieldBlock fieldBlock = templateManager.getFieldBlock( fieldBlockId ).get();
        Template template = templateManager.getTemplate( templateId ).get();

        templateManager.addFieldBlockToTheTemplate( template, fieldBlock );
    }

    /**
     * Adds {@link FieldBlock} to {@link Template}
     * @param template {@link Template} to add {@link FieldBlock} to
     * @param fieldBlock {@link FieldBlock} to add to {@link Template}
     */
    private void addFieldBlockToTheTemplate( Template template, FieldBlock fieldBlock ) {
        templateManager.addFieldBlockToTheTemplate( template, fieldBlock );
    }

    /**
     * Adds {@link CustomField} to {@link Template}
     * @param templateId {@link Template}'s id to add default {@link CustomField} to
     * @param customFieldId {@link CustomField}'s id to add to {@link Template}
     */
    public void addDefaultCustomFieldToTemplate( Long templateId, Long customFieldId ) {
        CustomField customField = customFieldManager.getCustomField( customFieldId ).get();
        Template template = templateManager.getTemplate( templateId ).get();

        templateManager.addDefaultCustomField( template, customField );
    }

    /**
     * Adds default {@link CustomField} to {@link Template}
     * @param template {@link Template} to add {@link CustomField} to
     * @param customField {@link CustomField}'s id to add to {@link Template}
     */
    private void addDefaultCustomFieldToTemplate( Template template, CustomField customField ) {
        templateManager.addDefaultCustomField( template, customField );
    }

    /**
     * Creates {@link Template}
     * @param name name of new {@link Template}
     * @param description description of new {@link Template}
     * @return created {@link Template}
     */
    public TemplateGQLO createTemplate( String name, String description ) {
        Template createdTemplate = new Template();
        createdTemplate.setName( name );
        createdTemplate.setDescription( description );
        createdTemplate.setDeleted( false );

        createdTemplate = templateManager.createTemplate( createdTemplate );

        return GQLMapperHelper.getGQLObjectFromEntity(
                createdTemplate,
                getFieldBlocksForTemplate( createdTemplate.getId() ).get(),
                getDefaultCustomFieldsForTemplate( createdTemplate.getId() ).get()
        );
    }

    /**
     * Removes default {@link CustomField} from {@link Template}
     * @param templateId {@link Template}'s id to remove default {@link CustomField} from
     * @param customFieldId {@link CustomField}'s it to remove from {@link Template}
     */
    public void removeDefaultCustomFieldFromTemplate( Long templateId, Long customFieldId ) {
        Optional<Template> template = templateManager.getTemplate( templateId );
        Optional<CustomField> customField = customFieldManager.getCustomField( customFieldId );

        if( template.isPresent() && customField.isPresent() )
            templateManager.removeDefaultCustomField( template.get(), customField.get() );
    }

    /**
     * Updates {@link Template}
     * @param id id of {@link Template} to update
     * @param name new name of {@link Template}
     * @param description new description of {@link Template}
     * @param content new {@link FieldBlock}s of {@link Template}
     * @param defaultCustomFields new default {@link CustomField}s of {@link Template}
     * @return updated {@link Template} if one with such id found
     */
    @Transactional
    public Optional<TemplateGQLO> updateTemplate(
            Long id,
            String name,
            String description,
            List<Long> content,
            List<Long> defaultCustomFields
    ) {
        return templateManager.getTemplate( id ).map( templateToUpdate -> {
            templateToUpdate.setName( name );

            templateToUpdate.setDescription( description );

            if( content != null ) {
                updateFieldBlocksOfTemplate( content, templateToUpdate );
            }

            if( defaultCustomFields != null ) {
                updateDefaultCustomFieldOfTemplate( defaultCustomFields, templateToUpdate );
            }

            Template updatedTemplate = templateManager.updateTemplate( templateToUpdate ).get();

            return GQLMapperHelper.getGQLObjectFromEntity(
                    updatedTemplate,
                    getFieldBlocksForTemplate( updatedTemplate.getId() ).get(),
                    getDefaultCustomFieldsForTemplate( updatedTemplate.getId() ).get()
            );
        });
    }

    private void updateDefaultCustomFieldOfTemplate( List<Long> defaultCustomFields, Template templateToUpdate ) {
        getDefaultCustomFieldsForTemplate( templateToUpdate.getId() )
                .ifPresent( defaultCustomFieldsOfTemplate ->
                        defaultCustomFieldsOfTemplate.forEach( defaultCustomField ->
                                removeDefaultCustomFieldFromTemplate( templateToUpdate.getId(), defaultCustomField.getId() )
                        )
                );

        defaultCustomFields.forEach( defaultCustomFieldId -> {
                    Optional<CustomField> defaultCustomField = customFieldManager.getCustomField( defaultCustomFieldId );
                    addDefaultCustomFieldToTemplate( templateToUpdate, defaultCustomField.get() );
                }
        );
    }

    private void updateFieldBlocksOfTemplate( List<Long> newDefaultCustomFields, Template templateToUpdate ) {
        getFieldBlocksForTemplate( templateToUpdate )
            .forEach( fieldBlock ->
                        removeFieldBlockFromTemplate( templateToUpdate.getId(), fieldBlock.getId() )
            );

        newDefaultCustomFields.forEach( fieldBlockId -> {
            Optional<FieldBlock> fieldBlock = templateManager.getFieldBlock( fieldBlockId );
                    addFieldBlockToTheTemplate( templateToUpdate, fieldBlock.get() );
                }
        );
    }

    private void removeFieldBlockFromTemplate( Long templateId, Long fieldBlockId ) {
        Optional<Template> template = templateManager.getTemplate( templateId );
        Optional<FieldBlock> fieldBlock = templateManager.getFieldBlock( fieldBlockId );

        if( template.isPresent() && fieldBlock.isPresent() )
            templateManager.removeFieldBlockFromTemplate( template.get(), fieldBlock.get() );
    }

    private void removeFieldBlockFromTemplate( Template template, FieldBlock fieldBlock ) {
        templateManager.removeFieldBlockFromTemplate( template, fieldBlock );
    }

    /**
     * Creates new {@link FieldBlock}
     * @param name name of new {@link FieldBlock}
     * @param description description of new {@link FieldBlock}
     * @return created {@link FieldBlock}
     */
    public FieldBlockGQLO createFieldBlock( String name, String description ) {
        FieldBlock fieldBlockToCreate = new FieldBlock();
        fieldBlockToCreate.setName( name );
        fieldBlockToCreate.setDescription( description );

        FieldBlock createdFieldBlock = templateManager.createFieldBlock( fieldBlockToCreate );

        return GQLMapperHelper.getGQLObjectFromEntity(
                createdFieldBlock,
                getRelatedCustomFieldsWithFieldBlock( createdFieldBlock )
        );
    }

    /**
     * Updates {@link FieldBlock}
     * @param id id of {@link FieldBlock} to update
     * @param name new name of {@link FieldBlock}
     * @param description new description of {@link FieldBlock}
     * @param content new {@link CustomField}s of {@link FieldBlock}
     * @return updated {@link FieldBlock} of one with such id were found
     */
    public Optional<FieldBlockGQLO> updateFieldBlock( Long id, String name, String description, List<Long> content ) {
        return templateManager.getFieldBlock( id ).map( fieldBlock -> {
            fieldBlock.setName( name );
            fieldBlock.setDescription( description );

            if ( content != null ) {
                updateCustomFieldsOfFieldBlock( fieldBlock, content );
            }

            return GQLMapperHelper.getGQLObjectFromEntity(
                    templateManager.updateFieldBlock( fieldBlock ).get(),
                    getRelatedCustomFieldsWithFieldBlock( fieldBlock )
            );
        });
    }

    private void updateCustomFieldsOfFieldBlock( FieldBlock fieldBlock, List<Long> content ) {
        templateManager.getCustomFieldsOfBlock( fieldBlock )
                .forEach( customField ->
                        templateManager.removeCustomFieldFromTheFieldBlockLayout( fieldBlock, customField )
                );

        content.forEach( customFieldId -> {
            CustomField customFieldToAdd = customFieldManager.getCustomField( customFieldId ).get();
            templateManager.addCustomFieldToTheFieldBlock( fieldBlock, customFieldToAdd );
        });
    }
}
