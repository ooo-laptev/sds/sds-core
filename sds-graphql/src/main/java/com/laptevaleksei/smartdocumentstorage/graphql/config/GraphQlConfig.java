/*
 * Copyright (c) 2019 - 2024, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.graphql.config;

import graphql.GraphQLContext;
import graphql.execution.CoercedVariables;
import graphql.language.StringValue;
import graphql.language.Value;
import graphql.scalars.ExtendedScalars;
import graphql.schema.Coercing;
import graphql.schema.CoercingParseValueException;
import graphql.schema.CoercingSerializeException;
import graphql.schema.GraphQLScalarType;
import org.springframework.boot.json.GsonJsonParser;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.graphql.execution.RuntimeWiringConfigurer;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * GraphQL configuration
 * @author <a href="mailto:laptevalexander28@gmail.com">Aleksandr Laptev</a> on 11-Nov-2023
 */
@Configuration
public class GraphQlConfig {

    /**
     * GraphQL runtime wiring config provider
     * @return runtime wiring config
     */
    @Bean
    public RuntimeWiringConfigurer runtimeWiringConfigurer() {
        return wiringBuilder ->
                wiringBuilder
                        .scalar( ExtendedScalars.GraphQLLong )
                        .scalar( ExtendedScalars.Json )
                        .scalar( mapScalar() );
    }

    /**
     * Scalar for Map type in GraphQL requests/responses
     * @return Map scalar type definition object
     */
    private GraphQLScalarType mapScalar() {
        return GraphQLScalarType.newScalar()
                .name("Map")
                .description("Map type")
                .coercing( new Coercing<Map<String, String>, String>(){
                        @Override
                        @SuppressWarnings( {"unchecked"} )
                        public String serialize(
                                Object dataFetcherResult,
                                GraphQLContext context,
                                Locale locale
                        ) {
                            if (dataFetcherResult instanceof Map) {
                                StringBuilder result = new StringBuilder();
                                result.append( '{' );
                                ((Map<String,String>)dataFetcherResult).forEach( (key, value) -> {
                                    result
                                            .append("\"")
                                            .append(key)
                                            .append("\":\"")
                                            .append(value)
                                            .append("\",");
                                });

                                result.deleteCharAt( result.length() - 1 );
                                result.append( '}' );
                                return result.toString();
                            } else {
                                throw new CoercingSerializeException( MessageFormat.format(
                                        "string.exception.expected_map_object",
                                        dataFetcherResult
                                    )
                                );
                            }
                        }

                        @Override
                        public Map<String, String> parseValue(
                                final Object input,
                                GraphQLContext context,
                                Locale locale
                        ) {
                            GsonJsonParser jsonParser = new GsonJsonParser();
                            if (input instanceof StringValue ) {
                                Map<String, Object> parsedMap = jsonParser.parseMap( String.valueOf( input ) );
                                Map<String, String> stringStringMap = new HashMap<>();
                                parsedMap.forEach( (key, value) -> stringStringMap.put( key, value.toString() ) );
                                return stringStringMap;
                            } else {
                                throw new CoercingParseValueException( MessageFormat.format(
                                        "string.exception.string_was_expected",
                                        input
                                    )
                                );
                            }
                        }

                        @Override
                        public Map<String, String> parseLiteral(
                                Value input,
                                CoercedVariables coercedVariables,
                                GraphQLContext context,
                                Locale locale
                        ) {
                            GsonJsonParser jsonParser = new GsonJsonParser();
                            if (input instanceof StringValue ) {
                                Map<String, Object> parsedMap = jsonParser.parseMap( ( ( StringValue ) input ).getValue() );
                                Map<String, String> stringStringMap = new HashMap<>();
                                parsedMap.forEach( (key, value) -> stringStringMap.put( key,
                                        value != null ? value.toString() : null
                                ));
                                return stringStringMap;
                            } else {
                                throw new CoercingParseValueException( MessageFormat.format(
                                        "string.exception.string_was_expected",
                                        input
                                    )
                                );
                            }
                        }
                }).build();

    }
}
