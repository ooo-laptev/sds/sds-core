/*
 * Copyright (c) 2019 - 2024, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.graphql.core.api.manager.graphql;


import com.laptevaleksei.smartdocumentstorage.core.domain.entity.Document;
import com.laptevaleksei.smartdocumentstorage.manager.DocumentManager;
import com.laptevaleksei.smartdocumentstorage.graphql.core.util.GQLMapperHelper;
import com.laptevaleksei.smartdocumentstorage.graphql.core.web.graphql.entity.DocumentGQLO;
import com.laptevaleksei.smartdocumentstorage.graphql.core.web.graphql.entity.DocumentLinkInputGQLO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.time.Instant;
import java.util.*;

/**
 * GraphQL manager for {@link DocumentGQLO}
 * @author <a href="mailto:laptevalexander28@gmail.com">Aleksandr Laptev</a> on 7-Nov-2023
 */
@Service
public class DocumentGQLManager {
    private final DocumentManager documentManager;
    private final DocumentLinkGQLManager documentLinkGQLManager;
    private final CustomFieldGQLManager customFieldGQLManager;
    private final TemplateGQLManager templateGQLManager;

    public DocumentGQLManager( DocumentManager documentManager, DocumentLinkGQLManager documentLinkGQLManager, CustomFieldGQLManager customFieldGQLManager, TemplateGQLManager templateGQLManager ) {
        this.documentManager = documentManager;
        this.documentLinkGQLManager = documentLinkGQLManager;
        this.customFieldGQLManager = customFieldGQLManager;
        this.templateGQLManager = templateGQLManager;
    }

    /**
     * Finds document by id.
     * @param id document's id
     * @return Optionally document if such exists
     */
    public Optional<DocumentGQLO> getDocument( Long id ) {
        Optional<Document> document = documentManager.getDocument( id );
        if( document.isPresent() ) {
            Document presentDocument = document.get();
            return Optional.of( GQLMapperHelper.getGQLObjectFromEntity(
                    presentDocument,
                    templateGQLManager.getTemplatesAssignedToDocument( presentDocument.getId() ).get(),
                    customFieldGQLManager.getAllCustomFieldValuesRelatedWithDocument( presentDocument.getId() ).get(),
                    documentLinkGQLManager.documentLinksByDocumentId( presentDocument.getId() ).get()
                )
            );
        } else
            return Optional.empty();
    }

    /**
     * Returns a page of documents.
     * @param pageable documents page request {@link Pageable}
     * @return page of documents
     */
    public Page<DocumentGQLO> getAllDocuments( Pageable pageable ) {
        return new PageImpl<DocumentGQLO>(documentManager.getAllDocuments( pageable )
                .stream()
                .map( document -> getDocument( document.getId() ).get() )
                .toList());
    }

    /**
     * Creates document
     * @return created document
     */
    public DocumentGQLO createDocument(  ) {
        Document newDocument = documentManager.createNewDocument();

        newDocument.setCreated( Date.from( Instant.now() ) );
        documentManager.updateDocument( newDocument );

        return GQLMapperHelper.getGQLObjectFromEntity(
                documentManager.updateDocument( newDocument ),
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>()
        );
    }

    /**
     * Updates document
     * @param id new id of document to update
     * @param created new creation date of document to update
     * @param assignedTemplates new assigned templates of document to update
     * @param customFieldValues new custom field values of document to update ({[custom field id] : [value]})
     * @param documentLinks new links of document to update ( if one of source or target documents are null - it will be replaced with id of updated document )
     * @return updated document of one with such id found
     */
    public Optional<DocumentGQLO> updateDocument(
            Long id,
            Date created,
            List<Long> assignedTemplates,
            Map<Long, String> customFieldValues,
            List<DocumentLinkInputGQLO> documentLinks
    ) {
        return documentManager
                .getDocument( id )
                .map( document -> {
                    document.setCreated( created );

                    if( assignedTemplates != null ) {
                        updateDocumentTemplates( assignedTemplates, document );
                    }

                    if( customFieldValues != null ) {
                        updateDocumentCustomFieldValues( customFieldValues, document );
                    }

                    if( documentLinks != null ) {
                        updateDocumentDocumentLinks( documentLinks, document );
                    }

                    return documentManager.updateDocument( document );
                }
        ).map( document ->
                GQLMapperHelper.getGQLObjectFromEntity(
                     document,
                    templateGQLManager.getTemplatesAssignedToDocument( document.getId() ).get(),
                    customFieldGQLManager.getAllCustomFieldValuesRelatedWithDocument( document.getId() ).get(),
                    documentLinkGQLManager.documentLinksByDocumentId( document.getId() ).get()
                )
        );
    }

    private void updateDocumentDocumentLinks( List<DocumentLinkInputGQLO> documentLinks, Document document ) {
        removeAllDocumentLinksFromDocument( document );

        documentLinks.forEach( documentLink -> {
            documentLink.setTargetDocument( documentLink.getTargetDocument() == null ? document.getId() : documentLink.getTargetDocument() );
            documentLink.setSourceDocument( documentLink.getSourceDocument() == null ? document.getId() : documentLink.getSourceDocument() );
            if (
                    documentLink.getSourceDocument() != document.getId() &&
                    documentLink.getTargetDocument() != document.getId()
            ) {
                throw new IllegalArgumentException( MessageFormat.format(
                        "exception.document_links_update_cannot_contain_other_document_links",
                        documentLink
                    )
                );
            } else {
                documentLinkGQLManager.linkDocuments(
                        documentLink.getSourceDocument(),
                        documentLink.getTargetDocument(),
                        documentLink.getLinkType()
                ).get();
            }
        });
    }

    private void removeAllDocumentLinksFromDocument( Document document ) {
        documentLinkGQLManager.documentLinksByDocumentId( document.getId() ).ifPresent(
                documentLinkGQLOS ->
                        documentLinkGQLOS.forEach( documentLink ->
                                documentLinkGQLManager.removeDocumentLink( documentLink.getId() )
                        )
        );
    }

    private void updateDocumentCustomFieldValues( Map<Long, String> customFieldValues, Document document ) {
        removeAllCustomFieldValuesFromDocument( document );

        customFieldValues.forEach( ( key, value ) ->
                customFieldGQLManager.setCustomFieldValue( key, document.getId(), value )
        );
    }

    private void removeAllCustomFieldValuesFromDocument( Document document ) {
        customFieldGQLManager
                .getAllCustomFieldValuesRelatedWithDocument( document.getId() )
                .ifPresent(customFieldGQLOS ->
                customFieldGQLOS.forEach(customFieldGQLO ->
                    customFieldGQLManager.setCustomFieldValue(
                            customFieldGQLO.getId(),
                            document.getId(),
                            null
                    )
                )
        );
    }

    private void updateDocumentTemplates( List<Long> assignedTemplates, Document document ) {
        removeAllTemplatesFromDocument( document );

        assignedTemplates.forEach( templateId ->
                        templateGQLManager.assignTemplateToDocument( document.getId(), templateId )
        );
    }

    private void removeAllTemplatesFromDocument( Document document ) {
        templateGQLManager
                .getTemplatesAssignedToDocument( document.getId() ).ifPresent( templatesAssignedToDocument ->
                    templatesAssignedToDocument.forEach( templateGQLO ->
                            templateGQLManager.unassignTemplateFromDocument( document.getId(), templateGQLO.getId() )
                    )
                );
    }
}
