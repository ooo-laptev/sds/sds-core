/*
 * Copyright (c) 2019 - 2024, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.graphql.core.util;

import com.laptevaleksei.smartdocumentstorage.core.domain.entity.CustomField;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.Document;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.documentlink.DocumentLink;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.documentlink.DocumentLinkType;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.template.FieldBlock;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.template.Template;
import com.laptevaleksei.smartdocumentstorage.graphql.core.web.graphql.entity.*;

import java.util.List;

public class GQLMapperHelper {

    private GQLMapperHelper() {}

    public static DocumentGQLO getGQLObjectFromEntity(
             Document document,
             List<TemplateGQLO> assignedTemplates,
             List<CustomFieldGQLO> assignedCustomFields,
             List<DocumentLinkGQLO> documentLinks
    ) {
        return  new DocumentGQLO(
                document.getId(),
                document.getCreated(),
                assignedTemplates,
                assignedCustomFields,
                documentLinks
        );
    }

    public static DocumentLinkTypeGQLO getGQLObjectFromEntity(
            DocumentLinkType documentLinkType
    )
    {
        return new DocumentLinkTypeGQLO(
                documentLinkType.getId(),
                documentLinkType.getLinkTypeName(),
                documentLinkType.getDescription(),
                documentLinkType.getOutwardDirectionName(),
                documentLinkType.getInwardDirectionName(),
                null
        );
    }

    public static DocumentLinkGQLO getGQLObjectFromEntity( DocumentLink documentLink ) {
        return new DocumentLinkGQLO(
                documentLink.getId(),
                new DocumentGQLO( documentLink.getSourceDocument().getId(), documentLink.getSourceDocument().getCreated(), null, null, null ),
                new DocumentGQLO( documentLink.getTargetDocument().getId(), documentLink.getSourceDocument().getCreated(), null, null, null ),
                getGQLObjectFromEntity( documentLink.getLinkType() )
        );
    }

    public static TemplateGQLO getGQLObjectFromEntity(
            Template template,
            List<FieldBlockGQLO> fieldBlocks,
            List<CustomFieldGQLO> defaultCustomFields
    ) {
        return new TemplateGQLO(
                template.getId(),
                template.getName(),
                template.getDescription(),
                fieldBlocks,
                defaultCustomFields
        );
    }

    public static FieldBlockGQLO getGQLObjectFromEntity(
            FieldBlock fieldBlock,
            List<CustomFieldGQLO> customFields
    ) {
        return new FieldBlockGQLO(
                fieldBlock.getId(),
                fieldBlock.getName(),
                fieldBlock.getDescription(),
                customFields
        );
    }

    public static CustomFieldGQLO getGQLObjectFromEntity( CustomField customField ) {
        return new CustomFieldGQLO(
                customField.getId(),
                customField.getName(),
                customField.getDescription(),
                customField.getCustomFieldType(),
                null
        );
    }

    public static CustomFieldGQLO getGQLObjectFromEntity( CustomField customField, String value ) {
        return new CustomFieldGQLO(
                customField.getId(),
                customField.getName(),
                customField.getDescription(),
                customField.getCustomFieldType(),
                value
        );
    }
}
