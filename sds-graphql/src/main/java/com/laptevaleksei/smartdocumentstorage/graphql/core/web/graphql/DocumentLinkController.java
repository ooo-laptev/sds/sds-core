/*
 * Copyright (c) 2019 - 2024, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.graphql.core.web.graphql;

import com.laptevaleksei.smartdocumentstorage.graphql.core.api.manager.graphql.DocumentLinkGQLManager;
import com.laptevaleksei.smartdocumentstorage.graphql.core.web.graphql.entity.DocumentLinkGQLO;
import com.laptevaleksei.smartdocumentstorage.graphql.core.web.graphql.entity.DocumentLinkTypeGQLO;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.stereotype.Controller;

import java.util.List;

/**
 * GraphQL controller for {@link DocumentLinkGQLO} and {@link DocumentLinkTypeGQLO}
 * @author <a href="mailto:laptevalexander28@gmail.com">Aleksandr Laptev</a> on 7-Nov-2023
 */
@Controller
public class DocumentLinkController {
    private final DocumentLinkGQLManager documentLinkGQLManager;

    public DocumentLinkController( DocumentLinkGQLManager documentLinkGQLManager ) {
        this.documentLinkGQLManager = documentLinkGQLManager;
    }

    @QueryMapping
    private  DocumentLinkTypeGQLO documentLinkTypeById( @Argument Long id ) {
        return documentLinkGQLManager.getDocumentLinkType( id ).orElseThrow();
    }

    @QueryMapping
    private  DocumentLinkGQLO documentLinkById( @Argument Long id ) {
        return documentLinkGQLManager.getDocumentLink( id ).orElseThrow();
    }

    @QueryMapping
    private List<DocumentLinkTypeGQLO> documentLinkTypes(
            @Argument int page,
            @Argument int size,
            @Argument String sort
    ) {
        return documentLinkGQLManager.getAllDocumentLinkTypes(
                PageRequest.of(
                        page,
                        size,
                        sort == null || sort.isEmpty() ? Sort.unsorted() : Sort.by( sort )
                )
        );
    }
    @QueryMapping
    private List<DocumentLinkGQLO> documentLinks(
            @Argument int page,
            @Argument int size,
            @Argument String sort
    ) {
        return documentLinkGQLManager.getAllDocumentLinks(
                PageRequest.of(
                        page,
                        size,
                        sort == null || sort.isEmpty() ? Sort.unsorted() : Sort.by( sort )
                )
        ).toList();
    }

    @MutationMapping
    private DocumentLinkGQLO documentLink(
            @Argument Long sourceDocumentId,
            @Argument Long targetDocumentId,
            @Argument Long typeId
    ) {
        return documentLinkGQLManager.linkDocuments( sourceDocumentId, targetDocumentId, typeId ).get();
    }

    @MutationMapping
    private DocumentLinkTypeGQLO documentLinkType(
            @Argument Long id,
            @Argument String name,
            @Argument String description,
            @Argument String outwardDirectionName,
            @Argument String inwardDirectionName
    ) {
        if( id == null ) {
            id = documentLinkGQLManager.createDocumentLinkType(
                    name,
                    description,
                    outwardDirectionName,
                    inwardDirectionName
            ).getId();
        }

        return documentLinkGQLManager.updateDocumentLinkType(
                id,
                name,
                description,
                outwardDirectionName,
                inwardDirectionName
        ).get();
    }
}
