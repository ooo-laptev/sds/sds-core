/*
 * Copyright (c) 2019 - 2024, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.graphql.core.web.graphql;

import com.laptevaleksei.smartdocumentstorage.graphql.core.api.manager.graphql.CustomFieldGQLManager;
import com.laptevaleksei.smartdocumentstorage.graphql.core.web.graphql.entity.CustomFieldGQLO;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.stereotype.Controller;

import java.util.List;

/**
 * GraphQL controller for {@link CustomFieldGQLO}
 * @author <a href="mailto:laptevalexander28@gmail.com">Aleksandr Laptev</a> on 7-Nov-2023
 */
@Controller
public class CustomFieldController {

    private final CustomFieldGQLManager customFieldGQLManager;

    public CustomFieldController( CustomFieldGQLManager customFieldGQLManager ) {
        this.customFieldGQLManager = customFieldGQLManager;
    }

    @QueryMapping
    private  CustomFieldGQLO customFieldById( @Argument Long id ) {
        return customFieldGQLManager.getCustomField( id ).orElseThrow();
    }

    @QueryMapping
    private  List<String> availableCustomFieldTypes() {
        return customFieldGQLManager.getAllCustomFieldTypes();
    }

    @MutationMapping
    private CustomFieldGQLO customField(
            @Argument Long id,
            @Argument String name,
            @Argument String description,
            @Argument String type
    ) {
        if ( id == null ) {
            id = customFieldGQLManager.createCustomField(
                    name,
                    description,
                    type
            ).getId();
        }

        return customFieldGQLManager.updateCustomField(
                id,
                name,
                description,
                type
        ).get();
    }
}
