/*
 * Copyright (c) 2019 - 2024, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.graphql.core.api.manager.graphql;


import com.laptevaleksei.smartdocumentstorage.core.domain.entity.Document;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.documentlink.DocumentLink;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.documentlink.DocumentLinkType;
import com.laptevaleksei.smartdocumentstorage.graphql.core.util.GQLMapperHelper;
import com.laptevaleksei.smartdocumentstorage.graphql.core.web.graphql.entity.DocumentLinkGQLO;
import com.laptevaleksei.smartdocumentstorage.graphql.core.web.graphql.entity.DocumentLinkTypeGQLO;
import com.laptevaleksei.smartdocumentstorage.manager.DocumentLinkManager;
import com.laptevaleksei.smartdocumentstorage.manager.DocumentManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * GraphQL manager class for {@link DocumentLinkGQLO}
 * @author <a href="mailto:laptevalexander28@gmail.com">Aleksandr Laptev</a> on 7-Nov-2023
 */
@Service
public class DocumentLinkGQLManager {

    private final DocumentLinkManager documentLinkManager;
    private final DocumentManager documentManager;
    private final CustomFieldGQLManager customFieldGQLManager;

    public DocumentLinkGQLManager( DocumentLinkManager documentLinkManager, DocumentManager documentManager, CustomFieldGQLManager customFieldGQLManager ) {
        this.documentLinkManager = documentLinkManager;
        this.documentManager = documentManager;
        this.customFieldGQLManager = customFieldGQLManager;
    }

    /**
     * Finds all document links related with document with provided id
     * @param id document id
     * @return list of {@link DocumentLinkGQLO} related with document
     */
    public Optional<List<DocumentLinkGQLO>> documentLinksByDocumentId( Long id ) {
        return documentManager.getDocument( id )
                .map( documentLinkManager::getAllLinkedDocuments )
                .map( documentLinks ->
                        documentLinks.stream()
                        .map( GQLMapperHelper::getGQLObjectFromEntity )
                        .toList()
                );
    }

    /**
     * Finds {@link DocumentLinkGQLO} by id
     * @param id id of {@link DocumentLinkGQLO} to find
     * @return {@link DocumentLinkGQLO} with provided id if such found, else - {@link Optional}.empty()
     */
    public Optional<DocumentLinkGQLO> getDocumentLink( Long id ) {
        return documentLinkManager
                .getDocumentLink( id )
                .map( GQLMapperHelper::getGQLObjectFromEntity );
    }

    /**
     * Finds {@link DocumentLinkTypeGQLO} by id
     * @param id id of {@link DocumentLinkTypeGQLO} to find
     * @return {@link DocumentLinkTypeGQLO} with provided id if such found, else - {@link Optional}.empty()
     */
    public Optional<DocumentLinkTypeGQLO> getDocumentLinkType( Long id ) {
        return documentLinkManager
                .getDocumentLinkType( id )
                .map( GQLMapperHelper::getGQLObjectFromEntity );
    }

    /**
     * Requests page of {@link DocumentLinkGQLO}s
     * @param pageable {@link DocumentLinkGQLO}s page request
     * @return list of {@link DocumentLinkGQLO}s found according to page request
     */
    public Page<DocumentLinkGQLO> getAllDocumentLinks( Pageable pageable ) {
        return new PageImpl<>(documentLinkManager.getAllDocumentLinks( pageable ).stream().map(
                documentLink -> this.getDocumentLink( documentLink.getId() ).get()
        ).toList());
    }

    /**
     * Requests page of {@link DocumentLinkTypeGQLO}s
     * @param pageable {@link DocumentLinkTypeGQLO}s page request
     * @return list of {@link DocumentLinkTypeGQLO}s found according to page request
     */
    public List<DocumentLinkTypeGQLO> getAllDocumentLinkTypes( Pageable pageable ) {
        return documentLinkManager.getAllDocumentLinkTypes( pageable ).stream().map(
                GQLMapperHelper::getGQLObjectFromEntity
        ).toList();
    }

    /**
     * Creates {@link DocumentLink} for provided documents
     * @param sourceDocumentId id of source document to link
     * @param targetDocumentId id of target document to link
     * @param linkTypeId id of link type
     * @return created link ( returns Optional.empty() if one of entities were not found by provided ids )
     */
    public Optional<DocumentLinkGQLO> linkDocuments(
            Long sourceDocumentId,
            Long targetDocumentId,
            Long linkTypeId
    ) {
        Optional<Document> sourceDocument = documentManager.getDocument( sourceDocumentId );
        Optional<Document> targetDocument = documentManager.getDocument( targetDocumentId );
        Optional<DocumentLinkType> linkType = documentLinkManager.getDocumentLinkType( linkTypeId );
        return getDocumentLink(
                documentLinkManager.linkDocuments(
                    sourceDocument.get(),
                    targetDocument.get(),
                    linkType.get()
                ).getId()
        );
    }

    /**
     * Removes {@link DocumentLink} by id
     * @param id id of DocumentLink to remove
     */
    public void removeDocumentLink( Long id ) {
        documentLinkManager.deleteDocumentLink( id );
    }

    /**
     * Creates {@link DocumentLinkType}
     * @param name name for new DocumentLinkType
     * @param description description for new DocumentLinkType
     * @param outwardDirectionName name of outward direction for new DocumentLinkType
     * @param inwardDirectionName name of inward direction for new DocumentLinkType
     * @return Created DocumentLinkType
     */
    public DocumentLinkTypeGQLO createDocumentLinkType( String name, String description, String outwardDirectionName, String inwardDirectionName ) {
        return GQLMapperHelper.getGQLObjectFromEntity(documentLinkManager.createDocumentLinkType(
                name,
                description,
                inwardDirectionName,
                outwardDirectionName
        ));
    }

    /**
     * Updates {@link DocumentLinkType}
     * @param id id of DocumentLinkType to update
     * @param name new name for DocumentLinkType
     * @param description new description for DocumentLinkType
     * @param outwardDirectionName new name of outward direction for DocumentLinkType
     * @param inwardDirectionName new name of inward direction for DocumentLinkType
     * @return updated DocumentLinkType if one with such id found
     */
    public Optional<DocumentLinkTypeGQLO> updateDocumentLinkType( Long id, String name, String description, String outwardDirectionName, String inwardDirectionName ) {
        return documentLinkManager.getDocumentLinkType( id )
                .map( documentLinkType -> {
                        documentLinkType.setLinkTypeName( name );

                        documentLinkType.setDescription( description );

                        documentLinkType.setOutwardDirectionName( outwardDirectionName );

                        documentLinkType.setInwardDirectionName( inwardDirectionName );

                    return GQLMapperHelper.getGQLObjectFromEntity(
                            documentLinkManager.updateDocumentLinkType( documentLinkType )
                    );
                });
    }
}
