/*
 * Copyright (c) 2019 - 2024, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.graphql.core.api.manager.graphql;

import com.laptevaleksei.smartdocumentstorage.core.domain.entity.CustomField;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.Document;
import com.laptevaleksei.smartdocumentstorage.manager.CustomFieldManager;
import com.laptevaleksei.smartdocumentstorage.manager.DocumentManager;
import com.laptevaleksei.smartdocumentstorage.graphql.core.util.GQLMapperHelper;
import com.laptevaleksei.smartdocumentstorage.graphql.core.web.graphql.entity.CustomFieldGQLO;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * GraphQL manager for {@link CustomFieldGQLO}
 * @author <a href="mailto:laptevalexander28@gmail.com">Aleksandr Laptev</a> on 7-Nov-2023
 */
@Service
public class CustomFieldGQLManager {
    private final CustomFieldManager customFieldManager;
    private final DocumentManager documentManager;

    public CustomFieldGQLManager(
            CustomFieldManager customFieldManager,
            DocumentManager documentManager
    ) {
        this.customFieldManager = customFieldManager;
        this.documentManager = documentManager;
    }

    /**
     * Finds CustomField by id.
     * @param id CustomField's id to find
     * @return Optionally (if custom field exists) custom field
     */
    public Optional<CustomFieldGQLO> getCustomField( Long id ) {
        Optional<CustomField> customField = customFieldManager.getCustomField( id );
        return customField.map( GQLMapperHelper::getGQLObjectFromEntity );
    }

    /**
     * Finds all custom fields related with document and finds their values.
     * @param id document's id
     * @return Optionally (if document exists) list with custom fields with values
     */
    public Optional<List<CustomFieldGQLO>> getAllCustomFieldValuesRelatedWithDocument( Long id ) {
        Optional<Document> entityDocument = documentManager.getDocument( id );
        return entityDocument.map( document -> {
            List<CustomField> customFieldsFromDatabase = customFieldManager.getAllCustomFieldsRelatedWithDocument( document );
            return customFieldsFromDatabase.stream().map( customField -> {
                Object value = customFieldManager.getCustomFieldValue( document.getId(), customField.getId() );
                return GQLMapperHelper.getGQLObjectFromEntity(
                        customField,
                        value != null ? value.toString() : null
                );
            } ).toList();
        });
    }

    /**
     * Provides all {@link CustomField} types registered in SDS
     * @return list of all custom field types as java classes package paths
     */
    public List<String> getAllCustomFieldTypes() {
        return customFieldManager.getAllCustomFieldTypes();
    }

    /**
     * Sets {@link CustomField} value
     * @param customFieldId CustomField to set value
     * @param documentId document to set value
     * @param value value to set
     */
    public void setCustomFieldValue( Long customFieldId, Long documentId, Object value ) {
        customFieldManager.setCustomFieldValue( documentId, customFieldId, value );
    }

    /**
     * Creates {@link CustomField}
     * @param name CustomField name
     * @param description CustomField description
     * @param type CustomField type name (package path)
     * @return created CustomField
     */
    public CustomFieldGQLO createCustomField(String name, String description, String type) {
        CustomField createdCustomField = customFieldManager.createCustomField( name, description, type );
        return GQLMapperHelper.getGQLObjectFromEntity( createdCustomField );
    }

    /**
     * Updates CustomField
     * @param id id of custom field to update
     * @param name new name for CustomField
     * @param description new description for CustomField
     * @param type new type for CustomField
     * @return updated CustomField if one with such id found
     */
    public Optional<CustomFieldGQLO> updateCustomField( Long id, String name, String description, String type ) {
        return customFieldManager.getCustomField( id ).map( customField -> {
            customField.setName( name );
            customField.setDescription( description );
            customField.setCustomFieldType( type );
            return GQLMapperHelper.getGQLObjectFromEntity( customFieldManager.updateCustomField( customField ) );
        });
    }
}
