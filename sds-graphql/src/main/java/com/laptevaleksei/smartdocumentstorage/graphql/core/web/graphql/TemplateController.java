/*
 * Copyright (c) 2019 - 2024, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.graphql.core.web.graphql;

import com.laptevaleksei.smartdocumentstorage.graphql.core.api.manager.graphql.TemplateGQLManager;
import com.laptevaleksei.smartdocumentstorage.graphql.core.web.graphql.entity.DocumentGQLO;
import com.laptevaleksei.smartdocumentstorage.graphql.core.web.graphql.entity.FieldBlockGQLO;
import com.laptevaleksei.smartdocumentstorage.graphql.core.web.graphql.entity.TemplateGQLO;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.stereotype.Controller;

import java.util.List;

/**
 * GraphQL controller for {@link TemplateGQLO} and {@link FieldBlockGQLO}
 * @author <a href="mailto:laptevalexander28@gmail.com">Aleksandr Laptev</a> on 7-Nov-2023
 */
@Controller
public class TemplateController {
    private final TemplateGQLManager templateGQLManager;

    public TemplateController( TemplateGQLManager templateGQLManager ) {
        this.templateGQLManager = templateGQLManager;
    }

    @QueryMapping
    private TemplateGQLO templateById( @Argument Long id ) {
        return templateGQLManager.getTemplate( id ).orElseThrow();
    }

    @QueryMapping
    private FieldBlockGQLO fieldBlockById( @Argument Long id ) {
        return templateGQLManager.getFieldBlock( id ).orElseThrow();
    }

    @QueryMapping
    private List<DocumentGQLO> documentsAttachedWithTemplate(
            @Argument int page,
            @Argument int size,
            @Argument String sort,
            @Argument Long templateId
    ) {
        return templateGQLManager.documentsAttachedWithTemplate(
                PageRequest.of(
                        page,
                        size,
                        sort == null || sort.isEmpty() ? Sort.unsorted() : Sort.by( sort )
                ),
                templateId
        ).orElseThrow();
    }

    @QueryMapping
    private List<FieldBlockGQLO> fieldBlocks(
            @Argument int page,
            @Argument int size,
            @Argument String sort
    ) {
        return templateGQLManager.fieldBlocks(
                PageRequest.of(
                        page,
                        size,
                        sort == null || sort.isEmpty() ? Sort.unsorted() : Sort.by( sort )
                )
        );
    }

    @QueryMapping
    private List<TemplateGQLO> templates(
            @Argument int page,
            @Argument int size,
            @Argument String sort
    ) {
        return templateGQLManager.templates(
                PageRequest.of(
                        page,
                        size,
                        sort == null || sort.isEmpty() ? Sort.unsorted() : Sort.by( sort )
                )
        );
    }

    @MutationMapping
    private TemplateGQLO template(
            @Argument Long id,
            @Argument String name,
            @Argument String description,
            @Argument List<Long> content,
            @Argument List<Long> defaultCustomFields
    ) {
        if( id == null ) {
            id = templateGQLManager.createTemplate(
                    name,
                    description
            ).getId();
        }

        return templateGQLManager.updateTemplate(
                id,
                name,
                description,
                content,
                defaultCustomFields
        ).get();
    }

    @MutationMapping
    private FieldBlockGQLO fieldBlock(
            @Argument Long id,
            @Argument String name,
            @Argument String description,
            @Argument List<Long> content
    ) {
        if( id == null ) {
            id = templateGQLManager.createFieldBlock(
                    name,
                    description
            ).getId();
        }

        return templateGQLManager.updateFieldBlock(
                id,
                name,
                description,
                content
        ).get();
    }
}
