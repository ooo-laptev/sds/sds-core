/*
 * Copyright (c) 2019 - 2024, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.graphql.core.web.graphql;

import com.laptevaleksei.smartdocumentstorage.graphql.core.api.manager.graphql.CustomFieldGQLManager;
import com.laptevaleksei.smartdocumentstorage.graphql.core.api.manager.graphql.DocumentGQLManager;
import com.laptevaleksei.smartdocumentstorage.graphql.core.api.manager.graphql.TemplateGQLManager;
import com.laptevaleksei.smartdocumentstorage.graphql.core.web.graphql.entity.CustomFieldGQLO;
import com.laptevaleksei.smartdocumentstorage.graphql.core.web.graphql.entity.DocumentGQLO;
import com.laptevaleksei.smartdocumentstorage.graphql.core.web.graphql.entity.DocumentLinkInputGQLO;
import com.laptevaleksei.smartdocumentstorage.graphql.core.web.graphql.entity.TemplateGQLO;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.graphql.data.method.annotation.SchemaMapping;
import org.springframework.stereotype.Controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * GraphQL controller for {@link DocumentGQLO}
 * @author <a href="mailto:laptevalexander28@gmail.com">Aleksandr Laptev</a> on 7-Nov-2023
 */
@Controller
public class DocumentController {

    private final DocumentGQLManager documentGQLManager;
    private final CustomFieldGQLManager customFieldGQLManager;
    private final TemplateGQLManager templateGQLManager;

    public DocumentController( DocumentGQLManager documentGQLManager, CustomFieldGQLManager customFieldGQLManager, TemplateGQLManager templateGQLManager ) {
        this.documentGQLManager = documentGQLManager;
        this.customFieldGQLManager = customFieldGQLManager;
        this.templateGQLManager = templateGQLManager;
    }

    @QueryMapping
    private  DocumentGQLO documentById( @Argument Long id ) {
        return documentGQLManager.getDocument( id ).orElseThrow();
    }

    @QueryMapping
    private  List<DocumentGQLO> documents(@Argument int page, @Argument int size, @Argument String sort)
    {
        return documentGQLManager
                .getAllDocuments(
                        PageRequest.of(
                                page,
                                size,
                                sort == null || sort.isEmpty() ? Sort.unsorted() : Sort.by( sort )
                        )
                ).stream().toList();
    }
    @SchemaMapping
    private  List<TemplateGQLO> assignedTemplates(DocumentGQLO document ) {
        return templateGQLManager.getTemplatesAssignedToDocument( document.getId() ).orElseThrow();
    }

    @SchemaMapping
    private  List<CustomFieldGQLO> customFieldValues(DocumentGQLO document ) {
        return customFieldGQLManager.getAllCustomFieldValuesRelatedWithDocument( document.getId() ).orElseThrow();
    }

    @MutationMapping
    private DocumentGQLO document(
            @Argument Long id,
            @Argument String created,
            @Argument List<Long> assignedTemplates,
            @Argument Map<String, String> customFieldValues,
            @Argument List<DocumentLinkInputGQLO> links
    ) throws ParseException {
        if( id == null )
            id = documentGQLManager.createDocument().getId();

        Map<Long, String> customFieldValuesMap = new HashMap<>();
        customFieldValues.forEach( (key, value) ->
                customFieldValuesMap.put( Long.parseLong( key ), value )
        );

        return documentGQLManager.updateDocument(
                id,
                created == null || created.isEmpty() ? new Date() : new SimpleDateFormat().parse( created ),
                assignedTemplates,
                customFieldValuesMap,
                links
        ).get();
    }
}
