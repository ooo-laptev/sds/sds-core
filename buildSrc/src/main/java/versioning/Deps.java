/*
 * Copyright (c) 2019 - 2024, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package versioning;

/**
 * Interface what contains dependencies.
 *
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 28-Dec-2023
 */
public interface Deps {

    /**
     * This interface contains versions for all dependencies.
     *
     * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 28-Dec-2023
     */
    interface Versions {
        String ARCHUNIT__VER = "1.1.0";
        String AWAITILITY__VER = "4.2.0";
        String APACHE_COLLECTIONS__VER = "4.4";
        String APACHE_COMMONS__VER = "3.12.0";
        String JAKARTA_ANNOTATION = "2.1.1";
        String JAKARTA_PERSISTENCE = "3.1.0";
        String JAKARTA_VALIDATION = "3.0.2";
        String JHIPSTER = "8.0.0-rc.1";
        String GOOGLE_GSON__VER = "2.8.9";
        String COMMONS_OI__VER = "2.6";
        String COMMONS_VALIDATIOR__VER = "1.7";
        String SPRING_BOOT__VER = "3.2.1";
        String SPRING__VER = "6.1.2";
        String HIBERNATE__VER = "6.2.9.Final";
        String MAP_STRUCT__VER = "1.5.5.Final";
        String LIQUIBASE__VER = "4.24.0";
        String LIQUIBASE_HIBERNATE6__VER = "4.24.0";
        String HIBERNATE_VALIDATOR__VER = "8.0.1.Final";
        String HIBERNATE_TYPES__VER = "2.21.1";
        String JUNIT = "4.13.2";
        String JUPITER = "5.9.2";
        String MOCKITO = "5.2.0";
        String GRAPHQL__VER = "21.0";
        String TESTCONTAINERS__VER = "1.17.6";
        String JACKSON__VER = "2.14.2";
        String SPRINGDOC_OPENAPI__VER = "2.0.4";
        String HIKARICP__VER = "5.0.1";
        String MICROMETER__VER = "1.11.2";
        String DROPWIZARD_METRICS__VER = "4.2.17";
        String JAVAX_CACHE__VER = "1.1.1";
        String POSTGRESQL = "42.6.0";
        String PICOCLI__VER = "4.7.5";
        String H2DB__VER = "2.1.210";
        String ZIPKIN_REPORT_BRAVE__VER = "2.17.1";
        String EHCACHE__VER = "3.10.8";
        String LOMBOK = "1.18.30";

        /**
         * Contains Gradle plugin versions
         *
         * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 29-Dec-2023
         */
        interface GradlePlugin {
            String JIB_PLUGIN__VER = "3.4.0";
            String GIT_PROPERTIES_PLUGIN__VER = "2.4.1";
            String SONARQUBE_PLUGIN__VER = "4.4.1.3373";
            String SPOTLESS_PLUGIN__VER = "6.22.0";
            String NOHTTP_PLUGIN__VER = "0.0.11";
            String GRADLE_MODERNIZER_PLUGIN__VER = "1.9.0";
            String GATLING_PLUGIN__VER = "3.9.5.6";
            String LIQUIBASE_PLUGIN__VER = "2.0.4";
        }
    }

    /**
     * This interface contains all Jakarta dependencies.
     *
     * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 28-Dec-2023
     */
    interface Jakarta {
        String ANNOTATION   = "jakarta.annotation:jakarta.annotation-api"                       + ':' + Versions.JAKARTA_ANNOTATION;
        String PERSISTENCE  = "jakarta.persistence:jakarta.persistence-api"                       + ':' + Versions.JAKARTA_PERSISTENCE;
        String VALIDATION   = "jakarta.validation:jakarta.validation-api"                       + ':' + Versions.JAKARTA_VALIDATION;
    }

    /**
     * This interface contains Hibernate dependencies.
     *
     * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 28-Dec-2023
     */
    interface Hibernate {
        String VALIDATOR                            = "org.hibernate.validator:hibernate-validator"                 + ':' + Versions.HIBERNATE_VALIDATOR__VER;
        String JPA_MODELGEN = "org.hibernate:hibernate-jpamodelgen" + ":" + Versions.HIBERNATE__VER;

        /**
         * This interface contains Hibernate ORM dependencies.
         *
         * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 28-Dec-2023
         */
        interface Orm {
            String CORE = "org.hibernate.orm:hibernate-core" + ":" + Versions.HIBERNATE__VER;
            String JCACHE = "org.hibernate.orm:hibernate-jcache" + ":" + Versions.HIBERNATE__VER;
        }
    }

    /**
     * This interface contains all libraries what are not categorized.
     *
     * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 28-Dec-2023
     */
    interface Libs {
        String JHIPSTER_FW                       = "tech.jhipster:jhipster-framework"                            + ':' + Versions.JHIPSTER;
        String GRAPHQL = "com.graphql-java:graphql-java-extended-scalars" + ":" + Versions.GRAPHQL__VER;
        String VLADMIHALCEA_HIBERNATE_TYPES = "com.vladmihalcea:hibernate-types-60" + ":" + Versions.HIBERNATE_TYPES__VER;
        String APACHE_COMMONS_COLLECTIONS_4 = "org.apache.commons:commons-collections4" + ":" + Versions.APACHE_COLLECTIONS__VER;
        String APACHE_COMMONS_LANG3 = "org.apache.commons:commons-lang3" + ":" + Versions.APACHE_COMMONS__VER;
        String AWAITILITY = "org.awaitility:awaitility" + ":" + Versions.AWAITILITY__VER;
        String GSON = "com.google.code.gson:gson" + ":" + Versions.GOOGLE_GSON__VER;
        String MAPSTRUCT = "org.mapstruct:mapstruct" + ":" + Versions.MAP_STRUCT__VER;
        String MAPSTRUCT_PROCESSOR = "org.mapstruct:mapstruct-processor" + ":" + Versions.MAP_STRUCT__VER;
        String HIKARICP = "com.zaxxer:HikariCP" + ":" + Versions.HIKARICP__VER;
        String COMMONS_VALIDATOR = "commons-validator:commons-validator" + ":" + Versions.COMMONS_VALIDATIOR__VER;
        String COMMONS_IO = "commons-io:commons-io" + ":" + Versions.COMMONS_OI__VER;
        String MICROMETER_REGISTRY_PROMETHEUS = "io.micrometer:micrometer-registry-prometheus" + ":" + Versions.MICROMETER__VER;
        String MICROMETER_TRACING_BRIDGE_BRAVE = "io.micrometer:micrometer-tracing-bridge-brave" + ":" + Versions.MICROMETER__VER;
        String DROPWIZARD_METRICS_CORE = "io.dropwizard.metrics:metrics-core" + ":" + Versions.DROPWIZARD_METRICS__VER;
        String JAVAX_CACHE_API = "javax.cache:cache-api" + ":" + Versions.JAVAX_CACHE__VER;
        String POSTGRESQL = "org.postgresql:postgresql" + ":" + Versions.POSTGRESQL;
        String PICOCLI = "info.picocli:picocli" + ":" + Versions.PICOCLI__VER;
        String H2DB = "com.h2database:h2" + ":" + Versions.H2DB__VER;
        String ZIPKIN_REPORTER_BRAVE = "io.zipkin.reporter2:zipkin-reporter-brave" + ":" + Versions.ZIPKIN_REPORT_BRAVE__VER;
        String EHCACHE_JAKARTA = "org.ehcache:ehcache" + ":" + Versions.EHCACHE__VER + ":" + "jakarta";
        String LOMBOK = "org.projectlombok:lombok" + ":" + Versions.LOMBOK;

        /**
         * Contains all fasterxml dependencies.
         *
         * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 28-Dec-2023
         */
        interface FasterXML {
            String JACKSON_MODULE_JAXB_ANNOTATIONS = "com.fasterxml.jackson.module:jackson-module-jaxb-annotations" + ":" + Versions.JACKSON__VER;

            /**
             * Contains fasterxml.datatype dependencies.
             *
             * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 28-Dec-2023
             */
            interface Datatype {
                String HIBERNATE6 = "com.fasterxml.jackson.datatype:jackson-datatype-hibernate6" + ":" + Versions.JACKSON__VER;
                String HPPC = "com.fasterxml.jackson.datatype:jackson-datatype-hppc" + ":" + Versions.JACKSON__VER;
                String JSR310 = "com.fasterxml.jackson.datatype:jackson-datatype-hppc" + ":" + Versions.JACKSON__VER;
            }
        }

        /**
         * Contains SpringDoc dependencies.
         *
         * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 28-Dec-2023
         */
        interface SpringDoc {
            /**
             * Contains all springdoc.openapi dependencies.
             *
             * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 28-Dec-2023
             */
            interface OpenAPI {
                String STARTER_WEBMVC_API = "org.springdoc:springdoc-openapi-starter-webmvc-api" + ":" + Versions.SPRINGDOC_OPENAPI__VER;
                String STARTER_WEBMVC_UI = "org.springdoc:springdoc-openapi-starter-webmvc-ui" + ":" + Versions.SPRINGDOC_OPENAPI__VER;
            }
        }

        /**
         * Contains Liquibase dependencies.
         *
         * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 28-Dec-2023
         */
        interface Liquibase {
            String CORE = "org.liquibase:liquibase-core" + ":" + Versions.LIQUIBASE__VER;

            /**
             * Contains dependencies of liquibase.ext.
             *
             * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 28-Dec-2023
             */
            interface Ext {
                String HIBERNATE6 = "org.liquibase.ext:liquibase-hibernate6" + ":" +Versions.LIQUIBASE_HIBERNATE6__VER;
            }
        }
    }

    /**
     * Contains dependencies used for testing purposes.
     *
     * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 28-Dec-2023
     */
    interface Testing {
        String JUNIT = "junit:junit" + ":" + Versions.JUNIT;
        String JUNIT_JUPITER = "org.junit.jupiter:junit-jupiter" + ":" + Versions.JUPITER;
        String MOCKITO_JUPITER = "org.mockito:mockito-junit-jupiter" + ":" + Versions.MOCKITO;
        String ARCHUNIT_API = "com.tngtech.archunit:archunit-junit5-api" + ":" + Versions.ARCHUNIT__VER;
        String ARCHUNIT_ENGINE = "com.tngtech.archunit:archunit-junit5-engine" + ":" + Versions.ARCHUNIT__VER;
    }

    /**
     * Contains allSpring dependencies.
     *
     * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 28-Dec-2023
     */
    interface Spring {
        String CONTEXT = "org.springframework:spring-context" + ":" + Versions.SPRING__VER;

        /**
         * Contains Sprong Boot dependencies.
         *
         * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 28-Dec-2023
         */
        interface Boot {
            String LOADER_TOOLS = "org.springframework.boot:spring-boot-loader-tools" + ":" + Versions.SPRING_BOOT__VER;
            String TEST = "org.springframework.boot:spring-boot-test" +":" + Versions.SPRING_BOOT__VER;
            String CONFIGURATION_PROCESSOR = "org.springframework.boot:spring-boot-configuration-processor" +":" + Versions.SPRING_BOOT__VER;

            /**
             * Contains Starter dependencies.
             *
             * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 28-Dec-2023
             */
            interface Starter {
                String DATA_JPA = "org.springframework.boot:spring-boot-starter-data-jpa" + ":" + Versions.SPRING_BOOT__VER;
                String ACTUATOR = "org.springframework.boot:spring-boot-starter-actuator" + ":" + Versions.SPRING_BOOT__VER;
                String WEB = "org.springframework.boot:spring-boot-starter-web" + ":" + Versions.SPRING_BOOT__VER;
                String SECURITY = "org.springframework.boot:spring-boot-starter-security" + ":" + Versions.SPRING_BOOT__VER;
                String GRAPHQL = "org.springframework.boot:spring-boot-starter-graphql" + ":" + Versions.SPRING_BOOT__VER;
                String TEST = "org.springframework.boot:spring-boot-starter-test" + ":" + Versions.SPRING_BOOT__VER;
                String MAIL = "org.springframework.boot:spring-boot-starter-mail" + ":" + Versions.SPRING_BOOT__VER;
                String DATA_ELASTICSEARCH = "org.springframework.boot:spring-boot-starter-data-elasticsearch" + ":" + Versions.SPRING_BOOT__VER;
                String LOGGING = "org.springframework.boot:spring-boot-starter-logging" + ":" + Versions.SPRING_BOOT__VER;
                String THYMELEAF = "org.springframework.boot:spring-boot-starter-thymeleaf" + ":" + Versions.SPRING_BOOT__VER;
                String UNDERTROW = "org.springframework.boot:spring-boot-starter-undertow" + ":" + Versions.SPRING_BOOT__VER;
                String OAUTH2_RESOURCE_SERVER = "org.springframework.boot:spring-boot-starter-oauth2-resource-server" + ":" + Versions.SPRING_BOOT__VER;
                String CACHE = "org.springframework.boot:spring-boot-starter-cache" + ":" + Versions.SPRING_BOOT__VER;
            }
        }

        /**
         * Contains Spring Data dependencies.
         *
         * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 28-Dec-2023
         */
        interface Data {
            String COMMONS = "org.springframework.data:spring-data-commons" + ":" + "3.1.4";
        }

        /**
         * Contains Spring Security dependencies.
         *
         * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 28-Dec-2023
         */
        interface Security {
            String DATA = "org.springframework.security:spring-security-data" + ":" + Versions.SPRING__VER;
            String TEST = "org.springframework.security:spring-security-test" + ":" + Versions.SPRING__VER;
        }
    }

    /**
     * Contains all Testcontainers dependencies.
     *
     * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 28-Dec-2023
     */
    interface TestContainers {
        String TESTCONTAINERS = "org.testcontainers:testcontainers" + ":" + Versions.TESTCONTAINERS__VER;
        String POSTGERSQL = "org.testcontainers:postgresql" + ":" + Versions.TESTCONTAINERS__VER;
        String ELASTICSEARCH = "org.testcontainers:elasticsearch" + ":" + Versions.TESTCONTAINERS__VER;
        String JUNIT_JUPITER = "org.testcontainers:junit-jupiter" + ":" + Versions.TESTCONTAINERS__VER;
    }
}
