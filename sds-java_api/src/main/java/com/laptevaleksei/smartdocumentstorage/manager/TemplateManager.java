/*
 * Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.manager;

import com.laptevaleksei.annotations.PublicApi;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.CustomField;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.Document;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.template.FieldBlock;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.template.FieldBlockLayout;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.template.Template;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.template.TemplateLayout;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Management-layer interface for Template management.
 *
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 01-Nov-2023
 */
@PublicApi
public interface TemplateManager {

    /**
     * This method provide FieldBlock by ID.
     *
     * @param id ID of FieldBlock.
     *
     * @return Instance of exists FieldBlock.
     */
    Optional<FieldBlock> getFieldBlock( Long id );

    /**
     * You can create {@link FieldBlock} directly via this method.
     *
     * @param fieldBlock Instance of {@link FieldBlock} which you want to create in DB (without ID).
     *
     * @return Instance of {@link FieldBlock} which saved in DB (with assigned ID).
     */
    FieldBlock createFieldBlock( FieldBlock fieldBlock );

    /**
     * This method provides functionality for delete exists FieldBlock.
     *
     * @param fieldBlock Object which should be deleted.
     */
    void deleteFieldBlock( FieldBlock fieldBlock );

    /**
     * This method return all exist FieldBlocks.
     *
     * @return List of all FieldBlocks.
     */
    List<FieldBlock> getAllFieldBlocks();

    /**
     * This method return all related CustomFields of specified FieldBlock.
     *
     * @param fieldBlock FieldBlock where we get related fields.
     *
     * @return List of related CustomFields.
     *
     * @see CustomField
     */
    List<CustomField> getCustomFieldsOfBlock( FieldBlock fieldBlock );

    /**
     * This method provides possibility of adding some CustomField into the FieldBlock on specified position.
     *
     * @param fieldBlock Where we place CustomField.
     * @param customField CustomField which should be added.
     * @param position Position where we want to see this CustomField.
     * @param required This parameter sets property which can specify requirements to fill specified
     * {@link CustomField} for given {@link FieldBlock}.
     *
     * @see TemplateLayout
     */
    void addCustomFieldToTheBlockAfter( FieldBlock fieldBlock, CustomField customField, Integer position, boolean required );

    /**
     * This method helps with removing CustomField from the specified FieldBlock. It changes Field sequences after deleting.
     *
     * @param fieldBlock Where we want to delete CustomField.
     * @param customField Which CustomField should be deleted.
     */
    void removeCustomFieldFromTheFieldBlockLayout( FieldBlock fieldBlock, CustomField customField );

    /**
     * This method help with changing CustomField position in related FieldBlock.
     *
     * @param fieldBlock FieldBlock where we want to change position of the CustomField.
     * @param customField CustomField which should be moved.
     * @param position New position of specified CustomField.
     */
    void changeCustomFieldPosition( FieldBlock fieldBlock, CustomField customField, Integer position );

    /**
     * This method provides position of the latest CustomField in specified FieldBlock.
     *
     * @param fieldBlock Where we count CustomFields.
     *
     * @return Number of the latest position.
     *
     * @see FieldBlockLayout
     */
    Integer getPositionOfLastCustomField( FieldBlock fieldBlock );

    /**
     * Provides functionality which can paste {@link CustomField} to the specified {@link FieldBlock}.
     * This method paste {@link CustomField} into the latest position.
     *
     * @param fieldBlock Where we want to paste a {@link CustomField}.
     * @param customField CustomField which we want to paste.
     * @param required This parameter sets property which can specify requirements to fill specified
     * {@link CustomField} for given {@link FieldBlock}.
     */
    void addCustomFieldToTheFieldBlock( FieldBlock fieldBlock, CustomField customField, boolean required );

    /**
     * The same as #addCustomFieldToTheFieldBlock, but required=False by default.
     *
     * @param fieldBlock Where we want to paste a {@link CustomField}.
     * @param customField CustomField which we want to paste.
     */
    void addCustomFieldToTheFieldBlock( FieldBlock fieldBlock, CustomField customField );

    /**
     * This method returns a list of related templates with specified FieldBlock.
     *
     * @param fieldBlock We try to find relations between Templates and this FieldBlock.
     *
     * @return List of related Templates.
     */
    List<Template> getRelatedTemplates( FieldBlock fieldBlock );

    /**
     * This method try to find Template in the storage by the specified ID.
     *
     * @param id ID of desired Template.
     *
     * @return Founded template object.
     */
    Optional<Template> getTemplate( Long id );

    /**
     * You can create {@link Template} directly via this method.
     *
     * @param template instance of {@link Template} without ID (ID should be null).
     *
     * @return Instance of saved {@link Template} (with assigned ID).
     */
    Template createTemplate( Template template );

    /**
     * This method provides functionality for delete exists Template.
     *
     * @param template Object which should be deleted from the storage.
     */
    void deleteTemplate( Template template );

    /**
     * This method provides functionality for delete exists Template by id.
     *
     * @param id ID of template.
     */
    void deleteTemplate( Long id );

    /**
     * Return all exists Templates.
     *
     * @return List of exists Templates.
     */
    List<Template> getAllTemplates();

    /**
     * This method provide all related FieldBlocks of desired Template.
     *
     * @param template Template for finding relations.
     *
     * @return List of related FieldBlocks.
     */
    List<FieldBlock> getFieldBlocksOfTheTemplate( Template template );

    /**
     * This method possible to paste some FieldBlock on concrete position.
     *
     * @param template Template where we want to insert the FieldBlock.
     * @param fieldBlock FieldBlock which should be inserted.
     * @param position We paste specified FieldBlock after desired position.
     *
     * @see TemplateLayout
     */
    void addFieldBlockToTheTemplateAfter( Template template, FieldBlock fieldBlock, Integer position );

    /**
     * This method provides position of the last CustomField in specified FieldBlock.
     *
     * @param template Where we count FieldBLocks,
     *
     * @return Number of last position.
     *
     * @see TemplateLayout
     */
    Integer getPositionOfLastFieldBlock( Template template );

    /**
     * This method provides position of the latest FieldBlock in specified Template.
     *
     * @param template Where we count FieldBlocks.
     * @param fieldBlock FieldBlock which should be added to layout related with specified Template.
     *
     * @see TemplateLayout
     */
    void addFieldBlockToTheTemplate( Template template, FieldBlock fieldBlock );

    /**
     * This method returns a list of related templates with specified Template.
     *
     * @param pageable Page parameters.
     * @param template We try to find relations between Documents and this Template.
     *
     * @return List of related Documents.
     */
    Page<Document> getRelatedDocuments( Pageable pageable, Template template );

    /**
     * You can use this method when you want to assign document to specified template.
     * If document already assigned to the template, it should be re-assigned to new one.
     *
     * @param document Document which you want to assign.
     * @param template Template which you want to use for specified document.
     */
    void assignTemplateToDocument( Document document, Template template );

    /**
     * Provides list of {@link Template}s related with specified {@link Document}.
     *
     * @param document {@link Document} which should be investigated.
     *
     * @return List of {@link Template}s.
     */
    List<Template> getTemplatesAssignedToDocument( Document document );

    /**
     * Provides list of {@link Document}s what are assigned to specified {@link Template}.
     *
     * @param pageable Page parameters.
     * @param template {@link Template} object to search relations.
     *
     * @return List of {@link Document}s what are assigned to specified {@link Template}.
     */
    Page<Document> getDocumentsWhichUsesTemplate( Pageable pageable, Template template );

    /**
     * This method will remove specified FieldBlock from the specified Template. Positions of rest FieldBlocks
     * will be recalculated.
     *
     * @param template We want to delete from this template.
     * @param fieldBlock We want to remove this FieldBlock from the specified Template.
     */
    void removeFieldBlockFromTemplate( Template template, FieldBlock fieldBlock );

    /**
     * This method help with changing FieldBlock position in related Template.
     *
     * @param template Template where we want to change position of the FieldBlock.
     * @param fieldBlock FieldBlock which should be moved.
     * @param position New position of specified FieldBlock.
     */
    void changeFieldBlockPosition( Template template, FieldBlock fieldBlock, Integer position );

    /**
     * Returns all {@link TemplateLayout} objects for specific {@link Template}.
     *
     * @param template {@link Template} for search relationships.
     *
     * @return List of {@link TemplateLayout} objects.
     */
    List<TemplateLayout> getTemplateLayouts( Template template );

    /**
     * You can use this method when you want to get all FieldBlocks which related with specified CustomFields.
     *
     * @param customField {@link CustomField} for searching relationship.
     *
     * @return List of related FieldBlocks.
     */
    List<FieldBlock> getRelatedFieldBlocksWithCustomField( CustomField customField );

    /**
     * Searches {@link FieldBlock} objects what are related with specified {@link Template}.
     *
     * @param template {@link Template} object for search.
     *
     * @return List of related {@link FieldBlock} objects.
     */
    List<FieldBlock> getRelatedFieldBlocksWithTemplate( Template template );

    /**
     * Searches all {@link CustomField} objects related with specified {@link FieldBlock}.
     *
     * @param fieldBlock {@link FieldBlock} object to search relationships.
     *
     * @return List of related {@link CustomField}.
     */
    List<CustomField> getRelatedCustomFieldsWithFieldBlock( FieldBlock fieldBlock );

    /**
     * This method will check that template with specified ID is exists.
     *
     * @param id ID of {@link Template}.
     *
     * @return TRUE if {@link Template}. with specified ID is exists.
     */
    boolean templateIsExists( Long id );

    /**
     * Just a CRUD operation for updating {@link Template} object in a DB.
     *
     * @param template {@link Template} instance.
     *
     * @return Updated {@link Template}.
     */
    Optional<Template> updateTemplate( Template template );

    /**
     * Returns {@link Template}s by page.
     *
     * @param pageable Page parameters.
     *
     * @return Paged {@link Template} list.
     */
    Page<Template> getAllManagedTemplates( Pageable pageable );

    /**
     * Returns {@link FieldBlock}s by page.
     *
     * @param pageable Page parameters.
     *
     * @return Paged {@link FieldBlock} list.
     */
    Page<FieldBlock> getAllManagedFieldBlocks( Pageable pageable );

    /**
     * Checks that {@link FieldBlock} is exist.
     *
     * @param id ID of {@link FieldBlock} to check.
     *
     * @return TRUE - when found. FALSE if not.
     */
    boolean fieldBlockIsExists( Long id );

    /**
     * Just a CRUD operation for {@link FieldBlock} update.
     *
     * @param fieldBlock {@link FieldBlock} object for update.
     *
     * @return Updated {@link FieldBlock} object.
     */
    Optional<FieldBlock> updateFieldBlock( FieldBlock fieldBlock );

    /**
     * Use this method for detaching {@link Template} from {@link Document} (or {@link Document} from {@link Template}).
     *
     * @param document {@link Document} which should be unassigned from {@link Template}.
     * @param template {@link Template} which should be unassigned from {@link Document}.
     */
    void unassignTemplateFromDocument( Document document, Template template );

    /**
     * Provides all {@link FieldBlock}s where specified {@link CustomField} is used.
     *
     * @param pageable Page parameters.
     * @param customField {@link CustomField} for relationship search.
     *
     * @return Paged list of {@link FieldBlock}s.
     */
    Page<FieldBlock> getFieldBlocksRelatedWithCustomFields( Pageable pageable, CustomField customField );

    /**
     * Provides all {@link Template}s where specified {@link FieldBlock} is used.
     *
     * @param pageable Page parameters.
     * @param fieldBlock {@link FieldBlock} for relationship search.
     *
     * @return Paged list of {@link Template}s.
     */
    Page<Template> getRelatedTemplates( Pageable pageable, FieldBlock fieldBlock );

    /**
     * Provides default (pre-view) {@link CustomField}s for specified {@link Template}.
     *
     * @param template {@link Template} for search.
     *
     * @return List of default {@link CustomField}s.
     */
    List<CustomField> getDefaultCustomFields( Template template );

    /**
     * Adds {@link CustomField} to default {@link CustomField} list of specified {@link Template}.
     *
     * @param template {@link Template} to add new default {@link CustomField}.
     * @param customField {@link CustomField} what should be added.
     */
    void addDefaultCustomField( Template template, CustomField customField );

    /**
     * Removes {@link CustomField} from default {@link CustomField} list of specified {@link Template}.
     *
     * @param template {@link Template} what should be changed.
     * @param customField {@link CustomField} what should be removed.
     */
    void removeDefaultCustomField( Template template, CustomField customField );

    /**
     * Moves default {@link CustomField} in {@link Template} to specified position.
     *
     * @param template {@link Template} what should be changed.
     * @param customField {@link CustomField} what should be moved.
     * @param position New position.
     */
    void changeDefaultCustomFieldPosition( Template template, CustomField customField, Integer position );

}
