/*
 * Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.manager;

import com.laptevaleksei.smartdocumentstorage.core.domain.entity.CustomField;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.Document;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.customfield.AbstractCustomFieldValue;

import java.util.List;

/**
 * Management-layer interface for CustomField value management.
 *
 * @author <a href="mailto:laptevalexander28@gmail.com">Aleksandr Laptev</a> on 7-Nov-2023
 */
public interface CustomFieldValueManager {
    /**
     * This method provides functionality of getting some value of any pair of Document and CustomField.
     *
     * @param document Document where you want to find value.
     * @param customField CustomField which responsible to store value for specified document.
     *
     * @return Returns value of Object type. You should to cast value!
     */
    Object getCustomFieldValue( Document document, CustomField customField);

    /**
     * This method created for usable process of saving value for specified Document-CustomField pair.
     *
     * @param document Document where you want to save value.
     * @param customField CustomField which responsible to store value.
     * @param value Value which you want to save.
     */
    void saveCustomFieldValue( Document document, CustomField customField, Object value );

    /**
     * Try to find all {@link AbstractCustomFieldValue}s by value.
     *
     * @param param Value for search
     *
     * @return List of value objects what a contains specified value.
     */
    List<AbstractCustomFieldValue> getCustomFieldValueObjectsByValue( String param );

    /**
     * Provides list of existing value objects related with specified {@link Document}.
     *
     * @param document {@link Document} as a parameter for search.
     *
     * @return List of found value objects.
     */
    List<AbstractCustomFieldValue> getCustomFieldValueObjectsByDocument( Document document );
}
