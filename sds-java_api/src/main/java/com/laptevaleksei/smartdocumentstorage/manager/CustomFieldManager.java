/*
 * Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.manager;

import com.laptevaleksei.annotations.PublicApi;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.CustomField;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.Document;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.customfield.AbstractCustomFieldValue;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Management-layer interface for CustomField management.
 *
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 01-Nov-2023
 */
@PublicApi
public interface CustomFieldManager extends CustomFieldValueManager, CustomFieldTypeManager{

    /**
     * Creates new customField with pre-defined parameters.
     *
     * @param name Name of new CustomField.
     * @param description Description for CustomField (optional).
     * @param customFieldType Full class name for CustomField renderer.
     *
     * @return Returns created and saved CustomField.
     */
    CustomField createCustomField( String name, String description, String customFieldType );

    /**
     * This method help with finding CustomField by ID in storage.
     *
     * @param id Identification of CustomField.
     *
     * @return Desired CustomField (if exists).
     */
    Optional<CustomField> getCustomField( Long id );

    /**
     * This method provides possibility for updating CustomField.
     *
     * @param customField CustomField object for a saving.
     *
     * @return Saved CustomField.
     */
    CustomField updateCustomField( CustomField customField );

    /**
     * You can delete exists CustomField using this method.
     *
     * @param customField Example of object which should be deleted.
     */
    void deleteCustomField( CustomField customField );

    /**
     * You can get a {@link Pageable} list of {@link CustomField}s.
     *
     * @param pageable Page parameters.
     *
     * @return Page by requested parameters with result.
     */
    Page<CustomField> getAllPagesCustomFieldDTO( Pageable pageable );

    /**
     * This method can check if CustomField is exists by specified id.
     *
     * @param id ID of CustomField which you want to check.
     *
     * @return True if CustomField is exists.
     */
    boolean customFieldIsExists( Long id );

    /**
     * Save value for Document + CustomField pair
     *
     * @param document {@link Document} where we want to store value.
     * @param customField Id of {@link CustomField} which should be used for data storing.
     * @param value Value which we want to store.
     */
    void setCustomFieldValue( Document document, CustomField customField, Object value );

    /**
     * You can get value for {@link Document} and {@link CustomField} pair.
     *
     * @param document {@link Document} for searching value.
     * @param customField {@link CustomField} for searching value.
     *
     * @return Raw value
     */
    Object getCustomFieldValue( Document document, CustomField customField );

    /**
     * Provides value for {@link Document} and {@link CustomField} by their IDs.
     *
     * @param documentId ID of {@link Document} for searching value.
     * @param customFieldId ID of {@link CustomField} for searching value.
     *
     * @return Returns raw value.
     */
    Object getCustomFieldValue( Long documentId, Long customFieldId );

    /**
     * Use this method if you want to set a value for {@link Document} and {@link CustomField} pair.
     *
     * @param documentId Id of {@link Document}.
     * @param customFieldId Id of {@link CustomField}.
     * @param value Value which should be saved.
     */
    void setCustomFieldValue( Long documentId, Long customFieldId, Object value );

    /**
     * Provides a list of all available {@link CustomField}s.
     *
     * @return List of available {@link CustomField}s.
     */
    List<CustomField> getAllCustomFields();

    /**
     * Returns list of {@link CustomField} objects what are related with specified {@link Document}
     *
     * @param document {@link Document} object for search.
     *
     * @return List of @{link CustomField} objects.
     */
    List<CustomField> getAllCustomFieldsRelatedWithDocument( Document document );

    /**
     * Try to find value objects by specified value.
     *
     * @param param Sample value for search.
     *
     * @return List of value objects.
     */
    List<AbstractCustomFieldValue> getMatches( String param );
}