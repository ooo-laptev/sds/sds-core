/*
 * Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.manager;

import com.laptevaleksei.annotations.PublicApi;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.Document;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Management-layer interface for Document management.
 *
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 01-Nov-2023
 */
@PublicApi
public interface DocumentManager {

    /**
     * This method will check that document is exists.
     *
     * @param id ID of document which you want to search.
     *
     * @return TRUE if document is exists.
     */
    boolean documentIsExists( Long id );

    /**
     * Provide functionality for creating new empty document.
     *
     * @return New empty Document. This document could not be saved in the storage currently after method executing.
     */
    Document createNewDocument();

    /**
     * This method help with finding document by ID in storage.
     *
     * @param id Identification of document.
     *
     * @return Desired document (if exists).
     */
    Optional<Document> getDocument( Long id );

    /**
     * This method provides possibility for updating existing document.
     *
     * @param document {@link Document} for a save (Should be with a real, not NULL ID).
     *
     * @return Updated {@link Document}.
     */
    Document updateDocument( Document document );

    /**
     * You can delete exists Document using this method.
     *
     * @param document Example of object which should be deleted.
     */
    void deleteDocument( Document document );

    /**
     * This method provides searching list of documents by Ids set.
     *
     * @param ids List of document identifiers.
     *
     * @return List of documents will be return.
     */
    List<Document> getDocuments( Set<Long> ids );

    /**
     * Provides completed list of all exist {@link Document}s.
     *
     * @return List of {@link Document}s.
     */
    List<Document> getAllDocuments();

    /**
     * This method provides list of all documents but with pagination.
     *
     * @param pageable Pagination settings.
     *
     * @return {@link Page} of {@link Document}
     */
    Page<Document> getAllDocuments( Pageable pageable );
}
