/*
 * Copyright (c) 2019 - 2024, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.manager;

import com.laptevaleksei.annotations.PublicApi;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.Document;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.documentlink.DocumentLink;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.documentlink.DocumentLinkType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Management-layer interface for DocumentLink and DocumentLinkType management.
 *
 * @author <a href="mailto:laptevalexander28@gmail.com">Aleksandr Laptev</a> on 7-Nov-2023
 */
@PublicApi
public interface DocumentLinkManager {

    /**
     * Links two documents.
     *
     * @param sourceDocument Source document.
     * @param targetDocument Target document
     * @param linkType Link type.
     *
     * @return Persisted link object.
     */
    DocumentLink linkDocuments( Document sourceDocument, Document targetDocument, DocumentLinkType linkType );

    /**
     * Provides linked {@link Document}s with both call types (source and target).
     *
     * @param document {@link Document} for relationship collection.
     *
     * @return Links for specified {@link Document}.
     */
    List<DocumentLink> getAllLinkedDocuments( Document document );

    /**
     * Provides linked {@link Document}s where specified document presented as source document.
     *
     * @param sourceDocument {@link Document} for relationship collection.
     *
     * @return Links for specified {@link Document}.
     */
    List<DocumentLink> getLinkedDocumentsForDocumentAsSource(Document sourceDocument);

    /**
     * Provides linked {@link Document}s where specified document presented as target document.
     *
     * @param targetDocument {@link Document} for relationship collection.
     *
     * @return Links for specified {@link Document}.
     */
    List<DocumentLink> getLinkedDocumentsForDocumentAsTarget(Document targetDocument);

    /**
     * Provides page of all available document link types.
     *
     * @param pageable Page parameters.
     *
     * @return Page of Document link types.
     */
    Page<DocumentLinkType> getAllDocumentLinkTypes( Pageable pageable );

    /**
     * Removes link between specified documents.
     *
     * @param sourceDocument Source document.
     * @param targetDocument Target document.
     */
    void removeDocumentLink( Document sourceDocument, Document targetDocument );

    /**
     * Deletes document link by id
     * @param id id of document link to delete
     */
    void deleteDocumentLink( Long id );

    /**
     * Provides all available link types.
     *
     * @return List of available link types.
     */
    List<DocumentLinkType> getLinkTypes();

    /**
     * Creates link type.
     *
     * @param linkTypeName name of link type
     * @param description description of link type
     * @param inwardDescription name of target document in link
     * @param outwardDescription name of source document in link
     * @return created DocumentLinkType
     */
    DocumentLinkType createDocumentLinkType(
            String linkTypeName,
            String description,
            String inwardDescription,
            String outwardDescription
    );

    /**
     * Finds {@link DocumentLinkType} by id
     * @param id id of DocumentLinkType to search
     * @return DocumentLinkType if one with such id was found
     */
    Optional<DocumentLinkType> getDocumentLinkType(Long id);

    /**
     * Updates {@link DocumentLinkType} according to documentLinkType param
     * @param documentLinkType DocumentLinkType data for update
     * @return updated DocumentLinkType
     */
    DocumentLinkType updateDocumentLinkType(DocumentLinkType documentLinkType);

    /**
     * Deletes {@link DocumentLinkType}
     * @param documentLinkType DocumentLinkType to delete
     */
    void deleteDocumentLinkType(DocumentLinkType documentLinkType);

    /**
     * Finds {@link DocumentLink} by id
     * @param linkId id of DocumentLink to search
     * @return DocumentLink if one with such id was found
     */
    Optional<DocumentLink> getDocumentLink(Long linkId);

    /**
     * Provides all available links.
     *
     * @param pageable Page parameters.
     *
     * @return List of available links.
     */
    List<DocumentLink> getAllDocumentLinks(Pageable pageable);
}
