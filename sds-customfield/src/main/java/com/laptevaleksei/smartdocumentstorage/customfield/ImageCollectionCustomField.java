/*
 * Copyright (c) 2019 - 2024, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.customfield;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.CustomField;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.Document;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.customfield.CustomFieldValueImage;
import com.laptevaleksei.smartdocumentstorage.customfieldprocessing.AbstractCustomFieldType;
import com.laptevaleksei.smartdocumentstorage.customfieldprocessing.CustomFieldProcessor;
import com.laptevaleksei.smartdocumentstorage.customfieldprocessing.exception.IllegalCustomFieldTypeValueException;
import com.laptevaleksei.smartdocumentstorage.repository.customfield.CustomFieldValueImageRepository;

import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 26-Dec-2021
 */
@CustomFieldProcessor(
    name = "ImageCustomField",
    availableTypeKeys = { "com.laptevaleksei.smartdocumentstorage.customfield.ImageCollectionCustomField" },
    usableRepository = "com.laptevaleksei.smartdocumentstorage.repository.customfield.CustomFieldValueImageRepository"
)
public class ImageCollectionCustomField extends AbstractCustomFieldType<CustomFieldValueImage, Collection<UUID>> {

    public ImageCollectionCustomField(Document document, CustomField customField, CustomFieldValueImageRepository repository) {
        super(document, customField, repository);
    }

    @Override
    public void setValue(Object value) {
        Optional<CustomFieldValueImage> requestedValueObject = requestValueObject();

        if (value == null && requestedValueObject.isEmpty()) {
            return;
        }

        CustomFieldValueImage valueObject;
        if (requestedValueObject.isPresent()) {
            valueObject = requestedValueObject.get();
        } else {
            valueObject = new CustomFieldValueImage();
            valueObject.setCustomField(customField);
            valueObject.setDocument(document);
        }

        if (value == null) {
            valueObject.setValue(null);
        } else {
            try {
                Gson gson = new Gson();
                UUID[] uuids = gson.fromJson(value.toString(), UUID[].class);
                Collection<UUID> listOfUuids = Arrays.stream(uuids).collect(Collectors.toList());
                valueObject.setValue(listOfUuids);
            } catch (JsonSyntaxException jse) {
                throw new IllegalCustomFieldTypeValueException(
                    "Incompatible type of value. Value should be instance of " + Collection.class.getCanonicalName(),
                    jse
                );
            }
        }
        repository.saveDataObject(valueObject);
    }

    @Override
    public boolean validate(Collection<UUID> value) {
        return true;
    }
}
