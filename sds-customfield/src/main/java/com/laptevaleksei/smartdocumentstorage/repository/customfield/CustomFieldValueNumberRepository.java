/*
 * Copyright (c) 2019 - 2024, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.repository.customfield;

import com.laptevaleksei.smartdocumentstorage.core.domain.entity.CustomField;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.Document;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.customfield.CustomFieldValueNumber;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 17-Mar-2020
 */
@Repository
public interface CustomFieldValueNumberRepository
    extends JpaRepository<CustomFieldValueNumber, Long>, CustomFieldValueRepository<Number, CustomFieldValueNumber> {
    @Override
    default Optional<CustomFieldValueNumber> getValueObject(Document document, CustomField customField) {
        return findCustomFieldValueNumberByDocumentAndCustomField(document, customField);
    }

    @Override
    default void saveDataObject(CustomFieldValueNumber typeObject) {
        save(typeObject);
    }

    /**
     * This method provides value by specified custom field and document.
     *
     * @param document document related with value.
     * @param customField Related custom field.
     *
     * @return Returns Optional of CustomFieldValue.
     */
    Optional<CustomFieldValueNumber> findCustomFieldValueNumberByDocumentAndCustomField(Document document, CustomField customField);

    @Override
    default List<CustomFieldValueNumber> findByValue(Number value) {
        return new ArrayList<>(findAllByValue(value));
    }

    Collection<CustomFieldValueNumber> findAllByValue(Number value);

    @Override
    @NonNull
    <S extends CustomFieldValueNumber> S save(@NonNull S entity);

    @Override
    List<CustomFieldValueNumber> findByDocument(Document document);
}
