/*
 * Copyright (c) 2019 - 2024, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.customfieldprocessing;

import com.laptevaleksei.smartdocumentstorage.core.domain.entity.CustomField;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation should be used for annotating a classes which should handle specific type of {@link CustomField}.
 * <br>
 * Use it like this:
 * <pre>
 *     {@code
 *          //~~~~~~~~~~~~~~~~~~~~~~~~
 *          @CustomFieldProcessor( name = "CheckboxCustomField",
 *                  availableTypeKeys = {
 *                          "com.laptevaleksei.smartdocumentstorage.customfield.CheckboxCustomField"
 *                  }, usableRepository = "com.laptevaleksei.smartdocumentstorage.repository.customfield.CustomFieldValueBooleanRepository" )
 *          public class CheckboxCustomField
 *              extends AbstractCustomFieldType<CustomFieldValueBoolean, Boolean> {
 *          //........................
 *          }
 *          //~~~~~~~~~~~~~~~~~~~~~~~~
 *      }
 * </pre>
 *
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 29-Jul-2021
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface CustomFieldProcessor {
    /**
     * This is a name for specific custom field type.
     *
     * @return String name of the type.
     */
    String name() default "";

    /**
     * The value contains a list or one item which means a type key which should be handled by this type class.
     *
     * @return String array of type keys.
     */
    String[] availableTypeKeys() default {};

    /**
     * The value contains a full qualified name of used repository for operations on data.
     *
     * @return Full qualified name of repository.
     */
    String usableRepository();
}
