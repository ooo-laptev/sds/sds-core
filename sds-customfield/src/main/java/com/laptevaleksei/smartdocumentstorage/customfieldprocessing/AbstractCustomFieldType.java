/*
 * Copyright (c) 2019 - 2024, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.customfieldprocessing;

import com.laptevaleksei.smartdocumentstorage.core.domain.entity.CustomField;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.Document;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.customfield.AbstractCustomFieldValue;
import com.laptevaleksei.smartdocumentstorage.repository.customfield.CustomFieldValueRepository;
import jakarta.annotation.Nullable;

import java.util.Optional;

/**
 * This class represents some specific type of {@link CustomField}.
 *
 * @param <T> Type object
 * @param <R> Raw value type
 *
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 29-Jul-2021
 */
@SuppressWarnings({ "unchecked", "rawtypes" })
public abstract class AbstractCustomFieldType<T extends AbstractCustomFieldValue<R>, R> {

    protected final CustomFieldValueRepository<R, T> repository;

    protected final CustomField customField;
    protected final Document document;

    protected <S extends CustomFieldValueRepository>AbstractCustomFieldType(Document document, CustomField customField, S repository) {
        this.customField = customField;
        this.document = document;
        this.repository = repository;
    }

    /**
     * Provides the raw value of current custom field type object.
     *
     * @return Value of type {@link R} for current custom field type object.
     */
    public R getValue() {
        return requestValueObject().map(AbstractCustomFieldValue::getValue).orElse(null);
    }

    /**
     * Sets a raw value for current custom field type object.
     *
     * @param value The raw value what should be used.
     */
    public abstract void setValue(@Nullable Object value);

    /**
     * Provides a CustomField value object used for current CustomField type.
     *
     * @return Typed instance of {@link AbstractCustomFieldValue} related to specific CustomField type.
     */
    public AbstractCustomFieldValue<R> getValueObject() {
        return requestValueObject().orElse(null);
    }

    /**
     * Validates value for current CustomFieldType before saving.
     *
     * @param value Value what should be validated.
     *
     * @return Validation result. {@link Boolean#TRUE} if value is valid.
     */
    public abstract boolean validate(R value);

    protected Optional<T> requestValueObject() {
        return repository.getValueObject(document, customField);
    }

    /**
     * Saves CustomField value object in the persistence level.
     *
     * @param customFieldValue Value object what should be saved.
     */
    public void saveCustomFieldValue(T customFieldValue) {
        repository.saveDataObject(customFieldValue);
    }
}
