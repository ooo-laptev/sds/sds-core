/*
 * Copyright (c) 2019 - 2024, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.customfieldprocessing.exception;

/**
 * This Exception type created for showing that incorrect value used for specific CustomField type.
 *
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 31-Jul-2021
 */
public class IllegalCustomFieldTypeValueException extends IllegalArgumentException {

    private static final long serialVersionUID = -1854527046316113683L;

    public IllegalCustomFieldTypeValueException() {
        // Just empty constructor
    }

    /**
     * Constructor with {@link String} parameter for adding a text message into exception.
     *
     * @param s {@link String} message what should be added into the exception.
     */
    public IllegalCustomFieldTypeValueException(String s) {
        super(s);
    }

    public IllegalCustomFieldTypeValueException(String message, Throwable cause) {
        super(message, cause);
    }

    public IllegalCustomFieldTypeValueException(Throwable cause) {
        super(cause);
    }
}
