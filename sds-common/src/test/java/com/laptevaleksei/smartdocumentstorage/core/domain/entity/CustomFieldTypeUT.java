/*
 * Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.domain.entity;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


/**
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 28-Jul-2020
 */
class CustomFieldTypeUT {


    @Test
    void testSetterGetterOfId() {
        Long id = 1L;
        CustomField cf = new CustomField();
        cf.setId( id );
        assertEquals( id, cf.getId() );
    }

    @Test
    void testSetterGetterOfName() {
        String name = "Name";
        CustomField cf = new CustomField();
        cf.setName( name );
        assertEquals( name, cf.getName() );
    }

    @Test
    void testSetterGetterOfDescription() {
        String description = "Description";
        CustomField cf = new CustomField();
        cf.setDescription( description );
        assertEquals( description, cf.getDescription() );
    }

    @Test
    void testSetterGetterOfCustomFieldType() {
        String customFieldType = "Test.CustomFieldType";
        CustomField cf = new CustomField();
        cf.setCustomFieldType( customFieldType );
        assertEquals( customFieldType, cf.getCustomFieldType() );
    }

    @Test
    void testEquals() {
        CustomField firstCf = createCustomFieldForTest();
        CustomField secondCf = copyCustomField( firstCf );

//        assertEquals( firstCf, secondCf );

        secondCf.setId( null );
        assertNotEquals( firstCf, secondCf );

        secondCf = copyCustomField( firstCf );
        secondCf.setName( null );
        assertNotEquals( firstCf, secondCf );

        secondCf = copyCustomField( firstCf );
        secondCf.setDescription( null );
        assertNotEquals( firstCf, secondCf );

        secondCf = copyCustomField( firstCf );
        secondCf.setCustomFieldType( null );
        assertNotEquals( firstCf, secondCf );

        assertNotNull( firstCf );

        assertNotEquals( firstCf, new Object() );
    }

    /**
     * Just creates test CustomField object with filled all properties.
     *
     * @return Ready for test CustomField object.
     */
    private CustomField createCustomFieldForTest() {
        CustomField customField = new CustomField();
        customField.setId( 1L );
        customField.setName( "Test custom field " );
        customField.setDescription( "CustomField for a test" );
        customField.setCustomFieldType( "Test.type" );
        return customField;
    }

    /**
     * Creates a copy of CustomField
     *
     * @param customField Object which should be copied.
     *
     * @return New copy.
     */
    private CustomField copyCustomField( CustomField customField ) {
        CustomField customFieldCopy = new CustomField();
        customFieldCopy.setId( customField.getId() );
        customFieldCopy.setName( customField.getName() );
        customFieldCopy.setDescription( customField.getDescription() );
        customFieldCopy.setCustomFieldType( customField.getCustomFieldType() );
        return customFieldCopy;
    }
}
