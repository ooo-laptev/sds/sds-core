/*
 * Copyright (c) 2019 - 2024, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.web.dto.template;

import com.laptevaleksei.smartdocumentstorage.core.domain.entity.template.FieldBlock;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.template.Template;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.template.TemplateLayout;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


/**
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 13-Aug-2020
 */
class TemplateLayoutDTOTest {

    @Test
    void testSetterGetterOfId() {
        Long id = 1L;
        TemplateLayoutDTO templateLayoutDTO = TemplateLayoutDTO.builder().setId( id ).build();

        assertEquals( id, templateLayoutDTO.getId() );
    }

    @Test
    void testSetterGetterOfSequence() {
        Integer sequence = 1;
        TemplateLayoutDTO templateLayoutDTO = TemplateLayoutDTO.builder().setSequence( sequence ).build();

        assertEquals( sequence, templateLayoutDTO.getSequence() );
    }

    @Test
    void testSetterGetterOfTemplateId() {
        Long templateId = 1L;
        TemplateLayoutDTO templateLayoutDTO = TemplateLayoutDTO.builder().setTemplateId( templateId ).build();

        assertEquals( templateId, templateLayoutDTO.getTemplateId() );
    }

    @Test
    void testSetterGetterOfFieldblockId() {
        Long fieldblockId = 1L;
        TemplateLayoutDTO templateLayoutDTO = TemplateLayoutDTO.builder().setFieldBlockId( fieldblockId ).build();

        assertEquals( fieldblockId, templateLayoutDTO.getFieldBlockId() );
    }

    @Test
    void testEquals() {
        TemplateLayoutDTO firstTemplateLayoutDTO = createTemplateLayoutDTOForTest();
        TemplateLayoutDTO secondTemplateLayoutDTO = copyTemplateLayoutDTO( firstTemplateLayoutDTO );

        assertEquals( firstTemplateLayoutDTO, secondTemplateLayoutDTO );

        secondTemplateLayoutDTO.setId( null );
        assertNotEquals( firstTemplateLayoutDTO, secondTemplateLayoutDTO );

        secondTemplateLayoutDTO = copyTemplateLayoutDTO( firstTemplateLayoutDTO );
        secondTemplateLayoutDTO.setSequence( null );
        assertNotEquals( firstTemplateLayoutDTO, secondTemplateLayoutDTO );

        secondTemplateLayoutDTO = copyTemplateLayoutDTO( firstTemplateLayoutDTO );
        secondTemplateLayoutDTO.setTemplateId( null );
        assertNotEquals( firstTemplateLayoutDTO, secondTemplateLayoutDTO );

        secondTemplateLayoutDTO = copyTemplateLayoutDTO( firstTemplateLayoutDTO );
        secondTemplateLayoutDTO.setFieldBlockId( null );
        assertNotEquals( firstTemplateLayoutDTO, secondTemplateLayoutDTO );

        assertNotNull( firstTemplateLayoutDTO );

        assertNotEquals( firstTemplateLayoutDTO, new Object() );
    }

    @Test
    void testCreateRealObject() {
        TemplateLayoutDTO templateLayoutDTO = createTemplateLayoutDTOForTest();
        TemplateLayout templateLayout = templateLayoutDTO.createRealObject();

        assertEquals( templateLayoutDTO.getId(), templateLayout.getId() );
        assertEquals( templateLayoutDTO.getSequence(), templateLayout.getSequence() );
        assertEquals( templateLayoutDTO.getTemplateId(), templateLayout.getTemplate().getId() );
        assertEquals( templateLayoutDTO.getFieldBlockId(), templateLayout.getFieldBlock().getId() );
    }

    @Test
    void testCreateDtoFromRealObject() {
        TemplateLayout templateLayout = new TemplateLayout();
        templateLayout.setId( 1L );
        templateLayout.setSequence( 1 );

        Template template = new Template();
        template.setId( 1L );
        templateLayout.setTemplate( template );

        FieldBlock fieldBlock = new FieldBlock();
        fieldBlock.setId( 1L );
        templateLayout.setFieldBlock( fieldBlock );

        TemplateLayoutDTO templateLayoutDTO = TemplateLayoutDTO.createDtoFromRealObject( templateLayout );

        assertEquals( templateLayout.getId(), templateLayoutDTO.getId() );
        assertEquals( templateLayout.getSequence(), templateLayoutDTO.getSequence() );
        assertEquals( templateLayout.getTemplate().getId(), templateLayoutDTO.getTemplateId() );
        assertEquals( templateLayout.getFieldBlock().getId(), templateLayoutDTO.getFieldBlockId() );
    }

    /**
     * Just creates test FieldBlock object with filled all properties.
     *
     * @return Ready for test TemplateLayoutDTO object.
     */
    private TemplateLayoutDTO createTemplateLayoutDTOForTest() {
        return new TemplateLayoutDTO( 1L, 1, 1L, 1L );
    }

    /**
     * Creates a copy of TemplateLayoutDTO
     *
     * @param templateLayoutDTO Object which should be copied.
     *
     * @return New copy.
     */
    private TemplateLayoutDTO copyTemplateLayoutDTO( TemplateLayoutDTO templateLayoutDTO ) {
        return TemplateLayoutDTO.builder()
                .setId( templateLayoutDTO.getId() )
                .setSequence( templateLayoutDTO.getSequence() )
                .setTemplateId( templateLayoutDTO.getTemplateId() )
                .setFieldBlockId( templateLayoutDTO.getFieldBlockId() )
                .build();
    }
}
