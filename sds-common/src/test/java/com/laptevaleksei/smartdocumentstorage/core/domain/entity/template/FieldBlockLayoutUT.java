/*
 * Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.domain.entity.template;


import com.laptevaleksei.smartdocumentstorage.core.domain.entity.CustomField;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


/**
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 29-Jul-2020
 */
class FieldBlockLayoutUT {


    @Test
    void testSetterGetterOfId() {
        Long id = 1L;
        FieldBlockLayout fieldBlockLayout = new FieldBlockLayout();
        fieldBlockLayout.setId( id );
        assertEquals( id, fieldBlockLayout.getId() );
    }

    @Test
    void testSetterGetterOfCustomField() {
        CustomField customField = new CustomField();
        FieldBlockLayout fieldBlockLayout = new FieldBlockLayout();
        fieldBlockLayout.setCustomField( customField );
        assertEquals( customField, fieldBlockLayout.getCustomField() );
    }

    @Test
    void testSetterGetterOfFieldBlock() {
        FieldBlock fieldBlock = new FieldBlock();
        FieldBlockLayout fieldBlockLayout = new FieldBlockLayout();
        fieldBlockLayout.setFieldBlock( fieldBlock );
        assertEquals( fieldBlock, fieldBlockLayout.getFieldBlock() );
    }

    @Test
    void testSetterGetterOfSequence() {
        Integer sequence = 1;
        FieldBlockLayout fieldBlockLayout = new FieldBlockLayout();
        fieldBlockLayout.setSequence( sequence );
        assertEquals( sequence, fieldBlockLayout.getSequence() );
    }

    @Test
    void testSetterGetterOfRequired() {
        FieldBlockLayout fieldBlockLayout = new FieldBlockLayout();
        fieldBlockLayout.setRequired( true );
        assertTrue( fieldBlockLayout.isRequired() );
    }

    @Test
    void testEquals() {
        FieldBlockLayout firstFieldBlockLayout = createFieldBlockLayoutForTest();
        FieldBlockLayout secondFieldBlockLayout = copyFieldBlockLayout( firstFieldBlockLayout );

//        assertEquals( firstFieldBlockLayout, secondFieldBlockLayout );

        secondFieldBlockLayout.setId( null );
        assertNotEquals( firstFieldBlockLayout, secondFieldBlockLayout );

        secondFieldBlockLayout = copyFieldBlockLayout( firstFieldBlockLayout );
        secondFieldBlockLayout.setCustomField( null );
        assertNotEquals( firstFieldBlockLayout, secondFieldBlockLayout );

        secondFieldBlockLayout = copyFieldBlockLayout( firstFieldBlockLayout );
        secondFieldBlockLayout.setFieldBlock( null );
        assertNotEquals( firstFieldBlockLayout, secondFieldBlockLayout );

        secondFieldBlockLayout = copyFieldBlockLayout( firstFieldBlockLayout );
        secondFieldBlockLayout.setSequence( null );
        assertNotEquals( firstFieldBlockLayout, secondFieldBlockLayout );

        secondFieldBlockLayout = copyFieldBlockLayout( firstFieldBlockLayout );
        secondFieldBlockLayout.setRequired( false );
        assertNotEquals( firstFieldBlockLayout, secondFieldBlockLayout );

        assertNotNull( firstFieldBlockLayout );

        assertNotEquals( firstFieldBlockLayout, new Object() );
    }

    /**
     * Just creates test FieldBlockLayout object with filled all properties.
     *
     * @return Ready for test FieldBlockLayout object.
     */
    private FieldBlockLayout createFieldBlockLayoutForTest() {
        FieldBlockLayout fieldBlockLayout = new FieldBlockLayout();
        fieldBlockLayout.setId( 1L );
        fieldBlockLayout.setCustomField( new CustomField() );
        fieldBlockLayout.setFieldBlock( new FieldBlock() );
        fieldBlockLayout.setSequence( 1 );
        fieldBlockLayout.setRequired( true );
        return fieldBlockLayout;
    }

    /**
     * Creates a copy of FieldBlockLayout
     *
     * @param fieldBlockLayout Object which should be copied.
     *
     * @return New copy.
     */
    private FieldBlockLayout copyFieldBlockLayout( FieldBlockLayout fieldBlockLayout ) {
        FieldBlockLayout fieldBlockLayoutCopy = new FieldBlockLayout();
        fieldBlockLayoutCopy.setId( fieldBlockLayout.getId() );
        fieldBlockLayoutCopy.setCustomField( fieldBlockLayout.getCustomField() );
        fieldBlockLayoutCopy.setFieldBlock( fieldBlockLayout.getFieldBlock() );
        fieldBlockLayoutCopy.setSequence( fieldBlockLayout.getSequence() );
        fieldBlockLayoutCopy.setRequired( fieldBlockLayout.isRequired() );
        return fieldBlockLayoutCopy;
    }

}
