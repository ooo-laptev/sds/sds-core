/*
 * Copyright (c) 2019 - 2024, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.web.dto.template;

import com.laptevaleksei.smartdocumentstorage.core.domain.entity.CustomField;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.template.FieldBlock;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.template.FieldBlockLayout;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


/**
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 13-Aug-2020
 */
class FieldBlockLayoutDTOTest {

    @Test
    void testSetterGetterOfId() {
        Long id = 1L;
        FieldBlockLayoutDTO fieldBlockLayoutDTO = FieldBlockLayoutDTO.builder().setId( id ).build();

        assertEquals( id, fieldBlockLayoutDTO.getId() );
    }

    @Test
    void testSetterGetterOfSequence() {
        Integer sequence = 1;
        FieldBlockLayoutDTO fieldBlockLayoutDTO = FieldBlockLayoutDTO.builder().setSequence( sequence ).build();

        assertEquals( sequence, fieldBlockLayoutDTO.getSequence() );
    }

    @Test
    void testSetterGetterOfFieldBlockId() {
        Long fieldBlockId = 1L;
        FieldBlockLayoutDTO fieldBlockLayoutDTO = FieldBlockLayoutDTO.builder().setFieldBlockId( fieldBlockId ).build();

        assertEquals( fieldBlockId, fieldBlockLayoutDTO.getFieldBlockId() );
    }

    @Test
    void testSetterGetterOfCustomFieldId() {
        Long customFieldId = 1L;
        FieldBlockLayoutDTO fieldBlockLayoutDTO = FieldBlockLayoutDTO.builder().setCustomFieldId( customFieldId ).build();

        assertEquals( customFieldId, fieldBlockLayoutDTO.getCustomFieldId() );
    }

    @Test
    void testEquals() {
        FieldBlockLayoutDTO firstFieldBlockLayoutDTO = createFieldBlockLayoutDTOForTest();
        FieldBlockLayoutDTO secondFieldBlockLayoutDTO = copyFieldBlockLayoutDTO( firstFieldBlockLayoutDTO );

        assertEquals( firstFieldBlockLayoutDTO, secondFieldBlockLayoutDTO );

        secondFieldBlockLayoutDTO.setId( null );
        assertNotEquals( firstFieldBlockLayoutDTO, secondFieldBlockLayoutDTO );

        secondFieldBlockLayoutDTO = copyFieldBlockLayoutDTO( firstFieldBlockLayoutDTO );
        secondFieldBlockLayoutDTO.setSequence( null );
        assertNotEquals( firstFieldBlockLayoutDTO, secondFieldBlockLayoutDTO );

        secondFieldBlockLayoutDTO = copyFieldBlockLayoutDTO( firstFieldBlockLayoutDTO );
        secondFieldBlockLayoutDTO.setFieldBlockId( null );
        assertNotEquals( firstFieldBlockLayoutDTO, secondFieldBlockLayoutDTO );

        secondFieldBlockLayoutDTO = copyFieldBlockLayoutDTO( firstFieldBlockLayoutDTO );
        secondFieldBlockLayoutDTO.setCustomFieldId( null );
        assertNotEquals( firstFieldBlockLayoutDTO, secondFieldBlockLayoutDTO );

        assertNotNull( firstFieldBlockLayoutDTO );

        assertNotEquals( firstFieldBlockLayoutDTO, new Object() );
    }

    @Test
    void testCreateRealObject() {
        FieldBlockLayoutDTO fieldBlockLayoutDTO = createFieldBlockLayoutDTOForTest();
        FieldBlockLayout fieldBlockLayout = fieldBlockLayoutDTO.createRealObject();

        assertEquals( fieldBlockLayoutDTO.getId(), fieldBlockLayout.getId() );
        assertEquals( fieldBlockLayoutDTO.getSequence(), fieldBlockLayout.getSequence() );
        assertEquals( fieldBlockLayoutDTO.getFieldBlockId(), fieldBlockLayout.getFieldBlock().getId() );
        assertEquals( fieldBlockLayoutDTO.getCustomFieldId(), fieldBlockLayout.getCustomField().getId() );


    }

    @Test
    void testCreateDtoFromRealObject() {
        FieldBlockLayout fieldBlockLayout = new FieldBlockLayout();
        fieldBlockLayout.setId( 1L );
        fieldBlockLayout.setSequence( 1 );

        FieldBlock fieldBlock = new FieldBlock();
        fieldBlock.setId( 1L );
        fieldBlockLayout.setFieldBlock( fieldBlock );

        CustomField customField = new CustomField();
        customField.setId( 1L );
        fieldBlockLayout.setCustomField( customField );

        FieldBlockLayoutDTO fieldBlockLayoutDTO = FieldBlockLayoutDTO.createDtoFromRealObject( fieldBlockLayout );

        assertEquals( fieldBlockLayout.getId(), fieldBlockLayoutDTO.getId() );
        assertEquals( fieldBlockLayout.getSequence(), fieldBlockLayoutDTO.getSequence() );
        assertEquals( fieldBlockLayout.getFieldBlock().getId(), fieldBlockLayoutDTO.getFieldBlockId() );
        assertEquals( fieldBlockLayout.getCustomField().getId(), fieldBlockLayoutDTO.getCustomFieldId() );
    }

    /**
     * Just creates test Template object with filled all properties.
     *
     * @return Ready for test Template object.
     */
    private FieldBlockLayoutDTO createFieldBlockLayoutDTOForTest() {
        return new FieldBlockLayoutDTO( 1L, 1, 1L, 1L, false );
    }

    /**
     * Creates a copy of FieldBlockLayoutDTO
     *
     * @param fieldBlockLayoutDTO Object which should be copied.
     *
     * @return New copy.
     */
    private FieldBlockLayoutDTO copyFieldBlockLayoutDTO( FieldBlockLayoutDTO fieldBlockLayoutDTO ) {
        return FieldBlockLayoutDTO.builder()
                .setId( fieldBlockLayoutDTO.getId() )
                .setSequence( fieldBlockLayoutDTO.getSequence() )
                .setFieldBlockId( fieldBlockLayoutDTO.getFieldBlockId() )
                .setCustomFieldId( fieldBlockLayoutDTO.getCustomFieldId() )
                .build();
    }
}
