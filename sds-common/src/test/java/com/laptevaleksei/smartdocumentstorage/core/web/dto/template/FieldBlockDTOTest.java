/*
 * Copyright (c) 2019 - 2024, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.web.dto.template;

import com.laptevaleksei.smartdocumentstorage.core.domain.entity.template.FieldBlock;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


/**
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 13-Aug-2020
 */
class FieldBlockDTOTest {

    @Test
    void testSetterGetterOfId() {
        Long id = 1L;
        FieldBlockDTO fieldBlockDTO = FieldBlockDTO.builder().setId( id ).build();
        assertEquals( id, fieldBlockDTO.getId() );
    }

    @Test
    void testSetterGetterOfName() {
        String name = "FieldBlock name";
        FieldBlockDTO fieldBlockDTO = FieldBlockDTO.builder().setName( name ).build();
        assertEquals( name, fieldBlockDTO.getName() );
    }

    @Test
    void testSetterGetterOfDescription() {
        String description = "FieldBlock description";
        FieldBlockDTO fieldBlockDTO = FieldBlockDTO.builder().setDescription( description ).build();
        assertEquals( description, fieldBlockDTO.getDescription() );
    }

    @Test
    void testEquals() {
        FieldBlockDTO firstFieldBlockDTO = createFieldBlockDTOForTest();
        FieldBlockDTO secondFieldBlockDTO = copyFieldBlockDTO( firstFieldBlockDTO );

        assertEquals( firstFieldBlockDTO, secondFieldBlockDTO );

        secondFieldBlockDTO.setId( null );
        assertNotEquals( firstFieldBlockDTO, secondFieldBlockDTO );

        secondFieldBlockDTO = copyFieldBlockDTO( firstFieldBlockDTO );
        secondFieldBlockDTO.setName( null );
        assertNotEquals( firstFieldBlockDTO, secondFieldBlockDTO );

        secondFieldBlockDTO = copyFieldBlockDTO( firstFieldBlockDTO );
        secondFieldBlockDTO.setDescription( null );
        assertNotEquals( firstFieldBlockDTO, secondFieldBlockDTO );

        assertNotNull( firstFieldBlockDTO );

        assertNotEquals( firstFieldBlockDTO, new Object() );
    }

    @Test
    void testCreateRealObject() {
        FieldBlockDTO fieldBlockDTO = createFieldBlockDTOForTest();
        FieldBlock fieldBlock = fieldBlockDTO.createRealObject();

        assertEquals( fieldBlockDTO.getId(), fieldBlock.getId() );
        assertEquals( fieldBlockDTO.getName(), fieldBlock.getName() );
        assertEquals( fieldBlockDTO.getDescription(), fieldBlock.getDescription() );
    }

    @Test
    void testCreateDtoFromRealObject() {
        FieldBlock fieldBlock = new FieldBlock();
        fieldBlock.setId( 1L );
        fieldBlock.setName( "Field block" );
        fieldBlock.setDescription( "Field block short description" );

        FieldBlockDTO fieldBlockDTO = FieldBlockDTO.createDtoFromRealObject( fieldBlock );

        assertEquals( fieldBlock.getId(), fieldBlockDTO.getId() );
        assertEquals( fieldBlock.getName(), fieldBlockDTO.getName() );
        assertEquals( fieldBlock.getDescription(), fieldBlockDTO.getDescription() );
    }

    /**
     * Just creates test FieldBlockDTO object with filled all properties.
     *
     * @return Ready for test FieldBlock object.
     */
    private FieldBlockDTO createFieldBlockDTOForTest() {
        return new FieldBlockDTO( 1L, "New fieldBlock DTO", "Just a test dto" );
    }

    /**
     * Creates a copy of FieldBlockDTO
     *
     * @param fieldBlockDTO Object which should be copied.
     *
     * @return New copy.
     */
    private FieldBlockDTO copyFieldBlockDTO( FieldBlockDTO fieldBlockDTO ) {
        return FieldBlockDTO.builder()
                .setId( fieldBlockDTO.getId() )
                .setName( fieldBlockDTO.getName() )
                .setDescription( fieldBlockDTO.getDescription() )
                .build();
    }
}
