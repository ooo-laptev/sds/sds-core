/*
 * Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.domain.entity.customfield;


import com.laptevaleksei.smartdocumentstorage.core.domain.entity.CustomField;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.Document;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;


class CustomFieldTypeValueStringUT {


    @Test
    void testSetterOfId() {
        Long id = 1L;
        CustomFieldValueString customFieldValue = new CustomFieldValueString();
        customFieldValue.setId( id );

        assertEquals( id, customFieldValue.getId() );
    }

    @Test
    void testSetterGetterOfValue() {
        String value = "123.456D";

        CustomFieldValueString customFieldValue = new CustomFieldValueString();
        customFieldValue.setValue( value );

        assertEquals( value, customFieldValue.getValue() );
    }


    @Test
    void testEquals() {
        CustomFieldValueString firstCustomFieldValue = createCustomFieldValueStringForTest();
        CustomFieldValueString secondCustomFieldValue = copyCustomFieldValue( firstCustomFieldValue );

//        assertEquals( firstCustomFieldValue, secondCustomFieldValue );

        secondCustomFieldValue.setId( null );
        assertNotEquals( firstCustomFieldValue, secondCustomFieldValue );

        secondCustomFieldValue = copyCustomFieldValue( firstCustomFieldValue );
        secondCustomFieldValue.setValue( null );
        assertNotEquals( firstCustomFieldValue, secondCustomFieldValue );

        secondCustomFieldValue = copyCustomFieldValue( firstCustomFieldValue );
        secondCustomFieldValue.setCustomField( null );
        assertNotEquals( firstCustomFieldValue, secondCustomFieldValue );

        secondCustomFieldValue = copyCustomFieldValue( firstCustomFieldValue );
        secondCustomFieldValue.setDocument( null );
        assertNotEquals( firstCustomFieldValue, secondCustomFieldValue );

        secondCustomFieldValue = copyCustomFieldValue( firstCustomFieldValue );
        secondCustomFieldValue.setCreated( null );
        assertNotEquals( firstCustomFieldValue, secondCustomFieldValue );

        secondCustomFieldValue = copyCustomFieldValue( firstCustomFieldValue );
        secondCustomFieldValue.setUpdated( null );
        assertNotEquals( firstCustomFieldValue, secondCustomFieldValue );

        assertNotNull( firstCustomFieldValue );

        assertNotEquals( firstCustomFieldValue, new Object() );
    }

    private CustomFieldValueString createCustomFieldValueStringForTest() {
        Date date = new Date();
        String value = "123.456D";

        CustomFieldValueString customFieldValueString = new CustomFieldValueString();
        customFieldValueString.setId( 1L );
        customFieldValueString.setValue( value );
        customFieldValueString.setCustomField( new CustomField() );
        customFieldValueString.setDocument( new Document() );
        customFieldValueString.setCreated( date );
        customFieldValueString.setUpdated( date );

        return customFieldValueString;
    }

    private CustomFieldValueString copyCustomFieldValue( CustomFieldValueString customFieldValue ) {
        CustomFieldValueString customFieldValueString = new CustomFieldValueString();
        customFieldValueString.setId( customFieldValue.getId() );
        customFieldValueString.setValue( customFieldValue.getValue() );
        customFieldValueString.setCustomField( customFieldValue.getCustomField() );
        customFieldValueString.setDocument( customFieldValue.getDocument() );
        customFieldValueString.setCreated( customFieldValue.getCreated() );
        customFieldValueString.setUpdated( customFieldValue.getUpdated() );
        return customFieldValueString;
    }
}
