/*
 * Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.domain.entity.documentlink;

import com.laptevaleksei.smartdocumentstorage.core.domain.entity.Document;
import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Unit-test for class {@link DocumentLink}.
 *
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 24-Aug-2021
 */
class DocumentLinkUT {

    @Test
    void testSetterGetterOfId() {
        Long id = 1L;
        DocumentLink documentLink = new DocumentLink();
        documentLink.setId( id );
        assertEquals( id, documentLink.getId() );
    }

    @Test
    void testSetterGetterOfLinkType() {
        DocumentLinkType linkType = createLinkTypeForTest();

        DocumentLink documentLink = new DocumentLink();
        documentLink.setLinkType( linkType );
        assertEquals( linkType, documentLink.getLinkType() );
    }

    @Test
    void testSetterGetterOfSourceDocument() {
        Document document = creatDocumentForTest();

        DocumentLink documentLink = new DocumentLink();
        documentLink.setSourceDocument( document );
        assertEquals( document, documentLink.getSourceDocument() );
    }

    @Test
    void testSetterGetterOfTargetDocument() {
        Document document = creatDocumentForTest();

        DocumentLink documentLink = new DocumentLink();
        documentLink.setTargetDocument( document );
        assertEquals( document, documentLink.getTargetDocument() );
    }


    @Test
    void testEquals() {
        DocumentLink firstDocumentLink = createDocumentLinkForTest();
        DocumentLink secondDocumentLink = copyDocumentLink( firstDocumentLink );

        assertEquals( firstDocumentLink, secondDocumentLink );

        secondDocumentLink.setId( null );
        assertNotEquals( firstDocumentLink, secondDocumentLink );

        secondDocumentLink = copyDocumentLink( firstDocumentLink );
        secondDocumentLink.setLinkType( null );
        assertNotEquals( firstDocumentLink, secondDocumentLink );

        secondDocumentLink = copyDocumentLink( firstDocumentLink );
        secondDocumentLink.setSourceDocument( null );
        assertNotEquals( firstDocumentLink, secondDocumentLink );

        secondDocumentLink = copyDocumentLink( firstDocumentLink );
        secondDocumentLink.setTargetDocument( null );
        assertNotEquals( firstDocumentLink, secondDocumentLink );

        secondDocumentLink = new DocumentLink();
        assertNotEquals( firstDocumentLink, secondDocumentLink );

        assertNotNull( firstDocumentLink );

        assertNotEquals( firstDocumentLink, new Object() );
    }

    private DocumentLink createDocumentLinkForTest() {
        DocumentLink documentLink = new DocumentLink();

        documentLink.setId( 1L );
        documentLink.setLinkType( createLinkTypeForTest() );
        documentLink.setSourceDocument( creatDocumentForTest() );
        documentLink.setTargetDocument( creatDocumentForTest() );

        return documentLink;
    }

    private DocumentLink copyDocumentLink( DocumentLink documentLinkForCopy ) {
        DocumentLink documentLink = new DocumentLink();
        documentLink.setId( documentLinkForCopy.getId() );
        documentLink.setLinkType( documentLinkForCopy.getLinkType() );
        documentLink.setSourceDocument( documentLinkForCopy.getSourceDocument() );
        documentLink.setTargetDocument( documentLinkForCopy.getTargetDocument() );

        return documentLink;
    }

    private DocumentLinkType createLinkTypeForTest() {
        DocumentLinkType documentLinkType = new DocumentLinkType();
        documentLinkType.setId( new Random().nextLong() );

        return documentLinkType;
    }

    private Document creatDocumentForTest() {
        Document document = new Document();
        document.setId( new Random().nextLong() );
        document.setCreated( new Date() );
        return document;
    }
}
