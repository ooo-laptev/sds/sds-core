/*
 * Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.web.dto.template;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


/**
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 29-Jul-2020
 */
class TemplateDTOTest {


    @Test
    void testSetterGetterOfId() {
        Long id = 1L;
        TemplateDTO templateDTO = new TemplateDTO();
        templateDTO.setId( id );
        assertEquals( id, templateDTO.getId() );
    }

    @Test
    void testSetterGetterOfName() {
        String name = "Template name";
        TemplateDTO templateDTO = new TemplateDTO();
        templateDTO.setName( name );
        assertEquals( name, templateDTO.getName() );
    }

    @Test
    void testSetterGetterOfDescription() {
        String description = "Template description";
        TemplateDTO templateDTO = new TemplateDTO();
        templateDTO.setDescription( description );
        assertEquals( description, templateDTO.getDescription() );
    }


    @Test
    void testEquals() {
        TemplateDTO firstTemplateDTO = createTemplateDTOForTest();
        TemplateDTO secondTemplateDTO = copyTemplateDTO( firstTemplateDTO );

        assertEquals( firstTemplateDTO, secondTemplateDTO );

        secondTemplateDTO.setId( null );
        assertNotEquals( firstTemplateDTO, secondTemplateDTO );

        secondTemplateDTO = copyTemplateDTO( firstTemplateDTO );
        secondTemplateDTO.setName( null );
        assertNotEquals( firstTemplateDTO, secondTemplateDTO );

        secondTemplateDTO = copyTemplateDTO( firstTemplateDTO );
        secondTemplateDTO.setDescription( null );
        assertNotEquals( firstTemplateDTO, secondTemplateDTO );

        assertNotNull( firstTemplateDTO );

        assertNotEquals( firstTemplateDTO, new Object() );
    }

    /**
     * Just creates test Template object with filled all properties.
     *
     * @return Ready for test Template object.
     */
    private TemplateDTO createTemplateDTOForTest() {
        return new TemplateDTO( 1L, "New template", "Just a test template" );
    }

    /**
     * Creates a copy of TemplateDTO
     *
     * @param templateDTO Object which should be copied.
     *
     * @return New copy.
     */
    private TemplateDTO copyTemplateDTO( TemplateDTO templateDTO ) {
        TemplateDTO templateDTO1 = new TemplateDTO();
        templateDTO1.setId( templateDTO.getId() );
        templateDTO1.setName( templateDTO.getName() );
        templateDTO1.setDescription( templateDTO.getDescription() );

        return templateDTO1;
    }
}
