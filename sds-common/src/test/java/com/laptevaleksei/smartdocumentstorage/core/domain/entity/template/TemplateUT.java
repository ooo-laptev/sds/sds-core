/*
 * Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.domain.entity.template;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


/**
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 29-Jul-2020
 */
class TemplateUT {


    @Test
    void testSetterGetterOfId() {
        Long id = 1L;
        Template template = new Template();
        template.setId( id );
        assertEquals( id, template.getId() );
    }

    @Test
    void testSetterGetterOfName() {
        String name = "Template name";
        Template template = new Template();
        template.setName( name );
        assertEquals( name, template.getName() );
    }

    @Test
    void testSetterGetterOfDescription() {
        String description = "Template description";
        Template template = new Template();
        template.setDescription( description );
        assertEquals( description, template.getDescription() );
    }

    @Test
    void testEquals() {
        Template firstTemplate = createTemplateForTest();
        Template secondTemplate = copyTemplate( firstTemplate );

//        assertEquals( firstTemplate, secondTemplate );

        secondTemplate.setId( null );
        assertNotEquals( firstTemplate, secondTemplate );

        secondTemplate = copyTemplate( firstTemplate );
        secondTemplate.setName( null );
        assertNotEquals( firstTemplate, secondTemplate );

        secondTemplate = copyTemplate( firstTemplate );
        secondTemplate.setDescription( null );
        assertNotEquals( firstTemplate, secondTemplate );

        assertNotEquals( firstTemplate, new Object() );
        assertNotNull( firstTemplate );
    }

    /**
     * Just creates test Template object with filled all properties.
     *
     * @return Ready for test Template object.
     */
    private Template createTemplateForTest() {
        Template template = new Template();
        template.setId( 1L );
        template.setName( "New template" );
        template.setDescription( "Just a test template" );
        return template;
    }

    /**
     * Creates a copy of Template
     *
     * @param template Object which should be copied.
     *
     * @return New copy.
     */
    private Template copyTemplate( Template template ) {
        Template templateCopy = new Template();
        templateCopy.setId( template.getId() );
        templateCopy.setName( template.getName() );
        templateCopy.setDescription( template.getDescription() );
        return templateCopy;
    }

}
