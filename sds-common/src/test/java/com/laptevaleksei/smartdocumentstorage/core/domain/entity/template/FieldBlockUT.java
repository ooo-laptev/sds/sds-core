/*
 * Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.domain.entity.template;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


/**
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 29-Jul-2020
 */
class FieldBlockUT {


    @Test
    void testSetterGetterOfId() {
        Long id = 1L;
        FieldBlock fieldBlock = new FieldBlock();
        fieldBlock.setId( id );
        assertEquals( id, fieldBlock.getId() );
    }

    @Test
    void testSetterGetterOfName() {
        String name = "Test name";
        FieldBlock fieldBlock = new FieldBlock();
        fieldBlock.setName( name );
        assertEquals( name, fieldBlock.getName() );
    }

    @Test
    void testSetterGetterOfDescription() {
        String description = "Test description";
        FieldBlock fieldBlock = new FieldBlock();
        fieldBlock.setDescription( description );
        assertEquals( description, fieldBlock.getDescription() );
    }

    @Test
    void testEquals() {
        FieldBlock firstFieldBlock = createFieldBlockForTest();
        FieldBlock secondFieldBlock = copyFieldBlock( firstFieldBlock );

        assertEquals( firstFieldBlock, secondFieldBlock );

        secondFieldBlock.setId( null );
        assertNotEquals( firstFieldBlock, secondFieldBlock );

        secondFieldBlock = copyFieldBlock( firstFieldBlock );
        secondFieldBlock.setName( null );
        assertNotEquals( firstFieldBlock, secondFieldBlock );

        secondFieldBlock = copyFieldBlock( firstFieldBlock );
        secondFieldBlock.setDescription( null );
        assertNotEquals( firstFieldBlock, secondFieldBlock );


        assertNotNull( firstFieldBlock );

        assertNotEquals( firstFieldBlock, new Object() );
    }

    /**
     * Just creates test FieldBlock object with filled all properties.
     *
     * @return Ready for test FieldBlock object.
     */
    private FieldBlock createFieldBlockForTest() {
        FieldBlock fieldBlock = new FieldBlock();
        fieldBlock.setId( 1L );
        fieldBlock.setName( "Test field block" );
        fieldBlock.setDescription( "test description" );
        return fieldBlock;
    }

    /**
     * Creates a copy of FieldBlock
     *
     * @param fieldBlock Object which should be copied.
     *
     * @return New copy.
     */
    private FieldBlock copyFieldBlock( FieldBlock fieldBlock ) {
        FieldBlock fieldBlockCopy = new FieldBlock();
        fieldBlockCopy.setId( fieldBlock.getId() );
        fieldBlockCopy.setName( fieldBlock.getName() );
        fieldBlockCopy.setDescription( fieldBlock.getDescription() );
        return fieldBlockCopy;
    }

}
