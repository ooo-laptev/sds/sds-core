/*
 * Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.domain.entity.template;

import com.laptevaleksei.smartdocumentstorage.core.domain.entity.CustomField;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Unit-test for class {@link TemplateDefaultCustomFieldLayout}.
 *
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 24-Aug-2021
 */
class TemplateDefaultCustomFieldLayoutUT {

    @Test
    void testSetterGetterOfId() {
        Long id = 1L;
        TemplateDefaultCustomFieldLayout templateDefaultCustomFieldLayout = new TemplateDefaultCustomFieldLayout();
        templateDefaultCustomFieldLayout.setId( id );

        assertEquals( id, templateDefaultCustomFieldLayout.getId() );
    }

    @Test
    void testSetterGetterOfCustomField() {
        CustomField customField = new CustomField();
        TemplateDefaultCustomFieldLayout templateDefaultCustomFieldLayout = new TemplateDefaultCustomFieldLayout();

        templateDefaultCustomFieldLayout.setCustomField( customField );

        assertEquals( customField, templateDefaultCustomFieldLayout.getCustomField() );
    }

    @Test
    void testSetterGetterOfTemplate() {
        Template template = new Template();
        TemplateDefaultCustomFieldLayout templateDefaultCustomFieldLayout = new TemplateDefaultCustomFieldLayout();

        templateDefaultCustomFieldLayout.setTemplate( template );

        assertEquals( template, templateDefaultCustomFieldLayout.getTemplate() );
    }

    @Test
    void testSetterGetterOfSequence() {
        Integer sequence = 1;
        TemplateDefaultCustomFieldLayout templateDefaultCustomFieldLayout = new TemplateDefaultCustomFieldLayout();
        templateDefaultCustomFieldLayout.setSequence( sequence );

        assertEquals( sequence, templateDefaultCustomFieldLayout.getSequence() );
    }

    @Test
    void testEquals() {
        TemplateDefaultCustomFieldLayout firstTemplateDefaultCustomFieldLayout = createTemplateDefaultCustomFieldLayoutForTest();
        TemplateDefaultCustomFieldLayout secondTemplateDefaultCustomFieldLayout = copyTemplateDefaultCustomFieldLayout( firstTemplateDefaultCustomFieldLayout );

//        assertEquals( firstTemplateDefaultCustomFieldLayout, secondTemplateDefaultCustomFieldLayout );

        secondTemplateDefaultCustomFieldLayout.setId( null );
        assertNotEquals( firstTemplateDefaultCustomFieldLayout, secondTemplateDefaultCustomFieldLayout );

        secondTemplateDefaultCustomFieldLayout = copyTemplateDefaultCustomFieldLayout( firstTemplateDefaultCustomFieldLayout );
        secondTemplateDefaultCustomFieldLayout.setCustomField( null );
        assertNotEquals( firstTemplateDefaultCustomFieldLayout, secondTemplateDefaultCustomFieldLayout );

        secondTemplateDefaultCustomFieldLayout = copyTemplateDefaultCustomFieldLayout( firstTemplateDefaultCustomFieldLayout );
        secondTemplateDefaultCustomFieldLayout.setTemplate( null );
        assertNotEquals( firstTemplateDefaultCustomFieldLayout, secondTemplateDefaultCustomFieldLayout );

        secondTemplateDefaultCustomFieldLayout = copyTemplateDefaultCustomFieldLayout( firstTemplateDefaultCustomFieldLayout );
        secondTemplateDefaultCustomFieldLayout.setSequence( null );
        assertNotEquals( firstTemplateDefaultCustomFieldLayout, secondTemplateDefaultCustomFieldLayout );

        secondTemplateDefaultCustomFieldLayout = new TemplateDefaultCustomFieldLayout();
        assertNotEquals( firstTemplateDefaultCustomFieldLayout, secondTemplateDefaultCustomFieldLayout );

        assertNotNull( firstTemplateDefaultCustomFieldLayout );

        assertNotEquals( firstTemplateDefaultCustomFieldLayout, new Object() );
    }

    private TemplateDefaultCustomFieldLayout createTemplateDefaultCustomFieldLayoutForTest() {
        TemplateDefaultCustomFieldLayout templateDefaultCustomFieldLayout = new TemplateDefaultCustomFieldLayout();

        templateDefaultCustomFieldLayout.setId( 1L );
        templateDefaultCustomFieldLayout.setCustomField( new CustomField() );
        templateDefaultCustomFieldLayout.setTemplate( new Template() );
        templateDefaultCustomFieldLayout.setSequence( 1 );

        return templateDefaultCustomFieldLayout;
    }

    private TemplateDefaultCustomFieldLayout copyTemplateDefaultCustomFieldLayout( TemplateDefaultCustomFieldLayout templateDefaultCustomFieldLayoutForCopy ) {
        TemplateDefaultCustomFieldLayout templateDefaultCustomFieldLayout = new TemplateDefaultCustomFieldLayout();

        templateDefaultCustomFieldLayout.setId( templateDefaultCustomFieldLayoutForCopy.getId() );
        templateDefaultCustomFieldLayout.setCustomField( templateDefaultCustomFieldLayoutForCopy.getCustomField() );
        templateDefaultCustomFieldLayout.setTemplate( templateDefaultCustomFieldLayoutForCopy.getTemplate() );
        templateDefaultCustomFieldLayout.setSequence( templateDefaultCustomFieldLayoutForCopy.getSequence() );

        return templateDefaultCustomFieldLayout;
    }
}
