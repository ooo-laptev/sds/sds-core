/*
 * Copyright (c) 2019 - 2024, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.web.dto;

import com.laptevaleksei.smartdocumentstorage.core.domain.entity.CustomField;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


/**
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 12-Aug-2020
 */
class CustomFieldTypeDTOTest {


    @Test
    void testSetterGetterOfId() {
        Long id = 1L;
        CustomFieldDTO customFieldDTO = CustomFieldDTO.builder().setId( id ).build();

        assertEquals( id, customFieldDTO.getId() );
    }

    @Test
    void testSetterGetterOfName() {
        String name = "Custom field";
        CustomFieldDTO customFieldDTO = CustomFieldDTO.builder().setName( name ).build();

        assertEquals( name, customFieldDTO.getName() );
    }

    @Test
    void testSetterGetterOfDescription() {
        String description = "Some short description.";
        CustomFieldDTO customFieldDTO = CustomFieldDTO.builder().setDescription( description ).build();

        assertEquals( description, customFieldDTO.getDescription() );
    }

    @Test
    void testSetterGetterOfCustomFieldType() {
        String customFieldType = "some.type.of.custom.field";
        CustomFieldDTO customFieldDTO = CustomFieldDTO.builder().setType( customFieldType ).build();

        assertEquals( customFieldType, customFieldDTO.getType() );
    }

    @Test
    void testCreateRealObject() {
        CustomFieldDTO customFieldDTO = createCustomFieldDTOForTest();
        CustomField realCustomField = customFieldDTO.createRealObject();

        assertEquals( customFieldDTO.getId(), realCustomField.getId() );
        assertEquals( customFieldDTO.getName(), realCustomField.getName() );
        assertEquals( customFieldDTO.getDescription(), realCustomField.getDescription() );
        assertEquals( customFieldDTO.getType(), realCustomField.getCustomFieldType() );
    }

    @Test
    void testCreateDtoFromRealObject() {
        CustomField realCustomField = new CustomField();
        realCustomField.setId( 1L );
        realCustomField.setName( "Custom field name" );
        realCustomField.setDescription( "" );
        realCustomField.setCustomFieldType( "some.type" );

        CustomFieldDTO customFieldDTO = CustomFieldDTO.createDtoFromRealObject( realCustomField );

        assertEquals( realCustomField.getId(), customFieldDTO.getId() );
        assertEquals( realCustomField.getName(), customFieldDTO.getName() );
        assertEquals( realCustomField.getDescription(), customFieldDTO.getDescription() );
        assertEquals( realCustomField.getCustomFieldType(), customFieldDTO.getType() );
    }

    @Test
    void testEquals() {
        CustomFieldDTO firstCustomFieldDTO = createCustomFieldDTOForTest();
        CustomFieldDTO secondCustomFieldDTO = copyCustomFieldDto( firstCustomFieldDTO );

        assertEquals( firstCustomFieldDTO, secondCustomFieldDTO );

        secondCustomFieldDTO.setId( null );
        assertNotEquals( firstCustomFieldDTO, secondCustomFieldDTO );

        secondCustomFieldDTO = copyCustomFieldDto( firstCustomFieldDTO );
        secondCustomFieldDTO.setName( null );
        assertNotEquals( firstCustomFieldDTO, secondCustomFieldDTO );

        secondCustomFieldDTO = copyCustomFieldDto( firstCustomFieldDTO );
        secondCustomFieldDTO.setDescription( null );
        assertNotEquals( firstCustomFieldDTO, secondCustomFieldDTO );

        secondCustomFieldDTO = copyCustomFieldDto( firstCustomFieldDTO );
        secondCustomFieldDTO.setType( null );
        assertNotEquals( firstCustomFieldDTO, secondCustomFieldDTO );

        assertNotNull( firstCustomFieldDTO );

        assertNotEquals( firstCustomFieldDTO, new Object() );
    }

    /**
     * Just creates test Template object with filled all properties.
     *
     * @return Ready for test Template object.
     */
    private CustomFieldDTO createCustomFieldDTOForTest() {
        return new CustomFieldDTO( 1L,
                "New custom field",
                "Just empty description",
                "some.type.of.customField" );
    }

    /**
     * Creates a copy of CustomFieldDTO
     *
     * @param customFieldDTO Object which should be copied.
     *
     * @return New copy.
     */
    private CustomFieldDTO copyCustomFieldDto( CustomFieldDTO customFieldDTO ) {
        return CustomFieldDTO.builder()
                .setId( customFieldDTO.getId() )
                .setName( customFieldDTO.getName() )
                .setDescription( customFieldDTO.getDescription() )
                .setType( customFieldDTO.getType() )
                .build();
    }

}
