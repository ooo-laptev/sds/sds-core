/*
 * Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.domain.entity.documentlink;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Unit-test for class {@link DocumentLinkType}.
 *
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 24-Aug-2021
 */
class DocumentLinkTypeUT {

    @Test
    void testSetterGetterOfId() {
        Long id = 1L;
        DocumentLinkType documentLinkType = new DocumentLinkType();
        documentLinkType.setId( id );
        assertEquals( id, documentLinkType.getId() );
    }

    @Test
    void testSetterGetterOfLinkTypeName() {
        String name = "some name";
        DocumentLinkType documentLinkType = new DocumentLinkType();
        documentLinkType.setLinkTypeName( name );

        assertEquals( name, documentLinkType.getLinkTypeName() );
    }

    @Test
    void testSetterGetterOfDescription() {
        String description = "some name";
        DocumentLinkType documentLinkType = new DocumentLinkType();
        documentLinkType.setDescription( description );

        assertEquals( description, documentLinkType.getDescription() );
    }

    @Test
    void testSetterGetterOfOutwardDirectionName() {
        String name = "some name";
        DocumentLinkType documentLinkType = new DocumentLinkType();
        documentLinkType.setOutwardDirectionName( name );

        assertEquals( name, documentLinkType.getOutwardDirectionName() );
    }

    @Test
    void testSetterGetterOfInwardDirectionName() {
        String name = "some name";
        DocumentLinkType documentLinkType = new DocumentLinkType();
        documentLinkType.setInwardDirectionName( name );

        assertEquals( name, documentLinkType.getInwardDirectionName() );
    }

    @Test
    void testEquals() {
        DocumentLinkType firstDocumentLinkType = createDocumentLinkTypeForTest();
        DocumentLinkType secondDocumentLinkType = copyDocumentLinkType( firstDocumentLinkType );

        assertEquals( firstDocumentLinkType, secondDocumentLinkType );

        secondDocumentLinkType.setId( null );
        assertNotEquals( firstDocumentLinkType, secondDocumentLinkType );

        secondDocumentLinkType = copyDocumentLinkType( firstDocumentLinkType );
        secondDocumentLinkType.setLinkTypeName( null );
        assertNotEquals( firstDocumentLinkType, secondDocumentLinkType );

        secondDocumentLinkType = copyDocumentLinkType( firstDocumentLinkType );
        secondDocumentLinkType.setDescription( null );
        assertNotEquals( firstDocumentLinkType, secondDocumentLinkType );

        secondDocumentLinkType = copyDocumentLinkType( firstDocumentLinkType );
        secondDocumentLinkType.setOutwardDirectionName( null );
        assertNotEquals( firstDocumentLinkType, secondDocumentLinkType );

        secondDocumentLinkType = copyDocumentLinkType( firstDocumentLinkType );
        secondDocumentLinkType.setInwardDirectionName( null );
        assertNotEquals( firstDocumentLinkType, secondDocumentLinkType );

        secondDocumentLinkType = new DocumentLinkType();
        assertNotEquals( firstDocumentLinkType, secondDocumentLinkType );

        assertNotNull( firstDocumentLinkType );

        assertNotEquals( firstDocumentLinkType, new Object() );
    }


    private DocumentLinkType createDocumentLinkTypeForTest() {
        DocumentLinkType documentLinkType = new DocumentLinkType();

        documentLinkType.setId( 1L );
        documentLinkType.setLinkTypeName( "Link type name" );
        documentLinkType.setDescription( "Some link type" );
        documentLinkType.setInwardDirectionName( "Inward direction name" );
        documentLinkType.setOutwardDirectionName( "Outward direction name" );

        return documentLinkType;
    }

    private DocumentLinkType copyDocumentLinkType( DocumentLinkType documentLinkTypeForCopy ) {
        DocumentLinkType documentLinkType = new DocumentLinkType();

        documentLinkType.setId( documentLinkTypeForCopy.getId() );
        documentLinkType.setLinkTypeName( documentLinkTypeForCopy.getLinkTypeName() );
        documentLinkType.setDescription( documentLinkTypeForCopy.getDescription() );
        documentLinkType.setInwardDirectionName( documentLinkTypeForCopy.getInwardDirectionName() );
        documentLinkType.setOutwardDirectionName( documentLinkTypeForCopy.getOutwardDirectionName() );

        return documentLinkType;
    }
}
