/*
 * Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.domain.entity.template;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


/**
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 29-Jul-2020
 */
class TemplateLayoutUT {


    @Test
    void testSetterGetterOfId() {
        Long id = 1L;
        TemplateLayout templateLayout = new TemplateLayout();
        templateLayout.setId( id );
        assertEquals( id, templateLayout.getId() );
    }

    @Test
    void testSetterGetterOfTemplate() {
        Template template = new Template();
        TemplateLayout templateLayout = new TemplateLayout();
        templateLayout.setTemplate( template );
        assertEquals( template, templateLayout.getTemplate() );
    }

    @Test
    void testSetterGetterOfFieldBlock() {
        FieldBlock fieldBlock = new FieldBlock();
        TemplateLayout templateLayout = new TemplateLayout();
        templateLayout.setFieldBlock( fieldBlock );
        assertEquals( fieldBlock, templateLayout.getFieldBlock() );
    }

    @Test
    void testSetterGetterOfSequence() {
        Integer sequence = 1;
        TemplateLayout templateLayout = new TemplateLayout();
        templateLayout.setSequence( sequence );
        assertEquals( sequence, templateLayout.getSequence() );
    }

    @Test
    void testEquals() {
        TemplateLayout firstTemplateLayout = createTemplateLayoutForTest();
        TemplateLayout secondTemplateLayout = copyTemplateLayout( firstTemplateLayout );

//        assertEquals( firstTemplateLayout, secondTemplateLayout );

        secondTemplateLayout.setId( null );
        assertNotEquals( firstTemplateLayout, secondTemplateLayout );

        secondTemplateLayout = copyTemplateLayout( firstTemplateLayout );
        secondTemplateLayout.setTemplate( null );
        assertNotEquals( firstTemplateLayout, secondTemplateLayout );

        secondTemplateLayout = copyTemplateLayout( firstTemplateLayout );
        secondTemplateLayout.setFieldBlock( null );
        assertNotEquals( firstTemplateLayout, secondTemplateLayout );

        secondTemplateLayout = copyTemplateLayout( firstTemplateLayout );
        secondTemplateLayout.setId( null );
        assertNotEquals( firstTemplateLayout, secondTemplateLayout );

        assertNotNull( firstTemplateLayout );

        assertNotEquals( firstTemplateLayout, new Object() );
    }

    /**
     * Just creates test TemplateLayout object with filled all properties.
     *
     * @return Ready for test TemplateLayout object.
     */
    private TemplateLayout createTemplateLayoutForTest() {
        TemplateLayout templateLayout = new TemplateLayout();
        templateLayout.setId( 1L );
        templateLayout.setTemplate( new Template() );
        templateLayout.setFieldBlock( new FieldBlock() );
        templateLayout.setSequence( 1 );
        return templateLayout;
    }

    /**
     * Creates a copy of TemplateLayout
     *
     * @param templateLayout Object which should be copied.
     *
     * @return New copy.
     */
    private TemplateLayout copyTemplateLayout( TemplateLayout templateLayout ) {
        TemplateLayout templateLayoutCopy = new TemplateLayout();
        templateLayoutCopy.setId( templateLayout.getId() );
        templateLayoutCopy.setTemplate( templateLayout.getTemplate() );
        templateLayoutCopy.setFieldBlock( templateLayout.getFieldBlock() );
        templateLayoutCopy.setSequence( templateLayout.getSequence() );
        return templateLayoutCopy;
    }

}
