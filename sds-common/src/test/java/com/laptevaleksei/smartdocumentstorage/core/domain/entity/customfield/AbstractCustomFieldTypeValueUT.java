/*
 * Copyright (c) 2019 - 2024, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.domain.entity.customfield;

import com.laptevaleksei.smartdocumentstorage.core.domain.entity.CustomField;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.Document;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;


class AbstractCustomFieldTypeValueUT {


    @Test
    void testGetterOfId() {
        AbstractCustomFieldValue<?> customFieldValue = Mockito.mock(
                AbstractCustomFieldValue.class,
                Mockito.CALLS_REAL_METHODS
        );
        doReturn(0L).when( customFieldValue ).getId();

        assertEquals( 0L, customFieldValue.getId() );
    }

    @Test
    void testSetterGetterOfDocument() {
        AbstractCustomFieldValue<?> customFieldValue = Mockito.mock(
                AbstractCustomFieldValue.class,
                Mockito.CALLS_REAL_METHODS
        );

        Document doc = new Document();
        customFieldValue.setDocument( doc );
        assertEquals( doc, customFieldValue.getDocument() );
    }

    @Test
    void testSetterGetterOfCustomField() {
        AbstractCustomFieldValue<?> customFieldValue = Mockito.mock(
                AbstractCustomFieldValue.class,
                Mockito.CALLS_REAL_METHODS
        );

        CustomField customField = new CustomField();
        customFieldValue.setCustomField( customField );
        assertEquals( customField, customFieldValue.getCustomField() );
    }

    @Test
    void testSetterGetterOfCreated() {
        AbstractCustomFieldValue<?> customFieldValue = Mockito.mock(
                AbstractCustomFieldValue.class,
                Mockito.CALLS_REAL_METHODS
        );

        Date created = new Date();
        customFieldValue.setCreated( created );
        assertEquals( created, customFieldValue.getCreated() );
    }

    @Test
    void testSetterGetterOfUpdated() {
        AbstractCustomFieldValue<?> customFieldValue = Mockito.mock(
                AbstractCustomFieldValue.class,
                Mockito.CALLS_REAL_METHODS
        );

        Date updated = new Date();
        customFieldValue.setUpdated( updated );
        assertEquals( updated, customFieldValue.getUpdated() );
    }

}
