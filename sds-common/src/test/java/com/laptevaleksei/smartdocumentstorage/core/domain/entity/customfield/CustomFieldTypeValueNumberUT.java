/*
 * Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.domain.entity.customfield;


import com.laptevaleksei.smartdocumentstorage.core.domain.entity.CustomField;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.Document;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;


class CustomFieldTypeValueNumberUT {


    @Test
    void testSetterOfId() {
        Long id = 1L;
        CustomFieldValueNumber customFieldValue = new CustomFieldValueNumber();
        customFieldValue.setId( id );

        assertEquals( id, customFieldValue.getId() );
    }

    @Test
    void testSetterGetterOfValue() {
        Double value = 123.456D;

        CustomFieldValueNumber customFieldValue = new CustomFieldValueNumber();
        customFieldValue.setValue( value );

        assertEquals( value, customFieldValue.getValue() );
    }


    @Test
    void testEquals() {
        CustomFieldValueNumber firstCustomFieldValue = createCustomFieldValueNumberForTest();
        CustomFieldValueNumber secondCustomFieldValue = copyCustomFieldValue( firstCustomFieldValue );

//        assertEquals( firstCustomFieldValue, secondCustomFieldValue );

        secondCustomFieldValue.setId( null );
        assertNotEquals( firstCustomFieldValue, secondCustomFieldValue );

        secondCustomFieldValue = copyCustomFieldValue( firstCustomFieldValue );
        secondCustomFieldValue.setValue( null );
        assertNotEquals( firstCustomFieldValue, secondCustomFieldValue );

        secondCustomFieldValue = copyCustomFieldValue( firstCustomFieldValue );
        secondCustomFieldValue.setCustomField( null );
        assertNotEquals( firstCustomFieldValue, secondCustomFieldValue );

        secondCustomFieldValue = copyCustomFieldValue( firstCustomFieldValue );
        secondCustomFieldValue.setDocument( null );
        assertNotEquals( firstCustomFieldValue, secondCustomFieldValue );

        secondCustomFieldValue = copyCustomFieldValue( firstCustomFieldValue );
        secondCustomFieldValue.setCreated( null );
        assertNotEquals( firstCustomFieldValue, secondCustomFieldValue );

        secondCustomFieldValue = copyCustomFieldValue( firstCustomFieldValue );
        secondCustomFieldValue.setUpdated( null );
        assertNotEquals( firstCustomFieldValue, secondCustomFieldValue );

        assertNotNull( firstCustomFieldValue );

        assertNotEquals( firstCustomFieldValue, new Object() );
    }

    private CustomFieldValueNumber createCustomFieldValueNumberForTest() {
        Date date = new Date();
        Double value = 123.456D;

        CustomFieldValueNumber customFieldValueNumber = new CustomFieldValueNumber();
        customFieldValueNumber.setId( 1L );
        customFieldValueNumber.setValue( value );
        customFieldValueNumber.setCustomField( new CustomField() );
        customFieldValueNumber.setDocument( new Document() );
        customFieldValueNumber.setCreated( date );
        customFieldValueNumber.setUpdated( date );

        return customFieldValueNumber;
    }

    private CustomFieldValueNumber copyCustomFieldValue( CustomFieldValueNumber customFieldValue ) {
        CustomFieldValueNumber customFieldValueNumber = new CustomFieldValueNumber();
        customFieldValueNumber.setId( customFieldValue.getId() );
        customFieldValueNumber.setValue( customFieldValue.getValue() );
        customFieldValueNumber.setCustomField( customFieldValue.getCustomField() );
        customFieldValueNumber.setDocument( customFieldValue.getDocument() );
        customFieldValueNumber.setCreated( customFieldValue.getCreated() );
        customFieldValueNumber.setUpdated( customFieldValue.getUpdated() );
        return customFieldValueNumber;
    }
}
