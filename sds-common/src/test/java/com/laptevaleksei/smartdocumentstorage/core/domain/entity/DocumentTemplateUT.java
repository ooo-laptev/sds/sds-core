/*
 * Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.domain.entity;

import com.laptevaleksei.smartdocumentstorage.core.domain.entity.template.Template;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


/**
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 28-Jul-2020
 */
class DocumentTemplateUT {


    @Test
    void testSetterGetterOfId() {
        Long id = 1L;
        DocumentTemplate docTemp = new DocumentTemplate();
        docTemp.setId( id );
        assertEquals( id, docTemp.getId() );
    }

    @Test
    void testSetterGetterOfDocument() {
        Document doc = new Document();
        DocumentTemplate docTemp = new DocumentTemplate();
        docTemp.setDocument( doc );
        assertEquals( doc, docTemp.getDocument() );
    }

    @Test
    void testSetterGetterOfTemplate() {
        Template template = new Template();
        DocumentTemplate docTemp = new DocumentTemplate();
        docTemp.setTemplate( template );
        assertEquals( template, docTemp.getTemplate() );
    }

    @Test
    void testEquals() {
        DocumentTemplate firstDocumentTemplate = createDocumentTemplateForTest();
        DocumentTemplate secondDocumentTemplate = copyDocumentTemplate( firstDocumentTemplate );

//        assertEquals( firstDocumentTemplate, secondDocumentTemplate );

        secondDocumentTemplate.setId( null );
        assertNotEquals( firstDocumentTemplate, secondDocumentTemplate );

        secondDocumentTemplate = copyDocumentTemplate( firstDocumentTemplate );
        secondDocumentTemplate.setDocument( null );
        assertNotEquals( firstDocumentTemplate, secondDocumentTemplate );

        secondDocumentTemplate = copyDocumentTemplate( firstDocumentTemplate );
        secondDocumentTemplate.setTemplate( null );
        assertNotEquals( firstDocumentTemplate, secondDocumentTemplate );

        assertNotEquals( firstDocumentTemplate, new Object() );
        assertNotNull( firstDocumentTemplate );

        assertNotNull( firstDocumentTemplate );

        assertNotEquals( firstDocumentTemplate, new Object() );
    }

    /**
     * Just creates test DocumentTemplate object with filled all properties.
     *
     * @return Ready for test DocumentTemplate object.
     */
    private DocumentTemplate createDocumentTemplateForTest() {
        DocumentTemplate documentTemplate = new DocumentTemplate();
        documentTemplate.setId( 1L );
        documentTemplate.setTemplate( new Template() );
        documentTemplate.setDocument( new Document() );
        return documentTemplate;
    }

    /**
     * Creates a copy of DocumentTemplate
     *
     * @param docTemp Object which should be copied.
     *
     * @return New copy.
     */
    private DocumentTemplate copyDocumentTemplate( DocumentTemplate docTemp ) {
        DocumentTemplate documentTemplateCopy = new DocumentTemplate();
        documentTemplateCopy.setId( docTemp.getId() );
        documentTemplateCopy.setDocument( docTemp.getDocument() );
        documentTemplateCopy.setTemplate( docTemp.getTemplate() );
        return documentTemplateCopy;
    }

}
