/*
 * Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.web.dto;

import com.laptevaleksei.smartdocumentstorage.core.domain.entity.Document;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;


/**
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 12-Aug-2020
 */
class DocumentDTOTest {

    @Test
    void testSetterGetterOfId() {
        Long id = 1L;
        DocumentDTO documentDTO = new DocumentDTO();
        documentDTO.setId( id );
        assertEquals( id, documentDTO.getId() );
    }

    @Test
    void testSetterGetterOfCreated() {
        Date created = new Date();
        DocumentDTO documentDTO = new DocumentDTO();
        documentDTO.setCreated( created );
        assertEquals( created, documentDTO.getCreated() );
    }

    @Test
    void testEquals() {
        DocumentDTO firstDocumentDTO = createDocumentDTOForTest();
        DocumentDTO secondDocumentDTO = copyDocumentDTO( firstDocumentDTO );

        assertEquals( firstDocumentDTO, secondDocumentDTO );

        secondDocumentDTO.setId( null );
        assertNotEquals( firstDocumentDTO, secondDocumentDTO );

        secondDocumentDTO = copyDocumentDTO( firstDocumentDTO );

        secondDocumentDTO.setCreated( null );
        assertNotEquals( firstDocumentDTO, secondDocumentDTO );

        assertNotNull( firstDocumentDTO );

        assertNotEquals( firstDocumentDTO, new Object() );
    }

    @Test
    void testCreateRealObject() {
        DocumentDTO documentDTO = createDocumentDTOForTest();
        Document realDocument = documentDTO.createRealObject();

        assertEquals( documentDTO.getId(), realDocument.getId() );
        assertEquals( documentDTO.getCreated(), realDocument.getCreated() );
    }

    @Test
    void testCreateDtoFromRealObject() {
        Document realDocument = new Document();
        realDocument.setId( 1L );
        realDocument.setCreated( new Date() );
        DocumentDTO documentDto = DocumentDTO.createDtoFromRealObject( realDocument );
        assertEquals( realDocument.getId(), documentDto.getId() );
        assertEquals( realDocument.getCreated(), documentDto.getCreated() );
    }

    /**
     * Just creates test Template object with filled all properties.
     *
     * @return Ready for test Template object.
     */
    private DocumentDTO createDocumentDTOForTest() {
        return new DocumentDTO( 1L, new Date() );
    }

    /**
     * Creates a copy of DocumentDTO
     *
     * @param documentDTO Object which should be copied.
     *
     * @return New copy.
     */
    private DocumentDTO copyDocumentDTO( DocumentDTO documentDTO ) {
        DocumentDTO dto = new DocumentDTO();
        dto.setId( documentDTO.getId() );
        dto.setCreated( documentDTO.getCreated() );

        return dto;
    }
}
