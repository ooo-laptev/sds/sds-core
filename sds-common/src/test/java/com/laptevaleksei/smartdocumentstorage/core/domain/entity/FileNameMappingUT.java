/*
 * Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.domain.entity;


import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Unit-test for class {@link FileNameMapping}.
 *
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 24-Aug-2021
 */
class FileNameMappingUT {

    @Test
    void testSetterGetterOfUuid() {
        UUID uuid = UUID.randomUUID();
        FileNameMapping fileNameMapping = new FileNameMapping();
        fileNameMapping.setUuid( uuid );

        assertEquals( uuid, fileNameMapping.getUuid() );
    }

    @Test
    void testSetterGetterOfOriginalFileName() {
        String originalFileName = "name";
        FileNameMapping fileNameMapping = new FileNameMapping();
        fileNameMapping.setOriginalFileName( originalFileName );

        assertEquals( originalFileName, fileNameMapping.getOriginalFileName() );
    }

    @Test
    void testEquals() {
        FileNameMapping firstFileNameMapping = createFileNameMappingForTest();
        FileNameMapping secondFileNameMapping = copyFileNameMapping( firstFileNameMapping );

        assertEquals( firstFileNameMapping, secondFileNameMapping );

        secondFileNameMapping.setUuid( null );
        assertNotEquals( firstFileNameMapping, secondFileNameMapping );

        secondFileNameMapping = copyFileNameMapping( firstFileNameMapping );
        secondFileNameMapping.setOriginalFileName( "other name" );
        assertNotEquals( firstFileNameMapping, secondFileNameMapping );

        secondFileNameMapping = new FileNameMapping();
        assertNotEquals( firstFileNameMapping, secondFileNameMapping );

        assertNotNull( firstFileNameMapping );

        assertNotEquals( firstFileNameMapping, new Object() );
    }


    private FileNameMapping createFileNameMappingForTest() {
        FileNameMapping fileNameMapping = new FileNameMapping();
        fileNameMapping.setUuid( UUID.randomUUID() );
        fileNameMapping.setOriginalFileName( "Test name" );

        return fileNameMapping;
    }

    private FileNameMapping copyFileNameMapping( FileNameMapping mappingToCopy ) {
        FileNameMapping fileNameMapping = new FileNameMapping();
        fileNameMapping.setUuid( mappingToCopy.getUuid() );
        fileNameMapping.setOriginalFileName( mappingToCopy.getOriginalFileName() );

        return fileNameMapping;
    }
}
