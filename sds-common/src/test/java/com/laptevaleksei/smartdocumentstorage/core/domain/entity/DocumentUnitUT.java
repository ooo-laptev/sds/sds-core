/*
 * Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.domain.entity;

import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;


/**
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 28-Jul-2020
 */
class DocumentUnitUT {


    @Test
    void testSetterGetterOfId() {
        Long id = 1L;
        Document doc = new Document();
        doc.setId( id );
        assertEquals( id, doc.getId() );
    }

    @Test
    void testSetterGetterOfCreated() {
        Date created = new Date();
        Document doc = new Document();
        doc.setCreated( created );
        assertEquals( created, doc.getCreated() );
    }


    @Test
    void testEquals() {
        Document firstDocument = createDocumentForTest();
        Document secondDocument = copyDocument( firstDocument );

//        assertEquals( firstDocument, secondDocument );

        secondDocument.setId( null );
        assertNotEquals( firstDocument, secondDocument );

        secondDocument = copyDocument( firstDocument );
        secondDocument.setCreated( null );
        assertNotEquals( firstDocument, secondDocument );

        assertNotEquals( firstDocument, new Object() );
        assertNotNull( firstDocument );

        assertNotNull( firstDocument );

        assertNotEquals( firstDocument, new Object() );
    }

    /**
     * Just creates test Document object with filled all properties.
     *
     * @return Ready for test Document object.
     */
    private Document createDocumentForTest() {
        Document doc = new Document();
        doc.setId( 1L );
        doc.setCreated( new Date() );
        return doc;
    }

    /**
     * Creates a copy of Document
     *
     * @param doc Object which should be copied.
     *
     * @return New copy.
     */
    private Document copyDocument( Document doc ) {
        Document documentCopy = new Document();
        documentCopy.setId( doc.getId() );
        documentCopy.setCreated( doc.getCreated() );
        return documentCopy;
    }

}
