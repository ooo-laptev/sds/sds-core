/*
 * Copyright (c) 2019 - 2024, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.web.dto.template;


import com.laptevaleksei.smartdocumentstorage.core.domain.entity.template.Template;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * DTO for {@link Template}
 *
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 28-Jul-2020
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TemplateDTO {

    protected Long id;

    protected String name;

    protected String description;


    public TemplateDTO( Template template ) {
        this.id = template.getId();
        this.name = template.getName();
        this.description = template.getDescription();
    }

    /**
     * This method converts real POJO to related DTO.
     *
     * @param template Object which should be converted.
     *
     * @return DTO instance.
     */
    public static TemplateDTO createDtoFromRealObject( Template template ) {
        return new TemplateDTO(
                template.getId(),
                template.getName(),
                template.getDescription() );
    }

    /**
     * This method can be used when you want to create real object from DTO instance
     *
     * @return Real POJO instead of DTO
     */
    public Template createRealObject() {
        return Template.builder()
                .setId( id )
                .setName( name )
                .setDescription( description )
                .build();
    }
}
