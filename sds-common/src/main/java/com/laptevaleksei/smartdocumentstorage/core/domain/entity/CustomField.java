/*
 * Copyright (c) 2019 - 2024, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.domain.entity;

import com.laptevaleksei.smartdocumentstorage.core.domain.AbstractEntity;
import jakarta.persistence.*;
import jakarta.validation.constraints.Size;
import lombok.*;

import java.io.Serial;


/**
 * You can use {@link CustomFieldBuilder} for instantiation of new {@link CustomField} objects.
 *
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 17-Mar-2020
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder( setterPrefix = "set" )
@EqualsAndHashCode( callSuper = true )
@Table( schema = "SDS", name = "CUSTOMFIELD" )
public class CustomField extends AbstractEntity<Long> {

    @Serial
    private static final long serialVersionUID = 990176007558405929L;

    @Id
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "CUSTOMFIELD_SEQ" )
    @SequenceGenerator( schema = "SDS", name = "CUSTOMFIELD_SEQ", allocationSize = 1 )
    private Long id;

    @Size( max = 50 )
    @Column( name = "name", length = 50 )
    private String name;

    @Size( max = 50 )
    @Column( name = "description" )
    private String description;

    @Size( max = 255 )
    @Column( name = "type" )
    private String customFieldType;
}
