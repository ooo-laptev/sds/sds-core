/*
 * Copyright (c) 2019 - 2024, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.domain.administration;

import com.laptevaleksei.smartdocumentstorage.core.domain.entity.CustomField;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.Document;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.DocumentTemplate;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.customfield.AbstractCustomFieldValue;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.documentlink.DocumentLink;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.documentlink.DocumentLinkType;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.template.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;


/**
 * This is a data bean class made for preparing backup data.
 *
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 05-Sep-2021
 */
@Data
@NoArgsConstructor
public class BackUp implements Serializable {

    private static final long serialVersionUID = 1241472327700119507L;

    private List<CustomField> customFields;
    private List<Template> templates;
    private List<FieldBlock> fieldBlocks;
    private List<FieldBlockLayout> fieldBlockLayouts;
    private List<TemplateLayout> templateLayouts;
    private List<TemplateDefaultCustomFieldLayout> templateDefaultCustomFieldLayouts;
    private List<Document> documents;
    private List<DocumentTemplate> documentTemplates;
    private List<AbstractCustomFieldValue> customFieldValues;
    private List<DocumentLinkType> documentLinkTypes;
    private List<DocumentLink> documentLinks;
}
