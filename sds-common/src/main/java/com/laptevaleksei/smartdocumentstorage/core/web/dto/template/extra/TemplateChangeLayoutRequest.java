/*
 * Copyright (c) 2019 - 2024, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.web.dto.template.extra;

import com.laptevaleksei.smartdocumentstorage.core.domain.entity.template.Template;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;


/**
 * Abstract class for request of changing {@link Template} layout.
 *
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 28-Apr-2021
 */
@Data
public class TemplateChangeLayoutRequest implements Serializable {

    @Serial
    private static final long serialVersionUID = 2634858596953282477L;

    private final Long templateId;
    private final Long fieldBlockId;
    private final Integer position;


    public TemplateChangeLayoutRequest( Long templateId, Long fieldBlockId, Integer position ) {

        if( templateId < 0 ) {
            throw new IllegalArgumentException( "Template id must not be less than zero!" );
        }
        if( fieldBlockId != null && fieldBlockId < 0 ) {
            throw new IllegalArgumentException( "FieldBlock id must not be less than zero!" );
        }
        if( position != null && position < 0 ) {
            throw new IllegalArgumentException( "Position index must not be less than zero!" );
        }

        this.templateId = templateId;
        this.fieldBlockId = fieldBlockId;
        this.position = position;
    }
}
