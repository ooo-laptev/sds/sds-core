/*
 * Copyright (c) 2019 - 2024, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.domain.entity.documentlink;

import com.laptevaleksei.smartdocumentstorage.core.domain.AbstractEntity;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.Document;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serial;


/**
 * This is a data bean for document link representation.
 *
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 23-Aug-2021
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode( callSuper = true )
@Table( schema = "SDS", name = "DOCUMENT_LINK" )
public class DocumentLink extends AbstractEntity<Long> {

    @Serial
    private static final long serialVersionUID = -4240638962589036233L;

    @Id
    @GeneratedValue( strategy = GenerationType.AUTO, generator = "DOCUMENT_LINK_SEQ" )
    @SequenceGenerator( schema = "SDS", name = "DOCUMENT_LINK_SEQ", allocationSize = 1 )
    @Column( name = "ID" )
    private Long id;

    @ManyToOne( optional = false )
    @JoinColumn( name = "SOURCE_DOCUMENT", updatable = false )
    private Document sourceDocument;

    @ManyToOne( optional = false )
    @JoinColumn( name = "TARGET_DOCUMENT", updatable = false )
    private Document targetDocument;

    @ManyToOne( optional = false )
    @JoinColumn( name = "LINK_TYPE", updatable = false )
    private DocumentLinkType linkType;
}
