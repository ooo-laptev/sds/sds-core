/*
 * Copyright (c) 2019 - 2024, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.web.dto.documentbrowser;

import com.laptevaleksei.smartdocumentstorage.core.domain.entity.Document;
import com.laptevaleksei.smartdocumentstorage.core.web.dto.DocumentDTO;
import com.laptevaleksei.smartdocumentstorage.core.web.dto.customfield.extra.ExtraCustomFieldDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serial;
import java.util.List;


/**
 * This is an extended data transfer object for {@link Document}.
 *
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 12-Aug-2021
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode( callSuper = true )
public class DocumentForDocumentBrowserDTO extends DocumentDTO {

    @Serial
    private static final long serialVersionUID = 7094460338844894453L;
    private List<ExtraCustomFieldDTO> defaultCustomFields;


    public DocumentForDocumentBrowserDTO( Document document, List<ExtraCustomFieldDTO> defaultCustomFields ) {
        super( document );
        this.defaultCustomFields = defaultCustomFields;
    }
}
