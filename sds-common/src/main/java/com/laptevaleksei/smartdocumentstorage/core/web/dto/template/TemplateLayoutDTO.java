/*
 * Copyright (c) 2019 - 2024, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.web.dto.template;


import com.laptevaleksei.smartdocumentstorage.core.domain.entity.template.FieldBlock;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.template.Template;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.template.TemplateLayout;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * This is a data transfer object for {@link TemplateLayout}.
 *
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 13-Aug-2020
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder( setterPrefix = "set" )
public class TemplateLayoutDTO {

    private Long id;

    private Integer sequence;

    private Long templateId;

    private Long fieldBlockId;


    public TemplateLayoutDTO( TemplateLayout templateLayout ) {
        id = templateLayout.getId();
        sequence = templateLayout.getSequence();
        templateId = templateLayout.getTemplate().getId();
        fieldBlockId = templateLayout.getFieldBlock().getId();
    }

    /**
     * This method converts real POJO to related DTO.
     *
     * @param templateLayout Object which should be converted.
     *
     * @return DTO instance.
     */
    public static TemplateLayoutDTO createDtoFromRealObject( TemplateLayout templateLayout ) {
        return new TemplateLayoutDTO(
                templateLayout.getId(),
                templateLayout.getSequence(),
                templateLayout.getTemplate().getId(),
                templateLayout.getFieldBlock().getId() );
    }

    /**
     * This method can be used when you want to create real object from DTO instance
     *
     * @return Real POJO instead of DTO
     */
    public TemplateLayout createRealObject() {
        return TemplateLayout.builder()
                .setId( id )
                .setSequence( 1 )
                .setTemplate( Template.builder().setId( templateId ).build() )
                .setFieldBlock( FieldBlock.builder().setId( fieldBlockId ).build() )
                .build();
    }
}
