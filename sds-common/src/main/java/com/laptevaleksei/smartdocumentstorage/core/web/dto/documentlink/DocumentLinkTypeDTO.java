/*
 * Copyright (c) 2019 - 2024, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.web.dto.documentlink;

import com.laptevaleksei.smartdocumentstorage.core.domain.entity.documentlink.DocumentLinkType;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serial;
import java.io.Serializable;


/**
 * This is a data transfer object for {@link DocumentLinkType}.
 *
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 23-Aug-2021
 */
@Data
@NoArgsConstructor
public class DocumentLinkTypeDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 7405914959778730478L;

    private Long id;

    private String linkTypeName;
    private String description;
    private String outwardDirectionName;
    private String inwardDirectionName;


    public DocumentLinkTypeDTO( DocumentLinkType documentLinkType ) {
        this.id = documentLinkType.getId();
        this.linkTypeName = documentLinkType.getLinkTypeName();
        this.description = documentLinkType.getDescription();
        this.outwardDirectionName = documentLinkType.getOutwardDirectionName();
        this.inwardDirectionName = documentLinkType.getInwardDirectionName();
    }


    public DocumentLinkType createRealObject() {
        DocumentLinkType documentLinkType = new DocumentLinkType();
        documentLinkType.setId( id );
        documentLinkType.setLinkTypeName( linkTypeName );
        documentLinkType.setOutwardDirectionName( outwardDirectionName );
        documentLinkType.setInwardDirectionName( inwardDirectionName );

        return documentLinkType;
    }
}
