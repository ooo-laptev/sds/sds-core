# Smart Document Storage `(or shortly SDS)`

![GNU GPL v3 or later logo](https://www.gnu.org/graphics/gplv3-or-later.png)

## Summary

SDS created for storing some data structured and flexible. This is a like phonebook where you can separate contacts for
templates (like `persons`, `companies` etc.)

But it is not limited only in phonebook use case. It is limited only with your imagination. You can also use it like
primitive ticket system or simple CRM.

## How to build

If you want to build it from source you should:

- Install JDK at least 17th version
- Download project sources
- Run gradle task `bootJar`
  In the end you can find `jar` file in `build/libs/` folder.

## Tested environment

For running this application you need a:

- Java runtime at least 17th version
- Some machine (can run on Raspberry Pi)
- PostgreSQL 16

## Configuration

Before you will start an application you need to provide a configuration.

For configure an application you need to create folder `config` nearby the `jar` file and create `application.yml`
inside `config` folder.

In `application.yml` you should specify database settings (see an example below):

```yaml
spring:
  datasource:
    type: com.zaxxer.hikari.HikariDataSource
    url: jdbc:h2:file:./h2db/db/smartdocumentstorage;DB_CLOSE_DELAY=-1
    username: smartdocumentstorage
    password:
```

> The example above will use local H2 database.

## How to run `jar` file

When you are got `jar` file and prepared configuration you can run it with command:

```shell
java -jar smartdocumentstorage-<version>.jar
```

## This README will be enhanced in future

`If you have some topics which you want to see here then please do not hesitate to contact me via GitLab ticket or email.`

## Credits

Author: Aleksei Laptev <a7l97nn@protonmail.com>
