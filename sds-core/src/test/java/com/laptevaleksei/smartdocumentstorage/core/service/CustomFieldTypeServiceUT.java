/*
 * Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.service;

import com.laptevaleksei.smartdocumentstorage.SmartDocumentStorageApp;
import com.laptevaleksei.smartdocumentstorage.TestHelper;
import com.laptevaleksei.smartdocumentstorage.core.api.exception.CustomFieldException;
import com.laptevaleksei.smartdocumentstorage.core.api.exception.CustomFieldNotFoundException;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.CustomField;
import com.laptevaleksei.smartdocumentstorage.core.repository.StoreException;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 1-May-2020
 */
@Disabled
@SpringBootTest(classes = SmartDocumentStorageApp.class)
@Transactional
class CustomFieldTypeServiceUT {

    @Autowired
    private CustomFieldService customFieldService;

    @Test
    void testCreateCustomField() throws CustomFieldException {
        CustomField customField = new CustomField();
        customField.setId(1L);
        customField.setName(TestHelper.CustomFieldConstants.FIRST_NAME);
        customField.setDescription(TestHelper.CustomFieldConstants.FIRST_NAME_FIELD_DESCRIPTION);
        customField.setCustomFieldType(TestHelper.CustomFieldConstants.FIRST_NAME_CUSTOM_FIELD_TYPE);

        CustomField finalCustomField = customField;
        assertThrows(StoreException.class, () -> customFieldService.createCustomField(finalCustomField));

        customField.setId(null);
        customField = customFieldService.createCustomField(customField);
        assertNotNull(customField.getId());
        assertEquals(TestHelper.CustomFieldConstants.FIRST_NAME, customField.getName());
        assertEquals(TestHelper.CustomFieldConstants.FIRST_NAME_FIELD_DESCRIPTION, customField.getDescription());
        assertEquals(TestHelper.CustomFieldConstants.FIRST_NAME_CUSTOM_FIELD_TYPE, customField.getCustomFieldType());
    }

    @Test
    void testUpdateCustomField() throws CustomFieldException {
        CustomField customField = new CustomField();
        customField.setName(TestHelper.CustomFieldConstants.FIRST_NAME);
        customField.setDescription(TestHelper.CustomFieldConstants.FIRST_NAME_FIELD_DESCRIPTION);
        customField.setCustomFieldType(TestHelper.CustomFieldConstants.FIRST_NAME_CUSTOM_FIELD_TYPE);
        customField = customFieldService.createCustomField(customField);

        String newDescr = "Some new description";
        customField.setDescription(newDescr);
        assertEquals(newDescr, customFieldService.updateCustomField(customField).getDescription());
    }

    @Test
    void testUpdateCustomFieldWithoutId() {
        CustomField customField = new CustomField();
        customField.setName(TestHelper.CustomFieldConstants.FIRST_NAME);
        customField.setDescription(TestHelper.CustomFieldConstants.FIRST_NAME_FIELD_DESCRIPTION);
        customField.setCustomFieldType(TestHelper.CustomFieldConstants.FIRST_NAME_CUSTOM_FIELD_TYPE);

        assertThrows(CustomFieldException.class, () -> customFieldService.updateCustomField(customField));
    }

    @Test
    void testUpdateCustomFieldWithUnknownId() {
        CustomField customField = new CustomField();
        customField.setId(100000000L);
        customField.setName(TestHelper.CustomFieldConstants.FIRST_NAME);
        customField.setDescription(TestHelper.CustomFieldConstants.FIRST_NAME_FIELD_DESCRIPTION);
        customField.setCustomFieldType(TestHelper.CustomFieldConstants.FIRST_NAME_CUSTOM_FIELD_TYPE);

        assertThrows(CustomFieldNotFoundException.class, () -> customFieldService.updateCustomField(customField));
    }

    @Test
    void testGetAllCustomFields() throws CustomFieldException {
        CustomField customField = new CustomField();
        customField.setName(TestHelper.CustomFieldConstants.FIRST_NAME);
        customField.setDescription(TestHelper.CustomFieldConstants.FIRST_NAME_FIELD_DESCRIPTION);
        customField.setCustomFieldType(TestHelper.CustomFieldConstants.FIRST_NAME_CUSTOM_FIELD_TYPE);

        customField = customFieldService.createCustomField(customField);

        CustomField customField2 = new CustomField();
        customField2.setName(TestHelper.CustomFieldConstants.LAST_NAME);
        customField2.setDescription(TestHelper.CustomFieldConstants.LAST_NAME_FIELD_DESCRIPTION);
        customField2.setCustomFieldType(TestHelper.CustomFieldConstants.LAST_NAME_CUSTOM_FIELD_TYPE);

        customField2 = customFieldService.createCustomField(customField2);

        List<CustomField> cfList = customFieldService.getAllCustomFields();

        CustomField finalCustomField = customField;
        assertTrue(cfList.stream().anyMatch(cf -> cf.getId().equals(finalCustomField.getId())));
        CustomField finalCustomField1 = customField2;
        assertTrue(cfList.stream().anyMatch(cf -> cf.getId().equals(finalCustomField1.getId())));
    }

    @Test
    void getAllCustomFieldsTest() throws CustomFieldException {
        CustomField customField = new CustomField();
        customField.setName(TestHelper.CustomFieldConstants.FIRST_NAME);
        customField.setDescription(TestHelper.CustomFieldConstants.FIRST_NAME_FIELD_DESCRIPTION);
        customField.setCustomFieldType(TestHelper.CustomFieldConstants.FIRST_NAME_CUSTOM_FIELD_TYPE);

        customField = customFieldService.createCustomField(customField);

        CustomField customField2 = new CustomField();
        customField2.setName(TestHelper.CustomFieldConstants.LAST_NAME);
        customField2.setDescription(TestHelper.CustomFieldConstants.LAST_NAME_FIELD_DESCRIPTION);
        customField2.setCustomFieldType(TestHelper.CustomFieldConstants.LAST_NAME_CUSTOM_FIELD_TYPE);

        customField2 = customFieldService.createCustomField(customField2);

        assertNotNull(customField.getId());
        assertEquals(TestHelper.CustomFieldConstants.FIRST_NAME, customField.getName());
        assertEquals(TestHelper.CustomFieldConstants.FIRST_NAME_FIELD_DESCRIPTION, customField.getDescription());
        assertEquals(TestHelper.CustomFieldConstants.FIRST_NAME_CUSTOM_FIELD_TYPE, customField.getCustomFieldType());

        assertNotNull(customField2.getId());
        assertEquals(TestHelper.CustomFieldConstants.LAST_NAME, customField2.getName());
        assertEquals(TestHelper.CustomFieldConstants.LAST_NAME_FIELD_DESCRIPTION, customField2.getDescription());
        assertEquals(TestHelper.CustomFieldConstants.LAST_NAME_CUSTOM_FIELD_TYPE, customField2.getCustomFieldType());
    }

    @Test
    void getCustomFieldTest() throws CustomFieldException {
        CustomField customField = new CustomField();
        customField.setName(TestHelper.CustomFieldConstants.FIRST_NAME);
        customField.setDescription(TestHelper.CustomFieldConstants.FIRST_NAME_FIELD_DESCRIPTION);
        customField.setCustomFieldType(TestHelper.CustomFieldConstants.FIRST_NAME_CUSTOM_FIELD_TYPE);

        customField = customFieldService.createCustomField(customField);

        assertNotNull(customField.getId());
        assertEquals(TestHelper.CustomFieldConstants.FIRST_NAME, customField.getName());
        assertEquals(TestHelper.CustomFieldConstants.FIRST_NAME_FIELD_DESCRIPTION, customField.getDescription());
        assertEquals(TestHelper.CustomFieldConstants.FIRST_NAME_CUSTOM_FIELD_TYPE, customField.getCustomFieldType());
    }

    @Test
    void getAllCustomFieldByIdsTest() throws CustomFieldException {
        CustomField customField = new CustomField();
        customField.setName(TestHelper.CustomFieldConstants.FIRST_NAME);
        customField.setDescription(TestHelper.CustomFieldConstants.FIRST_NAME_FIELD_DESCRIPTION);
        customField.setCustomFieldType(TestHelper.CustomFieldConstants.FIRST_NAME_CUSTOM_FIELD_TYPE);

        customField = customFieldService.createCustomField(customField);

        CustomField customField2 = new CustomField();
        customField2.setName(TestHelper.CustomFieldConstants.LAST_NAME);
        customField2.setDescription(TestHelper.CustomFieldConstants.LAST_NAME_FIELD_DESCRIPTION);
        customField2.setCustomFieldType(TestHelper.CustomFieldConstants.LAST_NAME_CUSTOM_FIELD_TYPE);

        customField2 = customFieldService.createCustomField(customField2);

        List<Long> ids = new ArrayList<>();
        ids.add(customField.getId());
        ids.add(customField2.getId());

        List<CustomField> customFields = customFieldService.getAllCustomFieldByIds(ids);

        assertEquals(2, customFields.size());
        assertTrue(customFields.contains(customField));
        assertTrue(customFields.contains(customField2));
    }

    @Test
    void saveCustomFieldTest() throws CustomFieldException {
        CustomField customField = new CustomField();
        customField.setName(TestHelper.CustomFieldConstants.FIRST_NAME);
        customField.setDescription(TestHelper.CustomFieldConstants.FIRST_NAME_FIELD_DESCRIPTION);
        customField.setCustomFieldType(TestHelper.CustomFieldConstants.FIRST_NAME_CUSTOM_FIELD_TYPE);

        customField = customFieldService.createCustomField(customField);

        assertNotNull(customField.getId());
    }

    @Test
    void deleteCustomFieldTest() throws CustomFieldException {
        CustomField customField = new CustomField();
        customField.setName(TestHelper.CustomFieldConstants.FIRST_NAME);
        customField.setDescription(TestHelper.CustomFieldConstants.FIRST_NAME_FIELD_DESCRIPTION);
        customField.setCustomFieldType(TestHelper.CustomFieldConstants.FIRST_NAME_CUSTOM_FIELD_TYPE);

        customField = customFieldService.createCustomField(customField);

        customFieldService.deleteCustomField(customField);

        assertNull(customFieldService.getCustomField(customField.getId()).orElse(null));
    }

    @Test
    void customFieldIsExistsTest() throws CustomFieldException {
        CustomField customField = new CustomField();
        customField.setName(TestHelper.CustomFieldConstants.FIRST_NAME);
        customField.setDescription(TestHelper.CustomFieldConstants.FIRST_NAME_FIELD_DESCRIPTION);
        customField.setCustomFieldType(TestHelper.CustomFieldConstants.FIRST_NAME_CUSTOM_FIELD_TYPE);

        customField = customFieldService.createCustomField(customField);

        assertTrue(customFieldService.customFieldIsExists(customField.getId()));
    }

    protected CustomField createCustomFieldWithoutIdForTest() {
        CustomField customField = new CustomField();
        customField.setName("New CustomField");
        customField.setDescription("Test description");
        customField.setCustomFieldType("java.lang.String");
        return customField;
    }
}
