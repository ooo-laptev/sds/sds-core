/*
 * Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage;

import java.util.Date;

/**
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 1-May-2020
 */
public class TestHelper {

    private TestHelper() {}

    public static class CustomFieldConstants {

        public static final String FIRST_NAME = "First name";
        public static final String LAST_NAME = "Last name";
        public static final String FIRST_NAME_FIELD_DESCRIPTION = "This field contains a first name of person";
        public static final String LAST_NAME_FIELD_DESCRIPTION = "This field contains a last name of person.";
        public static final String FIRST_NAME_CUSTOM_FIELD_TYPE = "com.laptevaleksei.smartdocumentstorage.customfield.renderer.FirstName";
        public static final String LAST_NAME_CUSTOM_FIELD_TYPE = "com.laptevaleksei.smartdocumentstorage.customfield.renderer.LastName";

        private CustomFieldConstants() {}
    }

    public static class TemplateConstants {

        public static final String FIRST_NAME_TEMPLATE = "First name template";
        public static final String LAST_NAME_TEMPLATE = "Last name template";
        public static final String FIRST_NAME_FIELD_DESCRIPTION_TEMPLATE = "This field contains a first description of template.";
        public static final String LAST_NAME_FIELD_DESCRIPTION_TEMPLATE = "This field contains a last description of template.";

        public TemplateConstants() {}
    }

    public static class FieldBlockConstants {

        public static final String FIRST_NAME_FIELDBLOCK = "First name fieldBlock";
        public static final String LAST_NAME_FIELDBLOCK = "Last name fieldBlock";
        public static final String FIRST_NAME_FIELD_DESCRIPTION_FIELDBLOCK = "This field contains a first description of fieldBlock.";
        public static final String LAST_NAME_FIELD_DESCRIPTION_FIELDBLOCK = "This field contains a last description of fieldBlock.";

        public FieldBlockConstants() {}
    }

    public static class DocumentConstants {

        public static final Date FIRST_DATE_DOCUMENT = new Date(543888000000L);
        public static final Date LAST_DATE_DOCUMENT = new Date(859334400000L);

        public DocumentConstants() {}
    }
}
