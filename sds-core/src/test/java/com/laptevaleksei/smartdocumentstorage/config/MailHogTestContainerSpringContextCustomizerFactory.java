/*
 * Copyright (c) 2019 - 2024, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.test.context.ContextConfigurationAttributes;
import org.springframework.test.context.ContextCustomizer;
import org.springframework.test.context.ContextCustomizerFactory;
import tech.jhipster.config.JHipsterConstants;

import java.util.Arrays;
import java.util.List;

/**
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 02-Jan-2024
 */
public class MailHogTestContainerSpringContextCustomizerFactory implements ContextCustomizerFactory {

    private Logger log = LoggerFactory.getLogger(MailHogTestContainerSpringContextCustomizerFactory.class);

    private static MailHogTestContainer mailHogContainer;

    @Override
    public ContextCustomizer createContextCustomizer( Class<?> testClass, List<ContextConfigurationAttributes> configAttributes ) {
        return (context, mergedConfig) -> {
            ConfigurableListableBeanFactory beanFactory = context.getBeanFactory();
            embeddedMailAnnotationProcess( testClass, context, beanFactory );
        };
    }

    private void embeddedMailAnnotationProcess( Class<?> testClass, ConfigurableApplicationContext context, ConfigurableListableBeanFactory beanFactory ) {
        TestPropertyValues testValues = TestPropertyValues.empty();
        EmbeddedMail embeddedMail = AnnotatedElementUtils.findMergedAnnotation( testClass, EmbeddedMail.class);
        boolean usingTestProdProfile = Arrays
                .asList( context.getEnvironment().getActiveProfiles())
                .contains("test" + JHipsterConstants.SPRING_PROFILE_PRODUCTION);
        // TODO: here should be profile check like in SqlTestContainersSpringContextCustomizerFactory
        if (null != embeddedMail) {
            log.debug("detected the EmbeddedMail annotation on class {}", testClass.getName());
            log.info("Warming up the mailhog");
            if (null == mailHogContainer ) {
                try {
                    Class<? extends MailHogTestContainer> containerClass = (Class<? extends MailHogTestContainer>) Class.forName(
                            this.getClass().getPackageName() + ".MailHogTestContainer"
                    );
                    mailHogContainer = beanFactory.createBean(containerClass);
                    beanFactory.registerSingleton(containerClass.getName(), mailHogContainer );
                } catch (ClassNotFoundException e) {
                    throw new RuntimeException(e);
                }
            }
            testValues = testValues.and("spring.mail.host=" + ( mailHogContainer.getTestContainer() ).getHost() );
            testValues = testValues.and("spring.mail.port=" + mailHogContainer.getPortSmtp() );

            log.info( testValues.toString() );
        }
        testValues.applyTo( context );
    }
}
