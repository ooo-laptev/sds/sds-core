/*
 * Copyright (c) 2019 - 2024, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.output.Slf4jLogConsumer;
import org.testcontainers.elasticsearch.ElasticsearchContainer;

/**
 * This is a testcontainer used for Elasticsearch initialization.
 *
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 26-Dec-2023
 */
public class ElasticsearchTestContainer implements InitializingBean, DisposableBean {

    private static final String IMAGE_NAME = "docker.elastic.co/elasticsearch/elasticsearch:7.15.2";

    private static final Logger log = LoggerFactory.getLogger(ElasticsearchTestContainer.class);

    private ElasticsearchContainer elasticsearchContainer;

    @Override
    public void destroy() throws Exception {
        if (null != elasticsearchContainer && elasticsearchContainer.isRunning()) {
            elasticsearchContainer.stop();
        }
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        if (null == elasticsearchContainer) {
            elasticsearchContainer =
                    new ElasticsearchContainer(IMAGE_NAME)
                            .withLogConsumer(new Slf4jLogConsumer(log))
                            .withExposedPorts( 9200 )
                            .withReuse(true);
        }
        if (!elasticsearchContainer.isRunning()) {
            elasticsearchContainer.start();
        }
    }

    public GenericContainer<ElasticsearchContainer> getTestContainer() {
        return elasticsearchContainer;
    }

}
