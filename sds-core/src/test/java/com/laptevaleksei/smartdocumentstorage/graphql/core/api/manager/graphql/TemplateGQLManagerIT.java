/*
 * Copyright (c) 2019 - 2024, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.graphql.core.api.manager.graphql;

import com.laptevaleksei.smartdocumentstorage.IntegrationTest;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.CustomField;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.template.FieldBlock;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.template.Template;
import com.laptevaleksei.smartdocumentstorage.manager.CustomFieldManager;
import com.laptevaleksei.smartdocumentstorage.manager.TemplateManager;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Collections;

@IntegrationTest
class TemplateGQLManagerIT {

    @Autowired
    private TemplateGQLManager templateGQLManager;

    @Autowired
    private CustomFieldManager customFieldManager;

    @Autowired
    private TemplateManager templateManager;

    @Test
    void updateTemplateWhenTemplateJustCreatedTest() {
        CustomField customField = customFieldManager.createCustomField(
            "Test field 1",
            "",
            "com.laptevaleksei.smartdocumentstorage.customfield.StringCustomField"
        );

        Template testTemplate1 = new Template();
        testTemplate1.setName("Test testTemplate1");
        testTemplate1 = templateManager.createTemplate(testTemplate1);

        FieldBlock testFieldBlock1 = new FieldBlock();
        testFieldBlock1.setName("Test FB");
        testFieldBlock1 = templateManager.createFieldBlock(testFieldBlock1);

        templateManager.addFieldBlockToTheTemplate(testTemplate1, testFieldBlock1);

        templateGQLManager.updateTemplate(
            testTemplate1.getId(),
            null,
            null,
            new ArrayList<Long>(Collections.singletonList(testFieldBlock1.getId())),
            new ArrayList<Long>(Collections.singletonList(customField.getId()))
        );

        templateGQLManager.updateTemplate(
                testTemplate1.getId(),
                null,
                null,
                new ArrayList<Long>(Collections.singletonList(testFieldBlock1.getId())),
                new ArrayList<Long>(Collections.singletonList(customField.getId()))
        );

        templateGQLManager.updateTemplate(
                testTemplate1.getId(),
                null,
                null,
                new ArrayList<Long>(Collections.singletonList(testFieldBlock1.getId())),
                new ArrayList<Long>(Collections.singletonList(customField.getId()))
        );
    }

    @Test
    void updateTemplateWhenTemplateWasExistAndConfiguredTest() {
        CustomField testCustomField1 = customFieldManager.createCustomField(
            "Test field 1",
            "",
            "com.laptevaleksei.smartdocumentstorage.customfield.StringCustomField"
        );

        Template testTemplate1 = new Template();
        testTemplate1.setName("Test testTemplate1");
        testTemplate1 = templateManager.createTemplate(testTemplate1);

        FieldBlock testFieldBlock1 = new FieldBlock();
        testFieldBlock1.setName("Test FB");
        testFieldBlock1 = templateManager.createFieldBlock(testFieldBlock1);

        templateManager.addFieldBlockToTheTemplate(testTemplate1, testFieldBlock1);

        templateGQLManager.updateTemplate(
            testTemplate1.getId(),
            null,
            null,
            new ArrayList<Long>(Collections.singletonList(testFieldBlock1.getId())),
            new ArrayList<Long>(Collections.singletonList(testCustomField1.getId()))
        );

        FieldBlock testFieldBlock2 = new FieldBlock();
        testFieldBlock2.setName("Test FB 2");
        testFieldBlock2 = templateManager.createFieldBlock(testFieldBlock2);

        CustomField testCustomField2 = customFieldManager.createCustomField(
            "Test field 2",
            "",
            "com.laptevaleksei.smartdocumentstorage.customfield.StringCustomField"
        );

        templateGQLManager.updateTemplate(
            testTemplate1.getId(),
            null,
            null,
            new ArrayList<Long>(Collections.singletonList(testFieldBlock2.getId())),
            new ArrayList<Long>(Collections.singletonList(testCustomField2.getId()))
        );
    }
}
