# ===================================================================
# Spring Boot configuration for the "stage" profile.
#
# This configuration overrides the application.yml file.
#
# More information on profiles: https://www.jhipster.tech/profiles/
# More information on configuration properties: https://www.jhipster.tech/common-application-properties/
# ===================================================================

# ===================================================================
# Standard Spring Boot properties.
# Full reference is available at:
# http://docs.spring.io/spring-boot/docs/current/reference/html/common-application-properties.html
# ===================================================================

logging:
  level:
    ROOT: INFO
    tech.jhipster: INFO
    com.laptevaleksei.smartdocumentstorage: INFO

management:
  prometheus:
    metrics:
      export:
        enabled: false

spring:
  devtools:
    restart:
      enabled: false
    livereload:
      enabled: false
  datasource:
    type: com.zaxxer.hikari.HikariDataSource
    url: jdbc:postgresql://${DB_SERVER_ADDRESS:127.0.0.1}:${DB_PORT:5432}/${DB_SCHEMA_NAME:sds}
    username: ${DB_USER:sds}
    password: ${DB_PASSWORD:sds-prod}
    hikari:
      poolName: Hikari
      auto-commit: false
#  elasticsearch:
#    rest:
#      username: sds
#      password: sds-prod
#      uris: http://localhost:9200

  # Replace by 'prod, faker' to add the faker context and have sample data loaded in production
  liquibase:
    contexts: prod
  mail:
    host: localhost
    port: 25
    username:
    password:
  thymeleaf:
    cache: true

# ===================================================================
# To enable TLS in production, generate a certificate using:
# keytool -genkey -alias smartdocumentstorage -storetype PKCS12 -keyalg RSA -keysize 2048 -keystore keystore.p12 -validity 3650
#
# You can also use Let's Encrypt:
# https://maximilian-boehm.com/hp2121/Create-a-Java-Keystore-JKS-from-Let-s-Encrypt-Certificates.htm
#
# Then, modify the server.ssl properties so your "server" configuration looks like:
#
# server:
#   port: 443
#   ssl:
#     key-store: classpath:config/tls/keystore.p12
#     key-store-password: password
#     key-store-type: PKCS12
#     key-alias: selfsigned
#     # The ciphers suite enforce the security by deactivating some old and deprecated SSL cipher, this list was tested against SSL Labs (https://www.ssllabs.com/ssltest/)
#     ciphers: TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA,TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA,TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384 ,TLS_DHE_RSA_WITH_AES_128_GCM_SHA256 ,TLS_DHE_RSA_WITH_AES_256_GCM_SHA384 ,TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256,TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384,TLS_DHE_RSA_WITH_AES_128_CBC_SHA256,TLS_DHE_RSA_WITH_AES_128_CBC_SHA,TLS_DHE_RSA_WITH_AES_256_CBC_SHA256,TLS_DHE_RSA_WITH_AES_256_CBC_SHA,TLS_RSA_WITH_AES_128_GCM_SHA256,TLS_RSA_WITH_AES_256_GCM_SHA384,TLS_RSA_WITH_AES_128_CBC_SHA256,TLS_RSA_WITH_AES_256_CBC_SHA256,TLS_RSA_WITH_AES_128_CBC_SHA,TLS_RSA_WITH_AES_256_CBC_SHA,TLS_DHE_RSA_WITH_CAMELLIA_256_CBC_SHA,TLS_RSA_WITH_CAMELLIA_256_CBC_SHA,TLS_DHE_RSA_WITH_CAMELLIA_128_CBC_SHA,TLS_RSA_WITH_CAMELLIA_128_CBC_SHA
# ===================================================================
server:
  port: 8080
  shutdown: graceful # see https://docs.spring.io/spring-boot/docs/current/reference/html/spring-boot-features.html#boot-features-graceful-shutdown
  compression:
    enabled: true
    mime-types: text/html,text/xml,text/plain,text/css,application/javascript,application/json,image/svg+xml
    min-response-size: 1024

# ===================================================================
# JHipster specific properties
#
# Full reference is available at: https://www.jhipster.tech/common-application-properties/
# ===================================================================

jhipster:
  http:
    cache: # Used by the CachingHttpHeadersFilter
      timeToLiveInDays: 1461
  cache: # Cache configuration
    ehcache: # Ehcache configuration
      time-to-live-seconds: 3600 # By default objects stay 1 hour in the cache
      max-entries: 1000 # Number of objects in each cache entry
  security:
    authentication:
      jwt:
        # This token must be encoded using Base64 and be at least 256 bits long (you can type `openssl rand -base64 64` on your command line to generate a 512 bits one)
        base64-secret: OWZjYjdjMTY0ZjAzODY4NzdlMGFjNzFhNzY5NjAxMWJmZjE3ZWU4ZmVmMTE4OThhZTM5NzQxOTVjOTU0ZGVmNDU5YzQ4YzdhZGFjZTUzYjg4NzJmNjhkNmY1Y2M5MmNjNjYwZDVjZDg5MDE4YjdjMGJlNGU4ZjA3MWRhODc2ODM=
        # Token is valid 24 hours
        token-validity-in-seconds: 86400
        token-validity-in-seconds-for-remember-me: 2592000
    remember-me:
      # security key (this key should be unique for your application, and kept secret)
      key: f6b62c45de01cbd8c73394544086a3838a8637109aae307638f7d835bbbfe99b2d1aaed6ada3ab254935684b65d9078f0094
  mail: # specific JHipster mail property, for standard properties see MailProperties
    base-url: http://my-server-url-to-change # Modify according to your server's URL
  logging:
    use-json-format: false # By default, logs are not in Json format
    logstash: # Forward logs to logstash over a socket, used by LoggingConfiguration
      enabled: false
      host: localhost
      port: 5000
      ring-buffer-size: 512
# ===================================================================
# Application specific properties
# Add your own application properties here, see the ApplicationProperties class
# to have type-safe configuration, like in the JHipsterProperties above
#
# More documentation is available at:
# https://www.jhipster.tech/common-application-properties/
# ===================================================================

# application:
sds:
  configuration:
    download_dir: ${UPLOADS_DIR:uploads}
amazon:
  s3:
    bucket-name: 'sds-prod'
    endpoint: ''
    access-key: ''
    secret-key: ''
