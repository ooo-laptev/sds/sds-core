<?xml version="1.0" encoding="utf-8"?>
<!--
  ~ Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
  ~ DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
  ~
  ~ This code is free software; you can redistribute it and/or modify it
  ~ under the terms of the GNU General Public License version 2 only, as
  ~ published by the Free Software Foundation. Aleksei Laptev designates this
  ~ particular file as subject to the "Classpath" exception as provided
  ~ by Aleksei Laptev in the LICENSE file that accompanied this code.
  ~
  ~ This code is distributed in the hope that it will be useful, but WITHOUT
  ~ ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  ~ FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
  ~ version 2 for more details (a copy is included in the LICENSE file that
  ~ accompanied this code).
  ~
  ~ You should have received a copy of the GNU General Public License version
  ~ 2 along with this work; if not, write to the Free Software Foundation,
  ~ Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
  ~
  ~ Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
  ~ questions.
  -->

<!--suppress ALL -->
<databaseChangeLog
        xmlns="http://www.liquibase.org/xml/ns/dbchangelog"
        xmlns:ext="http://www.liquibase.org/xml/ns/dbchangelog-ext"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://www.liquibase.org/xml/ns/dbchangelog http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-3.9.xsd
                        http://www.liquibase.org/xml/ns/dbchangelog-ext http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-ext.xsd">

    <changeSet id="00000000000000" author="smartdocumentstorage">
        <comment>
            Just creating a schema
        </comment>

        <sql dbms="h2, postgresql" endDelimiter=";">
            CREATE SCHEMA IF NOT EXISTS SDS
        </sql>
    </changeSet>

    <changeSet author="smartdocumentstorage" id="00000000000001">
        <comment>
            Creating a sequences for base tables for Smart Document Storage application.
        </comment>

        <createSequence schemaName="SDS" sequenceName="CUSTOMFIELD_SEQ" startValue="1" incrementBy="1"/>
        <createSequence schemaName="SDS" sequenceName="DOCUMENT_SEQ" startValue="1" incrementBy="1"/>
        <createSequence schemaName="SDS" sequenceName="CUSTOMFIELD_VALUE_DATE_SEQ" startValue="1" incrementBy="1"/>
        <createSequence schemaName="SDS" sequenceName="CUSTOMFIELD_VALUE_NUMBER_SEQ" startValue="1" incrementBy="1"/>
        <createSequence schemaName="SDS" sequenceName="CUSTOMFIELD_VALUE_STRING_SEQ" startValue="1" incrementBy="1"/>
        <createSequence schemaName="SDS" sequenceName="CUSTOMFIELD_VALUE_BOOLEAN_SEQ" startValue="1" incrementBy="1"/>


        <comment>
            Creating a sequences for Template API tables for Smart Document Storage application.
        </comment>

        <createSequence schemaName="SDS" sequenceName="DOCUMENTTEMPLATE_SEQ" startValue="1" incrementBy="1"/>
        <createSequence schemaName="SDS" sequenceName="TEMPLATE_SEQ" startValue="1" incrementBy="1"/>
        <createSequence schemaName="SDS" sequenceName="FIELDBLOCK_SEQ" startValue="1" incrementBy="1"/>
        <createSequence schemaName="SDS" sequenceName="TEMPLATE_LAYOUT_SEQ" startValue="1" incrementBy="1"/>
        <createSequence schemaName="SDS" sequenceName="FIELDBLOCK_LAYOUT_SEQ" startValue="1" incrementBy="1"/>
        <createSequence schemaName="SDS" sequenceName="TEMPLATE_DEFAULT_CUSTOMFIELD_LAYOUT_SEQ" startValue="1"
                        incrementBy="1"/>


        <comment>
            A Document linking stuff sequences
        </comment>

        <createSequence schemaName="SDS" sequenceName="DOCUMENT_LINK_TYPE_SEQ" startValue="1" incrementBy="1"/>
        <createSequence schemaName="SDS" sequenceName="DOCUMENT_LINK_SEQ" startValue="1" incrementBy="1"/>

    </changeSet>

    <changeSet author="smartdocumentstorage" id="00000000000002">
        <comment>
            Creating a base tables for Smart Document Storage application.
        </comment>

        <createTable schemaName="SDS" tableName="CUSTOMFIELD">
            <column name="ID" type="bigint" autoIncrement="true">
                <constraints nullable="false" primaryKey="true"/>
            </column>
            <column name="NAME" type="varchar(50)">
                <constraints nullable="true"/>
            </column>
            <column name="DESCRIPTION" type="varchar(255)">
                <constraints nullable="true"/>
            </column>
            <column name="TYPE" type="varchar(255)">
                <constraints nullable="true"/>
            </column>
            <column name="DELETED" type="boolean" defaultValueBoolean="false"/>
        </createTable>

        <createTable schemaName="SDS" tableName="DOCUMENT">
            <column name="ID" type="bigint">
                <constraints nullable="false" primaryKey="true"/>
            </column>
            <column name="CREATED" type="timestamp">
                <constraints nullable="true"/>
            </column>
            <column name="DELETED" type="boolean" defaultValueBoolean="false"/>
        </createTable>

        <createTable schemaName="SDS" tableName="CUSTOMFIELD_VALUE_DATE">
            <column name="ID" type="bigint">
                <constraints nullable="false" primaryKey="true"/>
            </column>
            <column name="CREATED" type="timestamp">
                <constraints nullable="true"/>
            </column>
            <column name="UPDATED" type="timestamp">
                <constraints nullable="true"/>
            </column>
            <column name="VALUE" type="timestamp">
                <constraints nullable="true"/>
            </column>
            <column name="CUSTOMFIELD" type="bigint">
                <constraints nullable="false"/>
            </column>
            <column name="DOCUMENT" type="bigint">
                <constraints nullable="false"/>
            </column>
            <column name="DELETED" type="boolean" defaultValueBoolean="false"/>
        </createTable>

        <createTable schemaName="SDS" tableName="CUSTOMFIELD_VALUE_NUMBER">
            <column name="ID" type="bigint">
                <constraints nullable="false" primaryKey="true"/>
            </column>
            <column name="CREATED" type="timestamp">
                <constraints nullable="true"/>
            </column>
            <column name="UPDATED" type="timestamp">
                <constraints nullable="true"/>
            </column>
            <column name="VALUE" type="double">
                <constraints nullable="true"/>
            </column>
            <column name="CUSTOMFIELD" type="bigint">
                <constraints nullable="false"/>
            </column>
            <column name="DOCUMENT" type="bigint">
                <constraints nullable="false"/>
            </column>
            <column name="DELETED" type="boolean" defaultValueBoolean="false"/>
        </createTable>

        <createTable schemaName="SDS" tableName="CUSTOMFIELD_VALUE_STRING">
            <column name="ID" type="bigint">
                <constraints nullable="false" primaryKey="true"/>
            </column>
            <column name="CREATED" type="timestamp">
                <constraints nullable="true"/>
            </column>
            <column name="UPDATED" type="timestamp">
                <constraints nullable="true"/>
            </column>
            <column name="VALUE" type="varchar(5000)">
                <constraints nullable="true"/>
            </column>
            <column name="CUSTOMFIELD" type="bigint">
                <constraints nullable="false"/>
            </column>
            <column name="DOCUMENT" type="bigint">
                <constraints nullable="false"/>
            </column>
            <column name="DELETED" type="boolean" defaultValueBoolean="false"/>
        </createTable>

        <createTable schemaName="SDS" tableName="CUSTOMFIELD_VALUE_BOOLEAN">
            <column name="ID" type="bigint">
                <constraints nullable="false" primaryKey="true"/>
            </column>
            <column name="CREATED" type="timestamp">
                <constraints nullable="true"/>
            </column>
            <column name="UPDATED" type="timestamp">
                <constraints nullable="true"/>
            </column>
            <column name="VALUE" type="boolean">
                <constraints nullable="true"/>
            </column>
            <column name="CUSTOMFIELD" type="bigint">
                <constraints nullable="false"/>
            </column>
            <column name="DOCUMENT" type="bigint">
                <constraints nullable="false"/>
            </column>
            <column name="DELETED" type="boolean" defaultValueBoolean="false"/>
        </createTable>


        <comment>
            Template API tables creation.
        </comment>

        <createTable schemaName="SDS" tableName="TEMPLATE">
            <column name="ID" type="bigint">
                <constraints nullable="false" primaryKey="true"/>
            </column>
            <column name="NAME" type="varchar(50)">
                <constraints nullable="true"/>
            </column>
            <column name="DESCRIPTION" type="varchar(255)">
                <constraints nullable="true"/>
            </column>
            <column name="DELETED" type="boolean" defaultValueBoolean="false"/>
        </createTable>

        <createTable schemaName="SDS" tableName="FIELDBLOCK">
            <column name="ID" type="bigint">
                <constraints nullable="false" primaryKey="true"/>
            </column>
            <column name="NAME" type="varchar(50)">
                <constraints nullable="true"/>
            </column>
            <column name="DESCRIPTION" type="varchar(255)">
                <constraints nullable="true"/>
            </column>
            <column name="DELETED" type="boolean" defaultValueBoolean="false"/>
        </createTable>

        <createTable schemaName="SDS" tableName="FIELDBLOCK_LAYOUT">
            <column name="ID" type="bigint">
                <constraints nullable="false" primaryKey="true"/>
            </column>
            <column name="CUSTOMFIELD" type="bigint">
                <constraints nullable="false"/>
            </column>
            <column name="FIELDBLOCK" type="bigint">
                <constraints nullable="false"/>
            </column>
            <column name="SEQUENCE" type="int">
                <constraints nullable="false"/>
            </column>
            <column name="REQUIRED" type="boolean">
                <constraints nullable="false"/>
            </column>
            <column name="DELETED" type="boolean" defaultValueBoolean="false"/>
        </createTable>

        <createTable schemaName="SDS" tableName="TEMPLATE_LAYOUT">
            <column name="ID" type="bigint">
                <constraints nullable="false" primaryKey="true"/>
            </column>
            <column name="TEMPLATE" type="bigint">
                <constraints nullable="false"/>
            </column>
            <column name="FIELDBLOCK" type="bigint">
                <constraints nullable="false"/>
            </column>
            <column name="SEQUENCE" type="int">
                <constraints nullable="false"/>
            </column>
            <column name="DELETED" type="boolean" defaultValueBoolean="false"/>
        </createTable>

        <createTable schemaName="SDS" tableName="TEMPLATE_DEFAULT_CUSTOMFIELD_LAYOUT">
            <column name="ID" type="bigint">
                <constraints nullable="false" primaryKey="true"/>
            </column>
            <column name="TEMPLATE" type="bigint">
                <constraints nullable="false"/>
            </column>
            <column name="CUSTOMFIELD" type="bigint">
                <constraints nullable="false"/>
            </column>
            <column name="SEQUENCE" type="int">
                <constraints nullable="false"/>
            </column>
            <column name="DELETED" type="boolean" defaultValueBoolean="false"/>
        </createTable>

        <createTable schemaName="SDS" tableName="DOCUMENT_TEMPLATE">
            <column name="ID" type="bigint">
                <constraints nullable="false" primaryKey="true"/>
            </column>
            <column name="DOCUMENT" type="bigint">
                <constraints nullable="false"/>
            </column>
            <column name="TEMPLATE" type="bigint">
                <constraints nullable="false"/>
            </column>
            <column name="DELETED" type="boolean" defaultValueBoolean="false"/>
        </createTable>


        <comment>
            File manager table creation.
        </comment>

        <createTable schemaName="SDS" tableName="FILE_NAME_MAPPING">
            <column name="UUID" type="uuid" autoIncrement="${autoIncrement}">
                <constraints nullable="false" primaryKey="true"/>
            </column>
            <column name="ORIGINAL_FILE_NAME" type="varchar(255)">
                <constraints nullable="true"/>
            </column>
        </createTable>


        <comment>
            A Document linking tables.
        </comment>

        <createTable schemaName="SDS" tableName="DOCUMENT_LINK_TYPE">
            <column name="ID" type="bigint">
                <constraints nullable="false" primaryKey="true"/>
            </column>
            <column name="LINK_TYPE_NAME" type="varchar(500)">
                <constraints nullable="true"/>
            </column>
            <column name="OUTWARD_LINK_NAME" type="varchar(500)">
                <constraints nullable="true"/>
            </column>
            <column name="INWARD_LINK_NAME" type="varchar(500)">
                <constraints nullable="true"/>
            </column>
            <column name="DESCRIPTION" type="varchar(5000)">
                <constraints nullable="true"/>
            </column>
            <column name="FOR_CUSTOMFIELD" type="bigint">
                <constraints nullable="false"/>
            </column>
            <column name="DELETED" type="boolean" defaultValueBoolean="false"/>
        </createTable>

        <createTable schemaName="SDS" tableName="DOCUMENT_LINK">
            <column name="ID" type="bigint">
                <constraints nullable="false" primaryKey="true"/>
            </column>
            <column name="SOURCE_DOCUMENT" type="bigint">
                <constraints nullable="false"/>
            </column>
            <column name="TARGET_DOCUMENT" type="bigint">
                <constraints nullable="false"/>
            </column>
            <column name="LINK_TYPE" type="bigint">
                <constraints nullable="false"/>
            </column>
            <column name="DELETED" type="boolean" defaultValueBoolean="false"/>
        </createTable>

        <createTable schemaName="SDS" tableName="AMAZON_OBJECT">
            <column name="ID" type="uuid" autoIncrement="${autoIncrement}">
                <constraints nullable="false" primaryKey="true"/>
            </column>
            <column name="OBJECT_URL" type="varchar(1000)">
                <constraints nullable="true"/>
            </column>
            <column name="DELETED" type="boolean" defaultValueBoolean="false"/>
        </createTable>
    </changeSet>

    <changeSet author="smartdocumentstorage" id="00000000000003">
        <comment>
            A foreign keys creation for base SDS tables
        </comment>

        <addForeignKeyConstraint baseTableSchemaName="SDS"
                                 baseColumnNames="CUSTOMFIELD"
                                 baseTableName="CUSTOMFIELD_VALUE_DATE"
                                 constraintName="FK_CUSTOMFIELD_OF_CUSTOMFIELD_VALUE_DATE"
                                 referencedColumnNames="ID"
                                 referencedTableSchemaName="SDS"
                                 referencedTableName="CUSTOMFIELD"/>

        <addForeignKeyConstraint baseTableSchemaName="SDS"
                                 baseColumnNames="DOCUMENT"
                                 baseTableName="CUSTOMFIELD_VALUE_DATE"
                                 constraintName="FK_DOCUMENT_OF_CUSTOMFIELD_VALUE_DATE"
                                 referencedColumnNames="ID"
                                 referencedTableSchemaName="SDS"
                                 referencedTableName="DOCUMENT"/>


        <addForeignKeyConstraint baseTableSchemaName="SDS"
                                 baseColumnNames="CUSTOMFIELD"
                                 baseTableName="CUSTOMFIELD_VALUE_NUMBER"
                                 constraintName="FK_CUSTOMFIELD_OF_CUSTOMFIELD_VALUE_NUMBER"
                                 referencedColumnNames="ID"
                                 referencedTableSchemaName="SDS"
                                 referencedTableName="CUSTOMFIELD"/>

        <addForeignKeyConstraint baseTableSchemaName="SDS"
                                 baseColumnNames="DOCUMENT"
                                 baseTableName="CUSTOMFIELD_VALUE_NUMBER"
                                 constraintName="FK_DOCUMENT_OF_CUSTOMFIELD_VALUE_NUMBER"
                                 referencedColumnNames="ID"
                                 referencedTableSchemaName="SDS"
                                 referencedTableName="DOCUMENT"/>


        <addForeignKeyConstraint baseTableSchemaName="SDS"
                                 baseColumnNames="CUSTOMFIELD"
                                 baseTableName="CUSTOMFIELD_VALUE_STRING"
                                 constraintName="FK_CUSTOMFIELD_OF_CUSTOMFIELD_VALUE_STRING"
                                 referencedColumnNames="ID"
                                 referencedTableSchemaName="SDS"
                                 referencedTableName="CUSTOMFIELD"/>

        <addForeignKeyConstraint baseTableSchemaName="SDS"
                                 baseColumnNames="DOCUMENT"
                                 baseTableName="CUSTOMFIELD_VALUE_STRING"
                                 constraintName="FK_DOCUMENT_OF_CUSTOMFIELD_VALUE_STRING"
                                 referencedColumnNames="ID"
                                 referencedTableSchemaName="SDS"
                                 referencedTableName="DOCUMENT"/>

        <addForeignKeyConstraint baseTableSchemaName="SDS"
                                 baseColumnNames="CUSTOMFIELD"
                                 baseTableName="CUSTOMFIELD_VALUE_BOOLEAN"
                                 constraintName="FK_CUSTOMFIELD_OF_CUSTOMFIELD_VALUE_BOOLEAN"
                                 referencedColumnNames="ID"
                                 referencedTableSchemaName="SDS"
                                 referencedTableName="CUSTOMFIELD"/>

        <addForeignKeyConstraint baseTableSchemaName="SDS"
                                 baseColumnNames="DOCUMENT"
                                 baseTableName="CUSTOMFIELD_VALUE_BOOLEAN"
                                 constraintName="FK_DOCUMENT_OF_CUSTOMFIELD_VALUE_BOOLEAN"
                                 referencedColumnNames="ID"
                                 referencedTableSchemaName="SDS"
                                 referencedTableName="DOCUMENT"/>

        <addForeignKeyConstraint baseTableSchemaName="SDS"
                                 baseColumnNames="FIELDBLOCK"
                                 baseTableName="FIELDBLOCK_LAYOUT"
                                 constraintName="FK_FIELDBLOCK_OF_FIELDBLOCK_LAYOUT"
                                 referencedColumnNames="ID"
                                 referencedTableSchemaName="SDS"
                                 referencedTableName="FIELDBLOCK"/>

        <addForeignKeyConstraint baseTableSchemaName="SDS"
                                 baseColumnNames="CUSTOMFIELD"
                                 baseTableName="FIELDBLOCK_LAYOUT"
                                 constraintName="FK_CUSTOMFIELD_OF_FIELDBLOCK_LAYOUT"
                                 referencedColumnNames="ID"
                                 referencedTableSchemaName="SDS"
                                 referencedTableName="CUSTOMFIELD"/>

        <addForeignKeyConstraint baseTableSchemaName="SDS"
                                 baseColumnNames="TEMPLATE"
                                 baseTableName="TEMPLATE_LAYOUT"
                                 constraintName="FK_TEMPLATE_OF_TEMPLATE_LAYOUT"
                                 referencedColumnNames="ID"
                                 referencedTableSchemaName="SDS"
                                 referencedTableName="TEMPLATE"/>

        <addForeignKeyConstraint baseTableSchemaName="SDS"
                                 baseColumnNames="FIELDBLOCK"
                                 baseTableName="TEMPLATE_LAYOUT"
                                 constraintName="FK_FIELDBLOCK_OF_TEMPLATE_LAYOUT"
                                 referencedColumnNames="ID"
                                 referencedTableSchemaName="SDS"
                                 referencedTableName="FIELDBLOCK"/>

        <addForeignKeyConstraint baseTableSchemaName="SDS"
                                 baseColumnNames="CUSTOMFIELD"
                                 baseTableName="TEMPLATE_DEFAULT_CUSTOMFIELD_LAYOUT"
                                 constraintName="FK_CUSTOMFIELD_OF_TEMPLATE_DEFAULT_CUSTOMFIELD_LAYOUT"
                                 referencedColumnNames="ID"
                                 referencedTableSchemaName="SDS"
                                 referencedTableName="CUSTOMFIELD"/>

        <addForeignKeyConstraint baseTableSchemaName="SDS"
                                 baseColumnNames="TEMPLATE"
                                 baseTableName="TEMPLATE_DEFAULT_CUSTOMFIELD_LAYOUT"
                                 constraintName="FK_TEMPLATE_OF_TEMPLATE_DEFAULT_CUSTOMFIELD_LAYOUT"
                                 referencedColumnNames="ID"
                                 referencedTableSchemaName="SDS"
                                 referencedTableName="TEMPLATE"/>

        <addForeignKeyConstraint baseTableSchemaName="SDS"
                                 baseColumnNames="TEMPLATE"
                                 baseTableName="DOCUMENT_TEMPLATE"
                                 constraintName="FK_TEMPLATE_OF_DOCUMENT_TEMPLATE"
                                 referencedColumnNames="ID"
                                 referencedTableSchemaName="SDS"
                                 referencedTableName="TEMPLATE"/>

        <addForeignKeyConstraint baseTableSchemaName="SDS"
                                 baseColumnNames="DOCUMENT"
                                 baseTableName="DOCUMENT_TEMPLATE"
                                 constraintName="FK_DOCUMENT_OF_DOCUMENT_TEMPLATE"
                                 referencedColumnNames="ID"
                                 referencedTableSchemaName="SDS"
                                 referencedTableName="DOCUMENT"/>

        <addForeignKeyConstraint baseTableSchemaName="SDS"
                                 baseColumnNames="FOR_CUSTOMFIELD"
                                 baseTableName="DOCUMENT_LINK_TYPE"
                                 constraintName="FK_DOCUMENT_LINK_TYPE_CUSTOMFIELD_RELATION"
                                 referencedColumnNames="ID"
                                 referencedTableSchemaName="SDS"
                                 referencedTableName="CUSTOMFIELD"/>
    </changeSet>


</databaseChangeLog>
