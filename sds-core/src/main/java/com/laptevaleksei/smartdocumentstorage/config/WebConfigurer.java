/*
 * Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.config;

import jakarta.servlet.ServletContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.server.WebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;
import org.springframework.util.CollectionUtils;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import tech.jhipster.config.JHipsterConstants;
import tech.jhipster.config.JHipsterProperties;
import tech.jhipster.config.h2.H2ConfigurationHelper;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;

import static java.net.URLDecoder.decode;

/**
 * Configuration of web application with Servlet 3.0 APIs.
 */
@Configuration
public class WebConfigurer implements ServletContextInitializer, WebServerFactoryCustomizer<WebServerFactory> {

    private final Logger log = LoggerFactory.getLogger( WebConfigurer.class );

    private final Environment env;

    private final JHipsterProperties jHipsterProperties;

    public WebConfigurer( Environment env, JHipsterProperties jHipsterProperties ) {
        this.env = env;
        this.jHipsterProperties = jHipsterProperties;
    }

    @Override
    public void onStartup( ServletContext servletContext ) {
        if ( env.getActiveProfiles().length != 0 ) {
            log.info( "Web application configuration, using profiles: {}", ( Object[] ) env.getActiveProfiles() );
        }

        if ( env.acceptsProfiles( Profiles.of( JHipsterConstants.SPRING_PROFILE_DEVELOPMENT ) ) ) {
            initH2Console( servletContext );
        }
        log.info( "Web application fully configured" );
    }

    /**
     * Customize the Servlet engine: Mime types, the document root, the cache.
     */
    @Override
    public void customize( WebServerFactory server ) {
        // When running in an IDE or with ./gradlew bootRun, set location of the static web assets.
        setLocationForStaticAssets( server );
    }

    private void setLocationForStaticAssets( WebServerFactory server ) {
        if ( server instanceof ConfigurableServletWebServerFactory servletWebServer ) {
            File root;
            String prefixPath = resolvePathPrefix();
            root = new File( prefixPath + "build/resources/main/static/" );
            if ( root.exists() && root.isDirectory() ) {
                servletWebServer.setDocumentRoot( root );
            }
        }
    }

    /**
     * Resolve path prefix to static resources.
     */
    private String resolvePathPrefix() {
        String fullExecutablePath = decode( this.getClass().getResource( "" ).getPath(), StandardCharsets.UTF_8 );
        String rootPath = Paths.get( "." ).toUri().normalize().getPath();
        String extractedPath = fullExecutablePath.replace( rootPath, "" );
        int extractionEndIndex = extractedPath.indexOf( "build/" );
        if ( extractionEndIndex <= 0 ) {
            return "";
        }
        return extractedPath.substring( 0, extractionEndIndex );
    }

    @Bean
    public CorsFilter corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = jHipsterProperties.getCors();
        if ( !CollectionUtils.isEmpty( config.getAllowedOrigins() ) || !CollectionUtils.isEmpty( config.getAllowedOriginPatterns() ) ) {
            log.debug( "Registering CORS filter" );
            source.registerCorsConfiguration( "/api/**", config );
            source.registerCorsConfiguration( "/management/**", config );
            source.registerCorsConfiguration( "/v3/api-docs", config );
            source.registerCorsConfiguration( "/swagger-ui/**", config );
        }
        return new CorsFilter( source );
    }

    /**
     * Initializes H2 console.
     */
    private void initH2Console( ServletContext servletContext ) {
        log.debug( "Initialize H2 console" );
        H2ConfigurationHelper.initH2Console( servletContext );
    }
}
