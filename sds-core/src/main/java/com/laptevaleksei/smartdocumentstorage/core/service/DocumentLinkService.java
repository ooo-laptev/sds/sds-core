/*
 * Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.service;

import com.laptevaleksei.smartdocumentstorage.core.domain.entity.Document;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.documentlink.DocumentLink;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.documentlink.DocumentLinkType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Services a {@link DocumentLink}s.
 *
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 23-Aug-2021
 */
public interface DocumentLinkService {
    /**
     * Creates link between {@link Document}s.
     *
     * @param relationShips Instance for creation.
     *
     * @return Created relationship with ID.
     */
    DocumentLink createDocumentLink(DocumentLink relationShips);

    /**
     * Provides {@link DocumentLink} item.
     *
     * @param id ID of item.
     *
     * @return Optional of {@link DocumentLink}.
     */
    Optional<DocumentLink> getDocumentLink(Long id);

    /**
     * Removes {@link DocumentLink} from the system.
     *
     * @param relationShips Instance for removing.
     */
    void deleteDocumentLink(DocumentLink relationShips);

    /**
     * Updates a {@link DocumentLink} item in storage.
     *
     * @param documentLink Instance of {@link DocumentLink} which should be updated.
     *
     * @return Returns updated entity.
     */
    DocumentLink updateLink(DocumentLink documentLink);

    /**
     * Provides Links for specified {@link Document}. Inclusive all connection type: source and target.
     *
     * @param document {@link Document} for relationship searching.
     *
     * @return List of {@link DocumentLink} for specified {@link Document}.
     */
    List<DocumentLink> getLinkedDocumentsForDocument(Document document);

    /**
     * Provides all links where specified document is source document.
     *
     * @param sourceDocument Object of source document
     *
     * @return List of links.
     */
    List<DocumentLink> getLinkedDocumentsForDocumentAsSource(Document sourceDocument);

    /**
     * Provides all links where specified document is target document.
     *
     * @param targetDocument Object of target document
     *
     * @return List of links.
     */
    List<DocumentLink> getLinkedDocumentsForDocumentAsTarget(Document targetDocument);

    /**
     * Provides page of all available document link types.
     *
     * @param pageable Page parameters.
     *
     * @return Page of Document link types.
     */
    Page<DocumentLinkType> getAllDocumentLinkTypes( Pageable pageable);

    /**
     * Provides all instances of {@link DocumentLink}.
     *
     * @return List of available {@link DocumentLink}.
     */
    List<DocumentLink> getAllDocumentLinks();

    /**
     * Provides all instances of {@link DocumentLink} by pages.
     * @param pageable page request
     * @return List of available {@link DocumentLink}.
     */
    Page<DocumentLink> getAllDocumentLinks( Pageable pageable);
}
