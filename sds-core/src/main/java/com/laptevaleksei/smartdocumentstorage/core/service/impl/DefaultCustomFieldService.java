/*
 * Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.service.impl;

import com.laptevaleksei.smartdocumentstorage.core.api.exception.CustomFieldException;
import com.laptevaleksei.smartdocumentstorage.core.api.exception.CustomFieldNotFoundException;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.CustomField;
import com.laptevaleksei.smartdocumentstorage.core.repository.CustomFieldRepository;
import com.laptevaleksei.smartdocumentstorage.core.repository.StoreException;
import com.laptevaleksei.smartdocumentstorage.core.service.CustomFieldService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 17-Mar-2020
 */
@Service
@Transactional
public class DefaultCustomFieldService implements CustomFieldService {

    private final Logger log = LoggerFactory.getLogger(DefaultCustomFieldService.class);

    private final CustomFieldRepository customFieldRepository;

    public DefaultCustomFieldService(CustomFieldRepository customFieldRepository) {
        this.customFieldRepository = customFieldRepository;
    }

    /**
     * This Predicate provides possibility to get all unique values by some criteria.
     *
     * @param keyExtractor Key which can be used for criteria.
     * @param <T> Type of object.
     *
     * @return Predicate which can be used in stream().filter(..).
     */
    public static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Map<Object, Boolean> seen = new ConcurrentHashMap<>();
        return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }

    @Override
    public CustomField updateCustomField(CustomField customField) {
        if (customField.getId() == null) throw new CustomFieldException("You try to update CustomField without ID!");

        if (!customFieldRepository.existsById(customField.getId())) throw new CustomFieldNotFoundException(
            "You try to update CustomField which does not exists!"
        );

        return customFieldRepository.save(customField);
    }

    @Override
    public CustomField createCustomField(CustomField customField) {
        if (customField.getId() != null) {
            throw new StoreException("You try to create CustomField with pre-defined ID!");
        }
        if (customField.isDeleted()) {
            throw new StoreException("You are trying to create a CustomField which is already marked as deleted! It will not be saved.");
        }

        return customFieldRepository.save(customField);
    }

    @Override
    public List<CustomField> getAllCustomFields() {
        return new ArrayList<>(customFieldRepository.findAll())
            .stream()
            .filter(customField -> !customField.isDeleted()) // TODO: wrap isDeleted check into the repository layer
            .collect(Collectors.toList());
    }

    @Override
    public Optional<CustomField> getCustomField(Long id) {
        return customFieldRepository.findById(id).filter(customField -> !customField.isDeleted()); // TODO: wrap isDeleted check into the repository layer
    }

    @Override
    public List<CustomField> getAllCustomFieldByIds(Collection<Long> ids) {
        return customFieldRepository.findAllById(ids).stream().filter(customField -> !customField.isDeleted()).collect(Collectors.toList()); // TODO: wrap isDeleted check into the repository layer
    }

    @Override
    public void deleteCustomField(CustomField customField) {
        customField.setDeleted(true);
        updateCustomField(customField);
    }

    @Override
    public boolean customFieldIsExists(Long id) {
        Optional<CustomField> optionalCustomField = getCustomField(id);
        return optionalCustomField.isPresent() && !optionalCustomField.get().isDeleted(); // TODO: wrap isDeleted check into the repository layer
    }

    @Override
    public Page<CustomField> getAllManagedCustomFields(Pageable pageable) {
        Page<CustomField> page = customFieldRepository.findAll(pageable);
        return new PageImpl<>(
            page.stream().filter(customField -> !customField.isDeleted()).collect(Collectors.toList()), // TODO: wrap isDeleted check into the repository layer
            page.getPageable(),
            page.getTotalElements()
        );
    }
}
