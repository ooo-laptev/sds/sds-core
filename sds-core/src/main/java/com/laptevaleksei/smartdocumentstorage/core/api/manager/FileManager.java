/*
 * Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.api.manager;

import com.laptevaleksei.smartdocumentstorage.core.service.FileService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Manager for file uploading and uploading.
 *
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 25-Jul-2021
 */
@Service
public class FileManager {

    private final FileService fileService;

    private final Path fileStorageLocation;

    public FileManager(@Value("${sds.configuration.download_dir}") String fileStoragePath, FileService fileService) {
        this.fileService = fileService;
        this.fileStorageLocation = Paths.get(fileStoragePath).toAbsolutePath().normalize();
        // TODO: comment-outed because it impacts on docker image. Improve this when feature with files will be in progress
        //        try {
        //            Files.createDirectories( this.fileStorageLocation );
        //        } catch ( Exception ex ) {
        //            throw new FileStorageException( "Could not create the directory where the uploaded files will be stored.", ex );
        //        }
    }

    /**
     * Use this method for file storing.
     *
     * @param file File which you want to store.
     *
     * @return Returns name of saved file.
     */
    public String storeFile(MultipartFile file) {
        return fileService.storeFile(file, fileStorageLocation);
    }

    /**
     * You can load file from file system using this method.
     *
     * @param fileName Name of file which you want to load.
     *
     * @return Returns file as {@link Resource}.
     */
    public Resource loadFileAsResource(String fileName) {
        return fileService.loadFileAsResource(fileName, fileStorageLocation);
    }

    /**
     * Deletes file by name.
     *
     * @param fileName Name of file which should be removed.
     */
    public void removeFile(String fileName) {
        fileService.removeFile(fileName, fileStorageLocation);
    }
}
