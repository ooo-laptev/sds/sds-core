/*
 * Copyright (c) 2019 - 2024, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.api.manager;

import com.laptevaleksei.annotations.Internal;
import com.laptevaleksei.smartdocumentstorage.core.api.exception.TemplateException;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.CustomField;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.Document;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.DocumentTemplate;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.template.FieldBlock;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.template.FieldBlockLayout;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.template.Template;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.template.TemplateLayout;
import com.laptevaleksei.smartdocumentstorage.core.service.TemplateService;
import com.laptevaleksei.smartdocumentstorage.manager.TemplateManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * This class provides a Template management.
 * You should use this functionality if you want to work with Templates.
 *
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 17-Mar-2020
 * @see FieldBlock
 * @see FieldBlockLayout
 * @see Template
 * @see TemplateLayout
 * @see DocumentTemplate
 */
@Service
@Internal
public class DefaultTemplateManagerImpl implements TemplateManager {

    private final Logger logger = LoggerFactory.getLogger(DefaultTemplateManagerImpl.class);

    private final TemplateService templateService;

    public DefaultTemplateManagerImpl(TemplateService templateService) {
        this.templateService = templateService;
    }

    @Override
    public Optional<FieldBlock> getFieldBlock(Long id) {
        return templateService.getFieldBlock(id);
    }

    @Override
    public FieldBlock createFieldBlock(FieldBlock fieldBlock) {
        return templateService.createFieldBlock(fieldBlock);
    }

    @Override
    public void deleteFieldBlock(FieldBlock fieldBlock) {
        templateService.deleteFieldBlock(fieldBlock);
    }

    @Override
    public List<FieldBlock> getAllFieldBlocks() {
        return templateService.getAllFieldBlock();
    }

    @Override
    public List<CustomField> getCustomFieldsOfBlock(FieldBlock fieldBlock) {
        return templateService.getAllCustomFieldRelatedWithFieldBlock(fieldBlock);
    }

    @Override
    public void addCustomFieldToTheBlockAfter(FieldBlock fieldBlock, CustomField customField, Integer position, boolean required) {
        if (templateService.customFieldIsExistsInFieldBlockLayout(fieldBlock, customField)) throw new TemplateException(
            "CustomField with id " +
            customField.getId() +
            " already exists in FieldBlock with id " +
            fieldBlock.getId() +
            ". Please use another method for moving!"
        );

        List<FieldBlockLayout> layout = templateService
            .getFieldBlockLayouts(fieldBlock)
            .stream()
            .sorted(Comparator.comparingInt(FieldBlockLayout::getSequence))
            .collect(Collectors.toList());

        List<FieldBlockLayout> layoutForUpdate = new ArrayList<>();

        FieldBlockLayout newLayout = FieldBlockLayout.builder()
            .setSequence(position)
            .setCustomField(customField)
            .setFieldBlock(fieldBlock)
            .setRequired(required)
            .build();
        layoutForUpdate.add(newLayout);

        updateFieldBlockLayout(layout, layoutForUpdate, position);
    }

    @Override
    public void removeCustomFieldFromTheFieldBlockLayout(FieldBlock fieldBlock, CustomField customField) {
        List<FieldBlockLayout> layout = templateService
            .getFieldBlockLayouts(fieldBlock)
            .stream()
            .sorted(Comparator.comparingInt(FieldBlockLayout::getSequence))
            .collect(Collectors.toList());

        List<FieldBlockLayout> filteredLayoutsByCustomField = layout
            .stream()
            .filter(fbl -> fbl.getCustomField().getId().equals(customField.getId()))
            .collect(Collectors.toList());

        if (filteredLayoutsByCustomField.isEmpty()) {
            throw new TemplateException("Specified CustomField does not exists in layout for specified FieldBlock!");
        }

        filteredLayoutsByCustomField.forEach(templateService::deleteFieldBlockLayout);

        List<FieldBlockLayout> layoutForUpdate = new ArrayList<>();

        filteredLayoutsByCustomField.forEach(
            it -> {
                for (FieldBlockLayout fieldBlockLayout : layout) {
                    if (fieldBlockLayout.getSequence() > it.getSequence()) {
                        Integer seq = fieldBlockLayout.getSequence();
                        fieldBlockLayout.setSequence(seq - 1);
                        layoutForUpdate.add(fieldBlockLayout);
                    }
                }
            }
        );

        for (FieldBlockLayout fieldBlockLayout : layoutForUpdate) {
            templateService.updateFieldBlockLayout(fieldBlockLayout);
        }
    }

    /**
     *
     * TODO: reimplement this method. Keep in mind that you need to add isRequired property for layout. Also you should to take current {@link FieldBlockLayout} to copy current state of isRequired parameter.
     */
    @Override
    public void changeCustomFieldPosition(FieldBlock fieldBlock, CustomField customField, Integer position) {
        List<FieldBlockLayout> layout = templateService
            .getFieldBlockLayouts(fieldBlock)
            .stream()
            .sorted(Comparator.comparingInt(FieldBlockLayout::getSequence))
            .collect(Collectors.toList());

        FieldBlockLayout forRemoving = layout.stream().filter(it -> it.getCustomField().equals(customField)).findFirst().orElse(null);

        boolean isRequiredField = false;
        if (forRemoving != null) {
            isRequiredField = forRemoving.isRequired();
            layout.remove(forRemoving);
            templateService.deleteFieldBlockLayout(forRemoving);
        }

        FieldBlockLayout newLayoutItem = FieldBlockLayout.builder()
            .setFieldBlock(fieldBlock)
            .setCustomField(customField)
            .setSequence(position)
            .setRequired(isRequiredField)
            .build();

        newLayoutItem = templateService.createFieldBlockLayout(newLayoutItem);

        layout.add(position - 1, newLayoutItem);

        for (int i = position; i < layout.size(); i++) {
            if (!layout.get(i).equals(newLayoutItem)) {
                if (layout.get(i).getSequence() != i + 1) {
                    layout.get(i).setSequence(i + 1);
                }
            }
        }

        layout.forEach(templateService::updateFieldBlockLayout);
    }

    /**
     * Removes duplications for {@link #addCustomFieldToTheBlockAfter(FieldBlock, CustomField, Integer, boolean)} and
     * {@link #changeCustomFieldPosition(FieldBlock, CustomField, Integer)}
     *
     * @param oldLayout Current Layout.
     * @param layoutForUpdate Layout with new {@link FieldBlockLayout}s
     * @param position Position where you want to start putting.
     */
    private void updateFieldBlockLayout(List<FieldBlockLayout> oldLayout, List<FieldBlockLayout> layoutForUpdate, Integer position) {
        for (FieldBlockLayout fieldBlockLayout : oldLayout) {
            if (fieldBlockLayout.getSequence() >= position) {
                Integer seq = fieldBlockLayout.getSequence();
                fieldBlockLayout.setSequence(seq + 1);
                layoutForUpdate.add(fieldBlockLayout);
            }
        }

        for (FieldBlockLayout fieldBlockLayout : layoutForUpdate) {
            if (fieldBlockLayout.getId() == null) {
                templateService.createFieldBlockLayout(fieldBlockLayout);
            } else {
                templateService.updateFieldBlockLayout(fieldBlockLayout);
            }
        }
    }

    @Override
    public Integer getPositionOfLastCustomField(FieldBlock fieldBlock) {
        return templateService
            .getFieldBlockLayouts(fieldBlock)
            .stream()
            .max(Comparator.comparing(FieldBlockLayout::getSequence))
            .orElse( FieldBlockLayout.builder().setSequence(0).build())
            .getSequence();
    }

    @Override
    public void addCustomFieldToTheFieldBlock(FieldBlock fieldBlock, CustomField customField, boolean required) {
        addCustomFieldToTheBlockAfter(fieldBlock, customField, getPositionOfLastCustomField(fieldBlock) + 1, required);
    }

    @Override
    public void addCustomFieldToTheFieldBlock(FieldBlock fieldBlock, CustomField customField) {
        addCustomFieldToTheFieldBlock(fieldBlock, customField, false);
    }

    @Override
    public List<Template> getRelatedTemplates(FieldBlock fieldBlock) {
        return templateService.getTemplatesRelatedWithFieldBlock(fieldBlock);
    }

    @Override
    public Optional<Template> getTemplate(Long id) {
        return templateService.getTemplate(id);
    }

    @Override
    public Template createTemplate(Template template) {
        return templateService.createTemplate(template);
    }

    @Override
    public void deleteTemplate(Template template) {
        templateService.deleteTemplate(template);
    }

    @Override
    public void deleteTemplate(Long id) {
        try {
            Template template = getTemplate(id).orElseThrow(TemplateException::new);

            getRelatedDocuments(null, template).forEach(document -> unassignTemplateFromDocument(document, template));

            templateService.deleteTemplate(template);
        } catch (TemplateException te) {
            logger.error(te.getLocalizedMessage());
        }
    }

    @Override
    public List<Template> getAllTemplates() {
        return templateService.getAllTemplates();
    }

    @Override
    public List<FieldBlock> getFieldBlocksOfTheTemplate(Template template) {
        List<TemplateLayout> lay = templateService.getTemplateLayouts(template);
        return lay.stream().map(TemplateLayout::getFieldBlock).collect(Collectors.toList());
    }

    @Override
    public void addFieldBlockToTheTemplateAfter(Template template, FieldBlock fieldBlock, Integer position) {
        if (templateService.fieldBlockIsExistsInTemplateLayout(template, fieldBlock)) {
            throw new TemplateException(
                "FieldBlock with id " +
                fieldBlock.getId() +
                " already exists in Template with id " +
                template.getId() +
                ". Please use another method for moving!"
            );
        }

        List<TemplateLayout> layout = templateService
            .getTemplateLayouts(template)
            .stream()
            .sorted(Comparator.comparingInt(TemplateLayout::getSequence))
            .collect(Collectors.toList());

        List<TemplateLayout> layoutForUpdate = new ArrayList<>();

        TemplateLayout newLayout = TemplateLayout.builder()
            .setSequence(position)
            .setFieldBlock(fieldBlock)
            .setTemplate(template)
            .build();
        layoutForUpdate.add(newLayout);

        updateTemplateLayout(layout, layoutForUpdate, position);
    }

    @Override
    public Integer getPositionOfLastFieldBlock(Template template) {
        return templateService
            .getTemplateLayouts(template)
            .stream()
            .max(Comparator.comparing(TemplateLayout::getSequence))
            .orElse(TemplateLayout.builder().setSequence(0).build())
            .getSequence();
    }

    @Override
    public void addFieldBlockToTheTemplate(Template template, FieldBlock fieldBlock) {
        addFieldBlockToTheTemplateAfter(template, fieldBlock, getPositionOfLastFieldBlock(template) + 1);
    }

    @Override
    public Page<Document> getRelatedDocuments(Pageable pageable, Template template) {
        return templateService.getAllDocumentRelatedWithTemplate(pageable, template);
    }

    @Override
    public void assignTemplateToDocument(Document document, Template template) {
        templateService.assignTemplateToDocument(document, template);
    }

    @Override
    public List<Template> getTemplatesAssignedToDocument(Document document) {
        return templateService.getTemplatesAssignedToDocument(document);
    }

    @Override
    public Page<Document> getDocumentsWhichUsesTemplate(Pageable pageable, Template template) {
        return templateService.getDocumentsWhichUsesTemplate(pageable, template);
    }

    @Override
    public void removeFieldBlockFromTemplate(Template template, FieldBlock fieldBlock) {
        List<TemplateLayout> layout = templateService
            .getTemplateLayouts(template)
            .stream()
            .sorted(Comparator.comparingInt(TemplateLayout::getSequence))
            .collect(Collectors.toList());

        TemplateLayout layoutForRemoving = null;

        Optional<TemplateLayout> first = layout.stream().filter(it -> it.getFieldBlock().equals(fieldBlock)).findFirst();

        if (first.isPresent()) {
            layoutForRemoving = first.get();
        }

        templateService.deleteTemplateLayout(layoutForRemoving);

        List<TemplateLayout> layoutsForSequenceUpdating = new ArrayList<>();
        layout =
            templateService
                .getTemplateLayouts(template)
                .stream()
                .sorted(Comparator.comparingInt(TemplateLayout::getSequence))
                .collect(Collectors.toList());

        for (int i = 0; i < layout.size(); i++) {
            if (i == 0) {
                if (layout.get(i).getSequence() != i + 1) {
                    layout.get(i).setSequence(i + 1);
                    layoutsForSequenceUpdating.add(layout.get(i));
                }
            } else {
                layout.get(i).setSequence(i + 1);
                layoutsForSequenceUpdating.add(layout.get(i));
            }
        }
        layoutsForSequenceUpdating.forEach(templateService::updateTemplateLayout);
    }

    @Override
    public void changeFieldBlockPosition(Template template, FieldBlock fieldBlock, Integer position) {
        templateService.changeFieldBlockPosition(template, fieldBlock, position);
    }

    @Override
    public List<TemplateLayout> getTemplateLayouts(Template template) {
        return templateService.getTemplateLayouts(template);
    }

    /**
     * Removes duplications for {@link #addFieldBlockToTheTemplateAfter(Template, FieldBlock, Integer)} and
     * {@link #addCustomFieldToTheBlockAfter(FieldBlock, CustomField, Integer, boolean)}
     *
     * @param oldLayout Current Layout.
     * @param layoutForUpdate Layout with new {@link TemplateLayout}s
     * @param position Position where you want to start putting.
     */
    private void updateTemplateLayout(List<TemplateLayout> oldLayout, List<TemplateLayout> layoutForUpdate, Integer position) {
        for (TemplateLayout templateLayout : oldLayout) {
            if (templateLayout.getSequence() >= position) {
                Integer seq = templateLayout.getSequence();
                templateLayout.setSequence(seq + 1);
                layoutForUpdate.add(templateLayout);
            }
        }
        for (TemplateLayout templateLayout : layoutForUpdate) {
            if (templateLayout.getId() == null) {
                templateService.createTemplateLayout(templateLayout);
            } else {
                templateService.updateTemplateLayout(templateLayout);
            }
        }
    }

    @Override
    public List<FieldBlock> getRelatedFieldBlocksWithCustomField(CustomField customField) {
        return templateService.getAllFieldBlockRelatedWithCustomField(customField);
    }

    @Override
    public List<FieldBlock> getRelatedFieldBlocksWithTemplate(Template template) {
        return templateService.getAllFieldBlockRelatedWithTemplate(template);
    }

    @Override
    public List<CustomField> getRelatedCustomFieldsWithFieldBlock(FieldBlock fieldBlock) {
        return templateService.getAllCustomFieldRelatedWithFieldBlock(fieldBlock);
    }

    @Override
    public boolean templateIsExists(Long id) {
        return templateService.templateIsExists(id);
    }

    @Override
    public Optional<Template> updateTemplate(Template template) {
        return templateService.updateTemplate(template);
    }

    @Override
    public Page<Template> getAllManagedTemplates(Pageable pageable) {
        return templateService.getAllManagedTemplates(pageable);
    }

    @Override
    public Page<FieldBlock> getAllManagedFieldBlocks(Pageable pageable) {
        return templateService.getAllManagedFieldBlocks(pageable);
    }

    @Override
    public boolean fieldBlockIsExists(Long id) {
        return templateService.fieldBlockIsExists(id);
    }

    @Override
    public Optional<FieldBlock> updateFieldBlock(FieldBlock fieldBlock) {
        return templateService.updateFieldBlock(fieldBlock);
    }

    @Override
    public void unassignTemplateFromDocument(Document document, Template template) {
        templateService.removeDocumentTemplateRelationship(document, template);
    }

    @Override
    public Page<FieldBlock> getFieldBlocksRelatedWithCustomFields(Pageable pageable, CustomField customField) {
        return templateService.getFieldBlocksRelatedWithCustomFields(pageable, customField);
    }

    @Override
    public Page<Template> getRelatedTemplates(Pageable pageable, FieldBlock fieldBlock) {
        return templateService.getRelatedTemplates(pageable, fieldBlock);
    }

    @Override
    public List<CustomField> getDefaultCustomFields(Template template) {
        return templateService.getDefaultCustomFields(template);
    }

    @Override
    public void addDefaultCustomField(Template template, CustomField customField) {
        templateService.addDefaultCustomField(template, customField);
    }

    @Override
    public void removeDefaultCustomField(Template template, CustomField customField) {
        templateService.removeDefaultCustomField(template, customField);
    }

    @Override
    public void changeDefaultCustomFieldPosition(Template template, CustomField customField, Integer position) {
        templateService.changeDefaultCustomFieldPosition(template, customField, position);
    }
}
