/*
 * Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.web.rest;

import com.laptevaleksei.smartdocumentstorage.core.api.exception.CustomFieldException;
import com.laptevaleksei.smartdocumentstorage.core.customfieldprocessing.CustomFieldTypeRegistry;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.CustomField;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.Document;
import com.laptevaleksei.smartdocumentstorage.core.security.AuthoritiesConstants;
import com.laptevaleksei.smartdocumentstorage.core.web.dto.CustomFieldDTO;
import com.laptevaleksei.smartdocumentstorage.core.web.rest.api.customfield.CustomFieldManagementApiResource;
import com.laptevaleksei.smartdocumentstorage.core.web.rest.api.customfield.CustomFieldUserApiResource;
import com.laptevaleksei.smartdocumentstorage.core.web.rest.jhipster.errors.BadRequestAlertException;
import com.laptevaleksei.smartdocumentstorage.manager.CustomFieldManager;
import com.laptevaleksei.smartdocumentstorage.manager.DocumentManager;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class CustomFieldResource implements CustomFieldManagementApiResource, CustomFieldUserApiResource {

    private static final List<String> ALLOWED_ORDERED_PROPERTIES = List.of("id", "name", "description", "type");

    private final Logger log = LoggerFactory.getLogger(CustomFieldResource.class);
    private final CustomFieldManager customFieldManager;
    private final CustomFieldTypeRegistry customFieldTypeRegistry;
    private final DocumentManager documentManager;

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    public CustomFieldResource(
        CustomFieldManager customFieldManager,
        CustomFieldTypeRegistry customFieldTypeRegistry,
        DocumentManager documentManager
    ) {
        this.customFieldManager = customFieldManager;
        this.customFieldTypeRegistry = customFieldTypeRegistry;
        this.documentManager = documentManager;
    }

    @Override
    @PostMapping("/customFields")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<CustomFieldDTO> createCustomField(@Valid @RequestBody CustomFieldDTO customFieldDTO) throws URISyntaxException {
        log.debug("REST request to save CustomField : {}", customFieldDTO);

        if (customFieldDTO.getId() != null) {
            throw new BadRequestAlertException("A new customField cannot already have an ID", "customFieldManagement", "idexists");
        } else {
            CustomField newCustomField = customFieldManager.createCustomField(
                customFieldDTO.getName(),
                customFieldDTO.getDescription(),
                customFieldDTO.getType()
            );
            return ResponseEntity
                .created(new URI("/api/customFields/" + newCustomField.getId()))
                .headers(HeaderUtil.createAlert(applicationName, "customFieldManagement.created", newCustomField.getName()))
                .body(CustomFieldDTO.createDtoFromRealObject(newCustomField));
        }
    }

    @Override
    @PutMapping("/customFields")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<CustomFieldDTO> updateCustomField(@Valid @RequestBody CustomFieldDTO customFieldDTO) {
        log.debug("REST request to update CustomField : {}", customFieldDTO);

        if (!customFieldManager.customFieldIsExists(customFieldDTO.getId())) {
            throw new CustomFieldException("CustomField with id=" + customFieldDTO.getId() + " not exists!");
        }

        try {
            customFieldDTO =
                CustomFieldDTO.createDtoFromRealObject(customFieldManager.updateCustomField(customFieldDTO.createRealObject()));
        } catch (CustomFieldException cfe) {
            log.error(cfe.getLocalizedMessage());
        }

        Optional<CustomFieldDTO> updatedCustomField = Optional.of(customFieldDTO);

        return ResponseUtil.wrapOrNotFound(
            updatedCustomField,
            HeaderUtil.createAlert(applicationName, "customFieldManagement.updated", customFieldDTO.getId().toString())
        );
    }

    @Override
    @GetMapping("/customFields")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<List<CustomFieldDTO>> getAllCustomFields(Pageable pageable) {
        if (!onlyContainsAllowedProperties(pageable)) {
            return ResponseEntity.badRequest().build();
        }

        final Page<CustomFieldDTO> page = customFieldManager.getAllPagesCustomFieldDTO(pageable).map(CustomFieldDTO::new);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    private boolean onlyContainsAllowedProperties(Pageable pageable) {
        return pageable.getSort().stream().map(Sort.Order::getProperty).allMatch(ALLOWED_ORDERED_PROPERTIES::contains);
    }

    @Override
    @GetMapping("/customFields/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<CustomFieldDTO> getCustomField(@PathVariable Long id) {
        log.debug("REST request to get CustomField : {}", id);
        Optional<CustomField> customField = customFieldManager.getCustomField(id);
        CustomFieldDTO customFieldDTO = new CustomFieldDTO();
        if (customField.isPresent()) {
            customFieldDTO = new CustomFieldDTO(customField.get());
        }
        return ResponseUtil.wrapOrNotFound(Optional.of(customFieldDTO));
    }

    @Override
    @DeleteMapping("/customFields/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Void> deleteCustomField(@PathVariable Long id) {
        log.debug("REST request to delete CustomField: {}", id);
        Optional<CustomField> customField = customFieldManager.getCustomField(id);
        if (customField.isPresent()) {
            customFieldManager.deleteCustomField(customField.get());

            return ResponseEntity
                .noContent()
                .headers(HeaderUtil.createAlert(applicationName, "customfieldManagement" + ".deleted", id.toString()))
                .build();
        }
        return ResponseEntity
            .notFound()
            .headers(HeaderUtil.createAlert(applicationName, "customfieldManagement" + ".notFound", id.toString()))
            .build();
    }

    @Override
    @GetMapping("/customFields/availableTypes")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public Set<String> getAvailableCustomFieldTypes() {
        return customFieldTypeRegistry.getMappingForTypeKeyAndTypeName().keySet();
    }

    @Override
    @GetMapping(value = "/customFields/value", params = { "documentId", "customFieldId" })
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public Object getCustomFieldValue(
        @RequestParam(value = "documentId") Long documentId,
        @RequestParam(value = "customFieldId") Long customFieldId
    ) {
        return customFieldManager.getCustomFieldValue(documentId, customFieldId);
    }

    @Override
    @PutMapping(value = "/customFields/value", params = { "documentId", "customFieldId", "value" })
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public void setCustomFieldValue(
        @RequestParam(value = "documentId") Long documentId,
        @RequestParam(value = "customFieldId") Long customFieldId,
        @RequestParam(value = "value") Object value
    ) {
        customFieldManager.setCustomFieldValue(documentId, customFieldId, value);
    }

    @GetMapping(value = "/customFields/relatedToDocument", params = { "documentId" })
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<List<CustomFieldDTO>> getCustomFieldsWhichAreRelatedWithDocument(
        @RequestParam(value = "documentId") Long documentId
    ) {
        Optional<Document> optionalDocument = documentManager.getDocument(documentId);
        if (optionalDocument.isPresent()) {
            List<CustomFieldDTO> dtos = customFieldManager
                .getAllCustomFieldsRelatedWithDocument(optionalDocument.get())
                .stream()
                .sorted(Comparator.comparing(CustomField::getId))
                .map(CustomFieldDTO::new)
                .collect(Collectors.toList());

            return new ResponseEntity<>(dtos, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
