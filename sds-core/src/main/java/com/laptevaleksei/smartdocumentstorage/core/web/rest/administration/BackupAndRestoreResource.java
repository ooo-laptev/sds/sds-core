/*
 * Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.web.rest.administration;

import com.laptevaleksei.smartdocumentstorage.core.domain.AbstractEntity;
import com.laptevaleksei.smartdocumentstorage.core.domain.administration.BackUp;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.CustomField;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.Document;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.DocumentTemplate;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.customfield.*;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.template.*;
import com.laptevaleksei.smartdocumentstorage.core.manager.BackupManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;

/**
 * This is a resource for backing-up and restoring system data
 *
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 05-Sep-2021
 */
@RestController
@RequestMapping("/api")
public class BackupAndRestoreResource {

    private final Logger log = LoggerFactory.getLogger(BackupAndRestoreResource.class);
    private final BackupManager backupManager;

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    public BackupAndRestoreResource(BackupManager backupManager) {
        this.backupManager = backupManager;
    }

    @GetMapping(path = "/backup")
    public void createBackUp() {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(Instant.now() + "_backUp.dat"))) {
            BackUp backUp = backupManager.backUp();
            oos.writeObject(backUp);
        } catch (Exception ex) {
            log.error(ex.getLocalizedMessage());
        }
    }

    @PostMapping(path = "/restore")
    public void restore(@RequestPart("file") MultipartFile backUp) {
        try (ObjectInputStream ois = new SecureObjectInputStream(backUp.getInputStream())) {
            BackUp backUpSource = (BackUp) ois.readObject();
            backupManager.restore(backUpSource);
        } catch (Exception ex) {
            log.error(ex.getLocalizedMessage());
        }
    }

    private static class SecureObjectInputStream extends ObjectInputStream {

        public SecureObjectInputStream(InputStream in) throws IOException {
            super(in);
        }

        @Override
        protected Class<?> resolveClass(ObjectStreamClass osc) throws IOException, ClassNotFoundException {
            if (!isAuthorizedClass(osc.getName())) {
                throw new InvalidClassException("Unauthorized deserialization", osc.getName());
            }
            return super.resolveClass(osc);
        }

        private boolean isAuthorizedClass(String className) {
            if (className.equals(AbstractEntity.class.getName())) return true;
            if (className.equals(BackUp.class.getName())) return true;
            if (className.equals(ArrayList.class.getName())) return true;
            if (className.equals(Document.class.getName())) return true;
            if (className.equals(DocumentTemplate.class.getName())) return true;
            if (className.equals(Template.class.getName())) return true;
            if (className.equals(TemplateLayout.class.getName())) return true;
            if (className.equals(TemplateDefaultCustomFieldLayout.class.getName())) return true;
            if (className.equals(FieldBlock.class.getName())) return true;
            if (className.equals(FieldBlockLayout.class.getName())) return true;
            if (className.equals(CustomField.class.getName())) return true;
            if (className.equals(AbstractCustomFieldValue.class.getName())) return true;
            if (className.equals(CustomFieldValueString.class.getName())) return true;
            if (className.equals(CustomFieldValueBoolean.class.getName())) return true;
            if (className.equals(CustomFieldValueImage.class.getName())) return true;
            if (className.equals(CustomFieldValueNumber.class.getName())) return true;
            if (className.equals(CustomFieldValueDate.class.getName())) return true;

            if (className.equals(Timestamp.class.getName())) return true;
            if (className.equals("java.time.Ser")) return true;
            if (className.equals(Date.class.getName())) return true;
            if (className.equals(Number.class.getName())) return true;
            if (className.equals(Long.class.getName())) return true;
            return className.equals(Integer.class.getName());
        }
    }
}
