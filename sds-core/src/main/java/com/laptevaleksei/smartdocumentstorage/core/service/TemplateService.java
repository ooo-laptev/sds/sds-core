/*
 * Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.service;

import com.laptevaleksei.smartdocumentstorage.core.domain.entity.CustomField;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.Document;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.DocumentTemplate;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.template.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing of {@link Template}.
 *
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 17-Mar-2020
 */
public interface TemplateService {
    /**
     * Create a template.
     *
     * @param template the entity to save.
     *
     * @return the persisted entity.
     */
    Template createTemplate(Template template);

    /**
     * Get all the templates.
     *
     * @return the list of entities.
     */
    List<Template> getAllTemplates();

    /**
     * Gets template by id.
     *
     * @param id the id of the entity.
     *
     * @return Optional of Template.
     */
    Optional<Template> getTemplate(Long id);

    /**
     * Gets template by id.
     *
     * @param template the template for updating.
     *
     * @return Optional of Template.
     */
    Optional<Template> updateTemplate(Template template);

    /**
     * Pageable representation of {@link Template} list.
     *
     * @param pageable Parameters for {@link Page} generation.
     *
     * @return Returns page with list of {@link Template}s.
     */
    Page<Template> getAllManagedTemplates(Pageable pageable);

    /**
     * This method just delete Template in storage.
     *
     * @param template Template which should be deleted.
     */
    void deleteTemplate(Template template);

    /**
     * This method will check that template with specified ID is exists.
     *
     * @param id ID of {@link Template}.
     *
     * @return TRUE if {@link Template}. with specified ID is exists.
     */
    boolean templateIsExists(Long id);

    /**
     * This method provides all Documents which related with specified Template.
     *
     * @param template Template for searching relations.
     *
     * @return Page of related documents with specified Template.
     */
    Page<Document> getAllDocumentRelatedWithTemplate(Pageable pageable, Template template);

    /**
     * This method provides related FieldBlocks with specified Template.
     *
     * @param template Specify Template for searching related FieldBlocks.
     *
     * @return Returns list of related FieldBlock objects.
     */
    List<FieldBlock> getAllFieldBlockRelatedWithTemplate(Template template);

    /**
     * This method try to find FieldBlocks related with specified CustomField.
     *
     * @param customField We want to find FieldBlocks related with this CustomField.
     *
     * @return List of related FieldBlocks.
     */
    List<FieldBlock> getAllFieldBlockRelatedWithCustomField(CustomField customField);

    /**
     * Create a template layout.
     *
     * @param templateLayout the entity to save.
     *
     * @return the persisted entity.
     */
    TemplateLayout createTemplateLayout(TemplateLayout templateLayout);

    /**
     * Gets all stored template layout entities.
     *
     * @return the list of entities.
     */
    List<TemplateLayout> getAllTemplateLayouts();

    /**
     * Gets template layout by id.
     *
     * @param id the id of the entity.
     *
     * @return Optional of TemplateLayout.
     */
    Optional<TemplateLayout> getTemplateLayoutById(Long id);

    /**
     * Updates TemplateLayout entity
     *
     * @param templateLayout entity which you want to update
     */
    void updateTemplateLayout(TemplateLayout templateLayout);

    /**
     * This method just delete TemplateLayout in storage.
     *
     * @param templateLayout TemplateLayout which should be deleted.
     */
    void deleteTemplateLayout(TemplateLayout templateLayout);

    /**
     * This method provides all TemplateLayout objects related with specified template.
     *
     * @param template Specify exists Template instance for searching layout objects.
     *
     * @return Returns list of related FieldBlock objects.
     */
    List<TemplateLayout> getTemplateLayouts(Template template);

    void changeFieldBlockPosition(Template template, FieldBlock fieldBlock, Integer position);

    /**
     * This method provides related Template with specified TemplateLayout.
     *
     * @param templateLayout the object for which you want to find related
     *
     * @return related Template.
     */
    Template getTemplateRelatedWithTemplateLayout(TemplateLayout templateLayout);

    /**
     * Create a documentTemplate
     *
     * @param documentTemplate the entity to save.
     *
     * @return the persisted entity.
     */
    DocumentTemplate createDocumentTemplate(DocumentTemplate documentTemplate);

    /**
     * Get the all documentTemplate.
     *
     * @return the list of entities.
     */
    List<DocumentTemplate> getAllDocumentTemplate();

    /**
     * THis method provides FieldBlockLayout entity by ID.
     *
     * @param id the id of entity.
     *
     * @return FieldBlockLayout object.
     */
    Optional<DocumentTemplate> getDocumentTemplateById(Long id);

    /**
     * Update a documentTemplate
     *
     * @param documentTemplate the entity to save.
     *
     * @return the persisted entity.
     */
    DocumentTemplate updateDocumentTemplate(DocumentTemplate documentTemplate);

    /**
     * This method delete FieldBlockLayout item from storage.
     *
     * @param documentTemplate documentTemplate which should be deleted.
     */
    void deleteDocumentTemplate(DocumentTemplate documentTemplate);

    /**
     * You can use this method if you want to know that FieldBlock is exists in Template or not.
     *
     * @param template Try to find FieldBlock in this Template.
     * @param fieldBlock This FieldBlock we try to find in specified Template.
     *
     * @return TRUE if FieldBlock is exists in Template, FALSE if not.
     */
    boolean fieldBlockIsExistsInTemplateLayout(Template template, FieldBlock fieldBlock);

    /**
     * Create a fieldBlock
     *
     * @param fieldBlock the entity to save.
     *
     * @return the persisted entity.
     */
    FieldBlock createFieldBlock(FieldBlock fieldBlock);

    /**
     * Get all the fieldBlock.
     *
     * @return the list of entities.
     */
    List<FieldBlock> getAllFieldBlock();

    /**
     * Get the "id" fieldBlock.
     *
     * @param id the id of the entity.
     *
     * @return the entity.
     */
    Optional<FieldBlock> getFieldBlock(Long id);

    /**
     * Just deletes FieldBlockLayout item from storage.
     *
     * @param fieldBlock fieldBlock which should be deleted.
     */
    void deleteFieldBlock(FieldBlock fieldBlock);

    /**
     * This method provides related FieldBlockLayout for specified FieldBlock.
     *
     * @param fieldBlock Specify FieldBlockLayout for searching related FieldBlock.
     *
     * @return Returns list of related fieldBlock objects.
     */
    List<FieldBlockLayout> getFieldBlockLayouts(FieldBlock fieldBlock);

    /**
     * Create a field block layout.
     *
     * @param fieldBlockLayout the entity to save.
     *
     * @return the persisted entity.
     */
    FieldBlockLayout createFieldBlockLayout(FieldBlockLayout fieldBlockLayout);

    /**
     * Updates {@link FieldBlockLayout}
     *
     * @param fieldBlockLayout Instance which you want to update.
     */
    void updateFieldBlockLayout(FieldBlockLayout fieldBlockLayout);

    /**
     * Get all stored field block layout entities.
     *
     * @return the list of entities.
     */
    List<FieldBlockLayout> getAllFieldBlockLayouts();

    /**
     * Gets field block layout by id.
     *
     * @param id the id of the entity.
     *
     * @return Optional of FieldBlockLayout.
     */
    Optional<FieldBlockLayout> getFieldBlockLayoutById(Long id);

    /**
     * This method just delete FieldBlockLayout in storage.
     *
     * @param fieldBlockLayout FieldBlockLayout which should be deleted.
     */
    void deleteFieldBlockLayout(FieldBlockLayout fieldBlockLayout);

    /**
     * This method provides related CustomField with specified FieldBlock.
     *
     * @param fieldBlock Specify CustomField for searching related FieldBlock.
     *
     * @return Returns list of related fieldBlock objects.
     */
    List<CustomField> getAllCustomFieldRelatedWithFieldBlock(FieldBlock fieldBlock);

    /**
     * You can use this method if you want to know that CustomField is exists in FieldBlock or not.
     *
     * @param fieldBlock Try to find CustomField in this FieldBlock.
     * @param customField This CustomField we try to find in specified FieldBlock.
     *
     * @return TRUE if CustomField is exists in FieldBLock, FALSE if not.
     */
    boolean customFieldIsExistsInFieldBlockLayout(FieldBlock fieldBlock, CustomField customField);

    /**
     * This method provides information about relation between document and template.
     *
     * @param document Specify document for searching relationship.
     * @param template Specify template for searching relationship.
     *
     * @return Returns TRUE if relation with template is exists.
     */
    boolean isDocumentAssignedToTemplate(Document document, Template template);

    /**
     * Assigns specified {@link Template} to {@link Document}
     *
     * @param document {@link Document} which should be attached to the {@link Template}.
     * @param template {@link Template} where should be attached {@link Document}.
     */
    void assignTemplateToDocument(Document document, Template template);

    /**
     * Provides templates which used by specified document.
     *
     * @param document Document for searching relationship.
     *
     * @return List of related templates.
     */
    List<Template> getTemplatesAssignedToDocument(Document document);

    /**
     * Provides relationships of {@link Document} with {@link Template}s.
     *
     * @param document {@link Document} for searching relationships.
     *
     * @return List of relationships.
     */
    List<DocumentTemplate> getDocumentTemplateRelationshipsByDocument(Document document);

    /**
     * Use this method if you want to check that CustomField required in specified FieldBlock.
     *
     * @param fieldBlock {@link FieldBlock} for checking requirements.
     * @param customField {@link CustomField} for checking requirements.
     *
     * @return TRUE if CustomField required in FieldBlock.
     */
    boolean customFieldIsRequiredInFieldBlock(FieldBlock fieldBlock, CustomField customField);

    /**
     * @param pageable Page configuration.
     *
     * @return Page of {@link FieldBlock}s.
     */
    Page<FieldBlock> getAllManagedFieldBlocks(Pageable pageable);

    /**
     * Provides information about existence of {@link FieldBlock}
     *
     * @param id ID which you want to check.
     *
     * @return Returns TRUE - if {@link FieldBlock} is exists with specified ID.
     */
    boolean fieldBlockIsExists(Long id);

    /**
     * This method updates data about {@link FieldBlock} (like name, description, etc.).
     *
     * @param fieldBlock {@link FieldBlock} which you want to update.
     *
     * @return Optional of {@link FieldBlock} which was updated.
     */
    Optional<FieldBlock> updateFieldBlock(FieldBlock fieldBlock);

    /**
     * Provides list of {@link Document}s which uses specified {@link Template}.
     *
     * @param template {@link Template} dor investigation.
     *
     * @return List of {@link Document}s.
     */
    Page<Document> getDocumentsWhichUsesTemplate(Pageable pageable, Template template);

    /**
     * Provides a list of {@link Document}s which uses specified {@link Template}.
     *
     * @param template {@link Template} for searching {@link Document}s.
     *
     * @return List of {@link Document}s.
     */
    List<Document> getDocumentsWhichUsesTemplate(Template template);

    /**
     * Removes relationship between {@link Document} and {@link Template}.
     *
     * @param document {@link Document} for searching relationship.
     * @param template {@link Template} for searching relationship.
     */
    void removeDocumentTemplateRelationship(Document document, Template template);

    /**
     * Provides templates which are related with specified {@link FieldBlock}.
     *
     * @param fieldBlock {@link FieldBlock} for searching relationship.
     *
     * @return List of related {@link Template}s.
     */
    List<Template> getTemplatesRelatedWithFieldBlock(FieldBlock fieldBlock);

    /**
     * Provides {@link FieldBlock}s which are related with {@link CustomField}.
     *
     * @param pageable Page parameters.
     * @param customField {@link CustomField} which should be used for search.
     *
     * @return Page of related {@link FieldBlock}s.
     */
    Page<FieldBlock> getFieldBlocksRelatedWithCustomFields(Pageable pageable, CustomField customField);

    /**
     * Provides {@link FieldBlock}s which are related with {@link CustomField}.
     *
     * @param customField {@link CustomField} which should be used for search.
     *
     * @return List of related {@link FieldBlock}s.
     */
    List<FieldBlock> getFieldBlocksRelatedWithCustomFields(CustomField customField);

    /**
     * Provides {@link Template}s which are related with {@link FieldBlock}.
     *
     * @param pageable Page parameters.
     * @param fieldBlock {@link FieldBlock} which should be used for search.
     *
     * @return Page of related {@link Template}s.
     */
    Page<Template> getRelatedTemplates(Pageable pageable, FieldBlock fieldBlock);

    /**
     * Provides {@link Template}s which are related with {@link FieldBlock}.
     *
     * @param fieldBlock {@link FieldBlock} which should be used for search.
     *
     * @return Page of related {@link Template}s.
     */
    List<Template> getRelatedTemplates(FieldBlock fieldBlock);

    /**
     * Provides list of {@link CustomField}s which is related with {@link Template}.
     *
     * @param template Specify {@link Template} where system should get default {@link CustomField}s.
     *
     * @return List of {@link CustomField}s.
     */
    List<CustomField> getDefaultCustomFields(Template template);

    /**
     * You can add {@link CustomField} to list of defaults.
     *
     * @param template Specify {@link Template} where system should add default {@link CustomField}s.
     * @param customField {@link CustomField} which will be added to list of defaults.
     *
     * @return List of {@link CustomField}s.
     */
    List<CustomField> addDefaultCustomField(Template template, CustomField customField);

    /**
     * Removes {@link CustomField} from list of defaults.
     *
     * @param template Specify {@link Template} where system should remove default {@link CustomField}s.
     * @param customField {@link CustomField} which will be removed from list of defaults.
     */
    void removeDefaultCustomField(Template template, CustomField customField);

    /**
     * Changes position of default {@link CustomField}.
     *
     * @param template Specify {@link Template} where system should move default {@link CustomField}s.
     * @param customField {@link CustomField} which will be removed from list of defaults.
     * @param position New position for {@link CustomField}.
     */
    void changeDefaultCustomFieldPosition(Template template, CustomField customField, Integer position);

    /**
     * Provides all available {@link TemplateDefaultCustomFieldLayout}s.
     *
     * @return List of all available {@link TemplateDefaultCustomFieldLayout}s.
     */
    List<TemplateDefaultCustomFieldLayout> getAllTemplateDefaultCustomFieldLayout();

    /**
     * Provides all available {@link TemplateDefaultCustomFieldLayout}s.
     *
     * @return List of available {@link DocumentTemplate}s.
     */
    List<DocumentTemplate> getAllDocumentTemplates();
}
