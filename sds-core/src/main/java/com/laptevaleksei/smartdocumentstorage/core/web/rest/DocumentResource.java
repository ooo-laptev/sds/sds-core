/*
 * Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.web.rest;

import com.laptevaleksei.smartdocumentstorage.core.api.exception.DocumentException;
import com.laptevaleksei.smartdocumentstorage.core.api.exception.DocumentNotFoundException;
import com.laptevaleksei.smartdocumentstorage.core.api.exception.TemplateException;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.Document;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.template.Template;
import com.laptevaleksei.smartdocumentstorage.core.security.AuthoritiesConstants;
import com.laptevaleksei.smartdocumentstorage.core.web.dto.DocumentDTO;
import com.laptevaleksei.smartdocumentstorage.core.web.rest.api.document.DocumentManagementApiResource;
import com.laptevaleksei.smartdocumentstorage.core.web.rest.api.document.DocumentUserApiResource;
import com.laptevaleksei.smartdocumentstorage.core.web.rest.jhipster.errors.BadRequestAlertException;
import com.laptevaleksei.smartdocumentstorage.manager.DocumentManager;
import com.laptevaleksei.smartdocumentstorage.manager.TemplateManager;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class DocumentResource implements DocumentManagementApiResource, DocumentUserApiResource {

    private static final List<String> ALLOWED_ORDERED_PROPERTIES = List.of("id", "created");

    private final Logger log = LoggerFactory.getLogger(DocumentResource.class);
    private final DocumentManager documentManager;
    private final TemplateManager templateManager;

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    public DocumentResource(DocumentManager documentManager, TemplateManager templateManager) {
        this.documentManager = documentManager;
        this.templateManager = templateManager;
    }

    @Override
    @PostMapping("/documents")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<DocumentDTO> createDocument(@Valid @RequestBody DocumentDTO documentDTO) throws URISyntaxException {
        log.debug("REST request to save Document : {}", documentDTO);

        if (documentDTO.getId() != null) {
            throw new BadRequestAlertException("A new document cannot already have an ID", "documentManagement", "idexists");
        } else {
            var newDocument = documentManager.createNewDocument();

            newDocument.setCreated(documentDTO.getCreated());
            documentManager.updateDocument(newDocument);
            List<Template> templatesList = new ArrayList<>();

            if (
                documentDTO.getAssignedTemplates() != null &&
                !documentDTO.getAssignedTemplates().isEmpty() &&
                documentDTO.getAssignedTemplates().get(0) != 0
            ) {
                for (Long templateId : documentDTO.getAssignedTemplates()) {
                    templatesList.add(templateManager.getTemplate(templateId).orElseThrow(TemplateException::new));
                }

                for (Template template : templatesList) {
                    templateManager.assignTemplateToDocument(newDocument, template);
                }
            }

            DocumentDTO dto = DocumentDTO.createDtoFromRealObject(newDocument);
            dto.setAssignedTemplates(new ArrayList<>());
            dto.getAssignedTemplates().addAll(documentDTO.getAssignedTemplates());

            return ResponseEntity
                .created(new URI("/api/documents/" + newDocument.getId()))
                .headers(HeaderUtil.createAlert(applicationName, "documentManagement.created", String.valueOf(newDocument.getId())))
                .body(dto);
        }
    }

    @Override
    @PutMapping("/documents")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<DocumentDTO> updateDocument(@Valid @RequestBody DocumentDTO documentDTO) {
        log.debug("REST request to update Document : {}", documentDTO);
        if (!documentManager.documentIsExists(documentDTO.getId())) {
            throw new DocumentNotFoundException("Document with id=" + documentDTO.getId() + " does not exists!");
        }
        try {
            List<Long> idsOfNewAssignedTemplates = documentDTO.getAssignedTemplates();
            if (idsOfNewAssignedTemplates != null) {
                Document doc = documentManager.getDocument(documentDTO.getId()).orElseThrow();
                List<Template> oldAssignedTemplatesList = templateManager.getTemplatesAssignedToDocument(doc);
                if (oldAssignedTemplatesList != null && !oldAssignedTemplatesList.isEmpty()) {
                    oldAssignedTemplatesList.forEach(
                        oldTemplate -> {
                            if (!idsOfNewAssignedTemplates.contains(oldTemplate.getId())) {
                                templateManager.unassignTemplateFromDocument(doc, oldTemplate);
                            }
                        }
                    );
                    idsOfNewAssignedTemplates.forEach(
                        id -> {
                            if (oldAssignedTemplatesList.stream().noneMatch(t -> t.getId().equals(id))) {
                                Template newTemplate = null;
                                try {
                                    newTemplate = templateManager.getTemplate(id).orElseThrow(TemplateException::new);
                                } catch (TemplateException e) {
                                    log.error(e.getLocalizedMessage());
                                }
                                templateManager.assignTemplateToDocument(doc, newTemplate);
                            }
                        }
                    );
                } else {
                    documentDTO
                        .getAssignedTemplates()
                        .forEach(
                            templateId -> {
                                Template template;
                                try {
                                    template = templateManager.getTemplate(templateId).orElseThrow(TemplateException::new);
                                    templateManager.assignTemplateToDocument(doc, template);
                                } catch (TemplateException e) {
                                    log.error(e.getLocalizedMessage());
                                }
                            }
                        );
                }
            }
            documentDTO = fillDocumentDTO(documentManager.updateDocument(documentDTO.createRealObject()));
        } catch (DocumentException e) {
            log.error(e.getLocalizedMessage());
        }
        Optional<DocumentDTO> updatedDocument = Optional.of(documentDTO);

        return ResponseUtil.wrapOrNotFound(
            updatedDocument,
            HeaderUtil.createAlert(applicationName, "documentManagement.updated", documentDTO.getId().toString())
        );
    }

    @Override
    @GetMapping("/documents")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<List<DocumentDTO>> getAllDocuments(Pageable pageable) {
        if (!onlyContainsAllowedProperties(pageable)) {
            return ResponseEntity.badRequest().build();
        }
        final Page<DocumentDTO> page = documentManager.getAllDocuments(pageable).map(this::fillDocumentDTO);
        var headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    private boolean onlyContainsAllowedProperties(Pageable pageable) {
        return pageable.getSort().stream().map(Sort.Order::getProperty).allMatch(ALLOWED_ORDERED_PROPERTIES::contains);
    }

    @Override
    @GetMapping("/documents/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<DocumentDTO> getDocument(@PathVariable Long id) {
        log.debug("REST request to get Document : {}", id);
        return ResponseUtil.wrapOrNotFound(Optional.of(fillDocumentDTO(documentManager.getDocument(id).orElseThrow())));
    }

    @Override
    @DeleteMapping("/documents/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<Void> deleteDocument(@PathVariable Long id) {
        log.debug("REST request to delete Document: {}", id);
        documentManager.deleteDocument(documentManager.getDocument(id).orElseThrow());
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createAlert(applicationName, "documentManagement" + ".deleted", id.toString()))
            .build();
    }

    /**
     * @param document Document which you want to use for DTO filling.
     *
     * @return Completed DTO.
     */
    public DocumentDTO fillDocumentDTO(Document document) {
        var dto = DocumentDTO.createDtoFromRealObject(document);
        List<Template> assignedTemplates = templateManager.getTemplatesAssignedToDocument(document);
        if (assignedTemplates != null && !assignedTemplates.isEmpty()) {
            dto.setAssignedTemplates(assignedTemplates.stream().map(Template::getId).collect(Collectors.toList()));
        } else {
            dto.setAssignedTemplates(new ArrayList<>());
        }
        return dto;
    }
}
