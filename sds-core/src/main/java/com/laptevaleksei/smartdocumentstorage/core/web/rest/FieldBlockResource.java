/*
 * Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.web.rest;

import com.laptevaleksei.smartdocumentstorage.core.domain.entity.CustomField;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.template.FieldBlock;
import com.laptevaleksei.smartdocumentstorage.core.security.AuthoritiesConstants;
import com.laptevaleksei.smartdocumentstorage.core.web.dto.CustomFieldDTO;
import com.laptevaleksei.smartdocumentstorage.core.web.dto.customfield.extra.ExtraCustomFieldDTO;
import com.laptevaleksei.smartdocumentstorage.core.web.dto.template.FieldBlockDTO;
import com.laptevaleksei.smartdocumentstorage.core.web.dto.template.TemplateDTO;
import com.laptevaleksei.smartdocumentstorage.core.web.dto.template.extra.ExtraFieldBlockDTO;
import com.laptevaleksei.smartdocumentstorage.core.web.dto.template.extra.FieldBlockChangeLayoutRequest;
import com.laptevaleksei.smartdocumentstorage.core.web.rest.api.template.fieldblock.FieldBlockManagementApiResource;
import com.laptevaleksei.smartdocumentstorage.core.web.rest.api.template.fieldblock.FieldBlockUserApiResource;
import com.laptevaleksei.smartdocumentstorage.core.web.rest.jhipster.errors.BadRequestAlertException;
import com.laptevaleksei.smartdocumentstorage.core.web.rest.jhipster.errors.IdAlreadyUsedException;
import com.laptevaleksei.smartdocumentstorage.manager.CustomFieldManager;
import com.laptevaleksei.smartdocumentstorage.manager.TemplateManager;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 15-Mar-2021
 */
@RestController
@RequestMapping(path = "/api")
public class FieldBlockResource implements FieldBlockManagementApiResource, FieldBlockUserApiResource {

    private static final List<String> ALLOWED_ORDERED_PROPERTIES = List.of("id", "name", "description");

    private static final String FIELDBLOCK_CREATED = "fieldBlockManagement.created";
    private static final String FIELDBLOCK_UPDATED = "fieldBlockManagement.updated";
    private static final String FIELDBLOCK_REMOVED = "fieldBlockManagement.deleted";

    private final Logger log = LoggerFactory.getLogger(FieldBlockResource.class);
    private final TemplateManager templateManager;
    private final CustomFieldManager customFieldManager;

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    public FieldBlockResource(TemplateManager templateManager, CustomFieldManager customFieldManager) {
        this.templateManager = templateManager;
        this.customFieldManager = customFieldManager;
    }

    @Override
    @PostMapping("/fieldBlocks")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<FieldBlock> createFieldBlock(@Valid @RequestBody FieldBlockDTO fieldBlockDTO) throws URISyntaxException {
        log.debug("REST request to save FieldBlock : {}", fieldBlockDTO);

        if (fieldBlockDTO.getId() != null) {
            throw new BadRequestAlertException("A new field block cannot already have an ID", "fieldBlockManagement", "id exists");
        } else {
            FieldBlock fieldBlock = templateManager.createFieldBlock(fieldBlockDTO.createRealObject());
            return ResponseEntity
                .created(new URI("/api/fieldBlocks/" + fieldBlock.getId()))
                .headers(HeaderUtil.createAlert(applicationName, FIELDBLOCK_CREATED, String.valueOf(fieldBlock.getId())))
                .body(fieldBlock);
        }
    }

    @Override
    @GetMapping("/fieldBlocks/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<FieldBlockDTO> getFieldBlock(@PathVariable Long id) {
        log.debug("REST request to get FieldBlock : {}", id);
        return ResponseUtil.wrapOrNotFound(templateManager.getFieldBlock(id).map(FieldBlockDTO::new));
    }

    @Override
    @GetMapping("/fieldBlocks")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<List<FieldBlockDTO>> getAllFieldBlock(Pageable pageable) {
        if (!onlyContainsAllowedProperties(pageable)) {
            return ResponseEntity.badRequest().build();
        }

        final Page<FieldBlockDTO> page = templateManager.getAllManagedFieldBlocks(pageable).map(FieldBlockDTO::new);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @Override
    @PutMapping("/fieldBlocks")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<FieldBlockDTO> updateFieldBlock(@Valid @RequestBody FieldBlockDTO fieldBlockDTO) throws IdAlreadyUsedException {
        log.debug("REST request to update FieldBlock : {}", fieldBlockDTO);
        if (!templateManager.fieldBlockIsExists(fieldBlockDTO.getId())) {
            throw new IdAlreadyUsedException();
        }
        Optional<FieldBlockDTO> updatedFieldBlock = templateManager
            .updateFieldBlock(fieldBlockDTO.createRealObject())
            .map(FieldBlockDTO::createDtoFromRealObject);

        return ResponseUtil.wrapOrNotFound(
            updatedFieldBlock,
            HeaderUtil.createAlert(applicationName, FIELDBLOCK_UPDATED, fieldBlockDTO.getId().toString())
        );
    }

    @Override
    @DeleteMapping("/fieldBlocks/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Void> deleteFieldBlock(@PathVariable Long id) {
        log.debug("REST request to delete FieldBlock: {}", id);
        Optional<FieldBlock> fieldBlock = templateManager.getFieldBlock(id);

        fieldBlock.ifPresent(templateManager::deleteFieldBlock);
        return ResponseEntity.noContent().headers(HeaderUtil.createAlert(applicationName, FIELDBLOCK_REMOVED, id.toString())).build();
    }

    @Override
    @GetMapping("/fieldBlockLayout")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<ExtraFieldBlockDTO> getFieldBlockLayout(FieldBlockChangeLayoutRequest changeRequest) {
        Optional<FieldBlock> fieldBlock = templateManager.getFieldBlock(changeRequest.getFieldBlockId());
        ExtraFieldBlockDTO extraFieldBlockDTO = new ExtraFieldBlockDTO();
        if (fieldBlock.isPresent()) {
            extraFieldBlockDTO = fillExtraFieldBlockDTO(fieldBlock.get());
        }

        return ResponseUtil.wrapOrNotFound(Optional.of(extraFieldBlockDTO));
    }

    @Override
    @PutMapping("/fieldBlockLayout")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<ExtraFieldBlockDTO> addItemToFieldBlockLayout(FieldBlockChangeLayoutRequest changeRequest) {
        Optional<FieldBlock> fieldBlock = templateManager.getFieldBlock(changeRequest.getFieldBlockId());
        ExtraFieldBlockDTO extraFieldBlockDTO = new ExtraFieldBlockDTO();
        if (fieldBlock.isPresent()) {
            Optional<CustomField> customField = customFieldManager.getCustomField(changeRequest.getCustomFieldId());
            if (customField.isPresent()) {
                templateManager.addCustomFieldToTheFieldBlock(fieldBlock.get(), customField.get(), false);

                extraFieldBlockDTO = fillExtraFieldBlockDTO(fieldBlock.get());
            }
        }
        return ResponseUtil.wrapOrNotFound(
            Optional.of(extraFieldBlockDTO),
            HeaderUtil.createAlert(applicationName, FIELDBLOCK_UPDATED, changeRequest.getFieldBlockId().toString())
        );
    }

    @Override
    @PatchMapping("/fieldBlockLayout")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<ExtraFieldBlockDTO> moveItemInFieldBlockLayout(FieldBlockChangeLayoutRequest changeRequest) {
        Optional<FieldBlock> fieldBlock = templateManager.getFieldBlock(changeRequest.getFieldBlockId());
        ExtraFieldBlockDTO extraFieldBlockDTO = new ExtraFieldBlockDTO();
        if (fieldBlock.isPresent()) {
            Optional<CustomField> customField = customFieldManager.getCustomField(changeRequest.getCustomFieldId());
            if (customField.isPresent()) {
                templateManager.changeCustomFieldPosition(fieldBlock.get(), customField.get(), changeRequest.getPosition());

                extraFieldBlockDTO = fillExtraFieldBlockDTO(fieldBlock.get());
            }
        }
        return ResponseUtil.wrapOrNotFound(
            Optional.of(extraFieldBlockDTO),
            HeaderUtil.createAlert(applicationName, FIELDBLOCK_UPDATED, changeRequest.getFieldBlockId().toString())
        );
    }

    @Override
    @DeleteMapping("/fieldBlockLayout")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<ExtraFieldBlockDTO> deleteItemFromFieldBlockLayout(FieldBlockChangeLayoutRequest changeRequest) {
        Optional<FieldBlock> fieldBlock = templateManager.getFieldBlock(changeRequest.getFieldBlockId());
        ExtraFieldBlockDTO extraFieldBlockDTO = new ExtraFieldBlockDTO();
        if (fieldBlock.isPresent()) {
            Optional<CustomField> customField = customFieldManager.getCustomField(changeRequest.getCustomFieldId());
            if (customField.isPresent()) {
                templateManager.removeCustomFieldFromTheFieldBlockLayout(fieldBlock.get(), customField.get());

                extraFieldBlockDTO = fillExtraFieldBlockDTO(fieldBlock.get());
            }
        }
        return ResponseUtil.wrapOrNotFound(
            Optional.of(extraFieldBlockDTO),
            HeaderUtil.createAlert(applicationName, FIELDBLOCK_UPDATED, changeRequest.getFieldBlockId().toString())
        );
    }

    @Override
    @GetMapping("/getRelatedTemplates")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<List<TemplateDTO>> getRelatedTemplates(Pageable pageable, Long fieldBlockId) {
        Optional<FieldBlock> possibleFieldBlock = templateManager.getFieldBlock(fieldBlockId);

        if (possibleFieldBlock.isPresent()) {
            final Page<TemplateDTO> page = templateManager
                .getRelatedTemplates(pageable, possibleFieldBlock.get())
                .map(TemplateDTO::createDtoFromRealObject);
            HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
            return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
        }

        Page<TemplateDTO> page = Page.empty(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @Override
    @GetMapping("/getFieldBlocksRelatedWithCustomField")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<List<FieldBlockDTO>> getFieldBlocksRelatedWithCustomField(Pageable pageable, Long customFieldId) {
        Optional<CustomField> possibleCustomField = customFieldManager.getCustomField(customFieldId);

        if (possibleCustomField.isPresent()) {
            final Page<FieldBlockDTO> page = templateManager
                .getFieldBlocksRelatedWithCustomFields(pageable, possibleCustomField.get())
                .map(FieldBlockDTO::createDtoFromRealObject);
            HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
            return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
        }

        Page<FieldBlockDTO> page = Page.empty(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * TODO: it should be refactored! It's temporary solution.
     * <p>
     * // get all CFs available in system
     * // minus
     * // get all customfields which ix used in current template
     * // return the remaining
     * // NOTE: should not be paginated!
     *
     * @return List of available CustomFields.
     */
    @Override
    @GetMapping("/fieldblock/getAvailableCustomFields")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public List<CustomFieldDTO> getAvailableCustomFieldsForAddingToFieldBLock() {
        return customFieldManager.getAllCustomFields().stream().map(CustomFieldDTO::new).collect(Collectors.toList());
    }

    @GetMapping("/fieldblock/getRelatedCustomFieldsWithFieldBlock/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<List<CustomFieldDTO>> getRelatedCustomFieldsWithFieldBlock(@PathVariable Long id) {
        Optional<FieldBlock> fieldBlock = templateManager.getFieldBlock(id);

        if (fieldBlock.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseUtil.wrapOrNotFound(
            Optional.of(
                templateManager
                    .getRelatedCustomFieldsWithFieldBlock(fieldBlock.get())
                    .stream()
                    .map(CustomFieldDTO::new)
                    .collect(Collectors.toList())
            ),
            HeaderUtil.createAlert(applicationName, "fieldBlock.created", fieldBlock.get().getName())
        );
    }

    /**
     * This method gets layout for {@link FieldBlock} and provides extended DTO with layout.
     *
     * @param fieldBlock {@link FieldBlock} which needs for extended DTO.
     *
     * @return Filled {@link ExtraFieldBlockDTO}.
     */
    private ExtraFieldBlockDTO fillExtraFieldBlockDTO(FieldBlock fieldBlock) {
        ExtraFieldBlockDTO extraFieldBlockDTO;

        List<ExtraCustomFieldDTO> layout = templateManager
            .getRelatedCustomFieldsWithFieldBlock(fieldBlock)
            .stream()
            .map(ExtraCustomFieldDTO::new)
            .collect(Collectors.toList());

        extraFieldBlockDTO = new ExtraFieldBlockDTO(fieldBlock);
        extraFieldBlockDTO.setContent(layout);

        return extraFieldBlockDTO;
    }

    private boolean onlyContainsAllowedProperties(Pageable pageable) {
        return pageable.getSort().stream().map(Sort.Order::getProperty).allMatch(ALLOWED_ORDERED_PROPERTIES::contains);
    }
}
