/*
 * Copyright (c) 2019 - 2024, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.service.impl;

import com.laptevaleksei.smartdocumentstorage.core.domain.entity.Document;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.documentlink.DocumentLink;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.documentlink.DocumentLinkType;
import com.laptevaleksei.smartdocumentstorage.core.repository.StoreException;
import com.laptevaleksei.smartdocumentstorage.core.repository.documentlink.DocumentLinkRepository;
import com.laptevaleksei.smartdocumentstorage.core.repository.documentlink.DocumentLinkTypeRepository;
import com.laptevaleksei.smartdocumentstorage.core.service.DocumentLinkService;
import com.laptevaleksei.smartdocumentstorage.core.service.DocumentLinkTypeService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 23-Aug-2021
 */
@Service
public class DefaultDocumentLinkService implements DocumentLinkTypeService, DocumentLinkService {

    private final DocumentLinkTypeRepository documentLinkTypeRepository;
    private final DocumentLinkRepository documentLinkRepository;

    public DefaultDocumentLinkService(
            DocumentLinkTypeRepository documentLinkTypeRepository,
            DocumentLinkRepository documentLinkRepository
    ) {
        this.documentLinkTypeRepository = documentLinkTypeRepository;
        this.documentLinkRepository = documentLinkRepository;
    }

    @Override
    public DocumentLinkType createLinkType(DocumentLinkType linkType) {
        if (linkType.getId() != null) {
            throw new StoreException("You try to create DocumentLinkType with pre-defined ID!");
        }
        if (linkType.isDeleted()) {
            throw new StoreException(
                    "You are trying to create a DocumentLinkType which is already marked as deleted! It will not be saved."
            );
        }
        return documentLinkTypeRepository.save(linkType);
    }

    @Override
    public Optional<DocumentLinkType> getLinkType(Long id) {
        return documentLinkTypeRepository.findById(id).filter(documentLinkType -> !documentLinkType.isDeleted());
    }

    @Override
    public List<DocumentLinkType> getAllLinkTypes() {
        return documentLinkTypeRepository
                .findAll()
                .stream()
                .filter(documentLinkType -> !documentLinkType.isDeleted())
                .collect(Collectors.toList());
    }

    @Override
    public DocumentLinkType updateLinkType(DocumentLinkType linkType) {
        if (linkType.getId() == null) {
            throw new StoreException("Id must be specified!");
        }
        return documentLinkTypeRepository.save(linkType);
    }

    @Override
    public void deleteLinkType(DocumentLinkType linkType) {
        linkType.setDeleted(true);
        documentLinkTypeRepository.save(linkType);
    }

    @Override
    public DocumentLink createDocumentLink(DocumentLink relationShips) {
        if (relationShips.getId() != null) {
            throw new StoreException( MessageFormat.format(
                    "exception.pre_defined_document_link_id",
                    relationShips
                )
            );
        }
        if (relationShips.isDeleted()) {
            throw new StoreException( MessageFormat.format(
                    "exception.trying_to_save_deleted_document",
                    relationShips
                )
            );
        }
        return documentLinkRepository.save(relationShips);
    }

    @Override
    public Optional<DocumentLink> getDocumentLink(Long id) {
        return documentLinkRepository.findById(id).filter(documentLink -> !documentLink.isDeleted());
    }

    @Override
    public void deleteDocumentLink(DocumentLink relationShips) {
        relationShips.setDeleted(true);
        updateLink(relationShips);
    }

    public DocumentLink updateLink(DocumentLink documentLink) {
        return documentLinkRepository.save(documentLink);
    }

    @Override
    public List<DocumentLink> getLinkedDocumentsForDocument(Document document) {
        return documentLinkRepository
                .findAllLinksByDocument(document)
                .stream()
                .filter(documentLink -> !documentLink.isDeleted())
                .collect(Collectors.toList());
    }

    @Override
    public List<DocumentLink> getLinkedDocumentsForDocumentAsSource(Document sourceDocument) {
        return documentLinkRepository
                .findAllBySourceDocument(sourceDocument)
                .stream()
                .filter(documentLink -> !documentLink.isDeleted())
                .collect(Collectors.toList());
    }

    @Override
    public List<DocumentLink> getLinkedDocumentsForDocumentAsTarget(Document targetDocument) {
        return documentLinkRepository
                .findAllByTargetDocument(targetDocument)
                .stream()
                .filter(documentLink -> !documentLink.isDeleted())
                .collect(Collectors.toList());
    }

    @Override
    public Page<DocumentLinkType> getAllDocumentLinkTypes( Pageable pageable) {
        Page<DocumentLinkType> page = documentLinkTypeRepository.findAll(pageable);
        return documentLinkTypeRepository.findAll(pageable);
    }

    @Override
    public List<DocumentLink> getAllDocumentLinks() {
        return new ArrayList<>(documentLinkRepository.findAll());
    }

    @Override
    public Page<DocumentLink> getAllDocumentLinks(Pageable pageable) {
        return new PageImpl<>(documentLinkRepository.findAll(pageable)
                .stream().filter( documentLink -> !documentLink.isDeleted() )
                .toList());
    }
}
