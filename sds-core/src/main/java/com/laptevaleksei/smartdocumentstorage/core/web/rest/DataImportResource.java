/*
 * Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.web.rest;

import com.laptevaleksei.smartdocumentstorage.core.domain.entity.Document;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.template.Template;
import com.laptevaleksei.smartdocumentstorage.core.security.AuthoritiesConstants;
import com.laptevaleksei.smartdocumentstorage.core.web.rest.response.UploadFileResponse;
import com.laptevaleksei.smartdocumentstorage.manager.CustomFieldManager;
import com.laptevaleksei.smartdocumentstorage.manager.DocumentManager;
import com.laptevaleksei.smartdocumentstorage.manager.TemplateManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.Date;
import java.util.Optional;

/**
 * @author <a href="mailto:laptevalexander28@gmail.com">Alexander Laptev</a> on 20-Aug-2021
 */
@RestController
@RequestMapping("/api/dataImport")
public class DataImportResource {

    private final Logger log = LoggerFactory.getLogger(DataImportResource.class);

    private final DocumentManager documentManager;
    private final TemplateManager templateManager;
    private final CustomFieldManager customFieldManager;

    public DataImportResource(DocumentManager documentManager, TemplateManager templateManager, CustomFieldManager customfieldManager) {
        this.documentManager = documentManager;
        this.templateManager = templateManager;
        this.customFieldManager = customfieldManager;
    }

    /**
     * This method converts String csv data to 2-dim array
     *
     * @param csvData {@link String} data of csv file
     *
     * @return {@link String[][]} table of csv data
     */
    private String[][] csvToTable(String csvData) {
        Object[] arr = csvData.lines().toArray();

        String[] fileDataArray = new String[arr.length];

        for (int i = 0; i < arr.length; i++) {
            try {
                fileDataArray[i] = arr[i].toString();
            } catch (NullPointerException ex) {
                // do some default initialization
            }
        }

        String[][] fileDataTable = new String[fileDataArray.length][];

        for (int i = 0; i < fileDataArray.length; ++i) {
            fileDataTable[i] = fileDataArray[i].split(";");
        }

        return fileDataTable;
    }

    /**
     * This method handling rest requests from <i>/api/dataImport/importFileData</i>.
     * <p>
     * Request must be like: <i>/api/dataImport/importFileData?templateID={templateID}</i>.
     * In request body must be a file with export data.
     *
     * @param multipartFile {@link MultipartFile} File to process.
     * @param templateID    Template id, to be used while importing file.
     *
     * @return {@link UploadFileResponse}
     * <p>
     * Response body will contain JSON data with file name and file size.
     * The "fileDownloadUri" field will be null.
     * <p>
     * Response example:
     * <pre>
     * {
     *   "fileName": "test.csv",
     *   "fileDownloadUri": null,
     *   "contentType": "text/csv",
     *   "fileSize": 358
     * }
     * </pre>
     */
    @PostMapping("/importFileData")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public UploadFileResponse importFileData(
        @RequestParam("file") MultipartFile multipartFile,
        @RequestParam(value = "templateID") Long templateID
    ) throws IOException {
        if (!multipartFile.isEmpty()) {
            if (multipartFile.getOriginalFilename() != null && multipartFile.getOriginalFilename().endsWith("csv")) {
                String[][] table = csvToTable(new String(multipartFile.getInputStream().readAllBytes(), StandardCharsets.UTF_8));

                for (int r = 1; r < table.length; ++r) {
                    Document documentCreated = documentManager.createNewDocument();
                    if (documentCreated != null) {
                        documentCreated.setCreated(new Date(System.currentTimeMillis()));
                        Long documentCreatedID = documentCreated.getId();

                        if ((documentCreated = documentManager.updateDocument(documentCreated)) != null) {
                            Optional<Template> templateAssignedToDocument = templateManager.getTemplate(templateID);
                            if (templateAssignedToDocument.isPresent()) {
                                templateManager.assignTemplateToDocument(documentCreated, templateAssignedToDocument.get());
                            } else {
                                log.error("Can't assign template ID: " + templateID + "\nTo document ID: " + documentCreated.getId());
                                return null;
                            }
                        } else {
                            log.error("Can't update document ID:" + documentCreatedID);
                            return null;
                        }
                        for (int c = 0; c < table[0].length; ++c) {
                            try {
                                if (table[r][c] != null && !table[r][c].isEmpty() && !table[0][c].isBlank()) {
                                    customFieldManager.setCustomFieldValue(documentCreated.getId(), Long.valueOf(table[0][c]), table[r][c]);

                                    Object customFieldData = customFieldManager.getCustomFieldValue(
                                        documentCreated.getId(),
                                        Long.valueOf(table[0][c])
                                    );

                                    if (customFieldData.equals(table[r][c])) {
                                        log.debug(
                                            "Written custom field value " + table[r][c] + " with documentID: " + documentCreated.getId()
                                        );
                                    } else {
                                        //                                        log.error( "Unavailable to write data to custom field: " + Long.valueOf( table[ 0 ][ c ] ) );
                                        //                                        log.error( "Document ID: " + documentCreated.getId() );
                                        //                                        log.error( "Field:\nRow: " + ( r + 1 ) + "\nCollum: " + ( c + 1 ) );
                                        //                                        log.error( "CF value sent: " + table[ r ][ c ] );
                                        //                                        log.error( "CF value got: " + customFieldData );
                                    }
                                }
                            } catch (Exception e) {
                                log.error(e.getLocalizedMessage());
                            }
                        }
                    } else {
                        log.error("Can't create document");
                    }
                }

                return new UploadFileResponse(
                    multipartFile.getOriginalFilename(),
                    null,
                    multipartFile.getContentType(),
                    multipartFile.getSize()
                );
            }
        }
        return null;
    }
}
