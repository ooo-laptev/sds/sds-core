/*
 * Copyright (c) 2019 - 2024, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.web.rest;

import com.laptevaleksei.smartdocumentstorage.core.api.manager.DocumentLinkManagerDefaultImpl;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.documentlink.DocumentLink;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.documentlink.DocumentLinkType;
import com.laptevaleksei.smartdocumentstorage.core.security.AuthoritiesConstants;
import com.laptevaleksei.smartdocumentstorage.core.web.dto.documentlink.DocumentLinkDTO;
import com.laptevaleksei.smartdocumentstorage.core.web.dto.documentlink.DocumentLinkTypeDTO;
import com.laptevaleksei.smartdocumentstorage.core.web.rest.api.linkeddocument.LinkedDocumentsManagementApiResource;
import com.laptevaleksei.smartdocumentstorage.core.web.rest.api.linkeddocument.LinkedDocumentsUserApiResource;
import com.laptevaleksei.smartdocumentstorage.manager.DocumentManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 23-Nov-2021
 */
@RestController
@RequestMapping("/api/linkedDocument")
public class LinkedDocumentApiResource implements LinkedDocumentsManagementApiResource, LinkedDocumentsUserApiResource {

    private final Logger log = LoggerFactory.getLogger(LinkedDocumentApiResource.class);

    private final DocumentLinkManagerDefaultImpl documentLinkManagerDefaultImpl;
    private final DocumentManager documentManager;

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    public LinkedDocumentApiResource( DocumentLinkManagerDefaultImpl documentLinkManagerDefaultImpl, DocumentManager documentManager) {
        this.documentLinkManagerDefaultImpl = documentLinkManagerDefaultImpl;
        this.documentManager = documentManager;
    }

    @Override
    @PostMapping("/config/linkType")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<DocumentLinkTypeDTO> createLinkType(@RequestBody DocumentLinkTypeDTO documentLinkTypeDTO) {
        DocumentLinkTypeDTO dto = new DocumentLinkTypeDTO(
            documentLinkManagerDefaultImpl.createDocumentLinkType(
                documentLinkTypeDTO.getLinkTypeName(),
                documentLinkTypeDTO.getDescription(),
                documentLinkTypeDTO.getInwardDirectionName(),
                documentLinkTypeDTO.getOutwardDirectionName()
            )
        );

        return ResponseUtil.wrapOrNotFound(
            Optional.of(dto),
            HeaderUtil.createAlert(applicationName, "documentLinkType.created", dto.getId().toString())
        );
    }

    @Override
    @GetMapping("/config/linkType/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public DocumentLinkTypeDTO getLinkType(@PathVariable Long id) {
        Optional<DocumentLinkType> optionalDocumentLinkType = documentLinkManagerDefaultImpl.getDocumentLinkType(id);
        return optionalDocumentLinkType.map(DocumentLinkTypeDTO::new).orElse(null);
    }

    @Override
    @GetMapping("/config/linkTypes")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public List<DocumentLinkTypeDTO> getAllDocumentLinkTypes(Pageable pageable) {
        return documentLinkManagerDefaultImpl.getAllDocumentLinkTypes(pageable)
                .stream().map(DocumentLinkTypeDTO::new).collect(Collectors.toList());
    }

    @Override
    @PutMapping("/config/linkType")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<DocumentLinkTypeDTO> updateLinkType(@RequestBody DocumentLinkTypeDTO documentLinkTypeDTO) {
        DocumentLinkTypeDTO dto = new DocumentLinkTypeDTO(
            documentLinkManagerDefaultImpl.updateDocumentLinkType(documentLinkTypeDTO.createRealObject())
        );

        return ResponseUtil.wrapOrNotFound(
            Optional.of(dto),
            HeaderUtil.createAlert(applicationName, "documentLinkType.updated", dto.getId().toString())
        );
    }

    @Override
    @DeleteMapping("/config/linkType/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Void> deleteLinkType(@PathVariable("id") Long id) {
        documentLinkManagerDefaultImpl.deleteDocumentLinkType( documentLinkManagerDefaultImpl.getDocumentLinkType(id).orElseThrow());
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createAlert(applicationName, "documentLinkType.deleted", id.toString()))
            .build();
    }

    @Override
    @PostMapping("/link")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<DocumentLinkDTO> createLink(@RequestBody DocumentLinkDTO documentLinkDTO) {
        DocumentLinkDTO dto = new DocumentLinkDTO(
            documentLinkManagerDefaultImpl.linkDocuments(
                documentLinkDTO.createRealObject().getSourceDocument(),
                documentLinkDTO.createRealObject().getTargetDocument(),
                documentLinkDTO.createRealObject().getLinkType()
            )
        );

        return ResponseUtil.wrapOrNotFound(
            Optional.of(dto),
            HeaderUtil.createAlert(applicationName, "documentLink.created", dto.getId().toString())
        );
    }

    @Override
    @GetMapping("/links/{sourceDocumentId}/source")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public List<DocumentLinkDTO> getLinkedDocumentsForDocumentAsSource(@PathVariable("sourceDocumentId") Long sourceDocumentId) {
        return documentLinkManagerDefaultImpl
            .getLinkedDocumentsForDocumentAsSource(documentManager.getDocument(sourceDocumentId).orElseThrow())
            .stream()
            .map(DocumentLinkDTO::new)
            .collect(Collectors.toList());
    }

    @Override
    @GetMapping("/links/{targetDocumentId}/target")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public List<DocumentLinkDTO> getLinkedDocumentsForDocumentAsTarget(@PathVariable("targetDocumentId") Long targetDocumentId) {
        return documentLinkManagerDefaultImpl
            .getLinkedDocumentsForDocumentAsTarget(documentManager.getDocument(targetDocumentId).orElseThrow())
            .stream()
            .map(DocumentLinkDTO::new)
            .collect(Collectors.toList());
    }

    @Override
    @GetMapping("/links/{documentId}/all")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public List<DocumentLinkDTO> getDocumentLinksForAllConnectionTypes(@PathVariable("documentId") Long documentId) {
        return documentLinkManagerDefaultImpl
            .getAllLinkedDocuments(documentManager.getDocument(documentId).orElseThrow())
            .stream()
            .map(DocumentLinkDTO::new)
            .collect(Collectors.toList());
    }

    @Override
    @DeleteMapping("/links/{documentLinkId}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<Void> deleteLink(@PathVariable("documentLinkId") Long linkId) {
        return null;
    }

    @GetMapping("/links/{documentLinkId}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<DocumentLinkDTO> getDocumentLink(@PathVariable("documentLinkId") Long linkId) {
        Optional<DocumentLink> link = documentLinkManagerDefaultImpl.getDocumentLink(linkId);

        DocumentLinkDTO dto = link.map(DocumentLinkDTO::new).orElse(null);

        return ResponseUtil.wrapOrNotFound(
            Optional.ofNullable(dto),
            HeaderUtil.createAlert(applicationName, "documentLink.found", dto != null ? dto.getId().toString() : "No data present!")
        );
    }

    @GetMapping("/links")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<List<DocumentLinkDTO>> getAllDocumentLinks(Pageable pageable) {
        List<DocumentLink> links = documentLinkManagerDefaultImpl.getAllDocumentLinks(pageable);

        List<DocumentLinkDTO> dtos = links.stream().map(DocumentLinkDTO::new).collect(Collectors.toList());

        return ResponseUtil.wrapOrNotFound(
            Optional.of(dtos),
            HeaderUtil.createAlert(
                applicationName,
                "documentLink.paged",
                !dtos.isEmpty() ? String.valueOf(dtos.size()) : "No data present!"
            )
        );
    }
}
