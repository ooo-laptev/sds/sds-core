/*
 * Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.service.util;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * This is a class for custom implementation of pagination.
 * This tool can be used in places where you need to use pagination for some custom solutions.
 * <p>
 * Usage example:
 *
 * <pre>{@code
 * //~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * @Override
 * public Page<Document> getDocumentsWhichUsesTemplate( Pageable pageable, Template template ) {
 *     List<Document> allRelatedDocuments = getDocumentsWhichUsesTemplate( template );
 *
 *     CustomPagination<Document> customPagination = new CustomPagination<>( allRelatedDocuments );
 *     return customPagination.getPage( pageable );
 * }
 *
 * //~~~~~~~~~~~~~~~~~~~~~~~~
 * }</pre>
 *
 *
 * <b>Warning!</b>
 * <i>This pagination tool is not completed and can produce performance issues! It is a mock implementation!</i>
 *
 * @param <T> - Type of data bean.
 *
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 15-May-2021
 */
public class CustomPagination<T> {

    protected final List<T> data;

    /**
     * Source data should be provided.
     *
     * @param data List of typed elements
     */
    public CustomPagination(List<T> data) {
        this.data = data;
    }

    /**
     * Provides a page based on {@link Pageable} properties.
     *
     * @param pageable Properties for paging.
     *
     * @return Returns a page based on requester parameters.
     */
    public Page<T> getPage(Pageable pageable) {
        if (pageable.isUnpaged()) {
            return Page.empty(pageable);
        }

        List<T> filteredData = cutDataForPage(data, pageable);

        return new PageImpl<>(filteredData, pageable, data.size());
    }

    /**
     * Extracts data from source data.
     *
     * @param dataForCutting List with source data.
     * @param pageable Parameters for data extracting.
     *
     * @return Extracted data with requested parameters.
     */
    private List<T> cutDataForPage(List<T> dataForCutting, Pageable pageable) {
        if (dataForCutting == null || dataForCutting.isEmpty()) {
            return dataForCutting;
        }

        if (pageable == null) {
            return dataForCutting;
        }

        int indexOfFirstElement = pageable.getPageSize() * (pageable.getPageNumber());

        if (isLastPageRequested(dataForCutting, pageable)) {
            int indexOfLastElement = dataForCutting.size();
            return dataForCutting.subList(indexOfFirstElement, indexOfLastElement);
        }

        return dataForCutting.subList(indexOfFirstElement, indexOfFirstElement + pageable.getPageSize());
    }

    /**
     * Checks that last page is requested.
     *
     * @param sourceData List with source data.
     * @param pageable Parameters for check on source data.
     *
     * @return TRUE - if last pare is requested.
     */
    private boolean isLastPageRequested(List<T> sourceData, Pageable pageable) {
        double pagesAreAvailable = getPageCount(sourceData, pageable) - 1D;
        return pagesAreAvailable == pageable.getPageNumber();
    }

    /**
     * Tells us how many pages ou can get with requested parameters.
     *
     * @param sourceData List with source data.
     * @param pageable Parameters for check on source data.
     *
     * @return Number of possible pages.
     */
    private int getPageCount(List<T> sourceData, Pageable pageable) {
        return (int) Math.ceil((double) sourceData.size() / (double) pageable.getPageSize());
    }
}
