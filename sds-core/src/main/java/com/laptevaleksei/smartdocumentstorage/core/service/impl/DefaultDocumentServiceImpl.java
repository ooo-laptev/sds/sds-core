/*
 * Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.service.impl;

import com.laptevaleksei.smartdocumentstorage.core.api.exception.DocumentException;
import com.laptevaleksei.smartdocumentstorage.core.api.exception.DocumentNotFoundException;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.Document;
import com.laptevaleksei.smartdocumentstorage.core.repository.DocumentRepository;
import com.laptevaleksei.smartdocumentstorage.core.service.DocumentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 28-Apr-2020
 */
@Service
@Transactional
public class DefaultDocumentServiceImpl implements DocumentService {

    private final Logger log = LoggerFactory.getLogger(DefaultDocumentServiceImpl.class);

    private final DocumentRepository documentRepository;

    public DefaultDocumentServiceImpl(DocumentRepository documentRepository) {
        this.documentRepository = documentRepository;
    }

    @Override
    public Document updateDocument(Document document) {
        if (document.getId() == null) {
            throw new DocumentException("You try to update document without ID. Maybe you wanted to create a new one?");
        } else if (documentRepository.existsById(document.getId())) {
            return documentRepository.save(document);
        } else {
            throw new DocumentNotFoundException("Document with ID=" + document.getId() + " does not exists!");
        }
    }

    @Override
    public List<Document> getAllDocuments() {
        return documentRepository.findAll().stream().filter(document -> !document.isDeleted()).collect(Collectors.toList());
    }

    @Override
    public Page<Document> getAllDocuments(Pageable pageable) {
        Page<Document> page = documentRepository.findAll(pageable);
        return new PageImpl<>(
            page.stream().filter(document -> !document.isDeleted()).collect(Collectors.toList()),
            page.getPageable(),
            page.getTotalElements()
        );
    }

    @Override
    public List<Document> getAllDocumentsByIds(Collection<Long> ids) {
        return documentRepository.findAllById(ids).stream().filter(document -> !document.isDeleted()).collect(Collectors.toList());
    }

    @Override
    public Document createDocument(Document document) {
        if (document.getId() != null) throw new DocumentException("You try to create Document with pre-defined ID!");
        return documentRepository.save(document);
    }

    @Override
    public Optional<Document> getDocument(Long id) {
        return documentRepository.findById(id).filter(customField -> !customField.isDeleted());
    }

    @Override
    public void deleteDocument(Document document) {
        document.setDeleted(true);
        updateDocument(document);
    }

    @Override
    public boolean documentIsExists(Long id) {
        Optional<Document> optionalDocument = getDocument(id);
        return optionalDocument.isPresent() && !optionalDocument.get().isDeleted();
    }
}
