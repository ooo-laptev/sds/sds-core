/*
 * Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.web.rest.api.template.fieldblock;

import com.laptevaleksei.smartdocumentstorage.core.domain.entity.template.FieldBlock;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.template.Template;
import com.laptevaleksei.smartdocumentstorage.core.web.dto.CustomFieldDTO;
import com.laptevaleksei.smartdocumentstorage.core.web.dto.template.FieldBlockDTO;
import com.laptevaleksei.smartdocumentstorage.core.web.dto.template.TemplateDTO;
import com.laptevaleksei.smartdocumentstorage.core.web.dto.template.extra.ExtraFieldBlockDTO;
import com.laptevaleksei.smartdocumentstorage.core.web.dto.template.extra.FieldBlockChangeLayoutRequest;
import com.laptevaleksei.smartdocumentstorage.core.web.rest.jhipster.errors.IdAlreadyUsedException;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.util.List;

/**
 * Interface for describing a FieldBlock Api for managing of them.
 *
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 23-Nov-2021
 */
public interface FieldBlockManagementApiResource {
    ResponseEntity<FieldBlockDTO> getFieldBlock(Long id);

    ResponseEntity<FieldBlock> createFieldBlock(FieldBlockDTO fieldBlockDTO) throws URISyntaxException;

    ResponseEntity<List<FieldBlockDTO>> getAllFieldBlock(Pageable pageable);

    ResponseEntity<FieldBlockDTO> updateFieldBlock(FieldBlockDTO fieldBlockDTO) throws IdAlreadyUsedException;

    ResponseEntity<Void> deleteFieldBlock(Long id);

    ResponseEntity<ExtraFieldBlockDTO> addItemToFieldBlockLayout(FieldBlockChangeLayoutRequest changeRequest);

    ResponseEntity<ExtraFieldBlockDTO> moveItemInFieldBlockLayout(FieldBlockChangeLayoutRequest changeRequest);

    ResponseEntity<ExtraFieldBlockDTO> deleteItemFromFieldBlockLayout(FieldBlockChangeLayoutRequest changeRequest);

    /**
     * @param fieldBlockId ID of {@link FieldBlock} which should be used for search.
     *
     * @return List of related {@link Template}'s.
     */
    ResponseEntity<List<TemplateDTO>> getRelatedTemplates(Pageable pageable, Long fieldBlockId);

    ResponseEntity<List<FieldBlockDTO>> getFieldBlocksRelatedWithCustomField(Pageable pageable, Long customFieldId);

    List<CustomFieldDTO> getAvailableCustomFieldsForAddingToFieldBLock();
}
