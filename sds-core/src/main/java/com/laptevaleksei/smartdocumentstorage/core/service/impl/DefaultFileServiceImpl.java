/*
 * Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.service.impl;

import com.laptevaleksei.smartdocumentstorage.core.api.exception.FileStorageException;
import com.laptevaleksei.smartdocumentstorage.core.api.exception.SDSFileNotFoundException;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.FileNameMapping;
import com.laptevaleksei.smartdocumentstorage.core.repository.FileNameMappingRepository;
import com.laptevaleksei.smartdocumentstorage.core.service.FileService;
import org.apache.commons.io.FilenameUtils;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

/**
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 25-Jul-2021
 */
@Service
public class DefaultFileServiceImpl implements FileService {

    private final FileNameMappingRepository fileNameMappingRepository;

    public DefaultFileServiceImpl(FileNameMappingRepository fileNameMappingRepository) {
        this.fileNameMappingRepository = fileNameMappingRepository;
    }

    @Override
    public String storeFile(MultipartFile file, Path fileStoragePath) {
        // Normalize file name
        String fileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));

        try {
            // Check if the file's name contains invalid characters
            if (fileName.contains("..")) {
                throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
            }

            FileNameMapping nameMapping = new FileNameMapping();
            nameMapping.setOriginalFileName(fileName);

            String newFileName =
                fileNameMappingRepository.save(nameMapping).getUuid().toString() + "." + FilenameUtils.getExtension(fileName);

            // Copy file to the target location (Replacing existing file with the same name)
            Path targetLocation = fileStoragePath.resolve(newFileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

            return newFileName;
        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }

    @Override
    public Resource loadFileAsResource(String fileName, Path fileStoragePath) {
        try {
            Optional<FileNameMapping> fileNameMapping = fileNameMappingRepository.findByUuid(
                UUID.fromString(FilenameUtils.getBaseName(fileName))
            );
            if (fileNameMapping.isEmpty()) {
                throw new SDSFileNotFoundException("File not found " + fileName);
            }

            FileNameMapping nameMapping = fileNameMapping.get();
            File originallyNamedFile = fileStoragePath
                .resolve(nameMapping.getUuid().toString() + "." + FilenameUtils.getExtension(fileName))
                .toFile();

            Path filePath = originallyNamedFile.toPath().normalize();
            Resource resource = new UrlResource(filePath.toUri());
            if (resource.exists()) {
                return resource;
            } else {
                throw new SDSFileNotFoundException("File not found on file system " + fileName);
            }
        } catch (MalformedURLException ex) {
            throw new SDSFileNotFoundException("File not found " + fileName, ex);
        }
    }

    @Override
    public void removeFile(String fileName, Path fileStoragePath) {
        try {
            Optional<FileNameMapping> fileNameMapping = fileNameMappingRepository.findByUuid(
                UUID.fromString(FilenameUtils.getBaseName(fileName))
            );
            if (fileNameMapping.isEmpty()) {
                throw new SDSFileNotFoundException("File not found " + fileName);
            }

            FileNameMapping nameMapping = fileNameMapping.get();
            fileNameMappingRepository.delete(nameMapping);

            Path filePath = fileStoragePath.resolve(fileName).normalize();

            Files.delete(filePath);
        } catch (IOException e) {
            throw new FileStorageException("Could not delete file " + fileName + ". Please try again!", e);
        }
    }
}
