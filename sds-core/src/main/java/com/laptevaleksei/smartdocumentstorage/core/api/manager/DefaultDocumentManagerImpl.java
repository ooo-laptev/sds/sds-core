/*
 * Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.api.manager;

import com.laptevaleksei.annotations.Internal;
import com.laptevaleksei.smartdocumentstorage.core.api.exception.DocumentException;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.Document;
import com.laptevaleksei.smartdocumentstorage.core.service.DocumentService;
import com.laptevaleksei.smartdocumentstorage.manager.DocumentManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Management-layer class for Document management.
 *
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 17-Mar-2020
 */
@Service
@Internal
public class DefaultDocumentManagerImpl implements DocumentManager {

    private final Logger logger = LoggerFactory.getLogger(DefaultDocumentManagerImpl.class);

    private final DocumentService documentService;

    public DefaultDocumentManagerImpl(DocumentService documentService) {
        this.documentService = documentService;
    }

    @Override
    public boolean documentIsExists(Long id) {
        return documentService.documentIsExists(id);
    }

    @Override
    public Document createNewDocument() {
        Document doc = null;
        try {
            doc = documentService.createDocument(new Document());
        } catch (DocumentException de) {
            logger.error(de.getLocalizedMessage());
        }
        return doc;
    }

    @Override
    public Optional<Document> getDocument(Long id) {
        return documentService.getDocument(id);
    }

    @Override
    public Document updateDocument(Document document) {
        return documentService.updateDocument(document);
    }

    @Override
    public void deleteDocument(Document document) {
        documentService.deleteDocument(document);
    }

    @Override
    public List<Document> getDocuments(Set<Long> ids) {
        return documentService.getAllDocumentsByIds(ids);
    }

    @Override
    public List<Document> getAllDocuments() {
        return documentService.getAllDocuments();
    }

    @Override
    public Page<Document> getAllDocuments(Pageable pageable) {
        return documentService.getAllDocuments(pageable);
    }
}
