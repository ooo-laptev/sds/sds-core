/*
 * Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.web.rest.api.template;

import com.laptevaleksei.smartdocumentstorage.core.web.dto.DocumentDTO;
import com.laptevaleksei.smartdocumentstorage.core.web.dto.customfield.extra.ExtraCustomFieldDTO;
import com.laptevaleksei.smartdocumentstorage.core.web.dto.template.TemplateDTO;
import com.laptevaleksei.smartdocumentstorage.core.web.dto.template.extra.ExtraTemplateDTO;
import com.laptevaleksei.smartdocumentstorage.core.web.dto.template.extra.TemplateChangeLayoutRequest;
import com.laptevaleksei.smartdocumentstorage.core.web.dto.template.extra.TemplateDefaultCustomFieldLayoutChangeRequest;
import com.laptevaleksei.smartdocumentstorage.core.web.rest.jhipster.errors.IdAlreadyUsedException;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.util.List;

/**
 * Interface for describing a Template Api for managing of them.
 *
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 23-Nov-2021
 */
public interface TemplateManagementApiResource {
    ResponseEntity<TemplateDTO> createTemplate(TemplateDTO templateDTO) throws URISyntaxException;

    ResponseEntity<TemplateDTO> getTemplate(Long id);

    ResponseEntity<List<TemplateDTO>> getAllTemplates(Pageable pageable);

    ResponseEntity<TemplateDTO> updateTemplate(TemplateDTO templateDTO) throws IdAlreadyUsedException;

    ResponseEntity<Void> deleteTemplate(Long id);

    ResponseEntity<ExtraTemplateDTO> getTemplateLayout(TemplateChangeLayoutRequest changeRequest);

    ResponseEntity<ExtraTemplateDTO> addItemToTemplateLayout(TemplateChangeLayoutRequest changeRequest);

    ResponseEntity<ExtraTemplateDTO> moveItemInTemplateLayout(TemplateChangeLayoutRequest changeRequest);

    ResponseEntity<ExtraTemplateDTO> deleteItemFromTemplateLayout(TemplateChangeLayoutRequest changeRequest);

    ResponseEntity<DocumentDTO> assignTemplateToDocument(Long documentId, Long templateId) throws URISyntaxException;

    ResponseEntity<DocumentDTO> unassignTemplateFromDocument(Long documentId, Long templateId) throws URISyntaxException;

    List<TemplateDTO> getTemplatesAssignedWithDocument(Long documentId);

    ResponseEntity<List<DocumentDTO>> getDocumentsAttachedWithTemplate(Pageable pageable, Long templateId);

    ResponseEntity<List<ExtraCustomFieldDTO>> addDefaultCustomField(Long templateId, Long customFieldId);

    ResponseEntity<List<ExtraCustomFieldDTO>> getTemplateDefaultCustomFields(Long templateId);

    ResponseEntity<List<ExtraCustomFieldDTO>> deleteDefaultCustomField(Long templateId, Long customFieldId);

    ResponseEntity<List<ExtraCustomFieldDTO>> moveDefaultCustomField(TemplateDefaultCustomFieldLayoutChangeRequest changeRequest);
}
