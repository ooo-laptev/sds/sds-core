/*
 * Copyright (c) 2019 - 2024, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.web.rest.api.linkeddocument;

import com.laptevaleksei.smartdocumentstorage.core.web.dto.documentlink.DocumentLinkTypeDTO;
import org.springframework.http.ResponseEntity;

/**
 * Interface for describing a LinkedDocuments Api for management of them.
 *
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 23-Nov-2021
 */
public interface LinkedDocumentsManagementApiResource {
    /**
     * Creates a new link type.
     *
     * @param documentLinkTypeDTO Parameters for link type creation.
     *
     * @return Returns created LinkType with ID.
     */
    ResponseEntity<DocumentLinkTypeDTO> createLinkType(DocumentLinkTypeDTO documentLinkTypeDTO);

    /**
     * Provides specific link type.
     *
     * @param id ID of link which is requested.
     *
     * @return LinkType.
     */
    DocumentLinkTypeDTO getLinkType(Long id);

    /**
     * Updates existing LinkType
     *
     * @param documentLinkTypeDTO Parameters for update.
     *
     * @return Returns updated link type.
     */
    ResponseEntity<DocumentLinkTypeDTO> updateLinkType(DocumentLinkTypeDTO documentLinkTypeDTO);

    /**
     * Deletes link type.
     *
     * @param id ID of link type which should be deleted.
     *
     * @return VOID.
     */
    ResponseEntity<Void> deleteLinkType(Long id);
}
