/*
 * Copyright (c) 2019 - 2024, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.customfieldprocessing;

import com.laptevaleksei.smartdocumentstorage.core.domain.entity.CustomField;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.Document;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.customfield.AbstractCustomFieldValue;
import com.laptevaleksei.smartdocumentstorage.customfieldprocessing.AbstractCustomFieldType;
import com.laptevaleksei.smartdocumentstorage.customfieldprocessing.CustomFieldProcessor;
import com.laptevaleksei.smartdocumentstorage.repository.customfield.CustomFieldValueRepository;
import jakarta.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.type.classreading.CachingMetadataReaderFactory;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.ClassUtils;
import org.springframework.util.SystemPropertyUtils;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.util.*;
import java.util.stream.Collectors;

/**
 * This is a type registry which handles all customfield types.
 *
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 29-Jul-2021
 */
@Service
@SuppressWarnings({ "rawtypes", "unchecked" })
public class CustomFieldTypeRegistry {

    private static final String CUSTOMFIELD_TYPE_PACKAGE = "com.laptevaleksei.smartdocumentstorage.customfield";

    private final Logger log = LoggerFactory.getLogger(CustomFieldTypeRegistry.class);

    private final ApplicationContext applicationContext;

    private final Map<String, Class> typeMapping = new HashMap<>();

    public CustomFieldTypeRegistry(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    /**
     * Instantiates processor for specified {@link CustomField}.
     *
     * @param customField {@link CustomField} which should be processed.
     * @param document {@link Document} where we want to manage a value in the future.
     *
     * @return Returns an instance of type class.
     */
    public AbstractCustomFieldType instantiateCustomFieldTypeObject( CustomField customField, Document document) {
        Class typeClass = typeMapping.get(customField.getCustomFieldType().strip());
        if (typeClass == null) {
            return null;
        }

        try {
            CustomFieldValueRepository repository = (CustomFieldValueRepository) applicationContext.getBean(
                getClassByFullQualifiedName(getUsableRepository(typeClass))
            );
            Constructor typeConstructor = typeClass.getConstructor(
                Document.class,
                CustomField.class,
                getClassByFullQualifiedName(getUsableRepository(typeClass))
            );

            return (AbstractCustomFieldType) typeConstructor.newInstance(document, customField, repository);
        } catch (
            ClassNotFoundException | NoSuchMethodException | InvocationTargetException | IllegalAccessException | InstantiationException e
        ) {
            log.error(e.getLocalizedMessage());
        }

        return null;
    }

    /**
     * Provides a class by full qualified name like 'com.laptevaleksei.SomeClassName'.
     *
     * @param qualifiedName Full qualified name of class which you want to get.
     *
     * @return Class.
     *
     * @throws ClassNotFoundException throws if specified class not found.
     */
    private Class getClassByFullQualifiedName(String qualifiedName) throws ClassNotFoundException {
        return Class.forName(qualifiedName);
    }

    @PostConstruct
    private void init() throws IOException, ClassNotFoundException {
        Set<Class> classes = findAnnotatedClasses(CUSTOMFIELD_TYPE_PACKAGE);
        if (classes.isEmpty()) {
            return;
        }

        classes.forEach(
            typeClass -> {
                String[] availableTypes = getAvailableTypeKeys(typeClass);
                String typeName = getTypeName(typeClass);

                if (typeName == null || typeName.isEmpty() || availableTypes == null || availableTypes.length == 0) {
                    return;
                }

                for (String availableType : availableTypes) {
                    typeMapping.put(availableType, typeClass);
                }
            }
        );
    }

    /**
     * Provides a value of 'name' attribute from annotation.
     *
     * @param typeClass Class of custom field type.
     *
     * @return Returns a name of type.
     */
    private String getTypeName(Class typeClass) {
        CustomFieldProcessor annotation = (CustomFieldProcessor) typeClass.getAnnotation(CustomFieldProcessor.class);
        return annotation.name();
    }

    /**
     * Provides information about keys which can be handled by current type class.
     *
     * @param typeClass Class of current type.
     *
     * @return String array of type keys.
     */
    private String[] getAvailableTypeKeys(Class typeClass) {
        CustomFieldProcessor annotation = (CustomFieldProcessor) typeClass.getAnnotation(CustomFieldProcessor.class);
        return annotation.availableTypeKeys();
    }

    /**
     * Provides the full qualified name of used repository for type class.
     *
     * @param typeClass Class of current type.
     *
     * @return Full qualified name of used repository.
     */
    private String getUsableRepository(Class typeClass) {
        CustomFieldProcessor annotation = (CustomFieldProcessor) typeClass.getAnnotation(CustomFieldProcessor.class);
        return annotation.usableRepository();
    }

    /**
     * Search for classes which annotated with {@link CustomFieldProcessor} and located in specified package.
     *
     * @param basePackage Package for search.
     *
     * @return Returns collection of classes.
     *
     * @throws IOException In case I/O errors.
     * @throws ClassNotFoundException Throws if the class can not be located.
     */
    private Set<Class> findAnnotatedClasses(String basePackage) throws IOException, ClassNotFoundException {
        ResourcePatternResolver resourcePatternResolver = new PathMatchingResourcePatternResolver();
        MetadataReaderFactory metadataReaderFactory = new CachingMetadataReaderFactory(resourcePatternResolver);

        Set<Class> candidates = new HashSet<>();
        String packageSearchPath = ResourcePatternResolver.CLASSPATH_ALL_URL_PREFIX + resolveBasePackage(basePackage) + "/" + "**/*.class";
        Resource[] resources = resourcePatternResolver.getResources(packageSearchPath);
        for (Resource resource : resources) {
            if (resource.isReadable()) {
                MetadataReader metadataReader = metadataReaderFactory.getMetadataReader(resource);
                if (isCandidate(metadataReader)) {
                    candidates.add(Class.forName(metadataReader.getClassMetadata().getClassName()));
                }
            }
        }
        return candidates;
    }

    private String resolveBasePackage(String basePackage) {
        return ClassUtils.convertClassNameToResourcePath(SystemPropertyUtils.resolvePlaceholders(basePackage));
    }

    /**
     * Checks that class contains annotation {@link CustomFieldProcessor}.
     *
     * @param metadataReader Metadata of class which should be processed.
     *
     * @return TRUE if class contains annotation {@link CustomFieldProcessor}.
     */
    private boolean isCandidate(MetadataReader metadataReader) {
        try {
            Class c = Class.forName(metadataReader.getClassMetadata().getClassName());
            if (c.getAnnotation(CustomFieldProcessor.class) != null) {
                return true;
            }
        } catch (Throwable e) {
            log.error(e.getLocalizedMessage());
        }
        return false;
    }

    /**
     * This method provides all registered type keys for all child of {@link AbstractCustomFieldType}.
     *
     * @return Set of type keys.
     */
    public Set<String> getAllTypeKeys() {
        return typeMapping.keySet();
    }

    /**
     * This method provides a pairs of type key and type name.
     *
     * @return Key-value pairs.
     */
    public Map<String, String> getMappingForTypeKeyAndTypeName() {
        return typeMapping.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, e -> getTypeName(e.getValue())));
    }

    public List<AbstractCustomFieldValue> getObjectsByValue(String param) {
        List<AbstractCustomFieldValue> result = new ArrayList<>();
        typeMapping
            .values()
            .forEach(
                typeClass -> {
                    try {
                        CustomFieldValueRepository repository = (CustomFieldValueRepository) applicationContext.getBean(
                            getClassByFullQualifiedName(getUsableRepository(typeClass))
                        );

                        if (((ParameterizedType) typeClass.getGenericSuperclass()).getActualTypeArguments()[1].equals(param.getClass())) {
                            result.addAll(repository.findByValue(param));
                        }
                    } catch (ClassNotFoundException e) {
                        log.error(e.getLocalizedMessage());
                    }
                }
            );

        return result;
    }

    /**
     * Provides all CustomFieldValue objects which are related with specified document.
     *
     * @param document Document as a parameter for search.
     *
     * @return List of existing value objects.
     */
    public List<AbstractCustomFieldValue> getObjectsByDocument(Document document) {
        if (document == null) {
            return new ArrayList<>();
        }
        Set<AbstractCustomFieldValue<?>> result = new HashSet<>();
        typeMapping
            .values()
            .forEach(
                typeClass -> {
                    try {
                        CustomFieldValueRepository repository = (CustomFieldValueRepository) applicationContext.getBean(
                            getClassByFullQualifiedName(getUsableRepository(typeClass))
                        );
                        List<AbstractCustomFieldValue<?>> valueObjects = (List<AbstractCustomFieldValue<?>>) repository
                            .findByDocument(document)
                            .stream()
                            .filter(
                                it ->
                                    ((AbstractCustomFieldValue<?>) it).getCustomField().getCustomFieldType().equals(typeClass.getTypeName())
                            )
                            .collect(Collectors.toList());

                        result.addAll(valueObjects);
                    } catch (ClassNotFoundException e) {
                        log.error(e.getLocalizedMessage());
                    }
                }
            );

        return new ArrayList<>(result).stream().distinct().collect(Collectors.toList());
    }
}
