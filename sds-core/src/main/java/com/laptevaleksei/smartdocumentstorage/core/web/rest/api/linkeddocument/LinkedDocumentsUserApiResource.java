/*
 * Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.web.rest.api.linkeddocument;

import com.laptevaleksei.smartdocumentstorage.core.web.dto.documentlink.DocumentLinkDTO;
import com.laptevaleksei.smartdocumentstorage.core.web.dto.documentlink.DocumentLinkTypeDTO;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import java.util.List;

/**
 * Interface for describing a LinkedDocuments User Api.
 *
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 23-Nov-2021
 */
public interface LinkedDocumentsUserApiResource {
    /**
     * Creates a link between two documents.
     *
     * @param documentLinkDTO Parameters for link creation.
     *
     * @return Created link with ID.
     */
    ResponseEntity<DocumentLinkDTO> createLink(DocumentLinkDTO documentLinkDTO);

    /**
     * Provides linked documents where specified document presented as source document.
     *
     * @param sourceDocumentId Id of source document.
     *
     * @return Returns List of related documents with their link types.
     */
    List<DocumentLinkDTO> getLinkedDocumentsForDocumentAsSource(Long sourceDocumentId);

    /**
     * Provides linked documents where specified document presented as target document.
     *
     * @param targetDocumentId Id of source document.
     *
     * @return Returns List of related documents with their link types.
     */
    List<DocumentLinkDTO> getLinkedDocumentsForDocumentAsTarget(Long targetDocumentId);

    /**
     * Returns all related documents with all call types (source and target)
     *
     * @param documentId Document ID for link searching.
     *
     * @return List of related documents.
     */
    List<DocumentLinkDTO> getDocumentLinksForAllConnectionTypes(Long documentId);

    /**
     * Provides page of all available document link types.
     *
     * @param pageable Page parameters.
     *
     * @return Page of Document link types.
     */
    List<DocumentLinkTypeDTO> getAllDocumentLinkTypes(Pageable pageable);

    /**
     * Deletes relationship between source document and target document by its ID.
     *
     * @param linkId ID if relationship.
     *
     * @return VOID.
     */
    ResponseEntity<Void> deleteLink(Long linkId);
}
