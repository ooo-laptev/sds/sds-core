/*
 * Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.web.rest;

import com.laptevaleksei.smartdocumentstorage.core.api.exception.CustomFieldException;
import com.laptevaleksei.smartdocumentstorage.core.api.exception.TemplateException;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.CustomField;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.Document;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.template.FieldBlock;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.template.Template;
import com.laptevaleksei.smartdocumentstorage.core.security.AuthoritiesConstants;
import com.laptevaleksei.smartdocumentstorage.core.web.dto.DocumentDTO;
import com.laptevaleksei.smartdocumentstorage.core.web.dto.customfield.extra.ExtraCustomFieldDTO;
import com.laptevaleksei.smartdocumentstorage.core.web.dto.template.TemplateDTO;
import com.laptevaleksei.smartdocumentstorage.core.web.dto.template.TemplateLayoutDTO;
import com.laptevaleksei.smartdocumentstorage.core.web.dto.template.extra.ExtraFieldBlockDTO;
import com.laptevaleksei.smartdocumentstorage.core.web.dto.template.extra.ExtraTemplateDTO;
import com.laptevaleksei.smartdocumentstorage.core.web.dto.template.extra.TemplateChangeLayoutRequest;
import com.laptevaleksei.smartdocumentstorage.core.web.dto.template.extra.TemplateDefaultCustomFieldLayoutChangeRequest;
import com.laptevaleksei.smartdocumentstorage.core.web.rest.api.template.TemplateManagementApiResource;
import com.laptevaleksei.smartdocumentstorage.core.web.rest.api.template.TemplateUserApiResource;
import com.laptevaleksei.smartdocumentstorage.core.web.rest.jhipster.errors.BadRequestAlertException;
import com.laptevaleksei.smartdocumentstorage.core.web.rest.jhipster.errors.IdAlreadyUsedException;
import com.laptevaleksei.smartdocumentstorage.manager.CustomFieldManager;
import com.laptevaleksei.smartdocumentstorage.manager.DocumentManager;
import com.laptevaleksei.smartdocumentstorage.manager.TemplateManager;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 27-May-2020
 */
@RestController
@RequestMapping(path = "/api")
public class TemplateResource implements TemplateManagementApiResource, TemplateUserApiResource {

    private static final List<String> ALLOWED_ORDERED_PROPERTIES = List.of("id", "name", "description", "defaultCustomFields");

    private static final String TEMPLATE_UPDATED = "templateManagement.updated";
    private static final String TEMPLATE_NOT_UPDATED = "templateManagement.notUpdated";

    private final Logger log = LoggerFactory.getLogger(TemplateResource.class);
    private final TemplateManager templateManager;
    private final DocumentManager documentManager;
    private final CustomFieldManager customFieldManager;

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    public TemplateResource(TemplateManager templateManager, DocumentManager documentManager, CustomFieldManager customFieldManager) {
        this.templateManager = templateManager;
        this.documentManager = documentManager;
        this.customFieldManager = customFieldManager;
    }

    @Override
    @PostMapping("/templates")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<TemplateDTO> createTemplate(@Valid @RequestBody TemplateDTO templateDTO) throws URISyntaxException {
        log.debug("REST request to save Template : {}", templateDTO);

        if (templateDTO.getId() != null) {
            throw new BadRequestAlertException("A new template cannot already have an ID", "templateManagement", "id exists");
        } else {
            TemplateDTO newTemplate = TemplateDTO.createDtoFromRealObject(templateManager.createTemplate(templateDTO.createRealObject()));
            return ResponseEntity
                .created(new URI("/api/templates/" + newTemplate.getId()))
                .headers(HeaderUtil.createAlert(applicationName, "templateManagement.created", String.valueOf(newTemplate.getId())))
                .body(newTemplate);
        }
    }

    @Override
    @GetMapping("/templates/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<TemplateDTO> getTemplate(@PathVariable Long id) {
        log.debug("REST request to get Template : {}", id);
        return ResponseUtil.wrapOrNotFound(templateManager.getTemplate(id).map(TemplateDTO::new));
    }

    @Override
    @GetMapping("/templates")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<List<TemplateDTO>> getAllTemplates(Pageable pageable) {
        if (!onlyContainsAllowedProperties(pageable)) {
            return ResponseEntity.badRequest().build();
        }

        final Page<TemplateDTO> page = templateManager.getAllManagedTemplates(pageable).map(TemplateDTO::new);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @Override
    @PutMapping("/templates")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<TemplateDTO> updateTemplate(@Valid @RequestBody TemplateDTO templateDTO) throws IdAlreadyUsedException {
        log.debug("REST request to update Template : {}", templateDTO);
        if (!templateManager.templateIsExists(templateDTO.getId())) {
            throw new IdAlreadyUsedException();
        }
        Optional<TemplateDTO> updatedTemplate = templateManager
            .updateTemplate(templateDTO.createRealObject())
            .map(TemplateDTO::createDtoFromRealObject);

        return ResponseUtil.wrapOrNotFound(
            updatedTemplate,
            HeaderUtil.createAlert(applicationName, TEMPLATE_UPDATED, templateDTO.getId().toString())
        );
    }

    @Override
    @DeleteMapping("/templates/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Void> deleteTemplate(@PathVariable Long id) {
        log.debug("REST request to delete Template: {}", id);
        templateManager.deleteTemplate(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createAlert(applicationName, "templateManagement" + ".deleted", id.toString()))
            .build();
    }

    @Override
    @GetMapping("/templateLayout")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<ExtraTemplateDTO> getTemplateLayout(TemplateChangeLayoutRequest changeRequest) {
        Optional<Template> template = templateManager.getTemplate(changeRequest.getTemplateId());
        ExtraTemplateDTO extraTemplateDTO = new ExtraTemplateDTO();
        if (template.isPresent()) {
            extraTemplateDTO = fillExtraTemplateDTO(template.get());
        }

        return ResponseUtil.wrapOrNotFound(Optional.of(extraTemplateDTO));
    }

    @Override
    @PutMapping("/templateLayout")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<ExtraTemplateDTO> addItemToTemplateLayout(TemplateChangeLayoutRequest changeRequest) {
        Optional<Template> template = templateManager.getTemplate(changeRequest.getTemplateId());
        ExtraTemplateDTO extraTemplateDTO = new ExtraTemplateDTO();
        if (template.isPresent()) {
            Optional<FieldBlock> fieldBlock = templateManager.getFieldBlock(changeRequest.getFieldBlockId());
            if (fieldBlock.isPresent()) {
                templateManager.addFieldBlockToTheTemplate(template.get(), fieldBlock.get());

                extraTemplateDTO = fillExtraTemplateDTO(template.get());
            }
        }
        return ResponseUtil.wrapOrNotFound(
            Optional.of(extraTemplateDTO),
            HeaderUtil.createAlert(applicationName, TEMPLATE_UPDATED, changeRequest.getTemplateId().toString())
        );
    }

    @Override
    @PatchMapping("/templateLayout")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<ExtraTemplateDTO> moveItemInTemplateLayout(TemplateChangeLayoutRequest changeRequest) {
        Optional<Template> template = templateManager.getTemplate(changeRequest.getTemplateId());
        ExtraTemplateDTO extraTemplateDTO = new ExtraTemplateDTO();
        if (template.isPresent()) {
            Optional<FieldBlock> fieldBlock = templateManager.getFieldBlock(changeRequest.getFieldBlockId());
            if (fieldBlock.isPresent()) {
                templateManager.changeFieldBlockPosition(template.get(), fieldBlock.get(), changeRequest.getPosition());

                extraTemplateDTO = fillExtraTemplateDTO(template.get());
            }
        }
        return ResponseUtil.wrapOrNotFound(
            Optional.of(extraTemplateDTO),
            HeaderUtil.createAlert(applicationName, TEMPLATE_UPDATED, changeRequest.getTemplateId().toString())
        );
    }

    @Override
    @DeleteMapping("/templateLayout")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<ExtraTemplateDTO> deleteItemFromTemplateLayout(TemplateChangeLayoutRequest changeRequest) {
        Optional<Template> template = templateManager.getTemplate(changeRequest.getTemplateId());
        ExtraTemplateDTO extraTemplateDTO = new ExtraTemplateDTO();
        if (template.isPresent()) {
            Optional<FieldBlock> fieldBlock = templateManager.getFieldBlock(changeRequest.getFieldBlockId());
            if (fieldBlock.isPresent()) {
                templateManager.removeFieldBlockFromTemplate(template.get(), fieldBlock.get());

                extraTemplateDTO = fillExtraTemplateDTO(template.get());
            }
        }
        return ResponseUtil.wrapOrNotFound(
            Optional.of(extraTemplateDTO),
            HeaderUtil.createAlert(applicationName, TEMPLATE_UPDATED, changeRequest.getTemplateId().toString())
        );
    }

    @Override
    @GetMapping("/assignTemplateToDocument")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<DocumentDTO> assignTemplateToDocument(Long documentId, Long templateId) throws URISyntaxException {
        Template template = templateManager.getTemplate(templateId).orElseThrow(TemplateException::new);
        Document document = documentManager.getDocument(documentId).orElseThrow();
        templateManager.assignTemplateToDocument(document, template);

        DocumentDTO dto = fillDocumentDTO(document);

        return ResponseEntity
            .created(new URI("/api/documents/" + dto.getId()))
            .headers(
                HeaderUtil.createEntityUpdateAlert(
                    applicationName,
                    true,
                    "documentManagement.documentAssignedToTemplate",
                    String.valueOf(dto.getId())
                )
            )
            .body(dto);
    }

    @Override
    @GetMapping("/unassignTemplateFromDocument")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<DocumentDTO> unassignTemplateFromDocument(Long documentId, Long templateId) throws URISyntaxException {
        Template template = templateManager.getTemplate(templateId).orElseThrow(TemplateException::new);
        Document document = documentManager.getDocument(documentId).orElseThrow();

        templateManager.unassignTemplateFromDocument(document, template);

        DocumentDTO dto = fillDocumentDTO(document);

        return ResponseEntity
            .created(new URI("/api/documents/" + dto.getId()))
            .headers(
                HeaderUtil.createEntityDeletionAlert(
                    applicationName,
                    true,
                    "documentManagement.documentUnassignedFromTemplate",
                    String.valueOf(dto.getId())
                )
            )
            .body(dto);
    }

    @Override
    @GetMapping("/getTemplatesAssignedWithDocument")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public List<TemplateDTO> getTemplatesAssignedWithDocument(Long documentId) {
        Document document = documentManager.getDocument(documentId).orElseThrow();
        List<Template> assignedTemplates = templateManager.getTemplatesAssignedToDocument(document);

        return assignedTemplates.stream().map(TemplateDTO::new).collect(Collectors.toList());
    }

    @Override
    @GetMapping("/getDocumentsAttachedWithTemplate")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<List<DocumentDTO>> getDocumentsAttachedWithTemplate(Pageable pageable, Long templateId) {
        Template template = templateManager.getTemplate(templateId).orElseThrow(TemplateException::new);

        final Page<DocumentDTO> page = templateManager
            .getDocumentsWhichUsesTemplate(pageable, template)
            .map(DocumentDTO::createDtoFromRealObject);

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @Override
    @PutMapping("/templateDefaultCustomFields")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<List<ExtraCustomFieldDTO>> addDefaultCustomField(Long templateId, Long customFieldId) {
        Template template = templateManager.getTemplate(templateId).orElseThrow(TemplateException::new);
        CustomField customField = customFieldManager.getCustomField(customFieldId).orElseThrow(CustomFieldException::new);

        templateManager.addDefaultCustomField(template, customField);

        ExtraTemplateDTO extraTemplateDTO = new ExtraTemplateDTO(template);
        extraTemplateDTO.setDefaultCustomFields(
            templateManager.getDefaultCustomFields(template).stream().map(ExtraCustomFieldDTO::new).collect(Collectors.toList())
        );

        return ResponseUtil.wrapOrNotFound(
            Optional.of(extraTemplateDTO.getDefaultCustomFields()),
            HeaderUtil.createAlert(applicationName, TEMPLATE_UPDATED, templateId.toString())
        );
    }

    @Override
    @GetMapping("/templateDefaultCustomFields")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<List<ExtraCustomFieldDTO>> getTemplateDefaultCustomFields(Long templateId) {
        Template template = templateManager.getTemplate(templateId).orElseThrow(TemplateException::new);

        List<ExtraCustomFieldDTO> defaultCustomFields = templateManager
            .getDefaultCustomFields(template)
            .stream()
            .map(ExtraCustomFieldDTO::new)
            .collect(Collectors.toList());

        return ResponseUtil.wrapOrNotFound(Optional.of(defaultCustomFields));
    }

    @Override
    @DeleteMapping("/templateDefaultCustomFields")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<List<ExtraCustomFieldDTO>> deleteDefaultCustomField(Long templateId, Long customFieldId) {
        Template template = templateManager.getTemplate(templateId).orElseThrow(TemplateException::new);

        ExtraTemplateDTO extraTemplateDTO = new ExtraTemplateDTO(template);
        extraTemplateDTO.setDefaultCustomFields(
            templateManager.getDefaultCustomFields(template).stream().map(ExtraCustomFieldDTO::new).collect(Collectors.toList())
        );

        List<CustomField> defaultCustomFields = templateManager.getDefaultCustomFields(template);
        if (!defaultCustomFields.isEmpty()) {
            CustomField customField = customFieldManager.getCustomField(customFieldId).orElseThrow();

            templateManager.removeDefaultCustomField(template, customField);
            extraTemplateDTO = new ExtraTemplateDTO(templateManager.updateTemplate(template).orElseThrow());
            extraTemplateDTO.setDefaultCustomFields(
                templateManager.getDefaultCustomFields(template).stream().map(ExtraCustomFieldDTO::new).collect(Collectors.toList())
            );

            return ResponseUtil.wrapOrNotFound(
                Optional.of(extraTemplateDTO.getDefaultCustomFields()),
                HeaderUtil.createAlert(applicationName, TEMPLATE_UPDATED, templateId.toString())
            );
        }
        return ResponseUtil.wrapOrNotFound(
            Optional.of(extraTemplateDTO.getDefaultCustomFields()),
            HeaderUtil.createAlert(applicationName, TEMPLATE_NOT_UPDATED, templateId.toString())
        );
    }

    @Override
    @PatchMapping("/templateDefaultCustomFields")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<List<ExtraCustomFieldDTO>> moveDefaultCustomField(TemplateDefaultCustomFieldLayoutChangeRequest changeRequest) {
        Optional<Template> template = templateManager.getTemplate(changeRequest.getTemplateId());
        ExtraTemplateDTO extraTemplateDTO = new ExtraTemplateDTO();
        if (template.isPresent()) {
            Optional<CustomField> customField = customFieldManager.getCustomField(changeRequest.getCustomFieldId());
            if (customField.isPresent()) {
                templateManager.changeDefaultCustomFieldPosition(template.get(), customField.get(), changeRequest.getPosition());

                extraTemplateDTO = fillExtraTemplateDTO(template.get());
            }
        }
        return ResponseUtil.wrapOrNotFound(
            Optional.of(extraTemplateDTO.getDefaultCustomFields()),
            HeaderUtil.createAlert(applicationName, TEMPLATE_UPDATED, changeRequest.getTemplateId().toString())
        );
    }

    @Override
    @GetMapping("/templateLayout/assignedToTemplate/{templateId}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<List<TemplateLayoutDTO>> getTemplateLayoutsForTemplate(@PathVariable Long templateId) {
        Optional<Template> template = templateManager.getTemplate(templateId);

        if (template.isEmpty()) return ResponseEntity.notFound().build();

        List<TemplateLayoutDTO> templateLayouts = templateManager
            .getTemplateLayouts(template.get())
            .stream()
            .map(TemplateLayoutDTO::new)
            .collect(Collectors.toList());

        return ResponseUtil.wrapOrNotFound(Optional.of(templateLayouts));
    }

    private boolean onlyContainsAllowedProperties(Pageable pageable) {
        return pageable.getSort().stream().map(Sort.Order::getProperty).allMatch(ALLOWED_ORDERED_PROPERTIES::contains);
    }

    /**
     * @param document Document which you want to use for DTO filling.
     *
     * @return Completed DTO.
     */
    public DocumentDTO fillDocumentDTO(Document document) {
        DocumentDTO dto = DocumentDTO.createDtoFromRealObject(document);
        List<Template> assignedTemplates = templateManager.getTemplatesAssignedToDocument(document);
        if (assignedTemplates != null && !assignedTemplates.isEmpty()) {
            dto.setAssignedTemplates(assignedTemplates.stream().map(Template::getId).collect(Collectors.toList()));
        } else {
            dto.setAssignedTemplates(new ArrayList<>());
        }
        return dto;
    }

    /**
     * This method gets layout for {@link Template} and provides extended DTO with layout.
     *
     * @param template {@link Template} which needs for extended DTO.
     *
     * @return Filled {@link ExtraTemplateDTO}.
     */
    private ExtraTemplateDTO fillExtraTemplateDTO(Template template) {
        ExtraTemplateDTO extraTemplateDTO;

        List<ExtraFieldBlockDTO> layout = templateManager
            .getRelatedFieldBlocksWithTemplate(template)
            .stream()
            .map(this::fillExtraFieldBlockDTO)
            .collect(Collectors.toList());

        extraTemplateDTO = new ExtraTemplateDTO(template);
        extraTemplateDTO.setDefaultCustomFields(
            templateManager.getDefaultCustomFields(template).stream().map(ExtraCustomFieldDTO::new).collect(Collectors.toList())
        );
        extraTemplateDTO.setContent(layout);

        return extraTemplateDTO;
    }

    /**
     * Fills extended {@link ExtraFieldBlockDTO} which is {@link ExtraFieldBlockDTO} with related data.
     *
     * @param fieldBlock {@link FieldBlock} object which should be used for creation of {@link ExtraFieldBlockDTO}.
     *
     * @return Returns completely filled {@link ExtraFieldBlockDTO}.
     */
    private ExtraFieldBlockDTO fillExtraFieldBlockDTO(FieldBlock fieldBlock) {
        ExtraFieldBlockDTO extraFieldBlockDTO;

        List<ExtraCustomFieldDTO> layout = templateManager
            .getRelatedCustomFieldsWithFieldBlock(fieldBlock)
            .stream()
            .map(this::fillExtraCustomFieldDTO)
            .collect(Collectors.toList());

        extraFieldBlockDTO = new ExtraFieldBlockDTO(fieldBlock);
        extraFieldBlockDTO.setContent(layout);

        return extraFieldBlockDTO;
    }

    /**
     * Filling completed {@link ExtraCustomFieldDTO} with all data.
     *
     * @param customField {@link CustomField} which should be used for {@link ExtraCustomFieldDTO} filling.
     *
     * @return Returns completely filled {@link ExtraCustomFieldDTO}.
     */
    private ExtraCustomFieldDTO fillExtraCustomFieldDTO(CustomField customField) {
        ExtraCustomFieldDTO extraCustomField;

        extraCustomField = new ExtraCustomFieldDTO(customField);

        return extraCustomField;
    }
}
