/*
 * Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.service;

import com.laptevaleksei.smartdocumentstorage.core.domain.entity.Document;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing of {@link Document}.
 *
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 17-Mar-2020
 */
public interface DocumentService {
    /**
     * Updates a document.
     *
     * @param document the entity to save.
     *
     * @return the persisted entity.
     */
    Document updateDocument(Document document);

    /**
     * Get all the documents.
     *
     * @return the list of entities.
     */
    List<Document> getAllDocuments();

    /**
     * Get all the documents.
     *
     * @param ids Collection of Documents for searching.
     *
     * @return the list of entities.
     */
    List<Document> getAllDocumentsByIds(Collection<Long> ids);

    /**
     * Creates a new document.
     *
     * @param document Instance of Document class.
     *
     * @return Returns instance of saved Document with ID.
     */
    Document createDocument(Document document);

    /**
     * You can use this method when you want to get some document by ID.
     *
     * @param id ID of document which you want.
     *
     * @return Optional of Document.
     */
    Optional<Document> getDocument(Long id);

    /**
     * You can delete document using this method.
     *
     * @param document Instance of the document which you want to delete.
     */
    void deleteDocument(Document document);

    /**
     * Check that document is exists.
     *
     * @param id ID of source Document.
     *
     * @return TRUE if Document with specified ID is exists.
     */
    boolean documentIsExists(Long id);

    /**
     * This method provides list of all documents but with pagination.
     *
     * @param pageable Pagination settings.
     *
     * @return {@link Page} of {@link Document}
     */
    Page<Document> getAllDocuments(Pageable pageable);
}
