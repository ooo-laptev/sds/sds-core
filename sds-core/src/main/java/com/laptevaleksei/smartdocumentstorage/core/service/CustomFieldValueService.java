/*
 * Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.service;

import com.laptevaleksei.smartdocumentstorage.core.domain.entity.CustomField;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.Document;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.customfield.AbstractCustomFieldValue;

import java.util.List;

/**
 * Service Interface for managing of {@link CustomField} and all extensions.
 *
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 17-Mar-2020
 */
public interface CustomFieldValueService {
    /**
     * Provides a value from pair of {@link Document} and {@link CustomField}.
     *
     * @param document {@link Document} where value should be.
     * @param customField {@link CustomField} which owns this value.
     *
     * @return Value of the pair.
     */
    Object getCustomFieldValue(Document document, CustomField customField);

    /**
     * This method created for usable process of saving value for specified Document-CustomField pair.
     *
     * @param document Document where you want to save value.
     * @param customField CustomField which responsible to store value.
     * @param value Value which you want to save.
     */
    void saveCustomFieldValue(Document document, CustomField customField, Object value);

    /**
     * Provides an object of type. It is an implementation of {@link AbstractCustomFieldValue}.
     *
     * @param document {@link Document} for search a value.
     * @param customField {@link CustomField} for search a value.
     *
     * @return Value object.
     */
    AbstractCustomFieldValue getValueObject(Document document, CustomField customField);

    /**
     * Provides a possibility to save value via value object. Value object an implementation of {@link AbstractCustomFieldValue}.
     *
     * @param customFieldValue A value object.
     */
    void saveCustomFieldValueObject(AbstractCustomFieldValue<?> customFieldValue);

    List<AbstractCustomFieldValue> findByValue(String param);

    /**
     * Provides list of existing value objects related with specified {@link Document}.
     *
     * @param document {@link Document} as a parameter for search.
     *
     * @return List of found value objects.
     */
    List<AbstractCustomFieldValue> findByDocument(Document document);
}
