/*
 * Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.web.rest;

import com.laptevaleksei.smartdocumentstorage.core.api.exception.TemplateException;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.CustomField;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.Document;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.template.FieldBlock;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.template.Template;
import com.laptevaleksei.smartdocumentstorage.core.security.AuthoritiesConstants;
import com.laptevaleksei.smartdocumentstorage.core.web.dto.customfield.extra.ExtraCustomFieldDTO;
import com.laptevaleksei.smartdocumentstorage.core.web.dto.template.extra.ExtraFieldBlockDTO;
import com.laptevaleksei.smartdocumentstorage.core.web.dto.template.extra.ExtraTemplateDTO;
import com.laptevaleksei.smartdocumentstorage.manager.CustomFieldManager;
import com.laptevaleksei.smartdocumentstorage.manager.DocumentManager;
import com.laptevaleksei.smartdocumentstorage.manager.TemplateManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * This resource provides endpoints to change data form many fields related with document.
 *
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 24-May-2021
 */
@RestController
@RequestMapping("/api")
public class FormResource {

    private final Logger log = LoggerFactory.getLogger(FormResource.class);

    private final CustomFieldManager customFieldManager;
    private final DocumentManager documentManager;
    private final TemplateManager templateManager;

    public FormResource(CustomFieldManager customFieldManager, DocumentManager documentManager, TemplateManager templateManager) {
        this.customFieldManager = customFieldManager;
        this.documentManager = documentManager;
        this.templateManager = templateManager;
    }

    /**
     * You can save whole form via this endpoint.
     *
     * @param documentId       ID of {@link Document} where should be saved data.
     * @param extraTemplateDTO Form data which should be saved.
     */
    @Transactional
    @PostMapping("/saveFormValues")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public void saveForm(
        @RequestParam(name = "documentId", required = false) Long documentId,
        @RequestBody ExtraTemplateDTO extraTemplateDTO
    ) {
        Document document;
        if (documentId == null) {
            Optional<Template> optionalTemplate = templateManager.getTemplate(extraTemplateDTO.getId());
            if (optionalTemplate.isEmpty()) {
                throw new TemplateException("Template with id '" + extraTemplateDTO.getId() + "' not found!");
            }

            document = documentManager.createNewDocument();
            templateManager.assignTemplateToDocument(document, optionalTemplate.get());
        } else {
            document = documentManager.getDocument(documentId).orElseThrow();
        }

        extraTemplateDTO
            .getContent()
            .forEach(
                extraFieldBlockDTO ->
                    extraFieldBlockDTO
                        .getContent()
                        .forEach(
                            extendedCustomFieldDTO -> {
                                var customField = customFieldManager.getCustomField(extendedCustomFieldDTO.getId()).orElseThrow();
                                customFieldManager.setCustomFieldValue(document, customField, extendedCustomFieldDTO.getValue());
                            }
                        )
            );
    }

    /**
     * Use this endpoint if you want to take form depends on {@link Document} and {@link Template} relationship.
     *
     * @param documentId ID of {@link Document}.
     * @param templateId ID of specific {@link Template}.
     *
     * @return Returns huge (depends on configuration) form (which is {@link ExtraTemplateDTO}).
     */
    @GetMapping("/getForm")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ExtraTemplateDTO getForm(Long documentId, Long templateId) {
        var template = templateManager.getTemplate(templateId).orElseThrow();

        Document document = null;
        if (documentId != null) {
            document = documentManager.getDocument(documentId).orElseThrow();
        }

        return fillExtraTemplateDTO(document, template);
    }

    /**
     * This method gets layout for {@link Template} and provides extended DTO with layout.
     *
     * @param template {@link Template} which needs for extended DTO.
     *
     * @return Filled {@link ExtraTemplateDTO}.
     */
    private ExtraTemplateDTO fillExtraTemplateDTO(Document document, Template template) {
        ExtraTemplateDTO extraTemplateDTO;

        List<ExtraFieldBlockDTO> layout = templateManager
            .getRelatedFieldBlocksWithTemplate(template)
            .stream()
            .map(extraFieldBlockDTO -> fillExtraFieldBlockDTO(document, extraFieldBlockDTO))
            .collect(Collectors.toList());

        extraTemplateDTO = new ExtraTemplateDTO(template);
        extraTemplateDTO.setDefaultCustomFields(
            templateManager
                .getDefaultCustomFields(template)
                .stream()
                .map(extraCustomFieldDTO -> fillExtraCustomFieldDTO(document, extraCustomFieldDTO))
                .collect(Collectors.toList())
        );
        extraTemplateDTO.setContent(layout);

        return extraTemplateDTO;
    }

    /**
     * Fills extended {@link ExtraFieldBlockDTO} which is {@link ExtraFieldBlockDTO} with related data.
     *
     * @param document   Current {@link Document} object.
     * @param fieldBlock {@link FieldBlock} object which should be used for creation of {@link ExtraFieldBlockDTO}.
     *
     * @return Returns completely filled {@link ExtraFieldBlockDTO}.
     */
    private ExtraFieldBlockDTO fillExtraFieldBlockDTO(Document document, FieldBlock fieldBlock) {
        ExtraFieldBlockDTO extraFieldBlockDTO;

        List<ExtraCustomFieldDTO> layout = templateManager
            .getRelatedCustomFieldsWithFieldBlock(fieldBlock)
            .stream()
            .map(extraCustomField -> fillExtraCustomFieldDTO(document, extraCustomField))
            .collect(Collectors.toList());

        extraFieldBlockDTO = new ExtraFieldBlockDTO(fieldBlock);
        extraFieldBlockDTO.setContent(layout);

        return extraFieldBlockDTO;
    }

    /**
     * Filling completed {@link ExtraCustomFieldDTO} with all data.
     *
     * @param document    Current {@link Document} which should be used for {@link CustomField}'s value getting.
     * @param customField {@link CustomField} which should be used for {@link ExtraCustomFieldDTO} filling.
     *
     * @return Returns completely filled {@link ExtraCustomFieldDTO}.
     */
    private ExtraCustomFieldDTO fillExtraCustomFieldDTO(Document document, CustomField customField) {
        ExtraCustomFieldDTO extraCustomField;

        Object value = null;

        if (document != null) {
            value = customFieldManager.getCustomFieldValue(document, customField);
        }

        extraCustomField = new ExtraCustomFieldDTO(customField);
        extraCustomField.setValue(value);

        return extraCustomField;
    }
}
