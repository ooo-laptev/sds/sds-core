/*
 * Copyright (c) 2019 - 2024, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.service.impl;

import com.laptevaleksei.smartdocumentstorage.core.api.exception.CustomFieldException;
import com.laptevaleksei.smartdocumentstorage.core.api.exception.TemplateException;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.CustomField;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.Document;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.DocumentTemplate;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.template.*;
import com.laptevaleksei.smartdocumentstorage.core.repository.StoreException;
import com.laptevaleksei.smartdocumentstorage.core.repository.template.*;
import com.laptevaleksei.smartdocumentstorage.core.service.TemplateService;
import com.laptevaleksei.smartdocumentstorage.core.service.util.CustomPagination;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 1-May-2020
 */
@Service
@Transactional
public class DefaultTemplateService implements TemplateService {

    private final Logger log = LoggerFactory.getLogger(DefaultTemplateService.class);

    private final TemplateRepository templateRepository;

    private final DocumentTemplateRepository documentTemplateRepository;

    private final FieldBlockRepository fieldBlockRepository;

    private final TemplateLayoutRepository templateLayoutRepository;

    private final FieldBlockLayoutRepository fieldBlockLayoutRepository;

    private final TemplateDefaultCustomFieldLayoutRepository templateDefaultCustomFieldLayoutRepository;

    public DefaultTemplateService(
        TemplateRepository templateRepository,
        DocumentTemplateRepository documentTemplateRepository,
        FieldBlockRepository fieldBlockRepository,
        TemplateLayoutRepository templateLayoutRepository,
        FieldBlockLayoutRepository fieldBlockLayoutRepository,
        TemplateDefaultCustomFieldLayoutRepository templateDefaultCustomFieldLayoutRepository
    ) {
        this.templateRepository = templateRepository;
        this.documentTemplateRepository = documentTemplateRepository;
        this.fieldBlockRepository = fieldBlockRepository;
        this.templateLayoutRepository = templateLayoutRepository;
        this.fieldBlockLayoutRepository = fieldBlockLayoutRepository;
        this.templateDefaultCustomFieldLayoutRepository = templateDefaultCustomFieldLayoutRepository;
    }

    @Override
    public Template createTemplate(Template template) {
        if (template.getId() != null) {
            throw new CustomFieldException("You try to create Template with pre-defined ID!");
        }
        if (template.isDeleted()) {
            throw new StoreException("You are trying to create a Template which is already marked as deleted! It will not be saved.");
        }
        return templateRepository.save(template);
    }

    @Override
    public List<Template> getAllTemplates() {
        return templateRepository.findAll().stream().filter(template -> !template.isDeleted()).collect(Collectors.toList());
    }

    @Override
    public void deleteTemplate(Template template) {
        template.setDeleted(true);
        updateTemplate(template);
    }

    @Override
    public Page<Document> getAllDocumentRelatedWithTemplate(Pageable pageable, Template template) {
        Page<Document> page = documentTemplateRepository.findAllByTemplate(pageable, template).map(DocumentTemplate::getDocument);

        return new PageImpl<>(
            page.stream().filter(document -> !document.isDeleted()).collect(Collectors.toList()),
            page.getPageable(),
            page.getTotalElements()
        );
    }

    @Override
    public List<FieldBlock> getAllFieldBlockRelatedWithTemplate(Template template) {
        List<FieldBlock> fieldBlockList = new ArrayList<>();
        List<TemplateLayout> layout = new ArrayList<>();
        templateLayoutRepository.findAllByTemplate(template).forEach(layout::add);

        layout.sort(Comparator.comparingInt(TemplateLayout::getSequence));
        layout.stream().filter(item -> !item.getFieldBlock().isDeleted()).forEach(item -> fieldBlockList.add(item.getFieldBlock()));

        return fieldBlockList;
    }

    @Override
    public List<FieldBlock> getAllFieldBlockRelatedWithCustomField(CustomField customField) {
        List<FieldBlock> fbd = new ArrayList<>();
        fieldBlockLayoutRepository
            .findAllByCustomField(customField)
            .forEach(
                it -> {
                    if (!it.getFieldBlock().isDeleted()) {
                        fbd.add(it.getFieldBlock());
                    }
                }
            );
        return fbd;
    }

    @Override
    public TemplateLayout createTemplateLayout(TemplateLayout templateLayout) {
        return templateLayoutRepository.save(templateLayout);
    }

    @Override
    public List<TemplateLayout> getAllTemplateLayouts() {
        return new ArrayList<>(templateLayoutRepository.findAll());
    }

    @Override
    public Optional<TemplateLayout> getTemplateLayoutById(Long id) {
        return templateLayoutRepository.findById(id);
    }

    @Override
    public void updateTemplateLayout(TemplateLayout templateLayout) {
        templateLayoutRepository.save(templateLayout);
    }

    @Override
    public void deleteTemplateLayout(TemplateLayout templateLayout) {
        templateLayoutRepository.delete(templateLayout);
    }

    @Override
    public List<TemplateLayout> getTemplateLayouts(Template template) {
        List<TemplateLayout> templateLayoutList = new ArrayList<>();
        templateLayoutRepository.findAllByTemplate(template).forEach(templateLayoutList::add);
        return templateLayoutList;
    }

    @Override
    public void changeFieldBlockPosition(Template template, FieldBlock fieldBlock, Integer position) {
        List<TemplateLayout> layout = getTemplateLayouts(template)
            .stream()
            .sorted(Comparator.comparingInt(TemplateLayout::getSequence))
            .collect(Collectors.toList());

        TemplateLayout forRemoving = layout.stream().filter(it -> it.getFieldBlock().equals(fieldBlock)).findFirst().orElse(null);
        layout.remove(forRemoving);
        deleteTemplateLayout(forRemoving);

        TemplateLayout newLayoutItem = TemplateLayout.builder()
            .setTemplate(template)
            .setFieldBlock(fieldBlock)
            .setSequence(position)
            .build();

        newLayoutItem = createTemplateLayout(newLayoutItem);

        layout.add(position - 1, newLayoutItem);

        for (int i = position; i < layout.size(); i++) {
            if (!layout.get(i).equals(newLayoutItem)) {
                if (layout.get(i).getSequence() != i + 1) {
                    layout.get(i).setSequence(i + 1);
                }
            }
        }

        layout.forEach(this::updateTemplateLayout);
    }

    @Override
    public Template getTemplateRelatedWithTemplateLayout(TemplateLayout templateLayout) {
        Optional<TemplateLayout> tl = templateLayoutRepository.findById(templateLayout.getId());
        return tl.map(TemplateLayout::getTemplate).orElse(null);
    }

    @Override
    public DocumentTemplate createDocumentTemplate(DocumentTemplate documentTemplate) {
        return documentTemplateRepository.save(documentTemplate);
    }

    @Override
    public List<DocumentTemplate> getAllDocumentTemplate() {
        return new ArrayList<>(documentTemplateRepository.findAll());
    }

    @Override
    public Optional<DocumentTemplate> getDocumentTemplateById(Long id) {
        return documentTemplateRepository.findById(id);
    }

    @Override
    public DocumentTemplate updateDocumentTemplate(DocumentTemplate documentTemplate) {
        return documentTemplateRepository.save(documentTemplate);
    }

    @Override
    public void deleteDocumentTemplate(DocumentTemplate documentTemplate) {
        documentTemplateRepository.delete(documentTemplate);
    }

    @Override
    public boolean fieldBlockIsExistsInTemplateLayout(Template template, FieldBlock fieldBlock) {
        List<TemplateLayout> templateLayout = new ArrayList<>();
        templateLayoutRepository.findAllByTemplate(template).forEach(templateLayout::add);

        return templateLayout.stream().anyMatch(tl -> tl.getFieldBlock().equals(fieldBlock));
    }

    @Override
    public FieldBlock createFieldBlock(FieldBlock fieldBlock) {
        if (fieldBlock.getId() != null) {
            throw new StoreException("You try to create CustomField with pre-defined ID!");
        }
        if (fieldBlock.isDeleted()) {
            throw new StoreException("You are trying to create a FieldBlock which is already marked as deleted! It will not be saved.");
        }
        return fieldBlockRepository.save(fieldBlock);
    }

    @Override
    public List<FieldBlock> getAllFieldBlock() {
        return fieldBlockRepository.findAll().stream().filter(fieldBlock -> !fieldBlock.isDeleted()).collect(Collectors.toList());
    }

    @Override
    public Optional<FieldBlock> getFieldBlock(Long id) {
        return fieldBlockRepository.findById(id).filter(fieldBlock -> !fieldBlock.isDeleted());
    }

    @Override
    public void deleteFieldBlock(FieldBlock fieldBlock) {
        fieldBlock.setDeleted(true);
        updateFieldBlock(fieldBlock);
    }

    @Override
    public List<FieldBlockLayout> getFieldBlockLayouts(FieldBlock fieldBlock) {
        List<FieldBlockLayout> fieldBlockLayoutList = new ArrayList<>();
        Iterable<FieldBlockLayout> fieldBlockLayout = fieldBlockLayoutRepository.findAllByFieldBlock(fieldBlock);
        if (fieldBlockLayout.iterator().hasNext()) {
            fieldBlockLayout.forEach(fieldBlockLayoutList::add);
        }
        return fieldBlockLayoutList;
    }

    @Override
    public FieldBlockLayout createFieldBlockLayout(FieldBlockLayout fieldBlockLayout) {
        return fieldBlockLayoutRepository.save(fieldBlockLayout);
    }

    @Override
    public void updateFieldBlockLayout(FieldBlockLayout fieldBlockLayout) {
        if (fieldBlockLayoutRepository.findById(fieldBlockLayout.getId()).isEmpty()) {
            throw new TemplateException("FieldBlockLayout with id " + fieldBlockLayout.getId() + " does not exists!");
        }
        fieldBlockLayoutRepository.save(fieldBlockLayout);
    }

    @Override
    public List<FieldBlockLayout> getAllFieldBlockLayouts() {
        return new ArrayList<>(fieldBlockLayoutRepository.findAll());
    }

    @Override
    public Optional<FieldBlockLayout> getFieldBlockLayoutById(Long id) {
        return fieldBlockLayoutRepository.findById(id);
    }

    @Override
    public void deleteFieldBlockLayout(FieldBlockLayout fieldBlockLayout) {
        fieldBlockLayoutRepository.delete(fieldBlockLayout);
    }

    @Override
    public List<CustomField> getAllCustomFieldRelatedWithFieldBlock(FieldBlock fieldBlock) {
        List<CustomField> customFieldList = new ArrayList<>();

        List<FieldBlockLayout> layout = new ArrayList<>();
        fieldBlockLayoutRepository.findAllByFieldBlock(fieldBlock).forEach(layout::add);

        layout.sort(Comparator.comparingInt(FieldBlockLayout::getSequence));
        layout.stream().filter(lay -> !lay.getCustomField().isDeleted()).forEach(item -> customFieldList.add(item.getCustomField()));

        return customFieldList;
    }

    @Override
    public boolean customFieldIsExistsInFieldBlockLayout(FieldBlock fieldBlock, CustomField customField) {
        List<FieldBlockLayout> fieldBlockLayouts = new ArrayList<>();
        fieldBlockLayoutRepository.findAllByFieldBlock(fieldBlock).forEach(fieldBlockLayouts::add);

        return fieldBlockLayouts.stream().anyMatch(fbl -> fbl.getCustomField().equals(customField));
    }

    @Override
    public boolean isDocumentAssignedToTemplate(Document document, Template template) {
        return getTemplatesAssignedToDocument(document).stream().filter(t -> t.getId().equals(template.getId())).count() == 1;
    }

    @Override
    public void assignTemplateToDocument(Document document, Template template) {
        if (isDocumentAssignedToTemplate(document, template)) {
            DocumentTemplate relationship = getDocumentTemplateRelationship(document, template);
            relationship.setTemplate(template);
            updateDocumentTemplate(relationship);
        } else {
            createDocumentTemplate( DocumentTemplate.builder().setDocument(document).setTemplate(template).build());
        }
    }

    public DocumentTemplate getDocumentTemplateRelationship(Document document, Template template) {
        return getDocumentTemplateRelationshipsByDocument(document)
            .stream()
            .filter(it -> it.getTemplate().equals(template))
            .findFirst()
            .orElse(null);
    }

    public List<DocumentTemplate> getDocumentTemplateRelationshipsByDocument(Document document) {
        List<DocumentTemplate> relationships = new ArrayList<>();
        documentTemplateRepository.findAllByDocument(document).forEach(relationships::add);
        return relationships;
    }

    @Override
    public List<Template> getTemplatesAssignedToDocument(Document document) {
        List<Template> result = new ArrayList<>();

        documentTemplateRepository
            .findAllByDocument(document)
            .forEach(
                it -> {
                    if (!it.getTemplate().isDeleted()) {
                        result.add(it.getTemplate());
                    }
                }
            );

        return result;
    }

    @Override
    public boolean customFieldIsRequiredInFieldBlock(FieldBlock fieldBlock, CustomField customField) {
        List<FieldBlockLayout> layout = new ArrayList<>();
        fieldBlockLayoutRepository.findAllByFieldBlock(fieldBlock).forEach(layout::add);

        boolean result = false;
        for (FieldBlockLayout fieldBlockLayout : layout) {
            if (fieldBlockLayout.getCustomField().equals(customField) && fieldBlockLayout.isRequired()) {
                result = true;
                break;
            }
        }
        return result;
    }

    @Override
    public boolean templateIsExists(Long id) {
        return getTemplate(id).filter(template -> !template.isDeleted()).isPresent();
    }

    @Override
    public Optional<Template> updateTemplate(Template template) {
        return Optional.of(templateRepository.save(template));
    }

    @Override
    public Page<Template> getAllManagedTemplates(Pageable pageable) {
        Page<Template> page = templateRepository.findAll(pageable);
        return new PageImpl<>(
            page.stream().filter(template -> !template.isDeleted()).collect(Collectors.toList()),
            page.getPageable(),
            page.getTotalElements()
        );
    }

    @Override
    public Optional<Template> getTemplate(Long id) {
        return templateRepository.findById(id).filter(template -> !template.isDeleted());
    }

    @Override
    public Page<FieldBlock> getAllManagedFieldBlocks(Pageable pageable) {
        Page<FieldBlock> page = fieldBlockRepository.findAll(pageable);
        return new PageImpl<>(
            page.stream().filter(fieldBlock -> !fieldBlock.isDeleted()).collect(Collectors.toList()),
            page.getPageable(),
            page.getTotalElements()
        );
    }

    @Override
    public boolean fieldBlockIsExists(Long id) {
        Optional<FieldBlock> optionalFieldBlock = getFieldBlock(id);
        return optionalFieldBlock.isPresent() && !optionalFieldBlock.get().isDeleted();
    }

    @Override
    public Optional<FieldBlock> updateFieldBlock(FieldBlock fieldBlock) {
        return Optional.of(fieldBlockRepository.save(fieldBlock));
    }

    @Override
    public Page<Document> getDocumentsWhichUsesTemplate(Pageable pageable, Template template) {
        List<Document> allRelatedDocuments = getDocumentsWhichUsesTemplate(template)
            .stream()
            .filter(document -> !document.isDeleted())
            .collect(Collectors.toList());

        CustomPagination<Document> customPagination = new CustomPagination<>(allRelatedDocuments);
        return customPagination.getPage(pageable);
    }

    @Override
    public List<Document> getDocumentsWhichUsesTemplate(Template template) {
        List<Document> result = new ArrayList<>();

        documentTemplateRepository.findAllByTemplate(template).forEach(it -> result.add(it.getDocument()));

        return result;
    }

    @Override
    public void removeDocumentTemplateRelationship(Document document, Template template) {
        DocumentTemplate dt = documentTemplateRepository.findDocumentTemplateByDocumentAndTemplate(document, template);
        documentTemplateRepository.delete(dt);
    }

    @Override
    public List<Template> getTemplatesRelatedWithFieldBlock(FieldBlock fieldBlock) {
        List<Template> templateList = new ArrayList<>();

        templateLayoutRepository.findAllByFieldBlock(fieldBlock).forEach(it -> templateList.add(it.getTemplate()));

        return templateList;
    }

    @Override
    public Page<FieldBlock> getFieldBlocksRelatedWithCustomFields(Pageable pageable, CustomField customField) {
        List<FieldBlock> allRelatedFieldBlocks = getFieldBlocksRelatedWithCustomFields(customField)
            .stream()
            .filter(fieldBlock -> !fieldBlock.isDeleted())
            .collect(Collectors.toList());

        CustomPagination<FieldBlock> customPagination = new CustomPagination<>(allRelatedFieldBlocks);
        return customPagination.getPage(pageable);
    }

    @Override
    public List<FieldBlock> getFieldBlocksRelatedWithCustomFields(CustomField customField) {
        List<FieldBlock> allRelatedFieldBlocks = new ArrayList<>();
        fieldBlockLayoutRepository.findAllByCustomField(customField).forEach(it -> allRelatedFieldBlocks.add(it.getFieldBlock()));
        return allRelatedFieldBlocks;
    }

    @Override
    public Page<Template> getRelatedTemplates(Pageable pageable, FieldBlock fieldBlock) {
        List<Template> allRelatedFieldBlocks = getRelatedTemplates(fieldBlock)
            .stream()
            .filter(template -> !template.isDeleted())
            .collect(Collectors.toList());

        CustomPagination<Template> customPagination = new CustomPagination<>(allRelatedFieldBlocks);
        return customPagination.getPage(pageable);
    }

    @Override
    public List<Template> getRelatedTemplates(FieldBlock fieldBlock) {
        List<Template> allRelatedTemplates = new ArrayList<>();
        templateLayoutRepository.findAllByFieldBlock(fieldBlock).forEach(it -> allRelatedTemplates.add(it.getTemplate()));
        return allRelatedTemplates;
    }

    @Override
    public List<CustomField> getDefaultCustomFields(Template template) {
        List<CustomField> defaultCustomFields = new ArrayList<>();

        getTemplateDefaultCustomFieldLayouts(template)
            .stream()
            .sorted(Comparator.comparingInt(TemplateDefaultCustomFieldLayout::getSequence))
            .filter(templateDefaultCustomFieldLayout -> !templateDefaultCustomFieldLayout.getCustomField().isDeleted())
            .collect(Collectors.toList())
            .forEach(it -> defaultCustomFields.add(it.getCustomField()));
        return defaultCustomFields;
    }

    @Override
    public List<CustomField> addDefaultCustomField(Template template, CustomField customField) {
        addDefaultCustomFieldToTheTemplateDefaultCustomFieldAfter(template, customField, getPositionOfLastDefaultCustomField(template) + 1);

        return getDefaultCustomFields(template);
    }

    @Override
    public void removeDefaultCustomField(Template template, CustomField customField) {
        List<TemplateDefaultCustomFieldLayout> layout = templateDefaultCustomFieldLayoutRepository
            .findByTemplateAndCustomField(template, customField)
            .stream()
            .sorted(Comparator.comparingInt(TemplateDefaultCustomFieldLayout::getSequence))
            .collect(Collectors.toList());

        TemplateDefaultCustomFieldLayout layoutForRemoving = null;

        Optional<TemplateDefaultCustomFieldLayout> first = layout
            .stream()
            .filter(it -> it.getCustomField().equals(customField))
            .findFirst();

        if (first.isPresent()) {
            layoutForRemoving = first.get();
        }

        if( layoutForRemoving == null ) return;
        templateDefaultCustomFieldLayoutRepository.delete(layoutForRemoving);

        List<TemplateDefaultCustomFieldLayout> layoutsForSequenceUpdating = new ArrayList<>();
        layout =
            getTemplateDefaultCustomFieldLayouts(template)
                .stream()
                .sorted(Comparator.comparingInt(TemplateDefaultCustomFieldLayout::getSequence))
                .collect(Collectors.toList());

        for (int i = 0; i < layout.size(); i++) {
            if (i == 0) {
                if (layout.get(i).getSequence() != i + 1) {
                    layout.get(i).setSequence(i + 1);
                    layoutsForSequenceUpdating.add(layout.get(i));
                }
            } else {
                layout.get(i).setSequence(i + 1);
                layoutsForSequenceUpdating.add(layout.get(i));
            }
        }
        templateDefaultCustomFieldLayoutRepository.saveAll(layoutsForSequenceUpdating);
    }

    @Override
    public void changeDefaultCustomFieldPosition(Template template, CustomField customField, Integer position) {
        List<TemplateDefaultCustomFieldLayout> layout = getTemplateDefaultCustomFieldLayouts(template)
            .stream()
            .sorted(Comparator.comparingInt(TemplateDefaultCustomFieldLayout::getSequence))
            .collect(Collectors.toList());

        TemplateDefaultCustomFieldLayout forRemoving = layout
            .stream()
            .filter(it -> it.getCustomField().equals(customField))
            .findFirst()
            .orElse(null);
        layout.remove(forRemoving);

        assert forRemoving != null;
        templateDefaultCustomFieldLayoutRepository.delete(forRemoving);

        TemplateDefaultCustomFieldLayout newLayoutItem = TemplateDefaultCustomFieldLayout.builder()
            .setTemplate(template)
            .setCustomField(customField)
            .setSequence(position)
            .build();

        newLayoutItem = templateDefaultCustomFieldLayoutRepository.save(newLayoutItem);

        layout.add(position - 1, newLayoutItem);

        for (int i = position; i < layout.size(); i++) {
            if (!layout.get(i).equals(newLayoutItem)) {
                if (layout.get(i).getSequence() != i + 1) {
                    layout.get(i).setSequence(i + 1);
                }
            }
        }

        templateDefaultCustomFieldLayoutRepository.saveAll(layout);
    }

    /**
     * Provides list of {@link TemplateDefaultCustomFieldLayout} for specified {@link Template}.
     *
     * @param template {@link Template} for searching of layout.
     *
     * @return List of {@link TemplateDefaultCustomFieldLayout}.
     */
    private List<TemplateDefaultCustomFieldLayout> getTemplateDefaultCustomFieldLayouts(Template template) {
        List<TemplateDefaultCustomFieldLayout> items = new ArrayList<>();

        templateDefaultCustomFieldLayoutRepository.findAllByTemplate(template).forEach(items::add);

        return items;
    }

    @Override
    public List<TemplateDefaultCustomFieldLayout> getAllTemplateDefaultCustomFieldLayout() {
        return new ArrayList<>(templateDefaultCustomFieldLayoutRepository.findAll());
    }

    @Override
    public List<DocumentTemplate> getAllDocumentTemplates() {
        return new ArrayList<>(documentTemplateRepository.findAll());
    }

    /**
     * Just checks that {@link CustomField} is exists in {@link Template} defaults.
     *
     * @param template {@link Template} where we will check.
     * @param customField {@link CustomField} which we will check.
     *
     * @return TRUE - if {@link CustomField} is exists in defaults of {@link Template}.
     */
    private boolean customFieldIsExistsInDefaultCustomFields(Template template, CustomField customField) {
        return getTemplateDefaultCustomFieldLayouts(template).stream().anyMatch(it -> it.getCustomField().equals(customField));
    }

    /**
     * Adds {@link CustomField} after specified position in the default {@link CustomField} list.
     *
     * @param template {@link Template} where {@link CustomField} should be added.
     * @param customField {@link CustomField} which should be added.
     * @param position Element number after which you want to insert.
     */
    private void addDefaultCustomFieldToTheTemplateDefaultCustomFieldAfter(Template template, CustomField customField, Integer position) {
        if (customFieldIsExistsInDefaultCustomFields(template, customField)) {
            throw new TemplateException(
                "CustomField with id " +
                customField.getId() +
                " already exists in Template with id " +
                template.getId() +
                ". Please use another method for moving!"
            );
        }

        List<TemplateDefaultCustomFieldLayout> layout = getTemplateDefaultCustomFieldLayouts(template)
            .stream()
            .sorted(Comparator.comparingInt(TemplateDefaultCustomFieldLayout::getSequence))
            .collect(Collectors.toList());

        List<TemplateDefaultCustomFieldLayout> layoutForUpdate = new ArrayList<>();

        TemplateDefaultCustomFieldLayout newLayout = TemplateDefaultCustomFieldLayout.builder()
            .setSequence(position)
            .setCustomField(customField)
            .setTemplate(template)
            .build();
        layoutForUpdate.add(newLayout);

        updateTemplateDefaultCustomFieldLayout(layout, layoutForUpdate, position);
    }

    /**
     * Provides position of the last item in defaults.
     *
     * @param template {@link Template} where we will count {@link CustomField}s.
     *
     * @return Position of last item.
     */
    private Integer getPositionOfLastDefaultCustomField(Template template) {
        return getTemplateDefaultCustomFieldLayouts(template)
            .stream()
            .max(Comparator.comparing(TemplateDefaultCustomFieldLayout::getSequence))
            .orElse( TemplateDefaultCustomFieldLayout.builder().setSequence(0).build())
            .getSequence();
    }

    private void updateTemplateDefaultCustomFieldLayout(
        List<TemplateDefaultCustomFieldLayout> oldLayout,
        List<TemplateDefaultCustomFieldLayout> layoutForUpdate,
        Integer position
    ) {
        for (TemplateDefaultCustomFieldLayout templateDefaultCustomFieldLayout : oldLayout) {
            if (templateDefaultCustomFieldLayout.getSequence() >= position) {
                Integer seq = templateDefaultCustomFieldLayout.getSequence();
                templateDefaultCustomFieldLayout.setSequence(seq + 1);
                layoutForUpdate.add(templateDefaultCustomFieldLayout);
            }
        }
        templateDefaultCustomFieldLayoutRepository.saveAll(layoutForUpdate);
    }
}
