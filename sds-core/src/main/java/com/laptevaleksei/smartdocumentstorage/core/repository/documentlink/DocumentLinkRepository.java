/*
 * Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.repository.documentlink;

import com.laptevaleksei.smartdocumentstorage.core.domain.entity.Document;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.documentlink.DocumentLink;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

/**
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 23-Aug-2021
 */
@Repository
public interface DocumentLinkRepository extends JpaRepository<DocumentLink, Long> {
    Collection<DocumentLink> findAllBySourceDocument(Document document);

    Collection<DocumentLink> findAllByTargetDocument(Document document);

    /**
     * Provides all relationships where {@link Document} is mentioned (inward or outward).
     *
     * @param document {@link Document} for search.
     *
     * @return List of relationships.
     */
    default Collection<DocumentLink> findAllLinksByDocument(Document document) {
        Collection<DocumentLink> result = findAllBySourceDocument(document);
        result.addAll(findAllByTargetDocument(document));
        return result;
    }
}
