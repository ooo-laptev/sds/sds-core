/*
 * Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.web.rest.api.customfield;

import com.laptevaleksei.smartdocumentstorage.core.web.dto.CustomFieldDTO;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Set;

/**
 * Interface for describing a CustomField Api for management of them.
 *
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 23-Nov-2021
 */
public interface CustomFieldManagementApiResource {
    /**
     * Creates CustomField
     *
     * @param customFieldDTO Parameters for CustomField creation.
     *
     * @return DTO with created CustomField.
     *
     * @throws URISyntaxException Happens if something went wrong.
     */
    ResponseEntity<CustomFieldDTO> createCustomField(CustomFieldDTO customFieldDTO) throws URISyntaxException;

    /**
     * {@code PUT /customFields} : Updates an existing CustomField.
     *
     * @param customFieldDTO the customField to update.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated customField.
     */
    ResponseEntity<CustomFieldDTO> updateCustomField(CustomFieldDTO customFieldDTO);

    /**
     * {@code GET /customFields} : get all customFields.
     *
     * @param pageable the pagination information.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body all customFields.
     */
    ResponseEntity<List<CustomFieldDTO>> getAllCustomFields(Pageable pageable);

    /**
     * {@code GET /customFields/:id} : get the "id" customField.
     *
     * @param id the name of the customField to find.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the "name" customField, or with
     * status
     * {@code 404 (Not Found)}.
     */
    ResponseEntity<CustomFieldDTO> getCustomField(Long id);

    /**
     * {@code DELETE /customFields/:id} : delete the "id" CustomField.
     *
     * @param id the name of the customField to delete.
     *
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    ResponseEntity<Void> deleteCustomField(Long id);

    /**
     * Provides a key-value pair for type keys and theirs names.
     *
     * @return Key-value pair of type keys and their names.
     */
    Set<String> getAvailableCustomFieldTypes();
}
