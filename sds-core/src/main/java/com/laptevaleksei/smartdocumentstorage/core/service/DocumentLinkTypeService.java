/*
 * Copyright (c) 2019 - 2024, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.service;

import com.laptevaleksei.smartdocumentstorage.core.domain.entity.documentlink.DocumentLinkType;

import java.util.List;
import java.util.Optional;

/**
 * Services a {@link DocumentLinkType}s.
 *
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 23-Aug-2021
 */
public interface DocumentLinkTypeService {
    /**
     * Creates a new link type.
     *
     * @param linkType Instance for link creation.
     */
    DocumentLinkType createLinkType(DocumentLinkType linkType);

    /**
     * Provides {@link DocumentLinkType} type by ID.
     *
     * @param id ID of item.
     *
     * @return Optional of {@link DocumentLinkType}.
     */
    Optional<DocumentLinkType> getLinkType(Long id);

    /**
     * Provides all link types.
     *
     * @return List of {@link DocumentLinkType}.
     */
    List<DocumentLinkType> getAllLinkTypes();

    /**
     * Updates a new link type.
     *
     * @param linkType Instance for link creation.
     */
    DocumentLinkType updateLinkType(DocumentLinkType linkType);

    /**
     * Deletes {@link DocumentLinkType}.
     *
     * @param linkType Instance which should be deleted.
     */
    void deleteLinkType(DocumentLinkType linkType);
}
