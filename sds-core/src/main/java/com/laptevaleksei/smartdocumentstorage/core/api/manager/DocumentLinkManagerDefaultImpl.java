/*
 * Copyright (c) 2019 - 2024, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.api.manager;

import com.laptevaleksei.annotations.Internal;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.Document;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.documentlink.DocumentLink;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.documentlink.DocumentLinkType;
import com.laptevaleksei.smartdocumentstorage.core.service.impl.DefaultDocumentLinkService;
import com.laptevaleksei.smartdocumentstorage.manager.CustomFieldManager;
import com.laptevaleksei.smartdocumentstorage.manager.DocumentLinkManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Default implementation of {@link DocumentLinkManager}.
 *
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 23-Aug-2021
 */
@Service
@Internal
public class DocumentLinkManagerDefaultImpl implements DocumentLinkManager {

    private final DefaultDocumentLinkService documentLinkService;
    private final CustomFieldManager customFieldManager;

    public DocumentLinkManagerDefaultImpl( DefaultDocumentLinkService documentLinkService, CustomFieldManager customFieldManager) {
        this.documentLinkService = documentLinkService;
        this.customFieldManager = customFieldManager;
    }

    @Override
    public DocumentLink linkDocuments(Document sourceDocument, Document targetDocument, DocumentLinkType linkType) {
        DocumentLink link = new DocumentLink();
        link.setSourceDocument(sourceDocument);
        link.setTargetDocument(targetDocument);
        link.setLinkType(linkType);

        return documentLinkService.createDocumentLink(link);
    }

    @Override
    public List<DocumentLink> getAllLinkedDocuments(Document document) {
        return documentLinkService.getLinkedDocumentsForDocument(document);
    }

    @Override
    public List<DocumentLink> getLinkedDocumentsForDocumentAsSource(Document sourceDocument) {
        return documentLinkService.getLinkedDocumentsForDocumentAsSource(sourceDocument);
    }

    @Override
    public List<DocumentLink> getLinkedDocumentsForDocumentAsTarget(Document targetDocument) {
        return documentLinkService.getLinkedDocumentsForDocumentAsTarget(targetDocument);
    }

    @Override
    public Page<DocumentLinkType> getAllDocumentLinkTypes( Pageable pageable) {
        return documentLinkService.getAllDocumentLinkTypes(pageable);
    }

    @Override
    public void removeDocumentLink(Document sourceDocument, Document targetDocument) {
        List<DocumentLink> optionalDocumentLink = documentLinkService.getLinkedDocumentsForDocument(sourceDocument);
        if (optionalDocumentLink.isEmpty()) {
            return;
        }
        Optional<DocumentLink> foundLink = optionalDocumentLink
            .stream()
            .filter(link -> link.getTargetDocument().equals(targetDocument))
            .findFirst();
        foundLink.ifPresent(documentLinkService::deleteDocumentLink);
    }

    @Override
    public void deleteDocumentLink( Long id ) {
        documentLinkService.getDocumentLink( id ).ifPresent( documentLinkService::deleteDocumentLink );
    }

    @Override
    public List<DocumentLinkType> getLinkTypes() {
        return documentLinkService.getAllLinkTypes();
    }

    @Override
    public DocumentLinkType createDocumentLinkType(
        String linkTypeName,
        String description,
        String inwardDescription,
        String outwardDescription
    ) {
        DocumentLinkType newLinkType = new DocumentLinkType();
        newLinkType.setLinkTypeName(linkTypeName);
        newLinkType.setDescription(description);
        newLinkType.setInwardDirectionName(inwardDescription);
        newLinkType.setOutwardDirectionName(outwardDescription);

        return documentLinkService.createLinkType(newLinkType);
    }

    @Override
    public Optional<DocumentLinkType> getDocumentLinkType(Long id) {
        return documentLinkService.getLinkType(id);
    }

    @Override
    public DocumentLinkType updateDocumentLinkType(DocumentLinkType documentLinkType) {
        return documentLinkService.updateLinkType(documentLinkType);
    }

    @Override
    public void deleteDocumentLinkType(DocumentLinkType documentLinkType) {
        documentLinkService.deleteLinkType(documentLinkType);
    }

    @Override
    public Optional<DocumentLink> getDocumentLink(Long linkId) {
        return documentLinkService.getDocumentLink(linkId);
    }

    @Override
    public List<DocumentLink> getAllDocumentLinks(Pageable pageable) {
        return documentLinkService.getAllDocumentLinks(pageable).toList();
    }
}
