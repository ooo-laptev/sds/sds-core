/*
 * Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.web.rest.api.document;

import com.laptevaleksei.smartdocumentstorage.core.web.dto.DocumentDTO;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.util.List;

/**
 * Interface for describing a Document Api for Users.
 *
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 23-Nov-2021
 */
public interface DocumentUserApiResource {
    /**
     * Creates a new Document.
     *
     * @param documentDTO Parameters for Document creation.
     *
     * @return DTO of created Document.
     *
     * @throws URISyntaxException Throw when something went wrong.
     */
    ResponseEntity<DocumentDTO> createDocument(DocumentDTO documentDTO) throws URISyntaxException;

    /**
     * {@code GET /documents} : get all documents.
     *
     * @param pageable the pagination information.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body all documents.
     */
    ResponseEntity<List<DocumentDTO>> getAllDocuments(Pageable pageable);

    /**
     * {@code GET /documents/:id} : get the "id" documents.
     *
     * @param id the name of the documents to find.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the "name" documents, or with
     * status {@code 404 (Not Found)}.
     */
    ResponseEntity<DocumentDTO> getDocument(Long id);

    /**
     * {@code DELETE /documents/:id} : delete the "id" Document.
     *
     * @param id the name of the document to delete.
     *
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    ResponseEntity<Void> deleteDocument(Long id);
}
