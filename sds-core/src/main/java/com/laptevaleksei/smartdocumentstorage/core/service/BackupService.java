/*
 * Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.service;

import com.laptevaleksei.smartdocumentstorage.core.domain.administration.BackUp;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.customfield.AbstractCustomFieldValue;
import com.laptevaleksei.smartdocumentstorage.core.repository.CustomFieldRepository;
import com.laptevaleksei.smartdocumentstorage.core.repository.DocumentRepository;
import com.laptevaleksei.smartdocumentstorage.core.repository.documentlink.DocumentLinkRepository;
import com.laptevaleksei.smartdocumentstorage.core.repository.documentlink.DocumentLinkTypeRepository;
import com.laptevaleksei.smartdocumentstorage.core.repository.template.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 05-Sep-2021
 */
@Service
public class BackupService {

    private final Logger log = LoggerFactory.getLogger(BackupService.class);

    private final CustomFieldService customFieldService;
    private final CustomFieldRepository customFieldRepository;

    private final TemplateService templateService;
    private final TemplateRepository templateRepository;
    private final FieldBlockRepository fieldBlockRepository;
    private final FieldBlockLayoutRepository fieldBlockLayoutRepository;
    private final TemplateLayoutRepository templateLayoutRepository;
    private final TemplateDefaultCustomFieldLayoutRepository templateDefaultCustomFieldLayoutRepository;

    private final DocumentService documentService;
    private final DocumentRepository documentRepository;

    private final DocumentTemplateRepository documentTemplateRepository;

    private final CustomFieldValueService customFieldValueService;

    private final DocumentLinkTypeService documentLinkTypeService;
    private final DocumentLinkTypeRepository documentLinkTypeRepository;

    private final DocumentLinkService documentLinkService;
    private final DocumentLinkRepository documentLinkRepository;

    public BackupService(
        CustomFieldService customFieldService,
        CustomFieldRepository customFieldRepository,
        TemplateService templateService,
        TemplateRepository templateRepository,
        FieldBlockRepository fieldBlockRepository,
        FieldBlockLayoutRepository fieldBlockLayoutRepository,
        TemplateLayoutRepository templateLayoutRepository,
        TemplateDefaultCustomFieldLayoutRepository templateDefaultCustomFieldLayoutRepository,
        DocumentService documentService,
        DocumentRepository documentRepository,
        DocumentTemplateRepository documentTemplateRepository,
        CustomFieldValueService customFieldValueService,
        DocumentLinkTypeService documentLinkTypeService,
        DocumentLinkTypeRepository documentLinkTypeRepository,
        DocumentLinkService documentLinkService,
        DocumentLinkRepository documentLinkRepository
    ) {
        this.customFieldService = customFieldService;
        this.customFieldRepository = customFieldRepository;
        this.templateService = templateService;
        this.templateRepository = templateRepository;
        this.fieldBlockRepository = fieldBlockRepository;
        this.fieldBlockLayoutRepository = fieldBlockLayoutRepository;
        this.templateLayoutRepository = templateLayoutRepository;
        this.templateDefaultCustomFieldLayoutRepository = templateDefaultCustomFieldLayoutRepository;
        this.documentService = documentService;
        this.documentRepository = documentRepository;
        this.documentTemplateRepository = documentTemplateRepository;
        this.customFieldValueService = customFieldValueService;
        this.documentLinkTypeService = documentLinkTypeService;
        this.documentLinkTypeRepository = documentLinkTypeRepository;
        this.documentLinkService = documentLinkService;
        this.documentLinkRepository = documentLinkRepository;
    }

    public BackUp createBackUp() {
        BackUp backUp = new BackUp();

        backUpCustomFields(backUp);
        backUpTemplates(backUp);
        backUpFieldBlocks(backUp);
        backUpFieldBlockLayouts(backUp);
        backUpTemplateLayouts(backUp);
        backUpTemplateDefaultCustomFieldLayouts(backUp);
        backUpDocuments(backUp);
        backUpDocumentTemplates(backUp);
        backUpCustomFieldValues(backUp);
        backUpDocumentLinkTypes(backUp);
        backUpDocumentLinks(backUp);

        return backUp;
    }

    public void restore(BackUp backUp) {
        restoreCustomFields(backUp);
        restoreTemplates(backUp);
        restoreFieldBlocks(backUp);
        restoreFieldBlockLayouts(backUp);
        restoreTemplateLayouts(backUp);
        restoreTemplateDefaultCustomFieldLayouts(backUp);
        restoreDocuments(backUp);
        restoreDocumentTemplates(backUp);
        restoreCustomFieldValues(backUp);
        restoreDocumentLinkTypes(backUp);
        restoreDocumentLinks(backUp);
    }

    private void backUpCustomFields(BackUp backUp) {
        log.info("Saving CustomFields in progress...");
        backUp.setCustomFields(customFieldService.getAllCustomFields());
        log.info("Saving of CustomFields is done!");
    }

    private void backUpTemplates(BackUp backUp) {
        log.info("Saving Templates in progress...");
        backUp.setTemplates(templateService.getAllTemplates());
        log.info("Saving of Templates is done!");
    }

    private void backUpFieldBlocks(BackUp backUp) {
        log.info("Saving FieldBlocks in progress...");
        backUp.setFieldBlocks(templateService.getAllFieldBlock());
        log.info("Saving of FieldBlocks is done!");
    }

    private void backUpFieldBlockLayouts(BackUp backUp) {
        log.info("Saving FieldBlockLayouts in progress...");
        backUp.setFieldBlockLayouts(templateService.getAllFieldBlockLayouts());
        log.info("Saving of FieldBlockLayouts is done!");
    }

    private void backUpTemplateLayouts(BackUp backUp) {
        log.info("Saving TemplateLayouts in progress...");
        backUp.setTemplateLayouts(templateService.getAllTemplateLayouts());
        log.info("Saving of TemplateLayouts is done!");
    }

    private void backUpTemplateDefaultCustomFieldLayouts(BackUp backUp) {
        log.info("Saving TemplateDefaultCustomFieldLayouts in progress...");
        backUp.setTemplateDefaultCustomFieldLayouts(templateService.getAllTemplateDefaultCustomFieldLayout());
        log.info("Saving of TemplateDefaultCustomFieldLayouts is done!");
    }

    private void backUpDocuments(BackUp backUp) {
        log.info("Saving Document in progress...");
        backUp.setDocuments(documentService.getAllDocuments());
        log.info("Saving of Document is done!");
    }

    private void backUpDocumentTemplates(BackUp backUp) {
        log.info("Saving DocumentTemplates in progress...");
        backUp.setDocumentTemplates(templateService.getAllDocumentTemplates());
        log.info("Saving of DocumentTemplates is done!");
    }

    private void backUpCustomFieldValues(BackUp backUp) {
        log.info("Saving CustomFieldValues in progress...");
        List<AbstractCustomFieldValue> customFieldValues = new ArrayList<>();
        backUp
            .getDocuments()
            .forEach(
                document -> {
                    log.info("Processing of Document with ID=" + document.getId() + " is started!");
                    backUp
                        .getCustomFields()
                        .forEach(
                            customField -> {
                                log.info(
                                    "Saving value for CustomField with ID=" +
                                    customField.getId() +
                                    " and NAME=" +
                                    customField.getName() +
                                    " is in progress..."
                                );
                                AbstractCustomFieldValue customFieldValue = customFieldValueService.getValueObject(document, customField);
                                if (customFieldValue != null) {
                                    customFieldValues.add(customFieldValueService.getValueObject(document, customField));
                                } else {
                                    log.info(
                                        "Value for CustomField with ID=" +
                                        customField.getId() +
                                        " and NAME=" +
                                        customField.getName() +
                                        " is NULL. Skipping..."
                                    );
                                }
                                log.info(
                                    "Saving value for CustomField with ID=" +
                                    customField.getId() +
                                    " and NAME=" +
                                    customField.getName() +
                                    " is done!"
                                );
                            }
                        );
                    log.info("Processing of Document with ID=" + document.getId() + " is done!");
                }
            );
        backUp.setCustomFieldValues(customFieldValues);
        log.info("Saving of CustomFieldValues is done!");
    }

    private void backUpDocumentLinkTypes(BackUp backUp) {
        log.info("Saving DocumentLinkTypes in progress...");
        backUp.setDocumentLinkTypes(documentLinkTypeService.getAllLinkTypes());
        log.info("Saving of DocumentLinkType is done!");
    }

    private void backUpDocumentLinks(BackUp backUp) {
        log.info("Saving DocumentLinks in progress...");
        backUp.setDocumentLinks(documentLinkService.getAllDocumentLinks());
        log.info("Saving of DocumentLinks is done!");
    }

    private void restoreCustomFields(BackUp backUp) {
        log.info("Try to restore CustomFields...");
        customFieldRepository.saveAll(backUp.getCustomFields());
        log.info("CustomFields are restored!");
    }

    private void restoreTemplates(BackUp backUp) {
        log.info("Try to restore Templates...");
        templateRepository.saveAll(backUp.getTemplates());
        log.info("Templates are restored!");
    }

    private void restoreFieldBlocks(BackUp backUp) {
        log.info("Try to restore FieldBlocks...");
        fieldBlockRepository.saveAll(backUp.getFieldBlocks());
        log.info("FieldBlocks are restored!");
    }

    private void restoreFieldBlockLayouts(BackUp backUp) {
        log.info("Try to restore FieldBlockLayouts...");
        backUp
            .getFieldBlockLayouts()
            .forEach(
                it -> {
                    try {
                        fieldBlockLayoutRepository.save(it);
                    } catch (Exception ex) {
                        log.error(ex.getLocalizedMessage());
                    }
                }
            );
        //        fieldBlockLayoutRepository.saveAll( backUp.getFieldBlockLayouts() );
        log.info("FieldBlockLayouts are restored!");
    }

    private void restoreTemplateLayouts(BackUp backUp) {
        log.info("Try to restore TemplateLayouts...");
        templateLayoutRepository.saveAll(backUp.getTemplateLayouts());
        log.info("TemplateLayouts are restored!");
    }

    private void restoreTemplateDefaultCustomFieldLayouts(BackUp backUp) {
        log.info("Try to restore TemplateDefaultCustomFieldLayout...");
        templateDefaultCustomFieldLayoutRepository.saveAll(backUp.getTemplateDefaultCustomFieldLayouts());
        log.info("TemplateDefaultCustomFieldLayout are restored!");
    }

    private void restoreDocuments(BackUp backUp) {
        log.info("Try to restore Documents...");
        documentRepository.saveAll(backUp.getDocuments());
        log.info("Documents are restored!");
    }

    private void restoreDocumentTemplates(BackUp backUp) {
        log.info("Try to restore DocumentTemplates...");
        backUp
            .getDocumentTemplates()
            .forEach(
                it -> {
                    try {
                        documentTemplateRepository.save(it);
                    } catch (Exception ex) {
                        log.error(ex.getLocalizedMessage());
                    }
                }
            );
        //        documentTemplateRepository.saveAll( backUp.getDocumentTemplates() );
        log.info("DocumentTemplates are restored!");
    }

    private void restoreCustomFieldValues(BackUp backUp) {
        log.info("Try to restore CustomFieldValues...");
        backUp
            .getCustomFieldValues()
            .forEach(
                customFieldValue -> {
                    try {
                        log.info(
                            "Saving value for Document with ID=" +
                            customFieldValue.getDocument().getId() +
                            " and CustomField with ID=" +
                            customFieldValue.getCustomField().getId() +
                            " is in progress..."
                        );
                        customFieldValueService.saveCustomFieldValueObject(customFieldValue);
                        log.info(
                            "Saving value for Document with ID=" +
                            customFieldValue.getDocument().getId() +
                            " and CustomField with ID=" +
                            customFieldValue.getCustomField().getId() +
                            " is done!"
                        );
                    } catch (Exception ex) {
                        log.error(ex.getLocalizedMessage());
                    }
                }
            );
        log.info("CustomFieldValues are restored!");
    }

    private void restoreDocumentLinkTypes(BackUp backUp) {
        log.info("Try to restore DocumentLinkTypes...");
        documentLinkTypeRepository.saveAll(backUp.getDocumentLinkTypes());
        log.info("DocumentLinkTypes are restored!");
    }

    private void restoreDocumentLinks(BackUp backUp) {
        log.info("Try to restore DocumentLinks...");
        documentLinkRepository.saveAll(backUp.getDocumentLinks());
        log.info("DocumentLinks are restored!");
    }
}
