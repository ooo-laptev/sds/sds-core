/*
 * Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.domain.administration;

import com.laptevaleksei.smartdocumentstorage.core.domain.entity.CustomField;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.Document;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.DocumentTemplate;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.customfield.AbstractCustomFieldValue;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.documentlink.DocumentLink;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.documentlink.DocumentLinkType;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.template.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 05-Sep-2021
 */
public class BackUp implements Serializable {

    private static final long serialVersionUID = 1241472327700119507L;

    private List<CustomField> customFields = new ArrayList<>();
    private List<Template> templates = new ArrayList<>();
    private List<FieldBlock> fieldBlocks = new ArrayList<>();
    private List<FieldBlockLayout> fieldBlockLayouts = new ArrayList<>();
    private List<TemplateLayout> templateLayouts = new ArrayList<>();
    private List<TemplateDefaultCustomFieldLayout> templateDefaultCustomFieldLayouts = new ArrayList<>();
    private List<Document> documents = new ArrayList<>();
    private List<DocumentTemplate> documentTemplates = new ArrayList<>();
    private List<AbstractCustomFieldValue> customFieldValues = new ArrayList<>();
    private List<DocumentLinkType> documentLinkTypes = new ArrayList<>();
    private List<DocumentLink> documentLinks = new ArrayList<>();

    public BackUp() {}

    public List<CustomField> getCustomFields() {
        return customFields;
    }

    public void setCustomFields(List<CustomField> customFields) {
        this.customFields = customFields;
    }

    public List<Template> getTemplates() {
        return templates;
    }

    public void setTemplates(List<Template> templates) {
        this.templates = templates;
    }

    public List<TemplateDefaultCustomFieldLayout> getTemplateDefaultCustomFieldLayouts() {
        return templateDefaultCustomFieldLayouts;
    }

    public void setTemplateDefaultCustomFieldLayouts(List<TemplateDefaultCustomFieldLayout> templateDefaultCustomFieldLayouts) {
        this.templateDefaultCustomFieldLayouts = templateDefaultCustomFieldLayouts;
    }

    public List<FieldBlock> getFieldBlocks() {
        return fieldBlocks;
    }

    public void setFieldBlocks(List<FieldBlock> fieldBlocks) {
        this.fieldBlocks = fieldBlocks;
    }

    public List<FieldBlockLayout> getFieldBlockLayouts() {
        return fieldBlockLayouts;
    }

    public void setFieldBlockLayouts(List<FieldBlockLayout> fieldBlockLayouts) {
        this.fieldBlockLayouts = fieldBlockLayouts;
    }

    public List<TemplateLayout> getTemplateLayouts() {
        return templateLayouts;
    }

    public void setTemplateLayouts(List<TemplateLayout> templateLayouts) {
        this.templateLayouts = templateLayouts;
    }

    public List<Document> getDocuments() {
        return documents;
    }

    public void setDocuments(List<Document> documents) {
        this.documents = documents;
    }

    public List<DocumentTemplate> getDocumentTemplates() {
        return documentTemplates;
    }

    public void setDocumentTemplates(List<DocumentTemplate> documentTemplates) {
        this.documentTemplates = documentTemplates;
    }

    public List<AbstractCustomFieldValue> getCustomFieldValues() {
        return customFieldValues;
    }

    public void setCustomFieldValues(List<AbstractCustomFieldValue> customFieldValues) {
        this.customFieldValues = customFieldValues;
    }

    public List<DocumentLinkType> getDocumentLinkTypes() {
        return documentLinkTypes;
    }

    public void setDocumentLinkTypes(List<DocumentLinkType> documentLinkTypes) {
        this.documentLinkTypes = documentLinkTypes;
    }

    public List<DocumentLink> getDocumentLinks() {
        return documentLinks;
    }

    public void setDocumentLinks(List<DocumentLink> documentLinks) {
        this.documentLinks = documentLinks;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BackUp backUp = (BackUp) o;
        return (
            Objects.equals(customFields, backUp.customFields) &&
            Objects.equals(templates, backUp.templates) &&
            Objects.equals(templateDefaultCustomFieldLayouts, backUp.templateDefaultCustomFieldLayouts) &&
            Objects.equals(fieldBlocks, backUp.fieldBlocks) &&
            Objects.equals(fieldBlockLayouts, backUp.fieldBlockLayouts) &&
            Objects.equals(templateLayouts, backUp.templateLayouts) &&
            Objects.equals(documents, backUp.documents) &&
            Objects.equals(documentTemplates, backUp.documentTemplates) &&
            Objects.equals(customFieldValues, backUp.customFieldValues) &&
            Objects.equals(documentLinkTypes, backUp.documentLinkTypes) &&
            Objects.equals(documentLinks, backUp.documentLinks)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            customFields,
            templates,
            templateDefaultCustomFieldLayouts,
            fieldBlocks,
            fieldBlockLayouts,
            templateLayouts,
            documents,
            documentTemplates,
            customFieldValues,
            documentLinkTypes,
            documentLinks
        );
    }
}
