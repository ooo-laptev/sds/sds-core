/*
 * Copyright (c) 2019 - 2024, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.api.manager;

import com.laptevaleksei.annotations.Internal;
import com.laptevaleksei.smartdocumentstorage.core.customfieldprocessing.CustomFieldTypeRegistry;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.CustomField;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.Document;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.customfield.AbstractCustomFieldValue;
import com.laptevaleksei.smartdocumentstorage.core.service.CustomFieldService;
import com.laptevaleksei.smartdocumentstorage.core.service.CustomFieldValueService;
import com.laptevaleksei.smartdocumentstorage.manager.CustomFieldManager;
import com.laptevaleksei.smartdocumentstorage.manager.DocumentManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Management-layer class for CustomField management.
 *
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 17-Mar-2020
 */
@Service
@Internal
public class DefaultCustomFieldManagerImpl implements CustomFieldManager {

    private final CustomFieldService customFieldService;

    private final DocumentManager documentManager;

    private final CustomFieldValueService customFieldValueService;

    private final CustomFieldTypeRegistry customFieldTypeRegistry;

    public DefaultCustomFieldManagerImpl(
            CustomFieldService customFieldService,
            DocumentManager documentManager,
            CustomFieldValueService customFieldValueService, CustomFieldTypeRegistry customFieldTypeRegistry ) {
        this.customFieldService = customFieldService;
        this.documentManager = documentManager;
        this.customFieldValueService = customFieldValueService;
        this.customFieldTypeRegistry = customFieldTypeRegistry;
    }

    @Override
    public CustomField createCustomField(String name, String description, String customFieldType) {
        return customFieldService.createCustomField(
                CustomField.builder().setName(name).setDescription(description).setCustomFieldType(customFieldType).build()
        );
    }

    @Override
    public Optional<CustomField> getCustomField(Long id) {
        return customFieldService.getCustomField(id);
    }

    @Override
    public CustomField updateCustomField(CustomField customField) {
        return customFieldService.updateCustomField(customField);
    }

    @Override
    public void deleteCustomField(CustomField customField) {
        customFieldService.deleteCustomField(customField);
    }

    @Override
    public Page<CustomField> getAllPagesCustomFieldDTO(Pageable pageable) {
        return customFieldService.getAllManagedCustomFields(pageable);
    }

    @Override
    public boolean customFieldIsExists(Long id) {
        return customFieldService.customFieldIsExists(id);
    }

    @Override
    public void setCustomFieldValue(Document document, CustomField customField, Object value) {
        saveCustomFieldValue(document, customField, value);
    }

    @Override
    public Object getCustomFieldValue(Long documentId, Long customFieldId) {
        Optional<Document> optionalDocument = documentManager.getDocument(documentId);
        Optional<CustomField> optionalCustomField = getCustomField(customFieldId);

        if (!optionalDocument.isPresent() || !optionalCustomField.isPresent()) {
            return null;
        }

        return getCustomFieldValue(optionalDocument.get(), optionalCustomField.get());
    }

    @Override
    public void setCustomFieldValue(Long documentId, Long customFieldId, Object value) {
        Optional<Document> optionalDocument = documentManager.getDocument(documentId);
        Optional<CustomField> optionalCustomField = getCustomField(customFieldId);

        if (!optionalDocument.isPresent() || !optionalCustomField.isPresent()) {
            return;
        }

        setCustomFieldValue(optionalDocument.get(), optionalCustomField.get(), value);
    }

    @Override
    public List<CustomField> getAllCustomFields() {
        return customFieldService.getAllCustomFields();
    }

    @Override
    public List<CustomField> getAllCustomFieldsRelatedWithDocument(Document document) {
        if (documentManager.documentIsExists(document.getId())) {
            return getCustomFieldValueObjectsByDocument(document)
                .stream()
                .map(AbstractCustomFieldValue::getCustomField)
                .collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

    @Override
    public List<AbstractCustomFieldValue> getMatches(String param) {
        return getCustomFieldValueObjectsByValue(param);
    }

    @Override
    public Object getCustomFieldValue(Document document, CustomField customField) {
        return customFieldValueService.getCustomFieldValue(document, customField);
    }

    @Override
    public void saveCustomFieldValue(Document document, CustomField customField, Object value) {
        customFieldValueService.saveCustomFieldValue(document, customField, value);
    }

    @Override
    public List<AbstractCustomFieldValue> getCustomFieldValueObjectsByValue(String param) {
        return customFieldValueService.findByValue(param);
    }

    @Override
    public List<AbstractCustomFieldValue> getCustomFieldValueObjectsByDocument(Document document) {
        return customFieldValueService.findByDocument(document);
    }

    @Override
    public List<String> getAllCustomFieldTypes() {
        return List.copyOf( customFieldTypeRegistry.getAllTypeKeys() );
    }
}
