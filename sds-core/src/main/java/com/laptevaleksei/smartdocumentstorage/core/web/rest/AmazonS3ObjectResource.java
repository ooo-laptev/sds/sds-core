/*
 * Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.web.rest;

import com.laptevaleksei.smartdocumentstorage.core.domain.entity.AmazonObject;
import com.laptevaleksei.smartdocumentstorage.core.service.amazon.AmazonS3ObjectService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import tech.jhipster.web.util.HeaderUtil;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 28-Nov-2021
 */
//@RestController
@RequestMapping("/api/multimedia")
public class AmazonS3ObjectResource {

    private final AmazonS3ObjectService amazonS3ObjectService;

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    public AmazonS3ObjectResource(AmazonS3ObjectService amazonS3ObjectService) {
        this.amazonS3ObjectService = amazonS3ObjectService;
    }

    @PostMapping("/files")
    public ResponseEntity<List<AmazonObject>> insertFiles(@RequestPart(value = "files") List<MultipartFile> files) {
        return ResponseEntity.ok(amazonS3ObjectService.insertMedias(files, "other/"));
    }

    @PostMapping("/file")
    public ResponseEntity<AmazonObject> insertFile(@RequestPart(value = "file") MultipartFile file) {
        return ResponseEntity.ok(amazonS3ObjectService.insertMedia(file, "other/"));
    }

    @PostMapping("/{documentId}/files")
    public ResponseEntity<List<AmazonObject>> uploadFilesForDocument(
        @RequestPart(value = "files") List<MultipartFile> files,
        @PathVariable("documentId") Long documentId
    ) {
        return ResponseEntity.ok(amazonS3ObjectService.insertMedias(files, documentId + "/"));
    }

    @PostMapping("/{documentId}/file")
    public ResponseEntity<AmazonObject> uploadFileForDocument(
        @RequestPart(value = "file") MultipartFile file,
        @PathVariable("documentId") Long documentId
    ) {
        return ResponseEntity.ok(amazonS3ObjectService.insertMedia(file, "/document/" + documentId + "/"));
    }

    @DeleteMapping("/{fileId}")
    public ResponseEntity<Void> deleteFile(@PathVariable("fileId") String uuid) {
        amazonS3ObjectService.removeImageFromS3Storage(UUID.fromString(uuid));
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createAlert(applicationName, "multimediaManagement" + ".deleted", uuid))
            .build();
    }

    @PostMapping(value = "/url", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<AmazonObject>> getFileUrls(@RequestBody List<String> ids) {
        return ResponseEntity.ok(amazonS3ObjectService.getUrls(ids.stream().map(UUID::fromString).collect(Collectors.toList())));
    }
}
