/*
 * Copyright (c) 2019 - 2024, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.service.impl;

import com.laptevaleksei.smartdocumentstorage.core.customfieldprocessing.CustomFieldTypeRegistry;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.CustomField;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.Document;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.customfield.AbstractCustomFieldValue;
import com.laptevaleksei.smartdocumentstorage.core.service.CustomFieldValueService;
import com.laptevaleksei.smartdocumentstorage.customfieldprocessing.AbstractCustomFieldType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Default implementation of {@link CustomFieldValueService}.
 *
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 17-Mar-2020
 */
@Service
public class DefaultCustomFieldValueService implements CustomFieldValueService {

    private final Logger logger = LoggerFactory.getLogger(DefaultCustomFieldValueService.class);

    private final CustomFieldTypeRegistry customFieldTypeRegistry;

    public DefaultCustomFieldValueService(CustomFieldTypeRegistry customFieldTypeRegistry) {
        this.customFieldTypeRegistry = customFieldTypeRegistry;
    }

    @SuppressWarnings({ "rawtypes" })
    @Override
    public Object getCustomFieldValue(Document document, CustomField customField) {
        AbstractCustomFieldType customFieldTypeProcessor = customFieldTypeRegistry.instantiateCustomFieldTypeObject(customField, document);
        return customFieldTypeProcessor.getValue();
    }

    @SuppressWarnings({ "rawtypes" })
    @Override
    public void saveCustomFieldValue(Document document, CustomField customField, Object value) {
        AbstractCustomFieldType customFieldTypeProcessor = customFieldTypeRegistry.instantiateCustomFieldTypeObject(customField, document);
        customFieldTypeProcessor.setValue(value);
    }

    @SuppressWarnings("rawtypes")
    @Override
    public AbstractCustomFieldValue<?> getValueObject(Document document, CustomField customField) {
        AbstractCustomFieldType customFieldTypeProcessor = customFieldTypeRegistry.instantiateCustomFieldTypeObject(customField, document);
        return customFieldTypeProcessor.getValueObject();
    }

    @SuppressWarnings("rawtypes")
    @Override
    public void saveCustomFieldValueObject(AbstractCustomFieldValue customFieldValue) {
        AbstractCustomFieldType customFieldTypeProcessor = customFieldTypeRegistry.instantiateCustomFieldTypeObject(
            customFieldValue.getCustomField(),
            customFieldValue.getDocument()
        );
        customFieldTypeProcessor.saveCustomFieldValue(customFieldValue);
    }

    @Override
    public List<AbstractCustomFieldValue> findByValue(String param) {
        return customFieldTypeRegistry.getObjectsByValue(param);
    }

    @Override
    public List<AbstractCustomFieldValue> findByDocument(Document document) {
        return customFieldTypeRegistry.getObjectsByDocument(document);
    }
}
