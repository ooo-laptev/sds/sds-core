/*
 * Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.web.rest;

import com.laptevaleksei.smartdocumentstorage.core.domain.entity.customfield.AbstractCustomFieldValue;
import com.laptevaleksei.smartdocumentstorage.core.domain.entity.template.Template;
import com.laptevaleksei.smartdocumentstorage.core.security.AuthoritiesConstants;
import com.laptevaleksei.smartdocumentstorage.core.web.dto.customfield.extra.ExtraCustomFieldDTO;
import com.laptevaleksei.smartdocumentstorage.core.web.dto.documentbrowser.DocumentForDocumentBrowserDTO;
import com.laptevaleksei.smartdocumentstorage.manager.CustomFieldManager;
import com.laptevaleksei.smartdocumentstorage.manager.TemplateManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 12-Aug-2021
 */
@RestController
@RequestMapping("/api/documentBrowser")
public class DocumentBrowserResource {

    private final Logger log = LoggerFactory.getLogger(DocumentBrowserResource.class);

    private final CustomFieldManager customFieldManager;
    private final TemplateManager templateManager;

    public DocumentBrowserResource(CustomFieldManager customFieldManager, TemplateManager templateManager) {
        this.customFieldManager = customFieldManager;
        this.templateManager = templateManager;
    }

    @GetMapping("/documents")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public Page<DocumentForDocumentBrowserDTO> provideDocumentList(Pageable pageable, Long currentTemplateId) {
        Optional<Template> optionalTemplate = templateManager.getTemplate(currentTemplateId);
        if (optionalTemplate.isEmpty()) {
            return Page.empty(pageable);
        }
        Template template = optionalTemplate.get();
        return templateManager
            .getRelatedDocuments(pageable, template)
            .map(
                doc -> {
                    List<ExtraCustomFieldDTO> defaultCustomFields = templateManager
                        .getDefaultCustomFields(optionalTemplate.get())
                        .stream()
                        .map(
                            cf -> {
                                ExtraCustomFieldDTO customFieldDTO = new ExtraCustomFieldDTO(cf);
                                customFieldDTO.setValue(customFieldManager.getCustomFieldValue(doc, cf));
                                return customFieldDTO;
                            }
                        )
                        .collect(Collectors.toList());
                    return new DocumentForDocumentBrowserDTO(doc, defaultCustomFields);
                }
            );
    }

    @GetMapping("/documents/_search")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public List<AbstractCustomFieldValue> search(String query) {
        String[] queryParams = query.toLowerCase().split(" ");
        List<AbstractCustomFieldValue> searchResult = new ArrayList<>();

        Arrays.stream(queryParams).forEach(param -> searchResult.addAll(customFieldManager.getMatches(param)));
        return searchResult;
    }
}
