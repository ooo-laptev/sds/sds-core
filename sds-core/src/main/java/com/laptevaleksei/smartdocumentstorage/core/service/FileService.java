/*
 * Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.service;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;

/**
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 25-Jul-2021
 */
public interface FileService {
    /**
     * You can save or update file using this method.
     *
     * @param file File which you want to store.
     * @param fileStoragePath Pathe where you want to store specified file.
     *
     * @return Returns file name.
     */
    String storeFile(MultipartFile file, Path fileStoragePath);

    /**
     * Use this method if you want to get file from file system.
     *
     * @param fileName Name of specific file.
     * @param fileStoragePath Path where file should be found.
     *
     * @return Returns file as {@link Resource}.
     */
    Resource loadFileAsResource(String fileName, Path fileStoragePath);

    /**
     * Remove files using this method.
     *
     * @param fileName File which should be removed.
     * @param fileStoragePath Path where file should be found and removed.
     */
    void removeFile(String fileName, Path fileStoragePath);
}
