/*
 * Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.manager;

import com.laptevaleksei.smartdocumentstorage.core.domain.administration.BackUp;
import com.laptevaleksei.smartdocumentstorage.core.service.BackupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 05-Sep-2021
 */
@Service
public class BackupManager {

    private final Logger log = LoggerFactory.getLogger(BackupManager.class);

    private final BackupService backupService;

    public BackupManager(BackupService backupService) {
        this.backupService = backupService;
    }

    public BackUp backUp() {
        log.info("Back up started...");
        BackUp backUp = backupService.createBackUp();
        log.info("Back up is done!");
        return backUp;
    }

    public void restore(BackUp backUp) {
        log.info("Restore process is started...");
        backupService.restore(backUp);
        log.info("Restore process is done!");
    }
}
