/*
 * Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.service.amazon;

//import com.amazonaws.services.s3.model.CannedAccessControlList;
//import com.amazonaws.services.s3.model.DeleteObjectRequest;
//import com.amazonaws.services.s3.model.PutObjectRequest;

import com.laptevaleksei.smartdocumentstorage.core.domain.entity.AmazonObject;
import com.laptevaleksei.smartdocumentstorage.core.repository.AmazonObjectRepository;
import com.laptevaleksei.smartdocumentstorage.core.service.exception.FileConversionException;
import com.laptevaleksei.smartdocumentstorage.core.service.exception.InvalidImageExtensionException;
import com.laptevaleksei.smartdocumentstorage.core.util.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * TODO: https://medium.com/analytics-vidhya/aws-s3-with-java-using-spring-boot-7f6fcf734aec
 *
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 23-Nov-2021
 */
//@Service
public class AmazonS3ObjectService extends AmazonClientService {

    private final Logger log = LoggerFactory.getLogger(AmazonS3ObjectService.class);

    private final AmazonObjectRepository amazonObjectRepository;

    public AmazonS3ObjectService(AmazonObjectRepository amazonObjectRepository) {
        this.amazonObjectRepository = amazonObjectRepository;
    }

    // Upload a List of Images to AWS S3.
    public List<AmazonObject> insertMedias(List<MultipartFile> medias, String folder) {
        List<AmazonObject> amazonMedias = new ArrayList<>();
        medias.forEach(image -> amazonMedias.add(uploadMediaToS3Storage(image, folder)));
        return amazonMedias;
    }

    public AmazonObject insertMedia(MultipartFile image, String folder) {
        return uploadMediaToS3Storage(image, folder);
    }

    public AmazonObject uploadMediaToS3Storage(MultipartFile multipartFile, String folder) {
        // Valid extensions array, like jpeg/jpg and png.
        List<String> validExtensions = Arrays.asList("jpeg", "jpg", "png", "heic", "mp4", "mov");

        String extension = FilenameUtils.getExtension(multipartFile.getOriginalFilename());
        assert extension != null;
        if (!validExtensions.contains(extension.toLowerCase())) {
            // If file have an invalid extension, call an Exception.
            //            log.warn( MessageUtil.getMessage( "invalid.image.extension" ) );
            throw new InvalidImageExtensionException(extension);
        } else {
            String url = uploadMultipartFileToS3Storage(multipartFile, folder);

            return amazonObjectRepository.save(new AmazonObject(url));
        }
    }

    public void removeImageFromS3Storage(AmazonObject amazonObject) {
        String fileName = amazonObject.getImageUrl().substring(amazonObject.getImageUrl().lastIndexOf("/") + 1);
        //        getClient().deleteObject(new DeleteObjectRequest(getBucketName(), fileName));
        amazonObjectRepository.delete(amazonObject);
    }

    public void removeImageFromS3Storage(UUID uuid) {
        AmazonObject amazonObject = amazonObjectRepository.getOne(uuid);
        amazonObject.setDeleted(true);
        amazonObjectRepository.delete(amazonObject);
        // TODO: implement removing from S3
        //        String[] name = amazonObject.getImageUrl().split( "/" );
        //        DeleteObjectRequest deleteObjectRequest =
        //                new DeleteObjectRequest( new DeleteObjectRequest( getBucketName(), name[name.length] ) );
        //        getClient().deleteObject( deleteObjectRequest );
    }

    private String uploadMultipartFileToS3Storage(MultipartFile multipartFile, String folder) {
        String fileUrl = null;

        try {
            // Get the file from MultipartFile.
            File file = FileUtils.convertMultipartToFile(multipartFile);
            File directory = new File(System.getProperty("java.io.tmpdir"));

            if (org.apache.commons.io.FileUtils.directoryContains(directory, file)) {
                // Extract the file name.
                String fileName = FileUtils.generateFileName(multipartFile);

                // Upload file.
                uploadPublicFile(fileName, folder, file);

                // Delete the file and get the File Url.
                boolean deleted = file.delete();

                if (!deleted) {
                    log.error("Object with name '" + multipartFile.getName().replaceAll("[\n\r\t]", "_") + "' has not deleted!");
                }
                fileUrl = getUrl().concat("/" + getBucketName() + "/" + folder).concat(fileName);
            }
        } catch (IOException e) {
            // If IOException on conversion or any file manipulation, call exception.
            //            log.warn( MessageUtil.getMessage( "multipart.to.file.convert.except" ), e );
            throw new FileConversionException();
        }

        return fileUrl;
    }

    /**
     * Uploads file to S3 into root folder.
     *
     * @param fileName Name for file.
     * @param file File which should be uploaded.
     */
    // Send image to AmazonS3, if have any problems here, the image fragments are removed from amazon.
    // Font: https://docs.aws.amazon.com/AWSJavaSDK/latest/javadoc/com/amazonaws/services/s3/AmazonS3Client.html#putObject%28com.amazonaws.services.s3.model.PutObjectRequest%29
    private void uploadPublicFile(String fileName, File file) {
        uploadPublicFile(fileName, "", file);
    }

    /**
     * Uploads file to S3 into specific folder.
     *
     * @param fileName Name for file.
     * @param folderName Name of folder (add "/" at the end.)
     * @param file File which should be uploaded.
     */
    private void uploadPublicFile(String fileName, String folderName, File file) {
        //        PutObjectRequest request = new PutObjectRequest(getBucketName(), folderName + fileName, file);
        //        getClient().putObject(request.withCannedAcl(CannedAccessControlList.PublicRead));
    }

    public List<AmazonObject> getUrls(List<UUID> uuids) {
        return amazonObjectRepository.findAllById(uuids);
    }
}
