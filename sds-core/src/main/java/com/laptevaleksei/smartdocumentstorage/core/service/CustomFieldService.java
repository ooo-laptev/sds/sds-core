/*
 * Copyright (c) 2019 - 2023, Aleksei Laptev and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. Aleksei Laptev designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Aleksei Laptev in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Aleksei Laptev a7l97nn@protonmail.com if you need additional information or have any
 * questions.
 */

package com.laptevaleksei.smartdocumentstorage.core.service;

import com.laptevaleksei.smartdocumentstorage.core.domain.entity.CustomField;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing of {@link CustomField}.
 *
 * @author <a href="mailto:a7l97nn@protonmail.com">Aleksei Laptev</a> on 17-Mar-2020
 */
public interface CustomFieldService {
    /**
     * Get all the customFields.
     *
     * @return the list of entities.
     */
    List<CustomField> getAllCustomFields();

    /**
     * Get CustomField by id.
     *
     * @param id ID of custom field which you want to find.
     *
     * @return Optional of CustomField.
     */
    Optional<CustomField> getCustomField(Long id);

    /**
     * Returns the list of all available custom fields by provided ids.
     *
     * @param ids Collection of ids of searching CustomFields
     *
     * @return List of CustomField objects.
     */
    List<CustomField> getAllCustomFieldByIds(Collection<Long> ids);

    /**
     * This method provides functionality for updating of CustomField object in system.
     *
     * @param customField CustomField which you want to update.
     *
     * @return Returns updated CustomField instance.
     */
    CustomField updateCustomField(CustomField customField);

    /**
     * This method creates new CustomField.
     *
     * @param customField Instance of CustomField which should be created in this system.
     *
     * @return Instance of CustomField with new ID.
     */
    CustomField createCustomField(CustomField customField);

    /**
     * This method just delete specified CustomField object from system.
     *
     * @param customField Instance of CustomField which you want to delete.
     */
    void deleteCustomField(CustomField customField);

    /**
     * You can check that CustomField with specified ID is exists.
     *
     * @param id ID which we want to try.
     *
     * @return TRUE if CustomField is exists. FALSE if not.
     */
    boolean customFieldIsExists(Long id);

    /**
     * Provides all available {@link CustomField}s via {@link Page}.
     *
     * @param pageable Parameter of {@link Page}.
     *
     * @return {@link Page} of {@link CustomField}s.
     */
    Page<CustomField> getAllManagedCustomFields(Pageable pageable);
}
